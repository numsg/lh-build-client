module.exports = {
  publicPath:  './' ,
  productionSourceMap: false,
  devServer: {
    port: 8081
  },
  css: {
    loaderOptions: {
      sass: {
        data: `@import "@/assets/styles/init.scss";`
      }
    }
  },
  configureWebpack: config =>{

    // 浏览器：web, electron : electron-renderer
    config.target = 'web'
  }
};
