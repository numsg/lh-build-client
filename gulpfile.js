
require('./scripts/i18n');
require('./scripts/skin');
require('./scripts/npm-publish');
require('./scripts/npm-unpublish');
require('./scripts/sonar');
require('./scripts/clean-cache');
// require('./scripts/postscc');
require('./scripts/scss');