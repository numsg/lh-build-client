var gulp = require('gulp');
var gulpautoprefixer = require('gulp-autoprefixer');
// var postcss = require('gulp-postcss');
// var csswring = require('csswring');
// var precss = require('precss');
var cleanCss = require('gulp-clean-css');
const del = require('del');
const shell = require('gulp-shell');

// 编译
gulp.task('npmBuild', shell.task(['npm run build']));

// 压缩CSS目录
gulp.task('gulpcleancss', ['npmBuild'], function () {
  return gulp.src('dist/css/**/*.css')
    .pipe(cleanCss())/*压缩css*/
    .pipe(gulpautoprefixer())/*处理浏览器厂商前缀*/
    .pipe(gulp.dest('build/css'))
});

// 清除CSS目录
gulp.task('deletecss', ['gulpcleancss'], () => {
  return del(['dist/css']);
});

// 拷贝
gulp.task('copycss', ['deletecss'], function () {
  gulp.src('build/css/*')
    .pipe(gulp.dest('dist/css'));
});



// /* 前面插件调用的语句略过 */
// gulp.task('postcss1', function () {
//   var processors = [
//     csswring,
//     precss,
//     autoprefixer({
//       browsers: ['last 2 version']
//     })
//   ];
//   return gulp.src('./src/**/*.scss')
//     .pipe(postcss(processors))
//     // .pipe(sourcemaps.init({ loadMaps: true }))
//     // .on('error', $.util.log)
//     // .pipe(sourcemaps.write('.', {
//     //   includeContent: false,
//     //   sourceRoot: '../src/scss'
//     // }))
//     .pipe(gulp.dest('build/css'));
// });