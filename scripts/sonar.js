
const gulp = require('gulp');
const del = require('del');
const sonarqubeScanner = require('sonarqube-scanner');

gulp.task('cleanSonar', () => {
  return del(['.scannerwork']);
});

gulp.task('sonar', ['cleanSonar'], function (callback) {
  process.env.SONAR_SCANNER_MIRROR = 'http://nexus.gsafety.com/repository/gs-assets/sonar-scanner/';
  sonarqubeScanner({
    serverUrl: 'http://sonarqube.gsafety.com/',
    token: '80d4ce0b5e91389128ef6f937eb15faa8bf41b68',
    options: {
      'sonar.projectKey': 'gsafety-sigt-callcenter-client',
      'sonar.projectName': '@gsafety/sigt-callcenter-client',
      'sonar.sources': 'src',
      'sonar.exclusions': 'src/lib/**',
      'sonar.sourceEncoding': 'UTF-8'
    }
  },
    callback
  );
});