const gulp = require('gulp');
const fs = require('fs');
const shell = require('gulp-shell');

//#region 卸载NPM包
// 卸载NPM包  执行命令 gulp npmUnpublish （备注：卸载需要注意和发布的NPM包版本一致）
const json = JSON.parse(fs.readFileSync('./package.json'));
gulp.task('npmUnpublish', [], shell.task(['npm unpublish @gsafety/sigt-callcenter-client@' + json.version]));
//#endregion