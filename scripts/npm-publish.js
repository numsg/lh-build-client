
const gulp = require('gulp');
const del = require('del');
// 发包
const shell = require('gulp-shell');
const jsonEditor = require('gulp-json-editor');

//#region  NPM发布成独立程序
const npmConfig = {
  npmPublish: {
    ip: 'http://nexus.gsafety.com/repository/npm-hosted/',
    username: 'deployment',
    password: '123456',
    email: 'tanjie@gsafety.com'
  }
};

gulp.task('npmBuild', shell.task(['npm run build']));

// 删除publish目录
gulp.task('clean', ['npmBuild'], () => {
  return del(['publish']);
});

// 清除package.json中不需要的属性
gulp.task('editPackage', ['clean'], () => {
  return gulp
    .src('package.json')
    .pipe(
      jsonEditor({
        scripts: '',
        dependencies: '',
        devDependencies: '',
        config: ''
      })
    )
    .pipe(gulp.dest('publish'));
});

// 复制打包目录
gulp.task('pre-publish', ['editPackage'], () => {
  return gulp.src('dist/**').pipe(gulp.dest('publish/dist'));
});

// 设置发布npm用户信息
gulp.task(
  'npmSetConfig',
  ['pre-publish'],
  shell.task([
    'npm set //' + npmConfig.npmPublish.ip + '/:username=' + npmConfig.npmPublish.username,
    'npm set //' + npmConfig.npmPublish.ip + '/:_password=' + npmConfig.npmPublish.password,
    'npm set //' + npmConfig.npmPublish.ip + '/:email=' + npmConfig.npmPublish.email,
    'npm set //' + npmConfig.npmPublish.ip + '/:always-auth=false'
  ])
);

// 发布NPM包  执行命令 gulp npmPublish (备注：在发布NPM包之前需要重新编译工程)
gulp.task(
  'npmPublish',
  ['npmSetConfig'],
  shell.task(['cd publish && npm publish --registry=http://nexus.gsafety.com/repository/npm-hosted/'])
);
//#endregion
