const gulp = require('gulp');
const merge = require('gulp-merge-json'); //合并json
const plumber = require('gulp-plumber'); //异常处理

//开发统一词条目录
const i18nPath = './src/i18n/';
//Gsafety包组件词条目录
const nodePath = './node_modules/@gsafety/';
//#region  词条合并
// 合并所有词条
gulp.task('merge-i18n', ['merge-i18n-all-zh'], () => {});

gulp.task('merge-i18n-all-zh', ['merge-i18n-zh'], function () {
  gulp
    .src([i18nPath + '**/zh-cn/zh-CN-out.json', i18nPath + '**/zh-cn/zh-CN-current.json'])
    .pipe(plumber())
    .pipe(
      merge({
        fileName: 'zh-cn/zh-CN.json'
      })
    )
    .pipe(gulp.dest(i18nPath));
});



gulp.task('merge-i18n-zh', function () {
  gulp
    .src([nodePath + '**/*zh-CN*.json'])
    .pipe(plumber())
    .pipe(
      merge({
        fileName: 'zh-cn/zh-CN-out.json'
      })
    )
    .pipe(gulp.dest(i18nPath));
});




//#endregion
