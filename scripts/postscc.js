// var gulp = require('gulp');
// var postcss = require('gulp-postcss');
// var autoprefixer = require('autoprefixer');
// var pxtorem = require('postcss-pxtorem');
// var pump = require('pump');

// // 基础使用gulp-postcss 在postcss.config.js上不需要配置plugins
// gulp.task('basiccssuse1', function () {
//   return gulp.src('dist/css/**/*.css')
//     .pipe(postcss())
//     .pipe(gulp.dest('build/css'));
// });


// gulp.task('autoprefixer', (cb) => {
//   pump([
//     gulp.src('src/assets/styles/base/reset.scss'),
//     postcss([autoprefixer({
//       overrideBrowserslist: ['> 1%', 'last 2 versions', 'Firefox ESR'], // 重要配置 详见下面
//       cascade: false //  是否美化属性值
//     })]),
//     gulp.dest('build/css')
//   ], cb)
// });

// gulp.task('css', function () {
//   var processors = [
//     autoprefixer({
//       overrideBrowserslist: ['> 1%', 'last 2 versions', 'Firefox ESR'], // 重要配置 详见下面
//       cascade: false //  是否美化属性值
//     }),
//     pxtorem({
//       replace: true
//     })
//   ];
//   return gulp.src(['dist/css/**/*.css'])
//     .pipe(postcss(processors))
//     .pipe(gulp.dest('build/postcss'));
// });