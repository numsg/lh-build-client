import { Vue, Component } from 'vue-property-decorator';
import LaboratoryRecordComponent from '../../components/laboratory-record/laboratory-record';
// import AddRecordComponent from '../../views/laboratory-process-record/laboratory-change/';

// 实验室过程记录列表
@Component({
  components: {
    LaboratoryRecordComponent,
    // AddRecordComponent
  }
})
export default class LaboratoryRecord extends Vue {
  showList: any = true;
  titlename: any = '实验数据采集列表';

  mounted() {
  }

  onShowList() {
    sessionStorage.removeItem('edit_project');
    const strWindowFeatures = `
    menubar=yes,
    location=yes,
    resizable=yes,
    scrollbars=yes,
    status=yes`;
    window.open(
      `${this.$store.getters.configs.addWindows}/#/laboratory-record-manager/add-record`,
      '',
      strWindowFeatures
      );
      // this.showList = !this.showList;
    }
}
