import { Vue, Component, Prop, Watch } from 'vue-property-decorator';
import DeviceServer from '@/api/laboratory-device-service';
import Player from '@/components/laboratory-record/add-record/player';
import laboratoryListService from '@/api/laboratory-list-service';
import LaboratoryRecordModel from '@/models/laboratory-record-model';
import RecordProjectModel from '@/models/record-project-model';
import LaboratoryTemplateList from '../../components/laboratory-record/laboratory-template-list/laboratory-template-list';
import SchoolLog from '@/models/school-laborary-log-info';
import LaboratoryLogService from '@/api/laborartory-log-service';
import dataRecordService from '@/api/data-record-service';
import TemplateDataTable from '../../components/laboratory-record/template-data-table/template-data-table';
import dataDictionaryUtil from '@/common/utils/data-dictionary-util';

@Component({
  components: {
    Player,
    LaboratoryTemplateList,
    TemplateDataTable
  }
})
export default class AddRecordComponent extends Vue {
  websocket: any;
  wsUrl: any = '';

  recordState: any = 0;

  /**
   * 设备列表
   */
  deviceGroup: any = [];
  /**
   * 当前设备列表GroupId
   */
  currentDeviceGroup: any = '';

  /**
   * 设备列表
   */
  deviceList: any = [];

  couldRecord: any = false;

  currentRecordProjectInfos: LaboratoryRecordModel = new LaboratoryRecordModel();

  /**
   * 中心编号
   */
  centerCode: any = '';

  /**
   * 项目名称
   */
  projectName: any = '';

  showRecord: any = true;
  uuidArr: any = [];

  showChooseTemplate: any = false;

  currentTemplate: any = {};
  trialResultDataSource: any = [];
  trialResultSno: any = '';

  showTemplateResult: any = false;

  project: any = {};
  currentRecord: any = {};

  isEditProject: any = false;

  selectResultData: any = [];

  created() {
    const temp = sessionStorage.getItem('edit_project');
    if (temp) {
      this.isEditProject = true;
      const obj = JSON.parse(temp);
      this.centerCode = obj.recordData.centerCode;
      this.currentRecord = obj.recordData;
      this.project = obj.projectData;
      this.projectName = obj.projectData.projectName;
      this.showRecord = false;
      this.showChooseTemplate = true;
      this.currentDeviceGroup = obj.projectData.deviceGroupId;
      this.recordState = 0;
    }
    this.init();
  }

  mounted() {
    this.wsUrl = 'ws' + this.$store.getters.configs.wsUrl;
    this.createWebsocket();
    window.addEventListener('beforeunload', (e: any) => this.beforeunloadHandler(e));
  }

  beforeunloadHandler(e: any) {
    this.sendStop();
    this.updateGroupStatus('0');
  }

  /**
   * 验证是否可以录制
   */
  validateCouldRecord() {
    let flag = false;
    setTimeout(() => {
      if (this.centerCode.length !== 11) {
        this.couldRecord = false;
        this.$message({ type: 'warning', message: '中心编号没有11位' });
        return;
      }
      if (!/^\d{4}\d{1,2}[A-Z]+[WC]\d{3}/.test(this.centerCode)) {
        this.couldRecord = false;
        this.$message({ type: 'warning', message: '中心编号错误，请检查' });
        return;
      }
      const ca = this.centerCode.substring(6, 7);
      if (!this.checkOffice(ca)) {
        this.$message({ type: 'warning', message: '科室信息与当前电脑不匹配' });
        this.couldRecord = false;
        return;
      }
    }, 100);
    if (
      this.projectName.replace(/\s*/g, '') !== '' &&
      this.centerCode.replace(/\s*/g, '') !== '' &&
      this.currentDeviceGroup.replace(/\s*/g, '') !== ''
    ) {
      flag = true;
    }
    this.couldRecord = flag;
  }

  checkOffice(ca: string) {
    const data: any = this.$store.state.app.dataConfigs;
    const ip = sessionStorage.getItem('login_ip');
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < data.length; i++) {
      if (data[i].ip === ip) {
        if (JSON.stringify(data[i].offices).indexOf(ca) >= 0) {
          return true;
        } else {
          return false;
        }
      }
    }
    return false;
  }

  async init() {
    const dGroup = await DeviceServer.GetAllDeviceGroupInfo();
    const data: any = this.$store.state.app.dataConfigs;
    const ip = sessionStorage.getItem('login_ip');
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < data.length; i++) {
      if (data[i].ip === ip) {
        dGroup.forEach((e: any) => {
          if (JSON.stringify(data[i].deviceGroup).indexOf(e.id) >= 0) {
            this.deviceGroup.push(e);
          }
        });
        break;
      }
    }
  }

  createWebsocket() {
    this.websocket = new WebSocket(this.wsUrl);
    // 成功连接后
    this.websocket.onopen = () => {
      console.log('websocket连接成功');
      this.websocket.send('000');
    };
    // 连接失败
    this.websocket.onerror = (evt: any) => {};
    // 收到信息后
    this.websocket.onmessage = (evt: any) => {};
    // 后端关闭ws的时候
    this.websocket.onclose = (evt: any) => {};
  }

  /**
   * 选择监控设备
   */
  async onDeviceGroupSelect(val: any) {
    this.currentDeviceGroup = val;
    this.deviceList = [];
    this.validateCouldRecord();
    const result = await DeviceServer.GetAllDeviceInfoByDeviceGroupId(val);
    if (result && Array.isArray(result)) {
      const tempData: any = [];
      result.forEach((item: any, index: any) => {
        const obj = Object.assign({}, item);
        obj['uuid'] = String(new Date().getFullYear()) + String(new Date().getMonth() + 1) + '/' + String(new Date().getTime()) + index;
        tempData.push(obj);
        this.uuidArr.push(obj.uuid);
      });
      this.deviceList = tempData;
    }
  }

  /**
   * 返回
   */
  onBack() {
    this.$alert('退出后数据可能会丢失，确认退出采集？', '', {
      confirmButtonText: '确定',
      cancelButtonText: '取消'
    }).then(() => {
      this.sendStop();
      this.recordState = 0;
      window.close();
      this.$emit('back-add');
    });
  }

  /**
   * 开始录制
   */
  async startRecording() {
    const isRepeat = await this.projectNameIsRepeat();
    if (isRepeat) {
      this.$message({ type: 'warning', message: '该中心编号下，项目名称重复，请更改项目名称' });
      return;
    }
    this.onStartRecord();
    this.recordState = 1;
    // this.deviceGroup[this.currentDeviceGroup.
    this.updateGroupStatus('1');
  }

  // status 1代表被占用，0代表空闲
  updateGroupStatus(status: string) {
    const dgroup: any = this.deviceGroup.filter((e: any) => e.id === this.currentDeviceGroup);
    if (dgroup && dgroup.length > 0) {
      dgroup[0].interface_type = status;
      DeviceServer.ModifyDeviceGroupInfo(dgroup[0]).then((result: any) => {});
    }
  }

  endRecording() {
    this.$alert('是否确认结束视频数据采集', '', {
      confirmButtonText: '确定',
      cancelButtonText: '取消'
    }).then(() => {
      this.onEndRecord();
      this.updateGroupStatus('0');
    });
  }

  /**
   * 结束视频录制
   */
  private sendStop() {
    this.uuidArr.forEach((i: any) => {
      this.websocket.send(i);
    });
  }

  /**
   * 开始录制
   */
  async onStartRecord() {
    const recordInfoData = new LaboratoryRecordModel();
    const recordProject = new RecordProjectModel();
    recordInfoData.centerCode = this.centerCode;
    recordProject.deviceGroupId = this.currentDeviceGroup;
    recordProject.projectName = this.projectName;
    // alert(this.currentTemplate.id);
    recordProject.resultId = this.findFirstTemplate().id;
    recordProject.templateRecordContent = this.currentTemplate.content;
    const user: any = sessionStorage.getItem('login_name');
    recordProject.submitName = user;
    recordProject.templateRecordData = JSON.stringify({ a: 1, b: 2 });
    const recordProjectModelList: any = [];
    this.deviceList.forEach((element: any) => {
      recordProjectModelList.push(element.uuid + '.mp4');
    });
    recordProject.historyVideoUrls = JSON.stringify(recordProjectModelList);
    const parameter = {
      recordInfo: recordInfoData,
      recordProjectInfo: recordProject
    };
    recordProject.verifyResult = this.checkAutoSubmit() ? 0 : -1;
    recordProject.approvalResult = this.checkAutoSubmit() ? 0 : -1;
    const result = await laboratoryListService.AddLaboratoryRecordProject(parameter);
    if (result && result.recordInfo) {
      this.currentRecord = result.recordInfo;
      this.project = result.recordProjectInfo ? result.recordProjectInfo : {};
      // this.sendStop();
      // this.showRecord = false;
      // this.showChooseTemplate = true;
      // this.recordState = 0;
    } else {
      this.$message({ type: 'error', message: '实验项目结束失败' });
    }
  }

  checkAutoSubmit(): boolean {
    const ddata = this.findDataByIP();
    return ddata[0].autoSubmit;
  }

  findDataByIP(): any {
    const data: any = this.$store.state.app.dataConfigs;
    const ip = sessionStorage.getItem('login_ip');
    const ddata: any = data.filter((d: any) => d.ip === ip);
    return ddata;
  }

  findFirstTemplate() {
    const ddata = this.findDataByIP();
    const type: any = ddata[0].templateType[0];
    const templates: any = dataDictionaryUtil.getTemplateListSource();
    const template: any = templates.filter((t: any) => t.type === type);
    return template[0];
  }

  /**
   * 结束录制
   */
  async onEndRecord() {
    // const isRepeat = await this.projectNameIsRepeat();
    // if (isRepeat) {
    //   this.$message({ type: 'warning', message: '该中心编号下，项目名称重复，请更改项目名称' });
    //   return;
    // }
    // const recordInfoData = new LaboratoryRecordModel();
    // const recordProject = new RecordProjectModel();
    // recordInfoData.centerCode = this.centerCode;
    // recordProject.deviceGroupId = this.currentDeviceGroup;
    // recordProject.projectName = this.projectName;
    // recordProject.resultId = '';
    // recordProject.templateRecordContent = this.currentTemplate.content;
    // const user: any = sessionStorage.getItem('login_name');
    // recordProject.submitName = user;
    // recordProject.templateRecordData = JSON.stringify({ a: 1, b: 2 });
    // const recordProjectModelList: any = [];
    // this.deviceList.forEach((element: any) => {
    //   recordProjectModelList.push(element.uuid + '.mp4');
    // });
    // recordProject.historyVideoUrls = JSON.stringify(recordProjectModelList);
    // const parameter = {
    //   recordInfo: recordInfoData,
    //   recordProjectInfo: recordProject
    // };
    // recordProject.verifyResult = -1;
    // recordProject.approvalResult = -1;
    const param = {
      approvalName: this.project.approvalName,
      approvalResult: this.project.approvalResult,
      approvalTime: this.project.approvalTime,
      deviceGroupId: this.project.deviceGroupId,
      historyVideoUrls: this.project.historyVideoUrls,
      id: this.project.id,
      modifyName: this.project.modifyName,
      modifyTime: this.project.modifyTime,
      projectName: this.projectName,
      recordId: this.currentRecord.id,
      resultId: this.project.resultId, // this.currentTemplate.id,
      submitName: this.project.submitName,
      submitTime: this.project.submitTime,
      templateRecordContent: this.currentTemplate.content,
      templateRecordData: JSON.stringify(this.$store.getters.TemplateLibrary_currentTemplateData),
      verifyName: this.project.verifyName,
      verifyResult: this.project.verifyResult,
      verifyTime: this.project.verifyTime
    };
    const result = await laboratoryListService.modifyLaboratoryRecordProject(param, this.project.id);
    if (result) {
      // this.currentRecord = result.recordInfo;
      // this.project = result.recordProjectInfo ? result.recordProjectInfo : {};
      this.sendStop();
      this.showRecord = false;
      this.showChooseTemplate = true;
      this.recordState = 0;
    } else {
      this.$message({ type: 'error', message: '实验项目结束失败' });
    }
    // if (result && result.recordInfo) {
    //   this.currentRecord = result.recordInfo;
    //   this.project = result.recordProjectInfo ? result.recordProjectInfo : {};
    //   this.sendStop();
    //   this.showRecord = false;
    //   this.showChooseTemplate = true;
    //   this.recordState = 0;
    // } else {
    //   this.$message({ type: 'error', message: '实验项目结束失败' });
    // }
  }

  /**
   * 重新录制
   */
  resetRecording() {
    this.validateCouldRecord();
    this.showRecord = true;
  }

  /**
   * 提交
   */
  async onSubmit() {
    const temData: any = this.$store.getters.TemplateLibrary_currentTemplateData;
    if (temData.type !== 5 && temData.tableData.length < 1) {
      this.$message({ type: 'success', message: '请选择实验结果数据!' });
      return;
    }
    // 如果等于5 查找datajson notnull是否为true
    if (temData.type === 5) {
      const dataJson: any = this.findDataByIP();
      if (dataJson[0].notNull && (temData.forceValue1 === undefined || temData.forceValue1 === '')) {
        this.$message({ type: 'success', message: '请输入实验结果数据!' });
        return;
      }
    }
    this.recordState = 0;
    const param = {
      approvalName: this.project.approvalName,
      approvalResult: 0,
      approvalTime: this.project.approvalTime,
      deviceGroupId: this.project.deviceGroupId,
      historyVideoUrls: this.project.historyVideoUrls,
      id: this.project.id,
      modifyName: this.project.modifyName,
      modifyTime: this.project.modifyTime,
      projectName: this.projectName,
      recordId: this.currentRecord.id,
      resultId: this.currentTemplate.id,
      submitName: this.project.submitName,
      submitTime: this.project.submitTime,
      templateRecordContent: this.currentTemplate.content,
      templateRecordData: JSON.stringify(this.$store.getters.TemplateLibrary_currentTemplateData),
      verifyName: this.project.verifyName,
      verifyResult: 0,
      verifyTime: this.project.verifyTime
    };
    const result = await laboratoryListService.modifyLaboratoryRecordProject(param, this.project.id);
    const updateParams = this.handleRecordDataUpdate();
    const updateRecordDataResult = await dataRecordService.updateRecordData(updateParams);
    console.log('更新实验结果数据', updateRecordDataResult);
    sessionStorage.removeItem('edit_project');
    if (result) {
      this.$message({ type: 'success', message: '提交成功' });
      this.addSystemLog(this.centerCode, '实验记录提交');
      this.$emit('back-add');
      setTimeout(() => {
        window.close();
      }, 500);
    }
  }

  private handleRecordDataUpdate() {
    const arr: any = [];
    this.selectResultData.forEach((item: any) => {
      const obj = Object.assign({}, item);
      const tempObj = {
        data: obj.data,
        dataid: obj.dataid,
        datatime: obj.datatime,
        datatype: obj.datatype,
        deviceId: obj.deviceId,
        sno: obj.sno,
        status: 1
      };
      arr.push(tempObj);
    });
    return arr;
  }

  /**
   * 判断名称是否重复
   * @returns
   */
  private async projectNameIsRepeat() {
    let isRepeat = false;
    const param = {
      centerCode: this.centerCode,
      laboraryId: '',
      pageNumber: 1,
      pageSize: 1000,
      status: 0
    };
    const recordDataSource = await laboratoryListService.GetAllLaboratoryRecords(param);
    if (recordDataSource && recordDataSource.total > 0) {
      recordDataSource.records.forEach((record: any) => {
        if (record.centerCode === this.centerCode) {
          record.recordProjectInfos.forEach((project: any) => {
            if (project.projectName === this.projectName) {
              isRepeat = true;
            }
          });
        }
      });
    }
    return isRepeat;
  }

  /**
   * 确认模板
   */
  chooseTemplate() {
    if (!this.currentTemplate.id) {
      this.$alert('请选择一个模板', '', {
        confirmButtonText: '确定',
        callback: () => {}
      });
    } else {
      this.showChooseTemplate = false;
      this.showRecord = false;
    }
  }

  /**
   * 选择模板
   * @param val
   */
  templateChoose(val: any) {
    this.currentTemplate = val;
  }

  onShowTemplateResult() {
    this.showTemplateResult = !this.showTemplateResult;
  }

  async addSystemLog(centerCode: any, desc: string) {
    const schoolLog = new SchoolLog();
    schoolLog.centercode = centerCode;
    schoolLog.operationdesc = desc;
    schoolLog.opeationpage = '实验数据采集列表';
    schoolLog.projectname = '实验室数据采集管理';
    const result = await LaboratoryLogService.AddSystemOperationLog(schoolLog);
  }

  /**
   * 选择的中心编号
   */
  selectCenterCode(val: any) {
    // this.validateCouldRecord();
  }

  /**
   * 输入中心编号，模糊提示
   * @param queryString
   * @param cb
   */
  async querySearch(queryString: any, cb: any) {
    const param = {
      centerCode: this.centerCode,
      laboraryId: '',
      pageNumber: 1,
      pageSize: 50,
      status: 0
    };
    const recordDataSource = await laboratoryListService.GetAllLaboratoryRecords(param);
    const data = this.buildSearchData(recordDataSource.records);
    cb(data);
  }

  private buildSearchData(data: any) {
    if (data && Array.isArray(data)) {
      const dataSource = data.map((i: any) => {
        const temp = {
          value: i.centerCode,
          name: i.centerCode
        };
        return temp;
      });
      return dataSource;
    } else {
      return [];
    }
  }

  /**
   * 选择模板类型之后，获取该类型的模板数据结果，type = 5 需要手动输入
   */
  async chooseTrialType() {
    const result = await dataRecordService.getLatestSchoolDataRecordInfoList(this.currentTemplate.type);
    this.trialResultDataSource = [];
    this.trialResultDataSource = result && Array.isArray(result) ? result : [];
  }

  /**
   * 确认模板之后，点击更新模板选择
   */
  changeTemplate() {
    this.showRecord = false;
    this.showChooseTemplate = true;
    this.recordState = 0;
  }

  onResultDataChoose(val: any) {
    let arr: any = [];
    if (this.currentTemplate.type === 4) {
      if (val.length > 0) {
        const obj: any = Object.assign({}, val[0]);
        obj.data = [JSON.parse(obj.data)];
        arr = [Number(obj.data[0].items[0]).toFixed(3), Number(obj.data[0].items[1]).toFixed(3)];
      }
    } else {
      this.showChooseTemplate = false;
      val.forEach((item: any) => {
        const obj = Object.assign({}, item);
        obj.data = [JSON.parse(obj.data)];
        arr.push(Number(obj.data[0].items[0]).toFixed(2));
      });
    }
    this.$store.dispatch('setCurrentTemplateData', {
      title: this.currentTemplate.name,
      time: val.length > 0 ? val[0].datatime : '',
      tableData: arr,
      type: this.currentTemplate.type
    });
    this.selectResultData = val;
  }
  destroyed() {
    window.removeEventListener('beforeunload', (e: any) => this.beforeunloadHandler(e));
  }
}
