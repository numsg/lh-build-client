import { Vue, Component } from 'vue-property-decorator';
import ResultInputTemplateComponent from '../../../components/result-input-template/result-input-template';

// 模板管理
@Component({
  components: { ResultInputTemplateComponent }
})
export default class TemplateManager extends Vue {}
