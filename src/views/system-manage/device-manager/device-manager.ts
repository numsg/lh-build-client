import { Vue, Component } from 'vue-property-decorator';
import DeviceInfo from '@/components/laboratory-device/laboratory-device/laboratory-device';
import DeviceGroupInfo from '@/components/laboratory-device/laboratory-device-group/laboratory-device-group';

// 设备管理
@Component({
    components: {
        DeviceInfo,
        DeviceGroupInfo
    }
})
export default class DeviceManager extends Vue {

}
