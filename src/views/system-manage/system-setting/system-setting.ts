import { Vue, Component, Watch } from 'vue-property-decorator';
import { Table } from 'element-ui';
import lodash from 'lodash';

import SettingService from '../../../api/system-manage/setting-service';
import { SettingTypeInfo } from '../../../common/models/system-manage/setting-type-info';
import { SettingType } from '../../../common/enums/setting-type';
import { RegularTypes } from '../../../common/constant/regular-type';
import { SettingInfo } from '../../../common/models/system-manage/setting-info';
import { sortStr } from '../../../common/utils/string-utils';

import { AssertNotNull } from '@gsafety/cad-gutil/dist/utilhelper';
import { InjectLog, Logger } from '@gsafety/cad-glog';



@Component
export default class SystemSetting extends Vue {
  /**
   * 日志组建
   */
  @InjectLog() logger!: Logger;
  /**
   * 类型全部
   */
  showAllTree = false;
  /**
   * 设置类型弹窗开关
   */
  settingTypeDialogVisible = false;
  /**
   * 设置信息弹窗开关
   */
  settingDialogVisible = false;
  /**
   * 类型搜索关键字
   */
  typeSearchKeyWord = '';
  /**
   * 配置类型
   */
  settingTypes: SettingTypeInfo[] = [];
  /**
   * 当前选中的设置类型
   */
  currentSelectSettingType: SettingTypeInfo = new SettingTypeInfo();
  /**
   * 添加OR编辑的设置类型信息
   */
  settingTypeEditing: SettingTypeInfo = new SettingTypeInfo();
  /**
   * 所有设置类型
   */
  allSettingTypeData: SettingTypeInfo[] = [];
  /**
   * 设置表单标题
   */
  operateTitle = '';
  /**
   * 设置信息查询关键字
   */
  settingSearchKeyWord = '';
  /**
   * 设置信息列表
   */
  settingList: SettingInfo[] = [];
  /**
   * 添加OR编辑的配置信息
   */
  settingEditing: SettingInfo = new SettingInfo();
  /**
   * 当前选择的设置信息
   */
  currentSelectSetting: SettingInfo = new SettingInfo();
  /**
   * 添加/编辑类型标识
   */
  addTypeFlag = false;
  /**
   * 添加/编辑设置标识
   */
  addSettingFlag = false;
  /**
   * 查询对象
   */
  queryData: any[] = [];
  /**
   * 类型编辑操作控制
   */
  disableEdit = true;
  /**
   * 类型删除操作控制
   */
  disableDelete = true;
  /**
   * 是否为密码设置
   */
  isPassword = false;
  /**
   * 密码设置对象
   */
  pwdSetting: SettingInfo = new SettingInfo();
  /**
   * SystemSetting创建Vue
   */
  created() {
    this.getSettingTypes();
  }
  /**
   * 设置类型校验规则
   */
  get settingTypeRules() {
    return {
      typeName: [
        { required: true, trigger: ['change', 'blur'], message: this.$t('SigtSystemSetting.TypeNameNotEmpty').toString() },
        { whitespace: true, trigger: ['blur', 'change'], message: this.$t('SigtSystemSetting.TypeNameNotEmpty').toString() },
        { validator: this.validateTypeName, trigger: ['blur', 'change'] }],
      typeId: [{ required: true, trigger: ['change', 'blur'], message: this.$t('SigtSystemSetting.TypeCodeNotEmpty').toString() },
      { pattern: RegularTypes.OnlyNumber, message: this.$t('SigtOrganization.PleaseEnterNumber').toString() }
      ]
    };
  }
  /**
   * 设置校验规则
   */
  get settingRules() {
    return {
      name: [
        { required: true, trigger: ['change', 'blur'], message: this.$t('SigtSystemSetting.ConfigNameNotEmpty').toString() },
        { whitespace: true, trigger: ['blur'], message: this.$t('SigtSystemSetting.ConfigNameNotEmpty').toString() },
        { validator: this.validateSettingName, trigger: ['blur', 'change'] }],
      value: [
        { required: true, trigger: ['change', 'blur'], message: this.$t('SigtSystemSetting.ConfigValueNotEmpty').toString() },
        { whitespace: true, trigger: ['blur', 'change'], message: this.$t('SigtSystemSetting.ConfigValueNotEmpty').toString() }
      ],
      description: [
        { required: true, trigger: ['change', 'blur'], message: this.$t('SigtSystemSetting.ConfigDescriptionNotEmpty').toString() },
        { whitespace: true, trigger: ['blur', 'change'], message: this.$t('SigtSystemSetting.ConfigDescriptionNotEmpty').toString() }
      ]
    };
  }
  /**
   * 设置类型名称校验
   */
  async validateTypeName(rule: any, value: any, callback: any) {

  }
  /**
   * 配置名称重复校验
   */
  async validateSettingName(rule: any, value: any, callback: any) {

  }
  /**
   * 设置类型查询
   * @param val 查询参数
   */
  @Watch('typeSearchKeyWord')
  searchTypeChange(val: string) {
    const strSearch = val.trim().toLowerCase();
    if (strSearch) {
      const settingTypesTemp = lodash.cloneDeep(this.allSettingTypeData);
      this.settingTypes = settingTypesTemp.filter((rd: any) => {
        return (this.showAllTree ? true : rd.enable === 1) && rd.typeName.toString().toLowerCase().includes(strSearch);
      });
    } else {
      if (this.showAllTree) {
        this.settingTypes = this.allSettingTypeData;
      } else {
        this.settingTypes = this.allSettingTypeData.filter((item) => item.enable === 1);
      }
    }
    this.setCurrentType();
  }
  /**
   * 设置当前选择的类型
   */
  setCurrentType() {
    if (this.settingTypes && this.settingTypes.length) {
      const settingTable = this.$refs.settingTypeTable as Table;
      if (this.currentSelectSettingType.id) {
        const checkeditem = this.settingTypes.find((st) => st.id === this.currentSelectSettingType.id);
        if (checkeditem) {
          settingTable.setCurrentRow(checkeditem);
          this.disableEdit = false;
          this.disableDelete = checkeditem.editable === 0;
          this.getSettings(this.currentSelectSettingType.typeId);
        }
      } else {
        settingTable.setCurrentRow(this.settingTypes[0]);
        this.getSettings(this.settingTypes[0].typeId);
        this.disableEdit = false;
        this.disableDelete = this.settingTypes[0].editable === 0;
        this.currentSelectSettingType = this.settingTypes[0];
      }
    }
  }
  /**
   * 类型树查询
   */
  @Watch('showAllTree')
  filterSettingType() {
    const tempData = lodash.cloneDeep(this.allSettingTypeData);
    if (!this.typeSearchKeyWord || this.typeSearchKeyWord === '') {
      if (this.showAllTree) {
        this.settingTypes = tempData;
      } else {
        this.settingTypes = tempData.filter((item) => item.enable === 1);
      }
    } else {
      if (this.showAllTree) {
        this.settingTypes = tempData.filter((item) => {
          return item.typeName.toLowerCase().includes(this.typeSearchKeyWord.toLowerCase());
        });
      } else {
        this.settingTypes = tempData.filter((item) => {
          return item.enable === 1 && (item.typeName.toLowerCase().includes(this.typeSearchKeyWord.toLowerCase()));
        });
      }
    }
    this.setCurrentType();
  }
  /**
   * 获取所有设置类型
   */
  async getSettingTypes() {
    await SettingService.findAllSettingType().then((result) => {
      if (result) {
        this.allSettingTypeData = result;
        if (!this.showAllTree) {
          this.settingTypes = result.filter((st: any) => st.enable === 1);
        } else {
          this.settingTypes = result;
        }
        this.setCurrentType();
      }
    });
  }
  /**
   * 获取所有的配置
   */
  async getAllSettings() {
    const result = await SettingService.findAllSettingInfos();
    if (result) {
      this.queryData = result;
      this.filterData();
    }
  }
  /**
   * 配置类型列表点击事件
   * @param val 类型信息
   */
  settingTypeChange(val: SettingTypeInfo) {
    this.currentSelectSettingType = val;
    this.disableEdit = false;
    this.disableDelete = val.editable === 0;
    this.getSettings(this.currentSelectSettingType.typeId);
  }
  /**
   * 新增系统设置类型弹出框
   */
  addSettingTypeDialog() {
    this.operateTitle = this.$t('SigtSystemSetting.AddSettingType').toString();
    this.settingTypeEditing = new SettingTypeInfo();
    this.settingTypeDialogVisible = true;
    this.addTypeFlag = true;
  }
  /**
   * 修改系统设置类型弹出框
   */
  editSettingTypeDialog() {
    this.operateTitle = this.$t('SigtSystemSetting.EditSettingType').toString();
    this.settingTypeEditing = JSON.parse(JSON.stringify(this.currentSelectSettingType));
    this.settingTypeDialogVisible = true;
    this.addTypeFlag = false;
  }
  /**
   * 删除系统设置类型
   */
  async deleteSettingType() {
    if (this.settingList && this.settingList.length > 0) {
      this.$message.warning(this.$t('SigtSystemSetting.NotAllowedDelSetting').toString());
      return;
    }

    this.$confirm(this.$t('SigtOrganization.DelOrgWarn').toString(), this.$t('SigtCommon.Tips').toString(), {
      confirmButtonText: this.$t('SigtCommon.Confirm').toString(),
      cancelButtonText: this.$t('SigtCommon.Cancel').toString(),
      type: 'warning',
      closeOnClickModal: false
    }).then( async () => {
       SettingService.deleteSettingType(this.currentSelectSettingType.id).then((res: boolean) => {
      if (res) {
        this.$message.success(this.$t('SigtCommon.OperateSuccess').toString());
      } else {
        this.$message.error(this.$t('SigtCommon.OperateError').toString());
      }
      }).catch((err: any) => {
        this.$message.error(this.$t('SigtCommon.OperateError').toString());
      }).finally(async () => {
        this.currentSelectSettingType = new SettingTypeInfo();
        await this.getSettingTypes();
        this.disableEdit = true;
      });
    });
  }
  /**
   * 保存设置类型
   */
  async saveSettingType() {
    SettingService.saveSettingType(this.settingTypeEditing)
      .then((result: any) => {
        if (result.errorCode === '1003') {
          this.$message.warning(this.$t('SigtSystemSetting.TypeNameExist').toString());
          return;
        }
        if (result.errorCode === '1005') {
          this.$message.warning(this.$t('SigtSystemSetting.TypeCodeExist').toString());
          return;
        }
        if (result.success) {
          this.$message.success(this.$t('SigtCommon.OperateSuccess').toString());
          this.currentSelectSettingType = result.settingType;
          this.getSettingTypes();
          this.settingTypeDialogVisible = false;
        } else {
          this.$message.error(this.$t('SigtCommon.OperateError').toString());
        }
      }).catch((err: any) => {
        console.log(err);
        this.$message.error(this.$t('SigtCommon.OperateError').toString());
      });
  }
  /**
   * 系统设置类型-表单验证
   */
  validSettingTypeForm() {
    const settingTypeForm: any = this.$refs.settingTypeForm;
    settingTypeForm.validate((valid: any) => {
      if (valid) {
        this.saveSettingType();
      }
    });
  }
  /**
   * 清空表单的验证
   */
  clearSettingTypeFormValidate() {
    const settingForm: any = this.$refs.settingForm;
    if (settingForm) {
      settingForm.clearValidate();
    }
  }
  /**
   * 查询
   */
  filterData() {
    const dataTemp = lodash.cloneDeep(this.queryData);
    const keyword = this.settingSearchKeyWord.trim().toLowerCase();
    const enableSettings = [];
    if (!keyword) {
      this.settingList = dataTemp;
    } else {
      this.settingList = dataTemp.filter((item: SettingInfo) => {
        return item.name && item.name.trim().toLowerCase().includes(keyword);
      });
    }
    if (!this.showAllTree) {
      for (const item of this.allSettingTypeData) {
        for (const ele of this.settingList) {
          if (item.enable === 1 && ele.paramType === item.typeId) {
            enableSettings.push(ele);
          }
        }
      }
      this.settingList = enableSettings;
    }
    this.setCurrentType();
  }
  /**
   * 重置查询
   */
  resetSearch() {
    this.settingSearchKeyWord = '';
    this.settingList = this.queryData;
  }
  /**
   * 获取参数列表数据
   */
  async getSettings(settingTypeId: any) {
    this.settingList = [];
    if (AssertNotNull(settingTypeId)) {
      const res = await SettingService.findByParamType(settingTypeId);

      this.queryData = res;
      const dataTemp = lodash.cloneDeep(this.queryData);
      const keyword = this.settingSearchKeyWord.trim().toLowerCase();
      if (!keyword) {
        this.settingList = dataTemp;
      } else {
        this.settingList = dataTemp.filter((item: SettingInfo) => {
          return item.name && item.name.trim().toLowerCase().includes(keyword);
        });
      }
      if (settingTypeId === 14 && this.settingList.length > 0) {
        this.pwdSetting = lodash.cloneDeep(this.settingList[0]);
        this.settingList[0].value = '••••••';
      }
    } else {
      this.getSettings(this.settingTypes[0].typeId);
    }
  }
  /**
   * 添加设置弹窗
   */
  addSettingDialog() {
    this.operateTitle = this.$t('SigtCommon.Add').toString() + this.$t('SigtSystemSetting.Config').toString();
    this.settingEditing = new SettingInfo();
    this.currentSelectSetting = new SettingInfo();
    this.settingDialogVisible = true;
    this.addSettingFlag = true;
  }
  /**
   * 编辑设置弹窗
   * @param setting 设置信息
   */
  editSettingDialog(setting: SettingInfo) {
    this.operateTitle = this.$t('SigtCommon.Edit').toString() + this.$t('SigtSystemSetting.Config').toString();
    if (setting.name === 'origanalPwd') {
      this.currentSelectSetting = this.pwdSetting;
      this.isPassword = true;
    } else {
      this.currentSelectSetting = setting;
      this.isPassword = false;
    }
    this.settingEditing = JSON.parse(JSON.stringify(this.currentSelectSetting));
    this.settingDialogVisible = true;
    this.addSettingFlag = false;
  }
  /**
   * 删除设置信息
   * @param setting 设置信息
   */
  deleteSetting(setting: SettingInfo) {
    this.currentSelectSetting = setting;
    this.settingEditing = JSON.parse(JSON.stringify(setting));
    this.$confirm(this.$t('SigtSystemSetting.DeleteWarn').toString(), this.$t('SigtCommon.Tips').toString(), {
      confirmButtonText: this.$t('SigtCommon.Confirm').toString(),
      cancelButtonText: this.$t('SigtCommon.Cancel').toString(),
      type: 'warning',
      closeOnClickModal: false
    }).then(() => {
      SettingService.deleteSetting(this.currentSelectSetting.settingId).then((res: boolean) => {
        if (res) {
          this.$message.success(this.$t('SigtCommon.OperateSuccess') as string);
        } else {
          this.$message.error(this.$t('SigtCommon.OperateError').toString());
        }
      }).catch(() => { }).finally(() => {
        this.getSettings(this.currentSelectSettingType.typeId);
      });
    });
  }
  /**
   * 设置信息-验证表单
   */
  validSettingForm() {
    const settingForm: any = this.$refs.settingForm;
    settingForm.validate((valid: any) => {
      if (valid) {
        this.saveSetting();
      }
    });
  }
  /**
   * 清空设置表单的验证
   */
  clearSettingFormValidate() {
    const settingForm: any = this.$refs.settingForm;
    if (settingForm) {
      settingForm.clearValidate();
    }
  }
  /**
   * 保存设置信息
   */
  saveSetting() {
    if (SettingType[Number(this.currentSelectSettingType.typeId)]) {
      this.settingEditing.paramType = SettingType[Number(this.currentSelectSettingType.typeId)];
    } else {
      this.settingEditing.paramType = null;
    }
    this.settingEditing.paramType = Number(this.currentSelectSettingType.typeId);
    SettingService.saveSetting(this.settingEditing)
      .then((result: any) => {
        if (result.errorCode === '1003') {
          this.$message.warning(this.$t('SigtSystemSetting.ConfigNameExist').toString());
          return;
        }
        if (result.success) {
          this.$message.success(this.$t('SigtCommon.OperateSuccess').toString());
          this.settingDialogVisible = false;
          this.getSettings(this.currentSelectSettingType.typeId);
        } else {
          this.$message.error(this.$t('SigtCommon.OperateError').toString());
        }
      }).catch((err: any) => {
        console.log(err);
        this.$message.error(this.$t('SigtCommon.OperateError').toString());
      });
  }
  /**
   * 简称排序
   */
  sortName(a: any, b: any) {
    return sortStr(a.name, b.name);
  }
}
