import { Vue, Component } from 'vue-property-decorator';
import LaboratoryManagerService from '@/api/laboratory-manager-service';
import SchoolLaboratoryInfo from '@/models/school-laboratory-info';
import ResultInfo from '@/models/result-info';
import SchoolLogService from '@/api/laborartory-log-service';
import SchoolLog from '@/models/school-laborary-log-info';

// 实验室管理
@Component({
  components: {}
})
export default class LaboratoryManager extends Vue {
  laboratoryList: SchoolLaboratoryInfo[] = [];
  pageSize = 10; // 后续优化成读取某个配置获取每页大小
  total = 0;
  pageCount = 0;
  currentPage = 1;
  keyword = '';
  resultInfo = new ResultInfo();
  loading = false;
  operateTitle = '';
  laboraryDialogVisible = false;
  addLabFlag = false;
  schoolLaboratoryInfo = new SchoolLaboratoryInfo();
  computerIps: string[] = [];


  dynamicItem = [{ ip: '' }];
  laboraryRules = {
    laborary_name: [
      { required: true, trigger: ['change', 'blur'], message: '请输入名称' },
      { whitespace: true, trigger: ['blur'], message: '不允许为空' }
    ],
    weight: [
      { required: true, trigger: ['change', 'blur'], message: '请输入权重值' },
      // { whitespace: true, trigger: ['blur', 'change'], message: '不允许为空' }
    ]
  };
  async mounted() {
    this.getData();
  }

  async getData() {
    this.resultInfo = await LaboratoryManagerService.GetLaboratoryInfobyPage(this.currentPage - 1, this.pageSize, this.keyword);
    this.laboratoryList = this.resultInfo.dataList;
    this.total = this.resultInfo.elementCount;
    this.pageCount = this.resultInfo.pageCount;
  }

  getIndex($index: any) {
    return (this.currentPage - 1) * this.pageSize + $index + 1;
  }

  async searchLab() {
    await this.getData();
  }

  async reset() {
    this.keyword = '';
    await this.getData();
  }

  add() {
    this.operateTitle = '添加';
    this.laboraryDialogVisible = true;
    this.schoolLaboratoryInfo = new SchoolLaboratoryInfo();
    this.addLabFlag = true;
  }
  validLaboraryForm() {
    const laboraryForm: any = this.$refs.laboraryForm;
    laboraryForm.validate((valid: any) => {
      if (valid) {
        if (this.addLabFlag === true) {
          this.saveLaborary();
        } else {
          this.updateLaborary();
        }
      }
    });
  }

  async saveLaborary() {
    const result = await LaboratoryManagerService.AddLaboratoryInfo(this.schoolLaboratoryInfo);
    if (result === true) {
      this.$message.success('添加成功');
      this.laboraryDialogVisible = false;
      const schoolLog = new SchoolLog();
      schoolLog.centercode = 'test111';
      schoolLog.opeationpage = '新增实验室页面';
      schoolLog.projectname = '实验室管理';
      await SchoolLogService.AddSystemOperationLog(schoolLog);
      this.getData();
    } else {
      this.$message.error('添加失败');
    }
  }

  editLab(lab: SchoolLaboratoryInfo) {
    this.operateTitle = '编辑';
    this.laboraryDialogVisible = true;
    this.addLabFlag = false;
    this.schoolLaboratoryInfo = lab;
  }
  deleteLab(lab: SchoolLaboratoryInfo) {
    this.$confirm('你确定要删除吗?', '系统提示', {
      confirmButtonText: '确认',
      cancelButtonText: '取消',
      type: 'warning',
      closeOnClickModal: false
    })
      .then(async () => {
        await this.deleteLaborary(lab.id);
      })
      .catch((err: any) => {
        this.$message.error(this.$t('SigtCommon.OperateError').toString());
      })
      .finally(async () => {
        await this.getData();
      });
  }

  async updateLaborary() {
    const result = await LaboratoryManagerService.UpdateLaboratoryInfo(this.schoolLaboratoryInfo);
    if (result === true) {
      this.$message.success('修改成功');
      this.laboraryDialogVisible = false;
      this.getData();
    } else {
      this.$message.error('修改失败');
    }
  }
  async deleteLaborary(id: string) {
    const result = await LaboratoryManagerService.DeletLaboratoryInfo(id);
    if (result === true) {
      this.$message.success('删除成功');
      this.laboraryDialogVisible = false;
      this.getData();
    } else {
      this.$message.error('删除失败');
    }
  }
  handleSizeChange(value: number) {
    this.pageSize = value;
    this.getData();
  }
  handleCurrentChange(vlue: number) {
    this.currentPage = vlue;
    this.getData();
  }
  beforeDestory() { }
}
