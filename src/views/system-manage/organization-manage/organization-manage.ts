import { Vue, Component, Watch } from 'vue-property-decorator';
import lodash from 'lodash';
import _ from 'lodash';
import { Action } from 'vuex-class';
import userManagerService from '@/api/system-manage/user-manager-service';
import { init } from 'echarts';
import { UserInfo } from '@/models/userinfo';
import roleAuthorityService from '@/api/system-manage/role-authority-service';
import { InjectLog, Logger } from '@/common/utils/logger';

/**
 * 用户管理
 */
@Component({
  components: {
  }
})
export default class OrganizationManage extends Vue {

  /**
   * 日志组件
   */
  @InjectLog() logger!: Logger;

  UserList: any = [];

  DialogVisible: any = false;
  operateTitle: any = '';
  userInfo: UserInfo = new UserInfo();
  roleList: any = [];
  currentrole: any = '';
  currentroleId: any;
  inputQuery = '';
  query = _.debounce(this._query, 300);
  mounted() {
    this.init();
    this.initRole();
  }

  async init() {
    this.UserList = await userManagerService.findAll();

  }

  async initRole() {
    this.roleList = await roleAuthorityService.getAllRoles()
      .catch((err: any) => {
        this.logger.error(`[RoleAuthority] -> roleAuthorityService.getAllRoles(): ${JSON.stringify(err)}`);
      });
  }

  adduser() {
    this.userInfo = new UserInfo();
    this.DialogVisible = true;
    this.operateTitle = '添加用户';
  }

  selectedChanged(val: any) {
    this.currentrole = val.description;
    this.currentroleId = val.id;
  }

  async dialogoperation() {
    if (this.operateTitle === '添加用户') {
      this.userInfo.roleid = this.currentroleId;
      this.userInfo.rolename = this.currentrole;
      this.userInfo.status = 0;
      const result = await userManagerService.addUser(this.userInfo);
      if (result) {
        this.$message.success('添加用户成功');
      } else {
        this.$message.error('添加用户失败');
      }
      this.DialogVisible = false;
      this.init();
    }
    if (this.operateTitle === '编辑用户') {
      const result = await userManagerService.updateUser(this.userInfo);
      if (result) {
        this.$message.success('编辑用户成功');
      } else {
        this.$message.error('编辑用户失败');
      }
      this.DialogVisible = false;
      this.init();
    }
  }
  updateuser(row: any) {
    this.userInfo = row;
    this.currentrole = this.userInfo.rolename;
    this.operateTitle = '编辑用户';
    this.DialogVisible = true;
  }
  deleteuser(row: any) {
    this.userInfo = row;
    this.$confirm('是否删除当前用户？', '操作', {
      confirmButtonText: '确认',
      cancelButtonText: '取消',
      type: 'warning',
      closeOnClickModal: false
    }).then(async () => {
      const result = await userManagerService.deleteUser(this.userInfo);
      if (result) {
        this.$message.success('删除用户成功');
        this.init();
      } else {
        this.$message.error('删除用户失败');
      }
    });
  }
  resetuser(row: any) {
    this.userInfo = row;
    this.userInfo.password = '123456';
    this.$confirm('是否重置当前用户密码吗？', '操作', {
      confirmButtonText: '确认',
      cancelButtonText: '取消',
      type: 'warning',
      closeOnClickModal: false
    }).then(async () => {
      const result = await userManagerService.resetUser(this.userInfo);
      if (result) {
        this.$message.success('重置用户密码成功');
        this.init();
      } else {
        this.$message.error('重置用户密码失败');
      }
    });
  }
  _query() {
    if (this.inputQuery.trim().length === 0) {
      this.init();
    } else {
      this.UserList = this.UserList.filter((into: any) => {
        return into.username.indexOf(this.inputQuery.trim()) > -1;
      });
    }
  }
}
