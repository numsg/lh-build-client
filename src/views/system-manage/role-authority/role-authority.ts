import { Vue, Component, Watch } from 'vue-property-decorator';
import { Getter } from 'vuex-class';
import roleAuthorityService from '../../../api/system-manage/role-authority-service';
import { RoleInfo } from '../../../common/models/role/role-info';
import { RoleMenu } from '../../../common/models/role/role-menu';
import menuService from '../../../api/menu-service';
import { utilHelper } from '@gsafety/cad-gutil';
import { AppTypes } from '@/store/types/app-types';
import { BizType } from '../../../common/constant/biz-type';
import lodash from 'lodash';
import { showLoading, hideLoading } from '../../../common/utils/loading';
import { InjectLog, Logger } from '@gsafety/cad-glog';
import { Table } from 'element-ui';


@Component
export default class RoleAuthority extends Vue {
  /**
   * 日志组件
   */
  @InjectLog() logger!: Logger;
  /**
   * 配置参数
   */
  @Getter(AppTypes.getters.CONFIGS)
  configs: any;
  /**
   * 角色数据
   */
  private roleData: any[] = [];
  /**
   * 当前菜单
   */
  private currentMenu: any[] = [];
  /**
   * 菜单数据
   */
  private menuData: any[] = [];
  /**
   * 折叠列
   */
  private foldList: any = [];
  /**
   * 角色菜单列表
   */
  private roleMenuList: any[] = [];
  /**
   * 是否是新增角色
   */
  isAddRole = true;
  /**
   * 角色操作类型
   */
  operateRoleTitle = '';
  /**
   * 机构弹框显示
   */
  roleDialogVisible = false;
  /**
   * 保存loading
   */
  saveLoading = false;
  /**
   * 折叠按钮状态
   */
  flodMenuDisplay = true;
  /**
   * 展开按钮状态
   */
  unFlodMenuDisplay = true;
  /**
   * 编辑的角色
   */
  roleInfo: RoleInfo = new RoleInfo();
  /**
   * 当前选中的角色
   */
  currentRole: RoleInfo = new RoleInfo();
  /**
   * 角色菜单关联
   */
  roleMenu: RoleMenu = new RoleMenu();
  /**
   * 业务类型
   */
  bizType: any[] = [];
  /**
   * 当前选中的业务类型,默认坐席员
   */
  currentBizType = BizType[2].id;
  /**
   * 编辑/删除可用控制
   */
  btnDisabled = true;
  /**
   * 角色查询关键字
   */
  roleKeyWord = '';
  /**
   * 所有角色数据
   */
  allData: any;
  /**
   * 保存按钮控制
   */
  saveBtnDisabled = true;
  /**
   * 保存按钮Loading
   */
  saveMenuLoading = false;
  /**
   * 保存角色表单的按钮Loading
   */
  saveRoleBtnLoading = false;
  /**
   * RoleAuthority创建函数
   */
  created() {
    this.getAllRoles();
    this.getMenu();
    this.bizType = BizType;
  }
  /**
   * 角色校验规则
   */
  get roleRules() {
    return {
      roleName: [
        { required: true, trigger: ['change', 'blur'], message: this.$t('SigtRoleAuthority.PleaseEnterRoleName').toString() },
        { whitespace: true, trigger: ['blur', 'change'], message: this.$t('SigtRoleAuthority.PleaseEnterRoleName').toString() },
        { validator: this.validateRoleName, trigger: ['blur', 'change'] }]
    };
  }
  /**
   * 组织机构重复验证
   */
  async validateRoleName(rule: any, value: any, callback: any) {

  }
  /**
   * 角色查询
   * @param val 输入值
   */
  @Watch('roleKeyWord')
  searchRole(val: string) {
    const strSearch = val.toString().trim().toLowerCase();
    if (strSearch) {
      const roleDataTemp = lodash.cloneDeep(this.allData);
      this.roleData = roleDataTemp.filter((rd: any) => {
        return rd.roleName.toString().toLowerCase().includes(strSearch);
      });
    } else {
      this.getAllRoles();
    }
    this.btnDisabled = true;
  }
  /**
   * 角色表单验证
   */
  validForm() {
    const refs: any = this.$refs.ruleForm;
    refs.validate((valid: any) => {
      if (valid) {
        this.opearteRole();
      }
    });
  }
  /**
   * 清空角色表单验证
   */
  clearFormValidate() {
    const ruleForm: any = this.$refs.ruleForm;
    if (ruleForm) {
      ruleForm.clearValidate();
    }
  }
  /**
   * 获取角色
   */
  async getAllRoles() {
    const result = await roleAuthorityService.getAllRoles()
      .catch((err: any) => {
        this.logger.error(`[RoleAuthority] -> roleAuthorityService.getAllRoles(): ${JSON.stringify(err)}`);
      });
    this.roleData = this.allData = result;
    const strSearch = this.roleKeyWord.trim().toLowerCase();
    if (strSearch) {
      const roleDataTemp = lodash.cloneDeep(this.allData);
      this.roleData = roleDataTemp.filter((rd: any) => {
        return rd.roleName.toString().toLowerCase().includes(strSearch);
      });
    }
    this.btnDisabled = true;
    this.saveBtnDisabled = true;
    const theRoleTable = this.$refs.roleTable as Table;
    if (this.currentRole.id) {
      this.rowClick(this.currentRole);
      const rowData = this.roleData.find((r) => r.id === this.currentRole.id);
      theRoleTable.setCurrentRow(rowData);
    } else {
      theRoleTable.setCurrentRow(this.roleData[0]);
      this.rowClick(this.roleData[0]);
    }
  }
  /**
   * 获取菜单数据
   */
  async getMenu() {
    showLoading(this.$refs.roleAuthorityList, 'roleAuthorityList');
    const result = await menuService.getMenuTree()
      .catch((err: any) => {
        this.logger.error(`[RoleAuthority] -> menuService.getMenuTree(): ${JSON.stringify(err)}`);
      })
      .finally(() => {
        hideLoading('roleAuthorityList');
      });
    this.menuData = this.formatConversion([], result);

  }
  /**
   * 保存操作
   */
  opearteRole() {
    this.saveRoleBtnLoading = true;
    if (this.isAddRole) {
      this.addRole();
    } else {
      this.editRole();
    }
    this.saveRoleBtnLoading = false;
  }
  /**
   * 新增角色
   */
  async addRole() {
    this.roleInfo.id = utilHelper.getUuid32();
    this.roleInfo.code = this.currentBizType;
    const result = await roleAuthorityService.AddRole(this.roleInfo)
      .catch((err: any) => {
        this.logger.error(`[RoleAuthority] -> RoleAuthorityService.AddRole(${this.roleInfo}): ${JSON.stringify(err)}`);
      });

    if (result && result.errorCode === '1003') {
      this.$message.warning(this.$t('SigtRoleAuthority.RoleExisted').toString());
      return;
    }
    if (result.success) {
      // 2.返回结果提示，控制窗口，刷新数据
      this.roleDialogVisible = false;
      this.getAllRoles();
      this.$message.success(this.$t('SigtCommon.OperateSuccess').toString());
    } else {
      // 3.失败提示
      this.$message.error(this.$t('SigtCommon.OperateError').toString());
    }
  }
  /**
   * 修改角色
   */
  async editRole() {
    const result = await roleAuthorityService.editRole(this.roleInfo)
      .catch((err: any) => {
        this.logger.error(`[RoleAuthority] -> RoleAuthorityService.editRole(${this.roleInfo}): ${JSON.stringify(err)}`);
      });
    if (result && result.errorCode === '1003') {
      this.$message.warning(this.$t('SigtRoleAuthority.RoleExisted').toString());
      return;
    }
    if (result.success) {
      // 2.返回结果提示，控制窗口，刷新数据
      this.roleDialogVisible = false;
      this.getAllRoles();
      this.$message.success(this.$t('SigtCommon.OperateSuccess').toString());
    } else {
      // 3.失败提示
      this.$message.error(this.$t('SigtCommon.OperateError').toString());
    }
  }
  /**
   * 删除角色
   */
  deleteRole() {
    if (!this.currentRole.id) {
      this.$message.warning(this.$t('SigtRoleAuthority.SelectRole').toString());
    } else {
      if (!this.currentRole.deletable) {
        this.$message.warning(this.$t('SigtRoleAuthority.NotAllowedDelRole').toString());
      } else {
        this.$confirm(this.$t('SigtRoleAuthority.DeleteRoleConfirm').toString(), this.$t('SigtCommon.Tips').toString(), {
          confirmButtonText: this.$t('SigtCommon.Confirm').toString(),
          cancelButtonText: this.$t('SigtCommon.Cancel').toString(),
          type: 'warning',
          closeOnClickModal: false
        }).then(async () => {
          const deleteRoleCodes: string[] = [];
          deleteRoleCodes.push(this.currentRole.id);
          const result = await roleAuthorityService.deleteRole(deleteRoleCodes)
            .catch((err: any) => {
              // tslint:disable-next-line:max-line-length
              this.logger.error(`[RoleAuthority] -> RoleAuthorityService.deleteRole(${deleteRoleCodes}): ${JSON.stringify(err)}`);
            });
          if (result) {
            this.$message.success(this.$t('SigtCommon.OperateSuccess').toString());
          } else {
            this.$message.error(this.$t('SigtCommon.OperateError').toString());
          }
        }).finally(() => {
          this.getAllRoles();
        });
      }
    }
  }
  /**
   * 新增角色弹出框
   */
  showAddRoleDialog() {
    this.isAddRole = true;
    this.operateRoleTitle = this.$t('SigtRoleAuthority.AddRole').toString();
    this.roleInfo = new RoleInfo();
    this.roleDialogVisible = true;
  }
  /**
   * 编辑角色弹出框
   */
  showEditRoleDialog() {
    if (!this.currentRole.id) {
      this.$message.warning(this.$t('SigtRoleAuthority.SelectRole').toString());
    } else {
      if (!this.currentRole.deletable) {
        this.$message.warning(this.$t('SigtRoleAuthority.NotAllowedEditRole').toString());
      } else {
        this.isAddRole = false;
        this.operateRoleTitle = this.$t('SigtRoleAuthority.EditRole').toString();
        this.$set(this.roleInfo, 'id', this.currentRole.id);
        this.$set(this.roleInfo, 'roleName', this.currentRole.roleName);
        this.$set(this.roleInfo, 'description', this.currentRole.description);
        this.$set(this.roleInfo, 'code', this.currentBizType);
        this.roleDialogVisible = true;
      }
    }
  }
  /**
   * 角色行点击事件
   * @param val 行数据
   */
  async rowClick(val: any) {
    this.saveBtnDisabled = false;
    this.currentRole = val;
    if (this.currentRole.deletable === 0) {
      this.btnDisabled = true;
    } else {
      this.btnDisabled = false;
    }
    // 关联角色菜单
    const result = await menuService.getMenuByRoleId(val.id, this.configs.systemUrl)
      .catch((err: any) => {
        this.logger.error(`[RoleAuthority] -> MenuService.getMenuByRoleId(${val.id},${this.configs.systemUrl}): ${JSON.stringify(err)}`);
      });
    this.currentMenu = result;
    for (const item of this.menuData) {
      if (this.currentMenu.map((i) => i.id).includes(item.id)) {
        this.$set(item, 'isCheck', true);
      } else {
        this.$set(item, 'isCheck', false);
      }
    }
  }
  /**
   * 保存角色菜单关联
   */
  async saveRoleMenu() {
    if (!this.currentRole.id) {
      this.$message.warning(this.$t('SigtRoleAuthority.SelectRole').toString());
    } else {
      this.saveMenuLoading = true;
      // 保存
      this.roleMenuList = [];
      for (const item of this.menuData) {
        if (item.isCheck) {
          this.roleMenu = new RoleMenu();
          this.roleMenu.id = utilHelper.getUuid32();
          this.roleMenu.roleId = this.currentRole.id;
          this.roleMenu.menuId = item.id;
          this.roleMenuList.push(this.roleMenu);
        }
      }
      // 如何没有菜单数据 则调用删除角色菜单接口 否则调用配置角色菜单接口
      if (this.roleMenuList.length === 0) {
        const result = await menuService.deleteRoleMenu(this.currentRole.id)
          .catch((err: any) => {
            this.logger.error(`[RoleAuthority] -> MenuService.deleteRoleMenu(${this.currentRole.id}): ${JSON.stringify(err)}`);
          });
        if (result) {
          this.$message.success(this.$t('SigtCommon.OperateSuccess').toString());
        } else {
          this.$message.error(this.$t('SigtCommon.OperateError').toString());
        }
      } else {
        const result = await menuService.configtRoleMenu(this.roleMenuList)
          .catch((err: any) => {
            this.logger.error(`[RoleAuthority] -> MenuService.configtRoleMenu(${this.roleMenuList}): ${JSON.stringify(err)}`);
          });
        if (result) {
          this.$message.success(this.$t('SigtCommon.OperateSuccess').toString());
        } else {
          this.$message.error(this.$t('SigtCommon.OperateError').toString());
        }
      }
      this.saveMenuLoading = false;
    }
  }
  /**
   * 隐藏新增角色窗体
   */
  hideRoleDialog() {
    this.roleDialogVisible = false;
  }
  /**
   * 记录菜单结构的首层节点
   */
  foldAllList() {
    return this.menuData.map((m) => m.__identity);
  }
  /**
   * 切换展开 还是折叠
   * @param row 行数据
   */
  toggleFoldingStatus(row: any) {
    if (this.foldList.includes(row.__identity)) {
      this.foldList.splice(this.foldList.indexOf(row.__identity), 1);
    } else {
      this.foldList.push(row.__identity);
    }
  }
  /**
   * 该方法会对每一行数据都做判断 如果foldList 列表中的元素 也存在与当前行的 id列表中  则该行不展示
   * @param param0 行数据
   */
  toggleDisplayTr({ row, index }: any) {
    for (const item of this.foldList) {
      // 如果foldList中元素存在于 row.__family中，则该行隐藏。  如果该行的自身标识等于隐藏元素，则代表该元素就是折叠点
      if (row.__family.includes(item) && row.__identity !== item) {
        return 'hidden-row';
      }
    }
    return '';
  }
  /**
   * 该方法会对每一行数据都做判断 如果foldList 列表中的元素 也存在与当前行的 id列表中  则该行不展示
   * @param row 行数据
   */
  toggleFoldingClass(row: any) {
    if (row.children.length === 0) {
      return 'permission_placeholder';
    } else {
      if (this.foldList.indexOf(row.__identity) === -1) {
        return 'el-icon-remove-outline';
      } else {
        return 'el-icon-circle-plus-outline';
      }
    }
  }
  /**
   * 菜单勾选
   * @param row 勾选行
   */
  checkMenu(row: any) {
    // 当前节点的勾选和取消
    for (const item of this.menuData) {
      // 当前节点勾选
      if (row.id === item.id) {
        this.$set(item, 'isCheck', row.isCheck);
      }
    }
    // 递归勾选取消子菜单
    this.checkMenuSon(row.children, row);
    if (row.isCheck) {
      // 子菜单勾选 父节点必勾选
      this.checkMenuParent(row);
    } else {
      // 如果子节点不选 寻找所有子节点全部不选则父节点反选
      this.cancelMenuParent(row);
    }
  }
  /**
   * 全选所有菜单
   */
  selectAllMenu() {
    // 全选
    for (const item of this.menuData) {
      this.$set(item, 'isCheck', true);
    }
  }
  /**
   * 反选所有菜单
   */
  unSelectAllMenu() {
    // 反选
    for (const item of this.menuData) {
      this.$set(item, 'isCheck', false);
    }
  }
  /**
   * 全部展开
   */
  unFoldMenu() {
    this.foldList = [];
  }
  /**
   * 全部折叠
   */
  flodMenu() {
    this.foldList = this.foldAllList();
  }
  /**
   * 数据扁平化
   * @param parent 父节点
   * @param children 子节点
   * @param family 家族关系
   * @param elderIdentity 标识
   */
  formatConversion(parent: any, children: any, family: any = [], elderIdentity = 'x') {
    // children如果长度等于0，则代表已经到了最低层
    // let page = (this.startPage - 1) * 10
    if (children.length > 0) {
      children.map((x: any, i: any) => {
        // 设置 __family 为家族关系 为所有父级，包含本身在内
        Vue.set(x, '__family', [...family, elderIdentity + '_' + i]);
        // 本身的唯一标识  可以理解为个人的身份证咯 一定唯一。
        Vue.set(x, '__identity', elderIdentity + '_' + i);
        parent.push(x);
        // 如果仍有子集，则进行递归
        if (x.children.length > 0) {
          this.formatConversion(parent, x.children, [...family, elderIdentity + '_' + i], elderIdentity + '_' + i);
        }
      });
    }
    return parent;
  }
  /**
   * 递归勾选取消子菜单
   * @param children 子菜单
   * @param selectMenu 菜单选择
   */
  checkMenuSon(children: any, selectMenu: any) {
    for (const item of this.menuData) {
      if (children.length > 0) {
        for (const childrenItem of children) {
          if (childrenItem.id === item.id) {
            this.$set(item, 'isCheck', selectMenu.isCheck);
            if (childrenItem.children.length > 0) {
              this.checkMenuSon(childrenItem.children, selectMenu);
            }
          }
        }
      }
    }
  }
  /**
   * 子菜单勾选 父节点必勾选
   * @param selectMenu 菜单选择控制
   */
  checkMenuParent(selectMenu: any) {
    for (const item of this.menuData) {
      if (selectMenu.parentId && selectMenu.parentId === item.id) {
        this.$set(item, 'isCheck', true);
        this.checkMenuParent(item);
      }
    }
  }
  /**
   * 如果子节点不选 寻找所有子节点全部不选则父节点反选
   * @param selectMenu 选择的菜单
   */
  cancelMenuParent(selectMenu: any) {
    const childrenCount = this.menuData.filter((m) => m.parentId === selectMenu.parentId && m.isCheck === true).length;
    if (childrenCount === 0) {
      for (const item of this.menuData) {
        if (item.id === selectMenu.parentId) {
          this.$set(item, 'isCheck', false);
          this.cancelMenuParent(item);
        }
      }
    }
  }
}
