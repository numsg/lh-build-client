import { Vue, Component } from 'vue-property-decorator';
import LaboratoryLogService from '@/api/laborartory-log-service';

// 日志管理
@Component({
    components: {
    }
})
export default class LogManager extends Vue {
    pageSize = 10; // 后续优化成读取某个配置获取每页大小
    total = 0;
    pageCount = 0;
    currentPage = 1;
    keyword = '';
    laboratoryLogList: any = [];

    mounted() {
        this.getData();
    }
    async getData() {
        const resultInfo = await LaboratoryLogService.GetLogByPage(this.currentPage - 1, this.pageSize, this.keyword);
        this.laboratoryLogList = resultInfo.dataList;
        this.total = resultInfo.elementCount;
        this.pageCount = resultInfo.pageCount;
    }
    async reset() {
        this.keyword = '';
        await this.getData();
    }

    async searchLab() {
        await this.getData();
    }

    getIndex($index: any) {
      return (this.currentPage - 1) * this.pageSize + $index + 1;
    }

    handleSizeChange(value: number) {
        this.pageSize = value;
        this.getData();
    }
    handleCurrentChange(vlue: number) {
        this.currentPage = vlue;
        this.getData();
    }
}
