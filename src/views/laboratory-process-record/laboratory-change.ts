import { Vue, Component } from 'vue-property-decorator';
import LaboratoryRecordComponent from '../../components/laboratory-record/laboratory-record';

// 实验变更记录管理
@Component({
    components: {
        LaboratoryRecordComponent
    }
})
export default class LaboratoryChange extends Vue {
    showList: any = true;
    titlename: any = '实验变更记录管理';

    mounted() {
    }

    onShowList() {
        this.showList = !this.showList;
    }
}
