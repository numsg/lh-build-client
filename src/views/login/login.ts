import { Vue, Component } from 'vue-property-decorator';
import { Getter, Action, Mutation } from 'vuex-class';
import { LoginTypes } from '../../store/types/login-type';
import { AppTypes } from '../../store/types/app-types';
import { LoginInfo } from '../../common/models/loginInfo';
import { RoleInfo } from '../../common/models/role/role-info';
import { ErrorCode } from '../../common/constant/error-code';
import { Md5 } from 'ts-md5';
import lodash from 'lodash';
import SettingService from '../../api/system-manage/setting-service';
import userService from '@/api/system-manage/user-service';
import { Path } from '@/router/router-type';

@Component
export default class Login extends Vue {

  // 席位服务registerInfo对象
  @Getter('registerInfo') registerInfo!: any;

  // 修改席位服务registerInfo对象
  @Action('setRegisterInfo') setRegisterInfo!: (valid: any) => void;

  // 当前语言环境
  @Getter(AppTypes.getters.LANGUAGE)
  currentLanguage: any;

  // 是否登录服务检查通过
  @Getter(LoginTypes.getters.ISLOGIN)
  isLogin: any;

  // 角色菜单
  @Getter(LoginTypes.getters.ROLE_MENUS)
  menus: any;

  // 修改语言环境
  @Action(AppTypes.actions.CHANGE_LANGUAGE)
  changeLanguageAction!: (language: string) => Promise<void>;

  // 设置登录用户信息
  @Mutation(LoginTypes.mutations.SET_USERINFO)
  setUserInfo!: (userInfo: any) => void;

  // 设置系统基础信息
  @Mutation(AppTypes.mutations.SET_SYSTEM_INFO)
  setSystemInfo!: (systemInfo: any) => void;

  // 登录成功或者失败设置操作
  @Action(LoginTypes.actions.CHANGE_LOGIN)
  changeLoginAction!: (loginInfo: any) => Promise<void>;

  // 登录用户信息
  userInfo = {
    username: '',
    password: '',
  };
  userId = '';
  loading = false;
  // ip
  strIp: any = '';
  // 当前语言环境信息
  language = 'zh-CN';
  // 当前系统信息
  systemInfo: any = '';

  showRoleInfo = false;
  // 当前选择的角色
  checkedRole: RoleInfo = new RoleInfo();
  loadingData = false;

  loginInfo: LoginInfo = new LoginInfo();

  //#region 登录参数验证方法
  get loginRules() {
    return {
      username: [{ required: true, trigger: ['blur', 'change'], validator: this.validateUsername }],
      password: [{ required: true, trigger: ['blur', 'change'], validator: this.validatePassword }]
    };
  }

  validateUsername(rule: any, value: any, callback: any) {
    if (value && value.trim().length > 0) {
      callback();
    } else {
      callback(new Error(this.$t('SigtLogin.usernameIsError').toString()));
    }
  }
  validatePassword(rule: any, value: any, callback: any) {
    if (value && value.trim().length > 0) {
      callback();
    } else {
      callback(new Error(this.$t('SigtLogin.PwdEmpty').toString()));
    }
  }
  //#endregion

  async created() {
    this.language = this.currentLanguage;
    // 缓存当前系统登录信息
    // await SettingService.getSettingInfoByName('SystemTitle').then((result: any) => {
    //   if (result) {
    //     this.systemInfo = result.Value;
    //     this.setSystemInfo(this.systemInfo);
    //   }
    // });

  }

  public handleLogin() {
    const lForm: any = this.$refs.loginForm;
    lForm.validate((valid: any) => {
      if (valid) {
        this.loading = true;
        sessionStorage.setItem('login_name', this.userInfo.username);
        sessionStorage.setItem('login_pwd', Md5.hashStr(this.userInfo.password).toString());
        this.login();
      } else {
        return false;
      }
    });
  }

  // 登录失败清除session
  clearSession() {
    sessionStorage.removeItem('login_name');
    sessionStorage.removeItem('login_pwd');
    sessionStorage.removeItem('login_ip');
    sessionStorage.removeItem('login_id');
  }
  // 登录校验
  async login() {
    // 深拷贝密码加密 防止登录输入框长度扩展
    // const userInfo = lodash.cloneDeep(this.userInfo);
    // userInfo.password = Md5.hashStr(userInfo.password).toString();
    // const result = await userService.loginvalidate(userInfo);
    // if (result.success) {
      // this.loading = false;
      // this.strIp = result.message.strIp;
    sessionStorage.setItem('login_ip', this.strIp);
      // sessionStorage.setItem('login_id', result.message.userId);
    this.$router.push({ path: 'LaboratoryRecord' });

    // } else {
    //   this.$message.error('登陆失败');
    //   this.loading = false;
    // }


  }

  // 语言切换
  changeLanguage(val: any) {
    this.$i18n.locale = val;
    this.changeLanguageAction(val);
    const lForm: any = this.$refs.loginForm;
    lForm.clearValidate();
  }
}
