import { Getter, Action, Mutation } from 'vuex-class';
import { Component, Mixins } from 'vue-property-decorator';
import { rxevent } from '@gsafety/rx-eventbus/dist/eventaggregator.service';
import { WsClient } from '@gsafety/presence-client/dist/ws-client';
import Notice, { NoticeMixin } from '@gsafety/cad-notice-module';
import { Message } from '@gsafety/cad-notice-module/dist/models/message';
import { utilHelper } from '@gsafety/cad-gutil';
import { MessageType } from '@gsafety/cad-notice-module/dist/models/message-type';
import { InjectLog, Logger } from '@gsafety/cad-glog';

import wsSeatService from '../../api/ws-seat-service';
import { MessageCallback } from '../../common/services/ws-notice/message-callback';
import { MessageHub } from '../../common/services/notice-callback/message-hub';
import { MessageEventKeys, ChatSessionKeys } from '../../common/constant/event-keys';
import LangSelect from '../../components/langselect/index.vue';
import { SeatStatus } from '../../common/models/seat-status';
import { Path } from '../../router/router-type';
import LoginInfoHeader from '../../components/header/login-info-header';
import { ClientInfo } from '../../common/models/client-info';
import { convertDurationMs } from '../../common/utils/screen-utils';
import { getEleUIFormat } from '../../common/utils/date-utils';
import { IpcRenderer } from '../../common/utils/ipc/ipc-renderer';
import { EventType } from '../../common/utils/ipc/ipc-event-type';
import AnnouncementInfo from '../../common/models/system-manage/announcement-info';
import announcementService from '../../api/system-manage/announcement-service';
import AnnouncementPerson from '../../common/models/system-manage/announcement-person';
import { LoginTypes } from '../../store/types/login-type';
import { AppTypes } from '../../store/types/app-types';
import { RegisterType } from '../../store/types/register-type';
import SettingService from '../../api/system-manage/setting-service';

@Component({
  components: {
    LoginInfoHeader
  }
})
export default class Header extends Mixins(NoticeMixin) {
  @InjectLog() logger!: Logger;

  @Getter('configs')
  configs: any;

  // 席位服务registerInfo对象
  @Getter(RegisterType.getters.registerInfo)
  registerInfo!: SeatStatus;

  // 获取系统基础信息
  @Getter(AppTypes.getters.SYSTEM_INFO)
  getSystemInfo: any;

  private wsClient: WsClient;
  private messageHub = new MessageHub();

  @Getter(LoginTypes.getters.LOGIN_INFO)
  loginInfo: any;

  @Mutation(LoginTypes.mutations.SET_ISLOGIN)
  setLogin!: (args: any) => void;

  // 设置系统基础信息
  @Mutation(AppTypes.mutations.SET_SYSTEM_INFO)
  setSystemInfo!: (systemInfo: any) => void;

  @Getter('sidebar')
  sidebar: any;

  @Action('LogOut')
  logOutAction: any;

  // 聊天会话-状态控制
  chatMsgNum = 0;
  chatMsgHidden = true;

  // 用于长时间未操作退出系统的时间
  oldTime: number = Date.now();
  timer!: NodeJS.Timeout;
  converstationMsgs: any[] = [];

  openFileNotifier: any = null;
  printNotifier: any = null;
  fileFullPath = '';

  // 当前系统信息
  systemInfo: any = '';

  /**
   * 公告弹窗
   */
  announcementDialogVisible = false;
  /**
   * 公告信息
   */
  announcementInfo = new AnnouncementInfo();
  /**
   * 时间格式
   */
  dateTimeFormat = getEleUIFormat().DATETIME;

  /**
   * 公告查询参数
   */
  annQueryModel = {
    userId: '',
    sendState: 0
  };

  /**
   * 暂存的离线消息列表
   */
  messageList: Message[] = [];
  constructor() {
    super();
    this.wsClient = new WsClient();
    MessageCallback.Ins.regist('MessageHub', this.messageHub);
  }

  created() {

  }
  async mounted() {
    // 缓存当前系统登录信息
    // await SettingService.getSettingInfoByName('SystemTitle').then((result: any) => {
    //   if (result) {
    //     this.systemInfo = result.Value;
    //     this.setSystemInfo(this.systemInfo);
    //   }
    // });

    // 注册socket数据
    // 组装socket数据  服务地址和心跳时间
    const option = {
      wsUrl: this.configs.websocketServicePort,
      heartInterval: this.configs && this.configs.heartInterval ? this.configs.heartInterval : 10000
    };
    // FIXME:  基clientInfo数据需要根据实际情况调整
    const clientInfo = {
      name: this.loginInfo.username,
      registerInfo: this.registerInfo
    } as ClientInfo;
    // socket 连接
    return;
    this.wsClient.connectServer(
      option,
      clientInfo,
      (data: any) => {
        MessageCallback.Ins.notice(data.message);
      },
      () => {
        wsSeatService.sendLoginNotice(this.registerInfo);
      },
      () => {
        wsSeatService.logOffSeatStatus(this.registerInfo);
      }
    );
  }

  home() {
    this.$router.push(Path.Home);
  }

  // 退出系统弹出框
  showLogOutTips() {
    this.$confirm(this.$t('SigtLogin.ConfirmLogOut').toString(), this.$t('SigtCommon.Tips').toString(), {
      confirmButtonText: this.$t('SigtCommon.Confirm').toString(),
      cancelButtonText: this.$t('SigtCommon.Cancel').toString(),
      closeOnClickModal: false,
      type: 'warning'
    })
      .then(() => {
        this.logout();

      })
      .catch(() => { });
  }

  logout() {
    // socket关闭
    this.wsClient.close();

    rxevent.unsubscribe(MessageEventKeys.NoticeMessage, 'NoticeMessage');
    // 消息清空
    this.$notice.clear();
    if (this.openFileNotifier) {
      this.openFileNotifier.close();
      this.openFileNotifier = null;
    }

    //  退出系统清除缓存
    sessionStorage.removeItem('login_name');
    sessionStorage.removeItem('login_pwd');
    sessionStorage.removeItem('login_ip');
    sessionStorage.removeItem('login_id');
    this.setLogin(false);
    // 最后才退出到登陆页面
    this.$router.push({ path: '/login' });
  }

  /**
   * 被迫下线
   * @memberof Header
   */
  forcedToLogoff(clientInfo: ClientInfo) {
    const message = clientInfo.hostname
      ? this.$tc('SigtCommon.AccountLoginInOtherCumputer', 0, [clientInfo.hostname])
      : this.$t('SigtCommon.AccountLoginInOtherSeat');
    this.$alert(message.toString(), this.$t('SigtCommon.Tips').toString(), {
      confirmButtonText: this.$i18n.t('SigtCommon.Confirm').toString(),
      type: 'warning'
    });
    this.logout();
  }

  /**
   * 设置锚点
   */
  incidentSelect(param: any) { }

  async viewDetail(message: Message) {
    // 订阅新消息
    rxevent.publish(MessageEventKeys.RemoveMessage, message.id);
    if (message.type === MessageType.ANNOUNCEMENT) {
      this.showAnnouncementInfo(message);
    }
    if (!message.payload) {
      return;
    }
    if (message.payload['navigation'] != null) {
      // tslint:disable-next-line:no-eval
      if (message.payload['navigation'].param === 'Order') {
        console.log(message.payload.navigation);
        const orderId = message.payload.id;


        if (message.payload['navigation'].param === 'Mail_Receive') {
          console.log(message.payload.navigation);
          this.$router.push({ path: '/mailbox-manager/mailbox-in', query: { mailId: message.payload.id } });
        }
        if (message.payload['navigation'].param === 'Mail_Apportion') {
          console.log(message.payload.navigation);
          this.$router.push({ path: '/mailbox-manager/mail-waiting', query: { mailId: message.payload.id } });
        }
      }
    }
  }


  /**
   * 计算距离上次操作的时间
   * @memberof Header
   */
  timeOutTimer() {
    // 超过30分钟未操作系统，退出登陆
    const timeOut = this.configs && this.configs.systemTimeout ? convertDurationMs(this.configs.systemTimeout) : 30 * 60 * 1000;
    this.timer = setInterval(() => {
      const newTime = Date.now();
      if (newTime - this.oldTime > timeOut) {
        clearInterval(this.timer);
        this.logout();
        this.$alert(this.$t('SigtCommon.LogoffWithLongTimeNoOperate').toString(), this.$t('SigtCommon.Tips').toString(), {
          confirmButtonText: this.$i18n.t('SigtCommon.Confirm').toString(),
          type: 'warning'
        });
      }
    }, 1000);
  }

  /**
   * 监听鼠标、键盘事件，长时间未操作退出登陆
   * @memberof Header
   */
  registerEvent() {
    const html = document.querySelector('html') as HTMLElement;
    html.addEventListener('click', this.opertorHandler);

    html.addEventListener('keydown', this.opertorHandler);

    html.addEventListener('mousemove', this.opertorHandler);

    html.addEventListener('mousewheel', this.opertorHandler);
  }

  opertorHandler() {
    this.oldTime = Date.now();
  }

  beforeDestroy() {
    this.wsClient.close();
    IpcRenderer.remove(EventType.OTHERS.DOWNLOAD_FILE_COMPLETED);
    IpcRenderer.remove(EventType.OTHERS.PRINTER_INFO);
    rxevent.unsubscribe(MessageEventKeys.ForcedToLogoff, 'Header');
    rxevent.unsubscribe(MessageEventKeys.NoticeMessage, 'NoticeMessage');
    rxevent.unsubscribe(ChatSessionKeys.ChatSessionPageNum, 'ChatSessionPageNum');
    rxevent.unsubscribe(ChatSessionKeys.ChatNotationMessage, 'ChatNotationMessage');
    clearInterval(this.timer);
    const html = document.querySelector('html') as HTMLElement;
    html.removeEventListener('click', this.opertorHandler);
    html.removeEventListener('keydown', this.opertorHandler);
    html.removeEventListener('mousemove', this.opertorHandler);
    html.removeEventListener('mousewheel', this.opertorHandler);
  }
  /**
   * 点击消息详情-展示公告详情
   * @param message 消息体
   */
  async showAnnouncementInfo(message: Message) {
    await announcementService.getAnnouncementsByTitle(message.content).then((resultAnn: any) => {
      if (resultAnn.value && resultAnn.value.length > 0) {
        if (resultAnn.value[0].status === 3) {
          this.$message.warning(this.$t('SigtAnnouncement.AnnouncementNotExit').toString());
          return;
        }
        this.announcementInfo = resultAnn.value[0];
        this.announcementDialogVisible = true;
      } else {
        this.$message.warning(this.$t('SigtAnnouncement.AnnouncementNotExit').toString());
        this.announcementDialogVisible = false;
        return;
      }
    }).catch((err: any) => {
      this.logger.error(`[showAnnouncementInfo] -> AnnouncementService.getAnnouncementsByTitle(${message.title}): ${JSON.stringify(err)}`);
    });
  }
}
