import { NavigationGuard, Route, RawLocation } from 'vue-router';
import store from '@/store';
import { Store } from 'vuex';
import { AppState } from '../store/states/app-state';
import { LoginTypes } from '@/store/types/login-type';

// 回调钩子  界面刷新后登录的初始化数据在钩子中处理  登录也会走这里  注意在router-index下添加beforeEnter

function initLoginInfo() {
  store.commit(LoginTypes.mutations.SET_USERINFO, {
    username: sessionStorage.getItem('login_name'),
    password: sessionStorage.getItem('login_pwd')
  });
  return store.dispatch(LoginTypes.actions.CHANGE_LOGIN);
}

export function AuthorizeGuard(store1: Store<{ app: AppState }>, loginUrl: string = '/login'): NavigationGuard {
  return async (to: Route, from: Route, next: (to?: RawLocation | false | ((vm: any) => any) | void) => void) => {
    const islogin = store.state.login.isLogin;
    console.log(to.path);
    // if(to.path === '/laboratory-record-manager/laboratory-record'){
    //   next();
    //   return;
    // }
    if (!islogin) {
      if (!sessionStorage.getItem('login_name') || !sessionStorage.getItem('login_pwd')) {
        next(loginUrl);
      } else {
        // 必须异步
        await initLoginInfo();
        next();
      }
    } else {
      next();
    }
  };
}
