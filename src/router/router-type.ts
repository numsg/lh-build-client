export enum Path {
  // 登录
  Login = '/login',
  // 404
  Error = '/404',
  // 初始化
  Default = '/',
  // 首页
  Home = '/home',
  // 系统管理
  menuOrganization = '/menuOrganization',

  // 实验过程记录管理
  LaboratoryRecordManager = '/laboratory-record-manager',
  LaboratoryRecord = 'laboratory-record',
  AddRecord = 'add-record',
  // DataRecord = 'data-record',
  // 实验变更记录管理
  LaboratoryProcessRecord = '/laboratory-process-record',
  LaboratoryChange = 'laboratory-change',
  // 实验记录审核管理
  LaboratoryApprovalManager = '/laboratory-approval-manager',
  LaboratoryApproval = 'laboratory-approval',
  LaboratoryVerify = 'laboratory-verify',

  // 系统管理
  DeviceManager = 'device-manager',
  LogManager = 'log-manager',
  TemplateManager = 'template-manager',
  laboratoryManager = 'laboratory-manager'
}

export enum RouterName {
  // 登录
  Login = 'Login',
  // 404
  Error = '404',
  // 初始默认
  Default = 'Default',

  // 首页
  Home = 'Home',

  // 系统管理
  SystemManage = 'menuSystemManage',
  // 组织机构
  Organization = 'menuOrganization',
  // 角色权限
  RoleAuth = 'menuRoleAuth',

  // 系统设置
  SystemSetting = 'systemSetting',
  // 实验过程记录管理
  LaboratoryRecord = 'LaboratoryRecord',
  // 实验变更记录管理
  LaboratoryChange = 'LaboratoryChange',
  // 实验记录审核管理
  LaboratoryApproval = 'LaboratoryApproval',
  LaboratoryVerify = 'LaboratoryVerify',

  // 系统管理
  DeviceManager = 'DeviceManager',
  LogManager = 'LogManager',
  TemplateManager = 'TemplateManager',
  laboratoryManager = 'laboratoryManager'
}

export const RouterPrefix = (routerName: string) => {
  return `RouterName.${routerName}`;
};
