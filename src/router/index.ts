import Vue from 'vue';
import Router from 'vue-router';

/* layout */
import Layout from '@/views/layout/layout.vue';

/* login */
import Login from '@/views/login/login.vue';

/* home */
import Home from '@/views/home/home.vue';

import store from '../store';

import { AuthorizeGuard } from './login-guard';

import { Path, RouterName, RouterPrefix } from './router-type';


Vue.use(Router);

export const constantRouterMap = [
  { path: Path.Login, name: RouterPrefix(RouterName.Login), component: Login, hidden: true },
  // 首页
  {
    path: Path.Default,
    component: Layout,
    name: RouterPrefix(RouterName.Default),
    // beforeEnter: AuthorizeGuard(store),
    hidden: true,
    children: [
      // 系统展示初始
      { path: Path.Home, component: Home }
    ]
  },
  // 实验过程记录管理
  {
    path: Path.LaboratoryRecordManager,
    component: Layout,
    redirect: 'noredirect',
    name: RouterPrefix(RouterName.LaboratoryRecord),
    // beforeEnter: AuthorizeGuard(store),
    icon: 'el-icon-user',
    hidden: false,
    children: [
      // 验记录列表
      {
        path: Path.LaboratoryRecord,
        component: () => import('@/views/laboratory-record-manager/laboratory-record.vue'),
        name: '实验采集数据列表'
      },
      {
        path: Path.AddRecord,
        component: () => import('@/views/laboratory-record-manager/add-record.vue'),
        name: '新增实验记录'
      },
      // {
      //   path: Path.DataRecord,
      //   component: () => import('@/views/laboratory-record-manager/data-record/data-record.vue'),
      //   name: '查看数据记录'
      // },
    ]
  },
  // 实验记录变更管理
  {
    path: Path.LaboratoryProcessRecord,
    component: Layout,
    redirect: 'noredirect',
    name: RouterPrefix(RouterName.LaboratoryChange),
    // beforeEnter: AuthorizeGuard(store),
    icon: 'el-icon-user',
    hidden: false,
    children: [
      // 验记录列表
      {
        path: Path.LaboratoryChange,
        component: () => import('@/views/laboratory-process-record/laboratory-change.vue'),
        name: '实验记录变更'
      }
    ]
  },
  // 实验记录审核管理
  {
    path: Path.LaboratoryApprovalManager,
    component: Layout,
    redirect: 'noredirect',
    name: RouterPrefix(RouterName.LaboratoryVerify),
    // beforeEnter: AuthorizeGuard(store),
    icon: 'el-icon-user',
    hidden: false,
    children: [
      // 审批
      {
        path: Path.LaboratoryVerify,
        component: () => import('@/views/laboratory-approval-manager/laboratory-verify/laboratory-verify.vue'),
        name: '实验记录审核'
      },
      // 批准
      {
        path: Path.LaboratoryApproval,
        component: () => import('@/views/laboratory-approval-manager/laboratory-approval/laboratory-approval.vue'),
        name: '实验记录批准'
      },
    ]
  },
  // 系统管理
  {
    path: Path.menuOrganization,
    component: Layout,
    name: RouterPrefix(RouterName.SystemManage),
    // beforeEnter: AuthorizeGuard(store),
    icon: 'el-icon-menu',
    children: [
      // 实验室管理
      {
        path: Path.laboratoryManager,
        component: () => import('@/views/system-manage/laboratory-manager/laboratory-manager.vue'),
        name: RouterPrefix(RouterName.laboratoryManager),
        icon: 'el-icon-date'
      },
      // 设备管理
      {
        path: Path.DeviceManager,
        component: () => import('@/views/system-manage/device-manager/device-manager.vue'),
        name: RouterPrefix(RouterName.DeviceManager),
        icon: 'el-icon-date'
      },
      // 模板管理
      {
        path: Path.TemplateManager,
        component: () => import('@/views/system-manage/template-manager/template-manager.vue'),
        name: RouterPrefix(RouterName.TemplateManager),
        icon: 'el-icon-date'
      },
      // 角色权限管理
      {
        path: 'roleAuth',
        component: () => import('@/views/system-manage/role-authority/role-authority.vue'),
        name: RouterPrefix(RouterName.RoleAuth),
        icon: 'el-icon-date'
      },
      // 组织机构管理
      {
        path: 'organization',
        component: () => import('@/views/system-manage/organization-manage/organization-manage'),
        name: RouterPrefix(RouterName.Organization),
        icon: 'el-icon-date'
      },
      // 系统设置
      {
        path: 'systemSetting',
        component: () => import('@/views/system-manage/system-setting/system-setting.vue'),
        name: RouterPrefix(RouterName.SystemSetting),
        icon: 'el-icon-date'
      },
      // 日志管理
      {
        path: Path.LogManager,
        component: () => import('@/views/system-manage/log-manager/log-manager.vue'),
        name: RouterPrefix(RouterName.LogManager),
        icon: 'el-icon-date'
      }
    ]
  },
  { path: '*', redirect: '/', hidden: true }
];

export default new Router({
  routes: constantRouterMap
});
