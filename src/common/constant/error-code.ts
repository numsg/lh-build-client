/**
 * 服务返回的错误码
 */
export const ErrorCode = {

  // 通用
  ParamIsEmpty: '1001', // 对象为空
  ServerInnerException: '1002', // 服务内部错误
  NameExisted: '1003', // 名称已存在
  DataNotFound: '1004', // 数据未找到
  CodeExisted: '1005', // 编码已存在
  PhoneExisted: '1006', // 席位号码已存在
  NetworkError: 'NetworkError', // 网络异常

  // 用户相关
  UsernameNotExist: '1101',  // 用户名不存在
  UserRolesNotExist: '1102', // 用户角色不存在
  OriginalPasswordErr: '1103', // 原始密码错误
  UsernameExisted: '1104', // 用户名已存在
  UserCodeExisted: '1105', // 用户编码已存在
  TitleExisted: '1201', // 标题已存在

  TimeExisted: '1201', // 时间段已存在
};
