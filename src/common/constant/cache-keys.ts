/**
 * 缓存key
 */
export const CacheKeys = {
  // 当前机器名
  computerName: 'computerName',
  // 当前登录人员分机号
  splogin: 'splogin',
};
