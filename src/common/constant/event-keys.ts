// 内部业务模块事件发布和订阅
export const EventKeys = {
  /**
   * 结束下载
   */
  EndUploading: 'EndUploading',
  /**
   * 客户信息刷新
   */
  RefreshCustomerData: 'RefreshCustomerData',

  /** 刷新360查询中的客户历史记录 */
  RefreshCustomerHistory: 'RefreshCustomerHistory',

  /** 关闭360查询弹框 */
  Close360SearchDialog: 'Close360SearchDialog'
};


// 消息事件发布和订阅
export const MessageEventKeys = {
  /**
   * 接收消息通知
   */
  NoticeMessage: 'NoticeMessage',
  /**
   * 强制登出
   */
  ForcedToLogoff: 'ForcedToLogoff',
  /**
   * 移除消息
   */
  RemoveMessage: 'RemoveMessage',
  /**
   * 新消息滚动
   */
  NewMessage: 'NewMessage',
};

export const SystemEventKeys = {
  /**
   * 设置类型被选择
   */
  SettingTypeSelected: 'SettingTypeSelected',
  /**
   * 设置参数首页加载
   */
  SettingHomeLoading: 'SettingHomeLoading',
  /**
   * 添加/编辑用户弹框参数
   */
  UserDialogParams: 'dialogAddVisibleEvent',
  /**
   * 用户弹窗事件
   */
  UserDialog: 'UserDialogEvent'
};

// 电话业务事件发布和订阅
export const SoftphoneEventKeys = {
  // 电话登出事件
  Logout: 'Logout',
  // 重置
  Reset: 'Reset',
  // 电话接听事件
  PickUp: 'PickUp',
  // 电话挂机事件
  HangUp: 'HangUp',
  // 电话转移事件
  Tansfer: 'Tansfer',
  // 电话三方通话
  ThreePart: 'ThreePart',
  // 电话结束三方通话
  ThreePartOver: 'ThreePartOver',
  // 拨出电话
  CallOut: 'CallOut',
  // 转接队列
  TransferToQueue: 'TransferToQueue',
  // 转移到IVR
  TransferToIVR: 'TransferToIVR',
  // 更新通话记录id
  UpdateRecordId: 'UpdateRecordId',
  // 工单弹框设置电话状态 打开 true置忙, 关闭false 置闲
  SetPhoneStatusByOrderDialog: 'SetPhoneStatusByOrderDialog',
  // 外线来电
  OnIncommingRing: 'OnIncommingRing',
  // 外呼振铃/内线来电
  OnOutboundRinging: 'OnOutboundRinging',
  // 外呼后外线接听
  OnOutbound: 'OnOutbound',
  // 工单处理完毕后,360弹框消失
  CloseOverSearchDialog: 'CloseOverSearchDialog'
};

// 即时通讯发布和订阅
export const ChatSessionKeys = {
  // 消息--会话收到新消息通知
  ChatSessionMessage: 'ChatSessionMessage',
  // 消息--向菜单发送通知消息
  ChatNotationMessage: 'ChatNotationMessage',
  // 消息--聊天会话页面数量变化通知
  ChatSessionPageNum: 'ChatSessionPageNum',
  MonitorChatMsg: 'MonitorChatMsg',
};

/**
 * 赛普智成事件
 */
export const SpzcEventKeys = {
  /**
   * ws连接
   */
  OnConnect_cb: 'OnConnect_cb',
  /**
   * ws断开连接
   */
  OnDisconnect_cb: 'OnDisconnect_cb',
  /**
   * 登录成功
   */
  OnLoginSuccess_cb: 'OnLoginSuccess_cb',
  /**
   * 登录失败：失败结果
   */
  OnLoginFail_cb: 'OnLoginFail_cb',
  /**
   * 出错
   */
  OnErrorNotice_cb: 'OnErrorNotice_cb',
  /**
   * 退出登录成功
   */
  OnLogOutSuccess_cb: 'OnLogOutSuccess_cb',
  /**
   * 获取分机列表：分机列表
   */
  ExtensionList_cb: 'extensionList_cb',
  /**
   * 来电振铃事件：来电号码、通话id、是否自动外呼、归属地省、归属地市、运营商、附加数据
   */
  OnIncommingRing_cb: 'OnIncommingRing_cb',
  /**
   * 来电应答事件：来电号码、通话id、是否自动外呼、归属地省、归属地市、运营商、附加数据
   */
  OnIncommingAnswer_cb: 'OnIncommingAnswer_cb',
  /**
   * 外呼通话事件：来电号码、通话id、归属地省、归属地市、运营商
   */
  OnOutbound_cb: 'OnOutbound_cb',
  /**
   * 挂机
   */
  OnHangup_cb: 'OnHangup_cb',
  /**
   * 忙碌
   */
  OnBusy_cb: 'OnBusy_cb',
  /**
   * 空闲
   */
  OnFree_cb: 'OnFree_cb',
  /**
   * 外拨振铃中： 对方号码、通话id
   */
  OnOutboundRinging_cb: 'OnOutboundRinging_cb',
  /**
   * 状态改变事件：话机状态、通话状态、座席状态、置忙状态、对方号码
   */
  OnStatusChanged_cb: 'OnStatusChanged_cb',
  /**
   * 通话状态：通话状态、对方号码
   */
  OnCallStatus_cb: 'OnCallStatus_cb',
  /**
   * 队列改变：空闲席位数、所有席位数、排队数、队列名
   */
  OnQueueStatusChanged_cb: 'OnQueueStatusChanged_cb'
};
