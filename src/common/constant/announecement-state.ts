/**
 * 公告状态
 */
export const AnnouncementState = [
  {
      id: 1,
      name: 'SigtAnnouncement.New'
  },
  {
      id: 2,
      name: 'SigtAnnouncement.Published'
  },
  {
      id: 3,
      name: 'SigtAnnouncement.Revoked'
  }
];
