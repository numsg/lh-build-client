export const QulityKey = {
  // 打开质检打分模态框(全屏)
  OpenBigQualityScore: 'OpenBigQualityScore',
  // 打开质检打分模态框(小屏)
  OpenLittleQualityScore: 'OpenLittleQualityScore',
  // 关闭大弹框(全屏)
  CloseBigQualityScore: 'CloseBigQualityScore',
  // 关闭小弹框(小屏)
  CloseLittleQualityScore: 'CloseLittleQualityScore'
};
