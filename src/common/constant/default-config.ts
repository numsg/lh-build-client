/**
 * 系统默认参数
 */
export const DefaultConfig = {
  /**
   * 内置用户
   */
  DEFAULT_USER: 'sigt_admin',

  /**
   * 用户重置初始密码
   */
  ORIGANAL_PWD: 'e10adc3949ba59abbe56e057f20f883e',

  /**
   * 组织机构数维度限制
   */
  DIMENSION_LIMIT: 5,

  /**
   * 默认原始密码
   */
  DEFAULT_USERPWD_KEY: 'origanalPwd',

  /**
   * 电话规则名称
   */
  PHONE_REGULAR_NAME: 'phoneRegular',

  /**
   * 电话长度
   */
  PHONE_LENGTH_NAME: 'phoneLength'
};
