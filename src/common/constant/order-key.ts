export const OrderEventKeys = {
  // 刷新任务盒
  RefreshOrderBox: 'RefreshOrderBox',
  // 打开新建工单弹框
  OpenNewOrder: 'OpenNewOrder'
};
