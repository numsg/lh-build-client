export const OrderNames = [
    {
        id: '0',
        name: 'SigtOrder.Complaint'
    },
    {
        id: '1',
        name: 'SigtOrder.Suggest'
    },
    {
        id: '2',
        name: 'SigtOrder.Advisory'
    },
    {
        id: '3',
        name: 'SigtOrder.Report'
    },
    {
        id: '4',
        name: 'SigtOrder.Customs'
    }
];
