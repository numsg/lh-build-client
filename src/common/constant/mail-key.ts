export const MailEventKeys = {
  // 刷新收件箱
  RefreshMailInBox: 'RefreshMailInBox',

  // 刷新待处理邮件
  RefreshMailWaitBox: 'RefreshMailWaitBox'
};
