/**
 * 知识点状态
 */
export const KnowledgeStatus = [
    {
        id: 0,
        name: 'SigtKnowledge.WaitForAudit'
    },
    {
        id: 1,
        name: 'SigtKnowledge.AuditPassed'
    },
    {
        id: 2,
        name: 'SigtKnowledge.AuditNotPassed'
    },
    {
        id: 3,
        name: 'SigtAnnouncement.New'
    }
];
