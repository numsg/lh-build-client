export const SeatPosition = {

    WIDTH: 146, // 席位宽度
    HEIGHT: 90, // 席位高度
    PADDING_LEFT: 20, // 席位之间的左间距
    PADDING_TOP: 20, // 席位之间的上间距
    OFFSET_LEFT: 20, // 距离左侧的默认值
    OFFSET_TOP: 125, // 距离顶部的默认距离
    COLUMS: 9 // 一行显示数目
};

