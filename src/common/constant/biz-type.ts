/**
 * 业务类型
 */
export const BizType = [
  /**
   * 主管
   */
  {
    id: 1,
    name: 'leader'
  },
  /**
   * 组长
   */
  {
    id: 2,
    name: 'foreman'
  },
  /**
   * 坐席员
   */
  {
    id: 3,
    name: 'operator'
  },
  /**
   * 其他
   */
  {
    id: 4,
    name: 'other'
  },
  /**
   * 测试
   */
  {
    id: 5,
    name: 'test'
  }
];
