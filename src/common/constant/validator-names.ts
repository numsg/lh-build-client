/**
 * 实体名,字段名称常量
 */
export const ValidatorFieldNames = {
    /**
     * 邮箱签名
     */
    MailSignatureEntity: 'MailSignatureEntity',
    /**
     * 邮箱签名/类型/组织机构/公告目录/席位名称字段
     */
    Name: 'name',
    /**
     * 邮箱类型
     */
    MailBoxTypeEntity: 'MailBoxTypeEntity',
    /**
     * 收件箱实体
     */
    MailBoxEntity: 'MailBoxEntity',
    /**
     * 组织机构实体
     */
    StructureEntity: 'StructureEntity',
    /**
     * 用户实体
     */
    UserEntity: 'UserEntity',
    /**
     * 用户名
     */
    UserName: 'username',
    /**
     * 员工编号
     */
    UserCode: 'userCode',
    /**
     * 用户电话
     */
    UserPhone: 'phone',
    /**
     * 角色实体
     */
    RoleEntity: 'RoleEntity',
    /**
     * 角色名
     */
    RoleName: 'roleName',
    /**
     * 节假日实体
     */
    HolidayEntity: 'HolidayEntity',
    /**
     * 节假日/人工服务时间/公告/知识库/知识库目录标题
     */
    Title: 'title',
    /**
     * 节假日实体
     */
    ServiceHourEntity: 'ServiceHourEntity',
    /**
     * 系统设置实体
     */
    SettingEntity: 'SettingEntity',
    /**
     * 设置名称
     */
    SettingName: 'Name',
    /**
     * 设置类型实体
     */
    SettingTypeEntity: 'SettingTypeEntity',
    /**
     * 实体类型名称
     */
    TypeName: 'typeName',
    /**
     * 公告目录实体
     */
    AnnouncementDirectoryEntity: 'AnnouncementDirectoryEntity',
    /**
     * 公告实体
     */
    AnnouncementEntity: 'AnnouncementEntity',
    /**
     * 席位实体
     */
    SeatEntity: 'SeatEntity',
    /**
     * 机器名
     */
    ComputerName: 'computerName',
    /**
     * 席位编码
     */
    SeatNum: 'seatNum',
    /**
     * 分组实体
     */
    FloorEntity: 'FloorEntity',
    /**
     * 分组名称号
     */
    FloorNumber: 'floorNumber',
    /**
     * 知识库实体
     */
    KnowledgeDataEntity: 'KnowledgeDataEntity',
    /**
     * 知识库目录
     */
    KnowledgeDirectoryEntity: 'KnowledgeDirectoryEntity'
};
