// 获取electron壳中的全局数据
export const PropFromWindow = {
  // 获取electron存储的机器名或者IP
  getPropFromWindow(path: string): any {
    const paths = path.split('.');
    let result;
    let temp = window as any;
    for (let i = 0; i <= paths.length; i++) {
      temp = temp[paths[i]];
      if (temp) {
        result = temp;
      } else {
        break;
      }
    }
    return result;
  }
};

/**
 * 浏览器加密
 * @param str 加密参数
 */
export function encode(str: string): string {
  return btoa(str);
}

/**
 * 浏览器解密
 * @param str 解密参数
 */
export function decode(str: string): string {
  return atob(str);
}
