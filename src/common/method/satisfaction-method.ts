
  // 求平均值
 export function avg(data: any) {
    let sum = 0;
    for (let index = 0; index < Number(data.length); index++) {
      sum += data[index];
    }
    return sum / data.length;
  }
