/**
 * 自定义搜索内容高亮
 * @param data 输入
 * @param key 键值
 * @param replaceText 替换对象
 */
 export function highLightText(data: string, key: string, replaceText: any): any {
    let item: any;
    item = replaceText[key].replace(data, `<span style="color: #f2784b;font-weight: bold">${data}</span>`);
    return item;
  }
/**
 * 高亮显示搜索内容（不区分大小写）
 * @param data 字符串
 * @param replaceText 替换字符串
 */
 export function highLightTextString(data: string,  replaceText: any): any {
    let item = data;
    if (data && replaceText) {
      const reg = new RegExp(replaceText, 'gi');
      item = data.replace(reg, (text: any) => {
      return  `<span style="color: #f2784b;font-weight: bold">${text}</span>`;
      });
    }
    return item;
  }
