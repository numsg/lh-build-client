import i18n from '@/i18n';
import lodash from 'lodash';

export default {
  getMonth(list: string[]) {
    const monthList: string[] = [];
    const month = '';
    if (list && list.length > 0) {
      list.forEach((element: string) => {
        element =  i18n.t(`Month.${element}`) as string;
        monthList.push(element);
      });
    }
    return monthList;
  }
};
