import store from '@/store';
import i18n from '@/i18n';
import { format } from 'date-fns';

export default {
  // 根据状态id获取状态名字
  transformState(stateId: string) {
    let state = '';
    const orderStates = store.getters && store.getters.orderStates ? store.getters.orderStates : '';
    if (orderStates && stateId) {
      const result = orderStates.find((item: any) => item.id === stateId);
      if (result) {
        state = result.name;
      }
    }
    return state;
  },
  // 根据id获取类型名字
  transferType(typeId: string) {
    let type = '';
    const orderTypes = store.getters && store.getters.orderTypes ? store.getters.orderTypes : '';
    if (orderTypes && typeId) {
      const result = orderTypes.find((item: any) => item.id === typeId);
      if (result) {
        type = result.name;
      }
    }
    return type;
  },
  // 根据id获取工单来源名字
  transferResource(resourceId: string) {
    let resource = '';
    const resources = store.getters && store.getters.orderResource ? store.getters.orderResource : '';
    if (resources && resourceId) {
      const result = resources.find((item: any) => item.id === resourceId);
      if (result) {
        resource = result.name;
      }
    }
    return resource;
  },
  // 根据id获取业务类型
  transferParentProblem(id: string) {
    let parentProblem = '';
    const parentProblems = store.getters && store.getters.parentProblems ? store.getters.parentProblems : '';
    if (parentProblems && id) {
      const result = parentProblems.find((item: any) => item.id === id);
      if (result) {
        parentProblem = result.name;
      }
    }
    return parentProblem;
  },
  // 根据id获取问题类型
  transferChildProblem(id: string) {
    let chilProblem = '';
    const chilProblems = store.getters && store.getters.chilProblems ? store.getters.chilProblems : '';
    if (chilProblems && id) {
      const result = chilProblems.find((item: any) => item.id === id);
      if (result) {
        chilProblem = result.name;
      }
    }
    return chilProblem;
  },
  // 转换utc时间
  timeFormatUtc(res: string) {
    let time = '';
    if (res) {
      time = format(new Date(res), store.state.app.dateType);
    }
    return time;
  },
  // 根据状态id获取动作名字
  transformAction(actionId: string) {
    let state = '';
    const orderActions = store.getters && store.getters.orderActions ? store.getters.orderActions : '';
    if (orderActions && actionId) {
      const result = orderActions.find((item: any) => item.id === actionId);
      if (result) {
        state = result.name;
      }
    }
    return state;
  }
};
