import i18n from '@/i18n';

export  function formatEvaluationType(type: number) {
  switch (type) {
    case 1:
      return i18n.t('PhoneBusiness.Poor').toString();
    case 2:
      return i18n.t('PhoneBusiness.NotSatisfied').toString();
    case 3:
      return i18n.t('PhoneBusiness.Fair').toString();
    case 4:
      return i18n.t('PhoneBusiness.Satisfied').toString();
    case 5:
      return i18n.t('PhoneBusiness.VerySatisfied').toString();
    case 6:
      return i18n.t('PhoneBusiness.Total').toString();
    default:
      return i18n.t('PhoneBusiness.Unknown').toString();
  }
}
