import store from '@/store';
export default {
  getGroupsByIds(ids: string) {
    let result = '';
    const groupLists = store.getters && store.getters.allDepartMents ? store.getters.allDepartMents : '';
    if (groupLists && ids) {
      const idList = ids.split(',');
      for (let index = 0; index < Number(idList.length); index++) {
        const dept = groupLists.find((item: any) => item.structNodeId === idList[index]);
        let delItems = '';
        if (dept) {
          delItems = dept.name;
        }
        if (delItems) {
          if (result) {
            result = result + ',' + delItems;
          } else {
            result = delItems;
          }
        }
      }
    }
    return result;
  },
  getGroupsListByIds(ids: string) {
    const result = [];
    const groupLists = store.getters && store.getters.allDepartMents ? store.getters.allDepartMents : '';
    if (groupLists && ids) {
      const idList = ids.split(',');
      for (let index = 0; index < Number(idList.length); index++) {
        const dept = groupLists.find((item: any) => item.structNodeId === idList[index]);
        let delItems = '';
        if (dept) {
          delItems = dept.name;
        }
        if (delItems) {
          result.push(delItems);
        }
      }
    }
    return result;
  }
};
