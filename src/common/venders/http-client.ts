
import { AxiosError } from 'axios';
import {
  useRequestInterceptor,
  useResponseInterceptor
} from '@gsafety/vue-httpclient/dist/interceptors/interceptor-helper';

import { gblog } from '../../common/utils/logger';

function jsonParseOrElse(json: string | null, other?: any) {
  return json ? JSON.parse(json) : other;
}

// http request 拦截器
useRequestInterceptor(
  (request) => {
    // 请求头中添加 Authorization Token
    const accessToken = jsonParseOrElse(sessionStorage.getItem('token'), {})['access_token'];
    if (!request.headers['Authorization'] && accessToken) {
      request.headers['Authorization'] = `Bearer ${accessToken}`;
    }
    return request;
  }
);

// http response 拦截器
useResponseInterceptor(
  (response) => {
    return response;
  },
  (error: AxiosError) => {
    const response = error.response ? error.response.data : {};
    const statusCode = error.response ? error.response.status : 0;
    if (statusCode === 400 && response.errorCode === 29999) {
      console.log();
    } else if (statusCode === 401 && response.error === 'invalid_token') {
      console.log();
    } else {
      // 记录错误日志
      const errorResponse = error.response ? JSON.stringify(error.response.data) : '';
      gblog.error(`${error.message}, ${errorResponse}`);
      return Promise.reject(error);
    }
  }
);
