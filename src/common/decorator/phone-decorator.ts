import { ThrottleSettings, throttle } from 'lodash';

/**
 * 电话操作节流装饰器
 * @param wait The number of milliseconds to debounce invocations to.
 * @param options The options object.
 * @param options.leading Specify invoking on the leading edge of the timeout.
 * @param options.trailing Specify invoking on the trailing edge of the timeout.
 */
export function Throttle(wait: number) {
    return (target: any, name: string, descriptor: any) => {
        descriptor.value = throttle(descriptor.value, wait, { leading: true, trailing: false });
    };
}
