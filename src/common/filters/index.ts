import Vue from 'vue';
import format from 'date-fns/format';
import store from '@/store';
import i18n from '../../i18n';
import { analysisDateFormat, timeFormate, timingFormat, timingFormat2, sessionTimeFormate } from './filter';
import { convertTimeToHMS } from '../../common/utils/date-utils';
import { formatEvaluationType } from '../../common/method/i18n-filter';

// 自定义过滤器
Vue.filter('date-format', (value: any, formatStr: any = 'YYYY-MM-DD HH:mm:ss') => format(value, formatStr));

Vue.filter('first-tree-letters', ( value: string ) => {
  if (!value) {
      return;
  }
  const str = value.substring(0, 2);
  return str.toUpperCase();
});

// 格式化时间 HH:mm:ss
Vue.filter('formatTimeToHMS', (time: any) => {
  return convertTimeToHMS(time);
});

// 格式化时间 HH:mm:ss
Vue.filter('formatTimeToMS', (time: any) => {
  const m = parseInt((time % 3600) / 60 + '', 10);
  const s = parseInt(((time % 3600) % 60) + '', 10);
  const mm = m < 10 ? '0' + m : m;
  const ss = s < 10 ? '0' + s : s;
  return mm + ':' + ss;
});

Vue.filter('showAnonymous', (name: string) => {
  if ( store.getters.configs.anonymousCharacter === name ) {
    return i18n.t('SigtTableRow.Anonymous');
  }
  return name;
});

Vue.filter('toFixed', (num: any) => {
  if (num && parseFloat(num) !== parseInt(num, 10)) {
    return num.toFixed(2);
  }
  return num;
});

// 格式化 DD/MM/YYYY hh:mm:ss
Vue.filter('formatPtDate', (date: any) => {
  if (date) {
    return format(date, store.state.app.dateType);
  } else {
    return '';
  }
});

// 根据通话状态值返回其国际化词条
Vue.filter('formatCallStatus', (status: any) => {
  switch (status) {
    case 0:
      return i18n.t('PhoneBusiness.Unknown').toString();
      break;
    case 1:
      return i18n.t('PhoneBusiness.PickUp').toString();
      break;
    case 2:
      return i18n.t('PhoneBusiness.UnPickUp').toString();
      break;
    case 3:
      return i18n.t('PhoneBusiness.Transfer').toString();
      break;
    default:
      return i18n.t('PhoneBusiness.Unknown').toString();
      break;
  }
});
// 根据通话类型值返回其国际化词条
Vue.filter('formatCallType', (type: any) => {
  switch (type) {
    case 0:
      return i18n.t('PhoneBusiness.Unknown').toString();
      break;
    case 1:
      return i18n.t('PhoneBusiness.CallIn').toString();
      break;
    case 2:
      return i18n.t('PhoneBusiness.CallOut').toString();
      break;
    case 3:
      return i18n.t('PhoneBusiness.Inner').toString();
      break;
    default:
      return i18n.t('PhoneBusiness.Unknown').toString();
      break;
  }
});

// 根据挂断原因值返回其国际化词条
Vue.filter('formatHangUpReason', (type: any) => {
  switch (type) {
    case 0:
      return i18n.t('PhoneBusiness.Unknown').toString();
      break;
    case 1:
      return i18n.t('PhoneBusiness.CallerHangUp').toString();
      break;
    case 2:
      return i18n.t('PhoneBusiness.CalleeHangUp').toString();
      break;
    case 3:
      return i18n.t('PhoneBusiness.SystemHangUp').toString();
      break;
    default:
      return i18n.t('PhoneBusiness.Unknown').toString();
      break;
  }
});

// 根据挂机位置值返回其国际化词条
Vue.filter('formatHangUpPosition', (type: any) => {
  switch (type) {
    case 0:
      return i18n.t('PhoneBusiness.Unknown').toString();
      break;
    case 1:
      return 'IVR';
      break;
    case 2:
      return i18n.t('PhoneBusiness.System').toString();
      break;
    case 3:
      return i18n.t('PhoneBusiness.Queue').toString();
      break;
    case 4:
      return i18n.t('PhoneBusiness.Ringing').toString();
      break;
    default:
      return i18n.t('PhoneBusiness.Unknown').toString();
      break;
  }
});

// 根据评价分值值返回其国际化词条
Vue.filter('formatEvalutionType', (type: number) => {
  return formatEvaluationType(type);
});

// 数据为空时默认展示暂无数据
Vue.filter('noData', (value: any) => {
  if (value !== null && value !== '' && value !== undefined) {
    return value;
  } else {
    return i18n.t('SigtCommon.NoData').toString();
  }
});

Vue.filter('analysisDateFormat', analysisDateFormat());
Vue.filter('timingFormat', timingFormat());
Vue.filter('timingFormat2', timingFormat2());
Vue.filter('timeFormate', timeFormate());
Vue.filter('sessionTimeFormate', sessionTimeFormate());
