import { format } from 'date-fns';

function formatTimeStr(_date?: any) {
  let date;
  if (_date) {
    date = new Date(_date);
  } else {
    date = new Date();
  }
  const year = date.getFullYear();
  const Month = date.getMonth() + 1;
  const month = Month < 10 ? '0' + Month : Month;
  const day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
  const hour = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
  const minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
  const second = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
  const dateTime = year + '-' + month + '-' + day + ' ' + hour + ':' + minutes + ':' + second;
  return dateTime;
}
function getRandom(num: number) {
  const chars = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L'];
  let res = '';
  for (let i = 0; i < num; i++) {
    const id = Math.ceil(Math.random() * 20);
    res += chars[id];
  }
  return res.toLowerCase();
}
function fileType(type: string) {
  const index = type.indexOf('/');
  const suffix = type.slice(0, index);
  switch (suffix) {
    case 'image':
      return 1;
      break;
    case 'text': case 'application':
      return 4;
      break;
    default:
      break;
  }
}
function getFileType(name: string) {
  const index = name.lastIndexOf('.');
  const suffix = name.slice(index + 1).toLowerCase();
  switch (suffix) {
    case 'png': case 'jpg': case 'bmp': case 'gif': case 'jpeg':
      return 1;
      break;
    case 'mp3': case 'wma': case 'wav':
      return 2;
      break;
    case 'mp4': case 'avi': case 'wmv': case 'mpg': case '3gp': case 'mov':
      return 3;
      break;
    default:
      return 4;
      break;
  }
}
// 排序
const compare = (prop: any) => {
  return (obj1: any, obj2: any) => {
    const val1 = obj1[prop];
    const val2 = obj2[prop];
    if (val1 < val2) {
      return -1;
    } else if (val1 > val2) {
      return 1;
    } else {
      return 0;
    }
  };
};
// 聊天会话时间格式化
export const timeFormate = () => {
  return (time: string) => {
    const nowTimeArr = formatTimeStr().split(' ')[0].split('-');
    const timeArr = time.split(' ')[0].split('-');
    if (nowTimeArr[0] === timeArr[0] && nowTimeArr[1] === timeArr[1] && nowTimeArr[2] === timeArr[2]) {
      return time.slice(11, 16);
    } else if (nowTimeArr[0] !== timeArr[0]) {
      return time;
    } else if (nowTimeArr[1] !== timeArr[1] || nowTimeArr[2] !== timeArr[2]) {
      return time.slice(5, 16);
    }
  };
};
// 会话页面时间过滤
export const sessionTimeFormate = () => {
  return (time: string) => {
    const nowTimeArr = formatTimeStr().split(' ')[0].split('-');
    const timeArr = time.split(' ')[0].split('-');
    if (nowTimeArr[0] === timeArr[0] && nowTimeArr[1] === timeArr[1] && nowTimeArr[2] === timeArr[2]) {
      return time.slice(11, 16);
    } else if (nowTimeArr[0] !== timeArr[0]) {
      return time.slice(0, 10);
    } else if (nowTimeArr[1] !== timeArr[1] || nowTimeArr[2] !== timeArr[2]) {
      return time.slice(5, 10);
    }
  };
};

// 时间计时过滤
export const timingFormat = () => {
  return (time: string) => {
    if (time) {
      const hour = parseInt((parseInt(time, 10) / 3600).toString(), 10);
      const minutes = parseInt(((parseInt(time, 10) - hour * 3600) / 60).toString(), 10);
      const second = parseInt((parseInt(time, 10) - hour * 3600 - minutes * 60).toString(), 10);

      const hourDisplay = hour.toString().length < 2 ? '0' + hour : hour;
      const minutesDisplay = minutes.toString().length < 2 ? '0' + minutes : minutes;
      const secondDisplay = second.toString().length < 2 ? '0' + second : second;

      return hourDisplay + ':' + minutesDisplay + ':' + secondDisplay;
    } else {
      return '';
    }
  };
};
// 时间计时过滤
export const timingFormat2 = () => {
  return (time: string) => {
    if (time) {
      const hour = parseInt((parseInt(time, 10) / 3600).toString(), 10);
      const minutes = parseInt(((parseInt(time, 10) - hour * 3600) / 60).toString(), 10);
      const second = parseInt((parseInt(time, 10) - hour * 3600 - minutes * 60).toString(), 10);

      const hourDisplay = hour > 0 ? hour + 'h' : '';
      const minutesDisplay = minutes > 0 ? minutes + 'm' : '';
      const secondDisplay = second > 0 ? second + 's' : '';

      return hourDisplay + minutesDisplay + secondDisplay;
    } else {
      return '';
    }
  };
};
// 日期显示过滤
export const analysisDateFormat = () => {
  return (_date: any) => {
    if (_date) {
      const date = format(_date, 'DD/MM/YYYY');
      return date;
    }
  };
};

export { formatTimeStr, getRandom, fileType, getFileType, compare };

