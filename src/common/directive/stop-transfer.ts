import { DirectiveOptions } from 'vue';

const stopTransfer = ($event: Event) => {
  $event.stopPropagation();
  $event.cancelBubble = true;
};


const StopTransfer: DirectiveOptions = {
  bind(el, binding) {
    el.addEventListener('click', stopTransfer, false);
  },
  unbind(el) {
    el.removeEventListener('click', stopTransfer, false);
  }
};

export default StopTransfer;
