import { DirectiveOptions } from 'vue';
import { DirectiveBinding } from 'vue/types/options';

const loadMore = (DOM: Element, binding: DirectiveBinding) => {
    if ( isNumber(DOM.scrollHeight) && isNumber(DOM.scrollTop) && isNumber(DOM.clientHeight) ) {
      const condition = ( DOM.scrollHeight ) - DOM.scrollTop <= DOM.clientHeight;
      if ( condition ) {
        binding.value();
      }
    }
};

const isNumber = (arg: any) =>  {
  if ( typeof arg === 'number' ) {
    return true;
  }
  return false;
};


const SelectLoadMore: DirectiveOptions = {
  bind(el, binding) {
    const SELECT_DOM = el.querySelector('.el-select-dropdown__wrap.el-scrollbar__wrap');
    if ( !!SELECT_DOM ) {
      SELECT_DOM.addEventListener('scroll', () => {
        loadMore(SELECT_DOM, binding);
      });
    }
  },
  unbind(el, binding) {
    const SELECT_DOM = el.querySelector('.el-select-dropdown__wrap.el-scrollbar__wrap');
    if ( !!SELECT_DOM ) {
      SELECT_DOM.removeEventListener('scroll', () => {
        loadMore(SELECT_DOM, binding);
      });
    }
  }
};

export default SelectLoadMore;
