import { DirectiveOptions } from 'vue';

const DialogWrapper: DirectiveOptions = {
  bind(el, binding) {
    el.style.width = 'calc( 100% - 251px )';
    el.style.overflow = 'hidden';
    el.style.left = 'auto';
    el.style.right = '0';
    el.style.height = 'calc(100% - 106px)';
    el.style.top = '106px';
  }
};

export default DialogWrapper;
