import { DirectiveOptions } from 'vue';

const dialogDrag: DirectiveOptions = {
  bind(el, binding, vnode, oldVnode) {
    // 弹框可拉伸最小宽高

    const minWidth = 400;

    const minHeight = 300;

    // 初始非全屏

    let isFullScreen = false;

    // 当前宽高

    let nowWidth = 0;

    let nowHight = 0;
    // 当前位移

    let nowOffsetLeft = 0;

    let newOffsetTop = 0;

    // 当前顶部高度

    const nowMarginTop = 0;

    // 获取弹框头部（这部分可双击全屏）
    const ele = el.querySelector('.dialogDragHead');
    const dialogHeaderEl = ele as HTMLElement;
    // 弹窗

    const dragDom = el.querySelector('.el-dialog') as HTMLDivElement;

    // 给弹窗加上overflow auto；不然缩小时框内的标签可能超出dialog；
    el.style.width = '0';
    el.style.height = '0';
    dragDom.style.overflow = 'overflow';
    dragDom.style.position = 'fixed';
    dragDom.style.left =  binding && binding.value && binding.value.left ? binding.value.left  : '20%';
    dragDom.style.minWidth = '50%';

    // 清除选择头部文字效果

    // 头部加上可拖动cursor

    dialogHeaderEl.style.cursor = 'move';

    // 此为特殊定制
    if ( dragDom ) {
      dragDom.addEventListener('click', ($event) => {
        if ( $event.currentTarget && ($event.currentTarget as HTMLDivElement).className ) {
          const className = ($event.currentTarget as HTMLDivElement).className;
          if ( className.includes('order-dialog') ) {
            if ( document.querySelector('.el-dialog__wrapper.overall-dialog') ) {
              const eventDOM = document.querySelector('.el-dialog__wrapper.order-dialog') as HTMLDivElement;
              const overAll = document.querySelector('.el-dialog__wrapper.overall-dialog') as HTMLDivElement;
              const overAllIndex = overAll.style.zIndex;
              const eventIndex = eventDOM.style.zIndex;
              if ( parseInt( eventDOM.style.zIndex, 10 ) <  parseInt( overAllIndex, 10 )) {
                eventDOM.style.zIndex = overAllIndex;
                overAll.style.zIndex = eventIndex;
                return;
              }
            }
          }

          if ( className.includes('customer-detail-overall') ) {
            if ( document.querySelector('.el-dialog__wrapper.order-dialog') ) {
              const eventDOM = document.querySelector('.el-dialog__wrapper.overall-dialog') as HTMLDivElement;
              const orderDom = document.querySelector('.el-dialog__wrapper.order-dialog') as HTMLDivElement;
              const orderIndex = orderDom.style.zIndex;
              const eventIndex = eventDOM.style.zIndex;
              if (  parseInt( eventDOM.style.zIndex, 10 ) <  parseInt( orderIndex, 10 ) ) {
                eventDOM.style.zIndex = orderIndex;
                orderDom.style.zIndex = eventIndex;
                return;
              }
            }
          }
        }

      });
    }

    // 获取原有属性 ie dom元素.currentStyle 火狐谷歌 window.getComputedStyle(dom元素, null);

    const sty = (dragDom as any).currentStyle || window.getComputedStyle(dragDom, null);

    const moveDown = (e: any) => {
      // 鼠标按下，计算当前元素距离可视区的距离
      const disX = e.clientX - dialogHeaderEl.offsetLeft;

      const disY = e.clientY - dialogHeaderEl.offsetTop;

      // 获取到的值带px 正则匹配替换

      // tslint:disable-next-line:one-variable-per-declaration
      let styL: any, styT: any;

      // 注意在ie中 第一次获取到的值为组件自带50% 移动之后赋值为px

      if (sty.left.includes('%')) {
        styL = +document.body.clientWidth * (+sty.left.replace(/\%/g, '') / 100);

        styT = +document.body.clientHeight * (+sty.top.replace(/\%/g, '') / 100);
      } else {
        styL = +sty.left.replace(/\px/g, '');

        styT = +sty.top.replace(/\px/g, '');
      }

      // tslint:disable-next-line:only-arrow-functions
      document.onmousemove = (event: any) => {
        // 通过事件委托，计算移动的距离

        const l = event.clientX - disX;

        const t = event.clientY - disY;

        // 移动当前元素

        dragDom.style.left = `${l + styL}px`;

        dragDom.style.top = `${t + styT}px`;

        // 将此时的位置传出去

        // binding.value({x:e.pageX,y:e.pageY})
      };

      // tslint:disable-next-line:only-arrow-functions
      document.onmouseup = function() {
        document.onmousemove = null;

        document.onmouseup = null;
      };
    };

    dialogHeaderEl.onmousedown = moveDown;

    // 双击头部全屏效果

    dialogHeaderEl.ondblclick = () => {
      if (isFullScreen === false) {
        nowHight = dragDom.clientHeight;

        nowWidth = dragDom.clientWidth;

        nowOffsetLeft = dragDom.offsetLeft;

        newOffsetTop = dragDom.offsetTop;

        dragDom.style.left = 0 + 'px';

        dragDom.style.top = 0 + 'px';

        dragDom.style.height = '100VH';

        dragDom.style.width = '100VW';

        isFullScreen = true;

        dialogHeaderEl.style.cursor = 'initial';

        dialogHeaderEl.onmousedown = null;
      } else {
        dragDom.style.height = nowHight + 'px';

        dragDom.style.width = nowWidth + 'px';

        dragDom.style.left = nowOffsetLeft + 'px';

        dragDom.style.top = newOffsetTop + 'px';

        dialogHeaderEl.style.cursor = 'move';

        dialogHeaderEl.onmousedown = moveDown;

        isFullScreen = false;
      }
    };

    dragDom.onmousemove = (e: any) => {
      const moveE = e;

      if (e.clientX > dragDom.offsetLeft + dragDom.clientWidth - 10 || dragDom.offsetLeft + 10 > e.clientX) {
        dragDom.style.cursor = 'w-resize';
      } else if (el.scrollTop + e.clientY > dragDom.offsetTop + dragDom.clientHeight - 10) {
        dragDom.style.cursor = 's-resize';
      } else {
        dragDom.style.cursor = 'default';

        dragDom.onmousedown = null;
      }

      // tslint:disable-next-line:no-shadowed-variable
      dragDom.onmousedown = (e: any) => {
        const clientX = e.clientX;

        const clientY = e.clientY;

        const elW = dragDom.clientWidth;

        const elH = dragDom.clientHeight;

        const EloffsetLeft = dragDom.offsetLeft;

        const EloffsetTop = dragDom.offsetTop;

        dragDom.style.userSelect = 'none';

        const ELscrollTop = el.scrollTop;

        // 判断点击的位置是不是为头部

        if (clientX > EloffsetLeft && clientX < EloffsetLeft + elW && clientY > EloffsetTop && clientY < EloffsetTop + 100) {
          // 如果是头部在此就不做任何动作，以上有绑定dialogHeaderEl.onmousedown = moveDown;
        } else {
          document.onmousemove = (eve: any) => {
            // eve.preventDefault(); // 移动时禁用默认事件

            // 左侧鼠标拖拽位置

            if (clientX > EloffsetLeft && clientX < EloffsetLeft + 10) {
              // 往左拖拽
              if (clientX > eve.clientX) {
                dragDom.style.width = elW + (clientX - eve.clientX) * 1 + 'px';
                dragDom.style.left = EloffsetLeft - (clientX - eve.clientX) * 1 + 'px';
              }

              // 往右拖拽

              if (clientX < eve.clientX) {
                if (dragDom.clientWidth < minWidth) {
                } else {
                  dragDom.style.width = elW - (eve.clientX - clientX) * 1 + 'px';
                  dragDom.style.left = EloffsetLeft - (clientX - eve.clientX) * 1 + 'px';
                }
              }
            }

            // 右侧鼠标拖拽位置

            if (clientX > EloffsetLeft + elW - 10 && clientX < EloffsetLeft + elW) {
              // 往左拖拽

              if (clientX > eve.clientX) {
                if (dragDom.clientWidth < minWidth) {
                } else {
                  dragDom.style.width = elW - (clientX - eve.clientX) * 1 + 'px';
                }
              }

              // 往右拖拽

              if (clientX < eve.clientX) {
                dragDom.style.width = elW + (eve.clientX - clientX) * 1 + 'px';
              }
            }

            // 底部鼠标拖拽位置

            if (ELscrollTop + clientY > EloffsetTop + elH - 20 && ELscrollTop + clientY < EloffsetTop + elH) {
              // 往上拖拽

              if (clientY > eve.clientY) {
                if (dragDom.clientHeight < minHeight) {
                } else {
                  dragDom.style.height = elH - (clientY - eve.clientY) * 1 + 'px';
                }
              }

              // 往下拖拽

              if (clientY < eve.clientY) {
                dragDom.style.height = elH + (eve.clientY - clientY) * 1 + 'px';
              }
            }
          };

          // 拉伸结束

          // tslint:disable-next-line:only-arrow-functions
          document.onmouseup = function() {
            document.onmousemove = null;

            document.onmouseup = null;
          };
        }
      };
    };
  }
};

export default dialogDrag;
