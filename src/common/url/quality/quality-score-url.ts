export const QualityScoreUrl = {
  // 保存打分
  saveScore: '/api/v1/quality/score',
  // 根据通话记录ID查询所有评分记录
  getScoreByCordId: '/api/v1/quality/score/call-record/{callRecordId}',
  // 根据通话记录ID查询质检申诉
  getComplaintsByCordId: '/api/v1/quality/complaints/call-record/{callRecordId}',
  // 删除质检打分(假删除)
  deleteScore: '/api/v1/quality/score/delete/{id}',

  // 分配质检评分任务
  qualityAssign: '/api/v1/quality/score/assign',

  // 申诉
  complaints: '/api/v1/quality/complaints',
  // 申诉重新分配
  reassignComplaints: '/api/v1/quality/complaints/reassign',
  // 驳回申诉
  rejectComplaints: '/api/v1/quality/complaints/reject'
};
