export const QualityTemplateUrl = {
  // 分页查询质检模板(仅含模板信息, 不含对应的质检标准内容)
  queryTemplatesByPage: '/api/v1/quality/templates/page',
  // 保存质检模板
  creatQualityTemplates: '/api/v1/quality/templates',
  // 根据质检模板ID查找质检标准(含父节点, 树结构返回)
  queryTempTreeByQaTempId: '/api/v1/quality/templates/{qaTemplateId}/rules/tree',
  // 更新质检模板
  editTemplate: '/api/v1/quality/templates/{qaTemplateId}',
  // 根据id删除质检模板
  deleteTemplate: '/api/v1/quality/templates/{qaTemplateId}',
  // 保存打分
  saveScore: '/api/v1/quality/score'
};
