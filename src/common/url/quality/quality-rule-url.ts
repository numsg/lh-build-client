export const QualityRuleUrl = {
  // 多条件组合查询质检标准表数据
  qualityRulesSearch: '/api/v1/quality/rules/search',
  // 保存质检标准 POST /api/v1/quality/rules保存质检标准
  addQualityRules: '/api/v1/quality/rules',
  // 批量保存质检标准
  addQualityBatchRules: '/api/v1/quality/rules/batch',
  // 修改评分标准 /api/v1/quality/rules/{qaRuleId}更新质检标准
  editQualityRule: '/api/v1/quality/rules/{qaRuleId}',
  // 根据ID删除质检标准及其子孙质检标准 /api/v1/quality/rules/{qaRuleId}
  deleteQualityRules: '/api/v1/quality/rules/{qaRuleId}',
  // 根据父标准Id分页查询子质检标准(树结构返回)
  queryCurrentRuleTreePage: '/api/v1/quality/rules/{parentRuleId}/tree/page',
  // 多条件组合查询质检通话记录(分页)
  queryCallRecord: '/api/v1/quality/call-records/search/page',
  // 查询所有质检标准(树结构返回)
  AllRuleTree: '/api/v1/quality/rules/tree',
};
