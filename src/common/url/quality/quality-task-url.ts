export const QualityTaskUrl = {
  // 分页查询质检任务
  queryTasksPage: '/api/v1/quality/tasks/page',
  // 保存质检任务
  saveTask: '/api/v1/quality/tasks',
  // 更新质检任务
  commonTask: '/api/v1/quality/tasks/{qaTaskId}',
  // 根据质检任务名称查询质检任务集合
  queryTasksByTaskName: '/api/v1/quality/tasks/name/{taskName}',
};
