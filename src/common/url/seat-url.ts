export const SeatUrls = {
    getAllFloors: '/api/v1/floor',
    addFloor: '/api/v1/floor',
    updateFloor: '/api/v1/floor',
    delFloors: '/api/v1/floor/multiDelete',

    addSeat: '/api/v1/seat',
    delSeat: '/api/v1/seat/multiDelete',
    updateSeat: '/api/v1/seat',
    getSeatByParam: '/api/v1/seat/getSeatByParam',
    saveSeatConfiguration: '/api/v1/seat/saveSeatConfiguration',
    updateByBatch: '/api/v1/seat/updateByBatch',

    addSeatRecord: '/api/v1/seatRecord',
    updateSeatRecordLoginInfo: '/api/v1/seatRecordLoginInfo',
    updateSeatRecordBusyInfo: '/api/v1/seatRecordBusyInfo',
    updateSeatBusyTime: '/api/v1/updateSeatBusy'
};
