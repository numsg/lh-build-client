export const MailBoxUrl = {
  /** 邮箱类型 */
  addMailType: '/mailbox-type/add',
  editMailType: '/mailbox-type/update',
  deleyeMailTypeById: '/mailbox-type/id/{id}',

  /** 邮箱信息 */
  addMailInfo: '/mailbox/add',
  updateMailInfo: '/mailbox/update',
  deleyeMailInfoById: '/mailbox/id/{id}',
  getMailInfoByGroupId: '/mailbox/business-group/{bussinessId}',


  /** 发件箱 */
  addSendMail: '/mailbox-send/add',
  deleteSendMail: '/mailbox-send/id/{id}',
  deleteSendMails: '/mailbox-send/deletes',
  // 邮件转发
  forward: '/mailbox-send/forward',
  // 邮件回复
  reply: '/mailbox-send/reply',


  /** 收件箱 */
  // 已读
  hasReadEmail: '/mailbox-receive/read/id/{id}',
  // 重新分配
  apportion: '/mailbox-receive/apportion',
  // 已处置
  disposeEmaiil: '/mailbox-receive/dispose',
  // 删除收件箱
  deleteInEmail: '/mailbox-receive/id/{id}',

  deleteInEmails: '/mailbox-receive/deletes',
  // 根据Id查找收件箱
  findEmailById: '/mailbox-receive/{id}',
  // 根据发件箱查询已发送附件
  attachments: '/mailbox-receive/attachments',

  /** 邮箱签名 */
  addMialSign: '/mail-sign/add',
  updateMialSign: '/mail-sign/update',
  deleteMialSignById: '/mail-sign/id/{id}'
};
