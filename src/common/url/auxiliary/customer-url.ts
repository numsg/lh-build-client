export const customerUrl = {
  addCustomerUrl: '/api/v1/customer',
  editCustomerUrl: '/api/v1/customer',
  deleteCustomerUrl: '/api/v1/customer/multiDelete',
  addByBatch: '/api/v1/customer/addByBatch',
  queryCustomerByPhone: '/api/v1/customer/{phone}',
  queryCustomerByName: '/api/v1/customer/getByName/{name}',
  queryTaxpayer: '/api/v1/taxpayer/{nif}',
  queryCustomerByNIF: '/api/v1/customer/getByNIF/{nif}'
};
