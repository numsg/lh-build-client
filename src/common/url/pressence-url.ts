// 获取席位注册的客户端API
export const PressenceUrl = {
  // 获取所有客户端席位集合信息
  getAllClientSeat: '/clients',

  // 发送通知
  sendMessage: '/message',

  // 根据条件查询客户端
  getClientsByProName: '/clients/{proName}/{proValues}/{isRegisterPro}',

  // 获取在线席位
  getAllRegisterInfos: '/registerInfos'
};
