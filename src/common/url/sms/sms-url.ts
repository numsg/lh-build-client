export const SmsUrls = {
  addSmsPost: '/sms/add',
  deleteSmsIdsPost: '/sms/deletes',
  deleteSmsIdDelete: '/sms/id/',
  resendSmssPost: '/sms/resend-list',

  addPhrasePost: '/sms-phrase/add',
  deletePhraseIdsPost: '/sms-phrase/deletes',
  deletePhraseIdDelete: '/sms-phrase/id/',
  updatePhrasePut: '/sms-phrase/update'
};
