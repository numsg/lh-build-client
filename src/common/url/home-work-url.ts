export const homeWorkUrl = {
    getCurrentUserWorkInfo: '/analysis/user/call?userCode=',
    getAllUserWorkInfo: '/analysis/user/call/all',
    getCurrentDepUserWorkInfo: '/analysis/user/call/department?departmentName=',
};
