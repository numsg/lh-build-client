export const MenuUrl = {
    getMenu: '/api/v1/menu',
    getMenuByRoleId: '/api/v1/menu/{roleId}',
    getMenuTreeByRoleId: '/api/v1/getTreeMenuByRoleId/{roleId}',
    getMenuTree: '/api/v1/getMenuTree',
    configMenus: '/api/v1/configMenus',
    deleteRoleMenus: '/api/v1/menu/{roleId}',
};
