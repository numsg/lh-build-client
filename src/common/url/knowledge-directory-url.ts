export const KnowledgeDirectoryUrl = {
    saveKnowledgeDirectory: '/api/v1/knowledge/knowledgeDirectory',
    deleteKnowledgeDirectory: '/api/v1/knowledge/knowledgeDirectory/delete/{0}',
    deleteKnowledgeData: '/api/v1/knowledge/knowledgeData/multiDelete',
    knowledgeData: '/api/v1/knowledge/knowledgeData',
    auditPass: '/api/v1/knowledge/approve',
    auditNotPass: '/api/v1/knowledge/unApprove',
    knowledgeCollection: '/api/v1/knowledge/knowledgeCollection',
    deleteknowledgeCollection: '/api/v1/knowledge/knowledgeCollection/{0}/{1}',
    addCollectiondDataRelation: '/api/v1/knowledge/addCollectionDataRelation',
    deleteCollectiondDataRelation: '/api/v1/knowledge/deleteCollectionDataRelation/{0}',
    getKnowledgeWithPage: '/api/v1/knowledge/getKnowledgeWithPage',
    updateReadingTime: '/api/v1/knowledge/updateReadingTime/{0}',
    addByBatch: '/api/v1/knowledge/addByBatch',
    publish: '/api/v1/knowledge/publish'
};
