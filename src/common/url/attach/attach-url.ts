export const AttachUrl = {
  // 上传多个附件
  uploadBatch: '/api/v1/attaches/upload/batch',

  // 附件查看
  attachesPreview: '/api/v1/attaches/{fileId}/preview',

  // 删除附件
  deleteAttach: '/api/v1/attaches/{fileId}/metadata/{moduleName}',

  // 根据fileId和所属模块查询文件
  attachesInfo: '/api/v1/attaches/{fileId}/metadata/{moduleName}',

  // 下载附件
  downLoad: '/api/v1/attaches/{fileId}/download',

  // 批量删除
  batchDelete: '/api/v1/attaches/metadata/{0}/batch-delete',
};
