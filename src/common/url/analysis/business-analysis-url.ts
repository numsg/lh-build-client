export const businessAnalysisUrl = {
  queryCallType: '/analysis/call/type',
  queryPhoneStatus: '/analysis/phone/status',
  queryCallSituation: '/analysis/call/situation',
  queryBusinessType: '/analysis/business/type',
  queryCallTypeMonth: '/analysis/call/type/month',
  queryCallHour: '/analysis/call/hour',
  queryPhoneStatusMonth: '/analysis/phone/status/month',
  queryTransferInfos: '/analysis/phone/transferInfo',
  queryOrderData: '/analysis/order/orderData',
  querySeatWorkData: '/analysis/seat/seatWorkData',

  // 根据条件查询平均分统计
  qaAverageStatistics: '/analysis/qa/average/statistics',
  // 根据条件查询每月/每周统计  POST /analysis/qa/time/statistics
  qaTimeStatistics: '/analysis/qa/time/statistics',
  // /analysis/qa/dep/statistics根据条件查询质检部门统计
  qaDepStatistics: '/analysis/qa/dep/statistics',
  // /analysis/satisfaction/statistics根据条件查询满意度统计
  satisfactionStatistic: '/analysis/satisfaction/statistics',
  // /analysis/call/statistics通话报表统计
  callStatistics: '/analysis/call/statistics',
  // /analysis/call/type/statistics通话报表类别统计
  callTypeStatistics: '/analysis/call/type/statistics',
};
