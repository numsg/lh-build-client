export const ScreenMonitorUrl = {
    businessType: '/analysis/business/type',
    workLoad: '/analysis/workload',
    departmentGroup: '/analysis/department',
    answerWait: '/analysis/phone/status/hour',
    phoneDistributeHour: '/analysis/phone/distribute/hour',
    phoneDistributeMonth: '/analysis/phone/distribute/month',
    phoneDetail: '/analysis/phone/detail',
    queueDetail: '/analysis/queue/related',
};
