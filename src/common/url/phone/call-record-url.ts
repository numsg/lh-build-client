export const CallRecordUrl = {
  // 保存呼叫记录(电话)
  saveCallInRecord: '/callinrecord/saveCallInRecord',
  // 修改通话记录(电话)
  updateCallInRecord: '/callinrecord/updateCallInRecord',
  // 查询通话记录(电话)
  callinRecordQuery: '/callinrecord/getCallInRecordByParam',


  // 保存呼叫记录(手台)
  saveRadioCallInRecord: '/radioCallinrecord/saveRadioCallInRecord',
  // 查询通话记录(手台)
  getRadioCallRecords: '/radioCallinrecord/getRadioCallInRecordByParam',
  // 修改通话记录(手台)
  updateRadioInRecord: '/radioCallinrecord/updateRadioCallInRecord'
};
