export const SigtCallRecordUrl = {

    // 新增通话记录 POST /api/v1/phone/call/record
    AddCallRecord: '/api/v1/phone/call/record',

    // 带条件查找通话记录（无分页，导出用） POST /api/v1/phone/call/record
    GetCallRecordByParam: '/api/v1/phone/call/record/no/page',

    // 分页带条件查找通话记录 GET /api/v1/phone/call/record/{pageInfo}
    GetCallRecordWithPage: '/api/v1/phone/call/record/page',

    // 获取所有通话记录 GET /api/v1/phone/call/record
    GetCallRecords: '/api/v1/phone/call/records',

    // 更新通话记录 POST /api/v1/phone/call/record/update
    UpdateCallRecord: '/api/v1/phone/call/record/update',

    // 根据ids更新通话记录orderId GET /api/v1/phone/call/record/{callRecordId}/{orderId}
    UpdateOrderIdByIds: '/api/v1/phone/call/record/ids/{callRecordId}/{orderId}'
};
