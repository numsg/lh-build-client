export const PhoneRecordUrl = {
  // 保存电话录音 (电话)
  savePhoneRecord: '/phonerecord/addPhoneRecord',
  // 根据录音Id获取录音 (电话)
  getPhoneRecordByIds: '/phonerecord/getPhoneRecordsByIds/{ids}',
  // 修改电话录音 (电话)
  updatePhoneRecord: '/phonerecord/updatePhoneRecord',
  // 根据来电id获取录音(电话)
  getPhoneRecordByCallRecId: '/phonerecord/getPhoneRecordByCallId/{callId}',
  // 新增回拨记录
  addCallBackRecord: '/call-back-record/',
  // 根据电话号码查询回拨记录
  findCallBackRecordByNum: '/call-back-record/{phoneNumber}',
  // 获取录音电话
  getPhoneRecord: '/phonerecord/getPoneRecordByParam',
  // 获取录音电话(无分页)
  getPhoneRecordNoPage: '/phonerecord/getPoneRecordByParamNoPage',

  // 根据来电id获取录音(手台)
  getRadioRecordByCallRecId: '/radioRecord/getRadioRecordByCallId/{callId}',
  // 保存电话录音(手台)
  saveRadioRecord: '/radioRecord/addRadioRecord',
  // 修改电话录音(手台)
  updateRadioRecord: '/radioRecord/updateRadioRecord',
  // 查询录音(手台)
  getRadioRecord: '/radioRecord/getRadioRecordByParam'

};
