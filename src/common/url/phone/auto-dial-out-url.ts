export const AutoDialOutUrl = {
  getDialOutList: '/api/v1/phone/dial/search/page?page={pageIndex}&size={pageSize}',
  getDialOutListNOPage: '/api/v1/phone/dial/search',
  deleteDialRecord: '/api/v1/phone/dial/{recordId}',
  importAutoDialRecords: '/api/v1/phone/dial/import',
  startAutoDial: '/api/v1/phone/dial/start/{projectId}',
  stopAutoDial: '/api/v1/phone/dial/stop/{projectId}'
};
