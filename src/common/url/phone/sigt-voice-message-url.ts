export const SigtVoiceMessageUrl = {
    pageQueryMessage: '/api/v1/phone/voice/message/search/page?page={pageIndex}&size={pageSize}',
    receiveMessge: '/api/v1/phone/voice/message/receive',
    disposeMessge: '/api/v1/phone/voice/message/dispose',
    deleteMessage: '/api/v1/phone/voice/message/delete/{voiceMessageId}',
    findMessage: '/api/v1/phone/voice/message/{voiceMessageId}',
};
