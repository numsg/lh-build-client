/**
 * 电话队列相关API
 */
export const PhoneQueueUrl = {
    // GET /api/v1/phone/queues/order/type获取所有队列信息（带工单类型）
    findAllWithOrderType: '/api/v1/phone/queues/order/type',
    /**
     * 获取电话队列和工单关联信息By工单类型Id
     */
    findOneWithOrderType: '/api/v1/phone/queue/{typeId}',

    // 查询所有队列名称
    findAllQueues: '/api/v1/phone/queues',


    // 新增队列
    addQueue: '/api/v1/phone/queues/add',
    // 修改队列
    modifyQueue: '/api/v1/phone/queues/modify',
    // 删除队列
    deleteQueue: '/api/v1/phone/queues/delete/{id}',
};
