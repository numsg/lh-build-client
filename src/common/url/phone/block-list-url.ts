export const BlockListUrl = {
  // post 新增黑名单
  add: '/api/v1/phone/block-list/add',

  // get 查询当前电话号码是否存在黑名单中
  numberExist: '/api/v1/phone/block-list/exist/number/{number}',

  // 多条件查询黑名单
  searchBlockList: '/api/v1/phone/block-list/search',

  // 多条件查询黑名单（带分页）
  searchBlockListWithPage: '/api/v1/phone/block-list/search/page',

  // 根据Id删除或修改黑名单
  deleteOrUpdateBlockList: '/api/v1/phone/block-list/{blockListId}',

   // 根据电话查找电话详细信息
   findLostCallByNumber: '/api/v1/phone/block-list/search/number/{number}',
};
