/**
 * 电话呼损相关API
 */
export const LostCallUrl = {
    // 根据条件查询呼损电话（带分页）
    findLostCallWithPage: '/api/v1/phone/lost/call/search/page?page={pageIndex}&size={pageSize}',
    // 批量签收呼损电话
    receiveLostCalls: '/api/v1/phone/lost/calls/receive',
    // 批量处理呼损电话
    disposeLostCalls: '/api/v1/phone/lost/calls/dispose',
    // 根据呼损电话查询呼损电话ID
    findLostCallById: '/api/v1/phone/lost/calls/{lostCallId}'
};
