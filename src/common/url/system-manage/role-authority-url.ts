export const RoleAuthorityUrl = {
    rolesManage: '/api/v1/roles/',
    UserRolesUrl: '/api/v1/configRoles/',
    rolesDelete: '/api/v1/roles/multiDelete/',
    getRolesByUserIdUrl: '/api/v1/getRolesByUserId/{userId}'
};
