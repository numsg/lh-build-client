export const HolidayUrls = {
    getAllUrl: '/api/v1/holiday',
    addUrl: '/api/v1/holiday',
    updateUrl: '/api/v1/holiday',
    deleteHolidaysUrl: '/api/v1/holiday/multiDelete',
    getStartAndEnd: '/api/v1/serviceHour/startAndEnd'
};
