export const AnnouncementUrls = {
  addAnnDirectoryUrl: '/api/v1/announcement/announcementDirectory',
  updateAnnDirectoryUrl: '/api/v1/announcement/announcementDirectory',
  deleteAnnDirectoryUrl: '/api/v1/announcement/announcementDirectory/{id}',
  addAnnUrl: '/api/v1/announcement',
  updateAnnUrl: '/api/v1/announcement',
  deleteAnnouncementUrl: '/api/v1/announcement/multiDelete',
  publishAnnouncementUrl: '/api/v1/announcement/publish',
  revokeAnnouncementUrl: '/api/v1/announcement/revoke/{id}',
  updateSendStateUrl: '/api/v1/announcement/updateSendState',
};
