export const ServiceTimeUrls = {
  getAllUrlByTitle: '/api/v1/getServiceHoursByTitle/{title}',
  getAllUrl: '/api/v1/serviceHour',
  addUrl: '/api/v1/serviceHour',
  delUrl: '/api/v1/serviceHour/multiDelete',
  updateUrl: '/api/v1/serviceHour'
};
