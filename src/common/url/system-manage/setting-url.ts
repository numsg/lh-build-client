export const SettingUrl = {
  /**
   * get post put
   */
  setting: '/api/v1/settings',
  /**
   * 查询所有设置的type名称
   */
  settingTypes: '/api/v1/settings/types',
  /**
   * 根据参数类型查找已启用配置项
   */
  findSettingByType: '/api/v1/settings/param-type/{paramType}',

  /**
   * 根据参数名称查找配置项
   */
  findSettingByName: '/api/v1/settings/name/{settingName}',

  /**
   * 查询、更新、删除地址参数
   */
  Query_Update_DeleteSetting: '/api/v1/settings/{settingId}',

  /**
   * 根据Id删除参数
   */
  deleteSettingType: '/api/v1/settings/types/{typeId}',
  /**
   * 根据参数类型查找所有配置项
   */
  findByParamType: '/api/v1/settings/param-type/findByParamType/{paramType}',
};
