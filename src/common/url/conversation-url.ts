export const ConversationUrl = {
  saveConversation: '/saveConversation',
  updateConversationStatus: '/updateConversationStatus',
  getConversationRecordById: '/getConversationRecordById',
  getConversationRecordList: '/getConversationRecordList?conversationId=',
  getMsgDistributionSettingByUserId: '/getMsgDistributionSettingByUserId?userId=',
  getConversationListBySeatId: '/getConversationListBySeatId',
  getQuickReplyInfos: '/getQuickReplyInfosById?userId=',
  deleteQuickReply: '/deleteQuickReply?quickReplyId=',
  addQuickReply: '/addQuickReply',
  updateQuickReply: '/updateQuickReply',
  messageInfo: '/messageInfo',
  saveMsgDistributionSetting: '/saveMsgDistributionSetting',
  updateConverSationRecordInfo: '/updateConverSationRecordInfo',
  getUndispostConversationInfo: '/getUndispostConversationInfoById?userId=',
  upload: '/upload/single',
  updateQuickReplyUsesCount: '/updateQuickReplyUsesCount?quickReplyId=',
  reverseConversation: '/reverseConversation',
  getAllUserInfo: '/api/v1/getAllUserInfo',
  savesingle: '/template/single',
  saveMulti: '/template',
  getTemplate: '/template',
  getTemplateByParentId: '/templates/',
  getFirstTemplate: '/inquire-first-template',
  deleteTemplate: '/template?id=',
  getOnlineRegistUser: '/getOnlineRegistUser',
  // 通过工单id查询会话消息记录(分页)
  getRecordByOrderId: '/conversation-recordInfo/page/order-id',
  // 根据会话ID查询会话信息
  getSingleConversation: '/getSingleConversation?conversationId='
};

