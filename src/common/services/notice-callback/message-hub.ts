import { rxevent } from '@gsafety/rx-eventbus/dist/eventaggregator.service';
import { Message } from '@gsafety/cad-notice-module/dist/models/message';
import { MessageEventKeys } from '@/common/constant/event-keys';
import store from '@/store';
import { ClientInfo } from '../../../common/models/client-info';
import moment from 'moment';

/**
 * 消息回调HUB事件
 */
export class MessageHub {
  constructor() { }

  /**
   * 通知公告消息通知回调HUB
   * @param clientInfo  信息
   */
  public announcementBoardNotice(clientInfo: any) {
    const message = new Message(
      clientInfo.id,
      clientInfo.type,
      clientInfo.title,
      clientInfo.content,
      moment(clientInfo.receiveTime, 'YYYY-MM-DD HH:mm:ss').utc().toDate(),
      clientInfo.payload
    );
    rxevent.publish(MessageEventKeys.NoticeMessage, message);
  }

  /**
   * 客户端被迫下线
   * @param {SeatStatus} clientInfo
   * @memberof MessageHub
   */
  forcedToLogoff(clientInfo: ClientInfo) {
    if (clientInfo.registerInfo.registerId === store.getters.registerInfo.registerId) {
      rxevent.publish(MessageEventKeys.ForcedToLogoff, clientInfo);
    }
  }
}
