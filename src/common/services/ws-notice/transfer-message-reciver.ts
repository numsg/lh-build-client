export class TransferMessageReciver<T> {

    /// 回调hub名称
    /// </summary>
    hub?: string;


    /// 回调方法名称
    /// </summary>
    method = '';

    /// 回调消息参数实体
    /// </summary>
    args?: T;
}
