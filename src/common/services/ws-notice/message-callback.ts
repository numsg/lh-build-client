import { HubContainer } from './hub-container';
import { TransferMessageReciver } from './transfer-message-reciver';

export class MessageCallback {
  public static Ins = new MessageCallback();
  private hubs: HubContainer[];
  constructor() {
    this.hubs = new Array<HubContainer>();
  }

  public regist(name: string, hub: any) {
    const h = new HubContainer();
    const a =
      this.hubs.filter((res) => {
        return res.name === name;
      }).length > 0;
    if (!a) {
      h.name = name;
      h.hub = hub;
      this.hubs.push(h);
    }
  }

  public notice(msg: any): any {
    const message: TransferMessageReciver<any> = msg;
    const name = message.hub;
    if (this.hubs != null) {
      const currentHub = this.hubs.filter((res) => {
        return res.name === name;
      })[0];
      const method = message.method;
      if (currentHub) {
        currentHub.hub[method](message.args);
      }
    }
  }
}
