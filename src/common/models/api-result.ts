/**
 * api操作返回的参数
 *
 * @export
 * @class ApiResult
 * @template T 自定义param
 */
export class ApiResult<T> {
  /**
   * 返回的结果
   *
   * @type {boolean}
   * @memberof ApiResult
   */
  result!: boolean;

  /**
   * 返回的传参
   *
   * @type {T}
   * @memberof ApiResult
   */
  param!: T;
}

