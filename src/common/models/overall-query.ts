import { OverAllQueryEnums } from '@/common/enums/over-all-query-enums.ts';
export class OverAllQueryModel {
  // 创建时间
  startTime!: string;
  // 标题 （短信邮件内容就写标题，工单就写类型）
  title!: string;
  // 关联id
  id!: string;
  // 关联类型
  overAllQueryEnums!: OverAllQueryEnums;
  // 对象
  obj!: any;
}
