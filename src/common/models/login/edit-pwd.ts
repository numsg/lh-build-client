/**
 * 修改密码
 */
export class EditPwd {
    username!: string;
    oldPassword!: string;
    newPassword!: string;
    confirmPassword!: string;
}
