/**
 * 分页数据
 *
 * @export
 * @interface PageData
 * @template T
 */
export interface PageData<T> {
  /**
   * 当前页数
   *
   * @type {number}
   * @memberof PageData
   */
  currentPage: number;

  /**
   * 每页大小
   *
   * @type {number}
   * @memberof PageData
   */
  pageSize: number;

  /**
   * 总数
   *
   * @type {number}
   * @memberof PageData
   */
  totalCount: number;

  /**
   * 总页数
   *
   * @type {number}
   * @memberof PageData
   */
  totalPage: number;

  /**
   * 数据
   *
   * @type {T[]}
   * @memberof PageData
   */
  data: T[];
}
