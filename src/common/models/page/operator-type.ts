/**
 * 条件运算符
 */

export const enum OperatorType {
  // 相等
  EQUAL = 0,

  // 小于
  LESS_THAN = 1,

  // 小于等于
  LESS_THAN_OR_EQUAL_TO = 2,

  // 大于
  GREATER_THAN = 3,

  // 大于等于
  GREATER_THAN_OR_EQUAL_TO = 4,

  // like
  LIKE = 5,

  // 以什么开始
  START_WITH = 6,

  // 包含
  CONTAINS = 7,

  // 在指定的数组值里面
  IN = 8,

  // 为NULL
  ISNULL = 9,

  // 不为NULL
  ISNOTNULL = 10
}
