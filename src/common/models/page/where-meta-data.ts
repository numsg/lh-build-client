import { OperatorType } from './operator-type';

/**
 * 查询条件对象
 * @class WhereMetaData
 */
export class WhereMetaData {
  /**
   * 查询操作符合
   */
  public operatorType?: OperatorType;

  /**
   * 查询的属性名称
   */
  public propertyName?: string;

  /**
   * 查询的具体值(可用'||'分隔，表示IN操作情况)
   */
  public searchTerm?: string;

  public predicateRelation?: any;
}
