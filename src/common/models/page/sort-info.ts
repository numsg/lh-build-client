/**
 * 排序实体
 * @class SortInfo
 */

export class SortInfo {
  // 是否正序
  asc?: boolean;

  // 排序的属性名
  name?: string;

  // 排序顺序索引
  sortIndex?: number;

}
