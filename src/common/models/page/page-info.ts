import {WhereMetaData} from './where-meta-data';
import {SortInfo} from './sort-info';

export default class PageInfo {
  public pageSize!: number;
  public pageIndex!: number;
  /**
   * 排序集合
   */
  public sortList?: SortInfo[];
  /**
   * 查询条件集合
   */
  public whereMetaDatas?: WhereMetaData[];
  constructor( size?: number, index?: number ) {
      this.pageSize = size || 5;
      this.pageIndex = index || 0;
  }
}
