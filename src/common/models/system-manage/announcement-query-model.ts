/**
 * 公告查询参数
 */
export class AnnouncementQueryModel {
    public announcementTitle!: string;
    public annDirectoryId!: string;
    public userId!: string;
    public sendState!: number;
    public announcementState!: string;
    public sort!: string[][];
    public pageIndex!: number;
    public pageSize!: number;

    constructor() {
        this.announcementTitle = '';
        this.annDirectoryId = '';
        this.userId = '';
        this.announcementState = '';
        this.sendState = 1;
        this.sort = [];
        this.pageIndex = 0;
        this.pageSize = 13;
    }
}
