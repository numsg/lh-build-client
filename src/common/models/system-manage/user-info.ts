export class UserInfo {
    public id!: string;

    public structNodeId!: string;

    public orgName!: string;

    public username!: string;

    public password!: string;

    public userCode!: string;

    public name!: string;

    public phone?: string;

    public email?: string;

    public address?: string;

    public createdTime?: string;

    public userRoleRelations!: any[];

    public userImage?: string;

    public position?: string;

    public roleId!: string;

    public roleName!: string;

    public mobilePhone?: string;

    public remark?: string;

    public gender!: number;

    public seatGroup?: string;

    public autoConfig!: string;

    constructor() {
        this.id = '';

        this.structNodeId = '';

        this.orgName = '';

        this.username = '';

        this.password = '';

        this.userCode = '';

        this.name = '';

        this.phone = '';

        this.email = '';

        this.address = '';

        this.createdTime = '';

        this.userRoleRelations = [];

        this.userImage = '';

        this.position = '';

        this.roleId = '';

        this.roleName = '';

        this.mobilePhone = '';

        this.remark = '';

        this.gender = 1;

        this.seatGroup = '';

        this.autoConfig = '0';
    }
}
