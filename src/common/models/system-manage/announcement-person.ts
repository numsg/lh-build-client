export default class AnnouncementPerson {
    id?: string;
    announcementId!: string;
    userId!: string;
    sendState!: number;
}
