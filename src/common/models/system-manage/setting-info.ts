/**
 * app参数配置
 */
export class SettingInfo {
  /**
   * 配置默认值
   */
  defaultValue!: string;
  /**
   * 描述
   */
  description!: string;
  /**
   * 配置项名称
   */
  name!: string;
  /**
   * 配置类型ID
   */
  paramType!: any;

  /**
   * 配置Id
   */
  settingId = '';
  /**
   * 配置项的值
   */
  value!: string;
}
