export class OrgStructure {
  public structNodeId!: string;
  public name!: string;
  public parentStructNodeId!: string;
  public parentStructName?: string;
  public type?: string;
  public isDelete?: boolean;
  public children?: [];
  public orderTypes?: string[];
  public structureOrderTypeRelations!: any[];
  public showDisabled = false;
  public totalNum!: number;
  constructor() {
    this.children = [];
    this.isDelete = false;
    this.name = '';
    this.orderTypes = [];
    this.parentStructName = '';
    this.parentStructNodeId = '';
    this.showDisabled = false;
    this.structNodeId = '';
    this.structureOrderTypeRelations = [];
    this.type = '';
    this.totalNum = 0;
  }
}
