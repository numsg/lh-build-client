export default class ServiceHour {
    id!: string;
    title!: string;
    startTime!: string;
    endTime!: string;
    creator!: string;
    createTime!: string;
    updateTime?: string;
}
