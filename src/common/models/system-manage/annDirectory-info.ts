export default class AnnDirectoryInfo {
  id!: string;
  name!: string;
  description?: string;
  parentId!: string;
  parentName?: string;
  createTime: any;
  creator?: string;
  creatorId?: string;
  children!: [];
}
