export default class AnnouncementInfo {
  id?: string;
  title!: string;
  content?: string;
  directoryId!: string;
  directoryName?: string;
  creatorId?: string;
  creator?: string;
  createTime?: any;
  // 过期时间
  expiredTime?: any;
  attachmentId = '';
  enable = '1';
  status = '1';
  announcementDirectoryEntity: any;
  publish?: boolean;
  announcementPersons!: any[];
}
