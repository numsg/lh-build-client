/**
 * 用户查询对象
 */
export class UserQueryModel {
    /**
     * 员工编号
     */
    public usercode!: string;
    /**
     * 姓名
     */
    public username!: string;
    /**
     * 机构ID
     */
    public structNodeId!: string;
    /**
     * 席位号码
     */
    public fixNumber!: string;
    /**
     * 角色ID
     */
    public roleId!: string;
    /**
     * 页码
     */
    public pageIndex!: number;
    /**
     * 每页条数
     */
    public pageSize!: number;
    /**
     * 筛选
     */
    public sort!: string[][];
    constructor() {
        this.usercode = '';
        this.username = '';
        this.structNodeId = '';
        this.fixNumber = '';
        this.roleId = '';
        this.pageIndex = 0;
        this.pageSize = 13;
        this.sort = [];
    }
}
