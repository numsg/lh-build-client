import { RoleInfo } from './role/role-info';

export class LoginInfo {
    public id!: string;

    public structNodeId!: string;

    public orgName!: string;

    public username!: string;

    public password?: string;

    public name!: string;

    public userCode!: string;

    public phone!: string;

    public currentRole!: RoleInfo;

    public userImage?: any;

    // 电话状态
    public phoneStatus?: string;
    // 电话状态改变时间
    public phoneStatusTime?: string;
}
