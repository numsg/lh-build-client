


/**
 * 席位注册信息
 */
export class SeatStatus {
  /**
   * 注册ID=用户ID
   */
  registerId!: string;
  /**
   * 人员姓名
   */
  orgPersonName!: string;
  /**
   * 席位号码
   */
  phone!: string;
  /**
   * 席位组
   */
  seatGroup!: string;
  /**
   * 自动话后配置0-关闭，1-仅呼入，2-仅呼出，3-全部
   */
  autoConfig!: string;
  /**
   * 用户名
   */
  username!: string;
  /**
   * 组织机构ID
   */
  structNodeId!: string;
  /**
   * 机构名称
   */
  orgName!: string;
  /**
   * 角色ID
   */
  roleId!: string;
  /**
   * 角色名
   */
  roleName!: string;
  /**
   * 员工编号CODE
   */
  userCode!: string;
  /**
   * 电话状态改变时间
   */
  phoneStatusTime!: string;
  /**
   * 电话状态持续时间
   */
  phoneStatusDuration!: string;

  constructor() {
    this.registerId = '';
    this.orgPersonName = '';
    this.phone = '';
    this.autoConfig = '0';
    this.seatGroup = '';
    this.phoneStatusTime = '';
    this.username = '';
    this.structNodeId = '';
    this.orgName = '';
    this.roleId = '';
    this.roleName = '';
    this.userCode = '';
    this.phoneStatusDuration = '';
  }
}
