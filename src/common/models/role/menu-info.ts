export class MenuInfo {
  public id!: string;

  public code!: string;

  public name?: string;

  public description?: string;

  public parentId?: string;

  public url?: string;

  public title?: string;

  public icon?: string;

  public order?: string;

  public available?: string;

  public level?: string;

  public isCheck?: boolean;
}
