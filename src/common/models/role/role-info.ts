export class RoleInfo {
  public id!: string;

  public code!: number;

  public roleName!: string;

  public description?: string;

  public bizType?: number;

  public deletable?: number;
}
