export interface FileInfo {
  id: string;
  fileId: string;
  fileName: string;
  contentType: string;
  extension: string;
  size: number;
  uploadDate: Date;
  system: string;
  module: string;
  businessId: string;
  remark: string;
  content: string;
}
