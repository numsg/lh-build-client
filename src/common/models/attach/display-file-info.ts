export class DisplayFileInfo {
  name!: string;
  id!: string;
  time!: string;
  type!: string;
  detail!: string;
  content!: string;
  fileType!: string;
  raw!: any;
  isNew!: boolean;
}
