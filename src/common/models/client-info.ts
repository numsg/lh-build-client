import { SeatStatus } from './seat-status';

export class ClientInfo {
    // 用户名
    name!: string;
    // IP地址
    ip!: string;
    // 计算机名称
    hostname!: string;
    // 席位号码
    seatNum!: string;
    // 席位信息
    registerInfo!: SeatStatus;
}
