import { UserInfo } from '../../../common/models/system-manage/user-info';
import { SeatStatus } from '../seat-status';

/**
 * 席位基本信息
 */
export default class SeatInfo {
    /**
     * ID
     */
    seatId!: string;
    /**
     * 席位名称
     */
    name!: string;
    /**
     * 机器名
     */
    computerName!: string;
    /**
     * 席位编号
     */
    seatNum!: string;
    /**
     * 本机IP
     */
    ipAddress?: string;
    /**
     * 分组号
     */
    floorNumber!: string;
    /**
     * 分组Id
     */
    floorId!: string;
    /**
     * 是否禁用，默认可用
     */
    enable = 1;
    /**
     * 关联的用户信息
     */
    userInfo!: UserInfo;
    /**
     * 注册的席位信息
     */
    registerInfo!: SeatStatus;
    /**
     * 底图位置X信息
     */
    x!: number;
    /**
     * 底图位置Y信息
     */
    y!: number;
    /**
     * 分组的信息
     */
    floorEntity: any;
    /**
     * 业务类别-未使用
     */
    seatTypeId!: number;
}
