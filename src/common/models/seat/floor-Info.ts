import SeatInfo from '../../../common/models/seat/seat-info';
/**
 * 分组信息
 */
export class FloorInfo {
    /**
     * id
     */
    id!: string;
    /**
     * 分组编号
     */
    floorNumber!: string;
    /**
     * 分组地图图片id
     */
    fileId?: string;
    /**
     * 楼层分组关联的席位信息
     */
    seatInfos!: SeatInfo[];
    /**
     * 分组颜色
     */
    floorColor!: string;
}
