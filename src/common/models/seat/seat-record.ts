
/**
 * 席位记录表
 */
export default class SeatRecord {
    /**
     * 席位记录ID
     */
    seatRecordId!: string;
    /**
     * 员工号码
     */
    userCode!: string;
    /**
     * 人员姓名
     */
    personName!: string;
    /**
     * 电话号码
     */
    phoneNumber!: string;
    /**
     * 登录时间
     */
    loginTime!: string;
    /**
     * 登出时间
     */
    loginOutTime!: string;
    /**
     * 在线时长
     */
    onlineDuration!: string;
    /**
     * 登录状态
     */
    loginStatus!: number;
    /**
     * 置忙次数
     */
    busyTimes!: number;
    /**
     * 忙碌时长
     */
    busyDuration!: string;
}
