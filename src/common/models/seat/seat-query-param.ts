/**
 * 席位查询对象
 */
export default class SeatQueryParam {
    enable?: any;
    floorId?: string;
}
