import {QueryConditionOptions} from '../../../common/models/query-condition/query-condition-options';

export enum QueryConditionType {
  Select = 'select',

  Input = 'input',

  DatePicker = 'date-picker',
}

export class QueryConditionInfo {
  /**
   * 类型
   *
   * @type {QueryConditionType}
   * @memberof QueryConditionInfo
   */
  type!: QueryConditionType;

  /**
   * 名称
   *
   * @type {string}
   * @memberof QueryConditionInfo
   */
  label!: string;

  /**
   * 绑定的属性
   *
   * @type {string}
   * @memberof QueryConditionInfo
   */
  key!: string;

  /**
   * 绑定的属性值
   *
   * @type {*}
   * @memberof QueryConditionInfo
   */
  value!: any;

  /**
   * select 选项
   *
   * @type {Array<QueryConditionOptions<any>>}
   * @memberof QueryConditionInfo
   */
  options!: Array<QueryConditionOptions<any>>;

  /**
   * sort 排序
   *
   * @type {number}
   * @memberof QueryConditionInfo
   */
  sort!: number;


  setType(type: QueryConditionType) {
    this.type = type;
    if ( type === QueryConditionType.Select ) {
      this.setOptions([]);
    }
    return this;
  }

  setLabel(label: string) {
    this.label = label;
    return this;
  }

  setSort(sort: number) {
    this.sort = sort;
    return this;
  }

  setKey(key: string) {
    this.key = key;
    return this;
  }

  setOptions(options: Array<QueryConditionOptions<any>>) {
    this.options = options;
    return this;
  }

  setValue(value: any) {
    this.value = value;
    return this;
  }

}
