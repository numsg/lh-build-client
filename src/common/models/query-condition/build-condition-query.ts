import { QueryConditionInfo, QueryConditionType } from '../../../common/models/query-condition/query-condition-info';
import {QueryConditionOptions} from '../../../common/models/query-condition/query-condition-options';

export class BuildConditionQuery  {
  [index: string]: any;

  init() {
    for ( const key of Object.keys(this) ) {
      const newKey = `_${key}`;
      this[newKey] = new QueryConditionInfo()
      .setKey(key)
      .setLabel('')
      .setValue('')
      .setSort(0)
      .setType(QueryConditionType.Input);
    }
    return this;
  }


  setLabel(key: string, value: string) {
    if ( key.substr(0, 1) === '_' ) {
      throw new Error('Attribute cannot start with "_"');
    }
    const newKey = `_${key}`;
    if ( this[newKey] && this[newKey].setLabel ) {
      this[newKey].setLabel(value);
    }
    return this;
  }


  setKey(key: string, value: string) {
    if ( key.substr(0, 1) === '_' ) {
      throw new Error('Attribute cannot start with "_"');
    }
    const newKey = `_${key}`;
    if ( this[newKey] && this[newKey].setKey ) {
      this[newKey].setKey(value);
    }
    return this;
  }


  setType(key: string, value: QueryConditionType) {
    if ( key.substr(0, 1) === '_' ) {
      throw new Error('Attribute cannot start with "_"');
    }
    const newKey = `_${key}`;
    if ( this[newKey] && this[newKey].setType ) {
      this[newKey].setType(value);
    }
    return this;
  }

  setSort(key: string, sort: number) {
    if ( key.substr(0, 1) === '_' ) {
      throw new Error('Attribute cannot start with "_"');
    }
    const newKey = `_${key}`;
    if ( this[newKey] && this[newKey].setSort ) {
      this[newKey].setSort(sort);
    }
    return this;
  }

  setTime(startTime: string, endTime: string, label: string) {
    const newKey = `_${startTime}_${endTime}`;
    this[newKey] = new QueryConditionInfo()
    .setKey(newKey)
    .setLabel(label)
    .setValue('')
    .setType(QueryConditionType.DatePicker);
    return this[newKey] as QueryConditionInfo;
  }

  setOptions(key: string, value: Array<QueryConditionOptions<any>>) {
    if ( key.substr(0, 1) === '_' ) {
      throw new Error('Attribute cannot start with "_"');
    }
    const newKey = `_${key}`;
    if ( this[newKey] && this[newKey].setOptions ) {
      this[newKey].setOptions(value);
    }
    return this[newKey];
  }
}
