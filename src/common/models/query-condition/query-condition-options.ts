export class QueryConditionOptions<T> {
  label!: string;

  value!: T;
}
