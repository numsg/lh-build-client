

export class SortParam {
  key!: string;

  sort!: string;
}

interface SortTypeType {
  ascending: string;
  descending: string;
  [index: string]: string;
}

export const SortType: SortTypeType = {
  ascending: 'asc',

  descending: 'desc',
};
