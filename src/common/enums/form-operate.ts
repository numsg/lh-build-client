export enum FormOperate {
  /**
   * 新建
   */
  Add = 'Add',

  /**
   * 修改(回复)
   */
  Edit = 'Edit',

  /**
   * 展示
   */
  Show = 'Show'
}
