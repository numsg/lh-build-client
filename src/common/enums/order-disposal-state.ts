/**
 * 工单状态
 */
export enum OrderDisposalState {
  /**
   * 暂存
   */
  Temporary = 0,

  /**
   * 处理中
   */
  Processing = 1,

  /**
   * 受理
   */
  Aceitar = 2,

  /**
   * 完结
   */
  Final = 3
}
export enum OrderDisposalStateEnum {
  /**
   * 暂存
   */
  Temporary = 'Temporary',

  /**
   * 处理中
   */
  Processing = 'Processing',

  /**
   * 受理
   */
  Aceitar = 'Aceitar',

  /**
   * 完结
   */
  Final = 'Final'
}
