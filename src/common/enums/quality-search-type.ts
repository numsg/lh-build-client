/**
 * 质检查询类型
 */
export enum QualitySearchType {
  /**
   * 质检打分
   */
  QualityScore = 'QualityScore',
  /**
   * 我的通话质检
   */
  QualityCall = 'QualityCall',
  /**
   * 申诉处理
   */
  QualityAppealProcess = 'QualityAppealProcess'
}
