/**
 * 工单类型
 */
export enum OrderType {
  /**
   * 投诉
   */
  Complaint = 0,

  /**
   * 建议
   */
  Suggest = 1,

  /**
   * 咨询
   */
  Advisory = 2,

  /**
   * 举报
   */
  Report = 3,

  /**
   * 海关
   */
  Customs = 4
}
export enum OrderTypeEnum {
  /**
   * 投诉
   */
  Complaint = 'Complaint',

  /**
   * 建议
   */
  Suggest = 'Suggest',

  /**
   * 咨询
   */
  Advisory = 'Advisory',

  /**
   * 举报
   */
  Report = 'Report',

  /**
   * 海关
   */
  Customs = 'Customs'
}
