export enum RoleCode {
  /**
   * 操作员
   */
  Operador = 3,
  /**
   * 组长
   */
  Supervisor = 2,
  /**
   * 主管
   */
  Coordenador = 1
}
export enum RoleCodeEnums {
  /**
   * 操作员
   */
  Operador = 'Operador',
  /**
   * 主管
   */
  Coordenador = 'Coordenador',
  /**
   * 组长
   */
  Supervisor = 'Supervisor'
}
