export enum OrderAction {
  /**
   * 暂存
   */
  Temporary = 0,

  /**
   * 直接处理
   */

  DirectProcessing = 1,

  /**
   * 流转
   */
  Transfer = 2,

  /**
   * 接单
   */
  Receive = 3,

  /**
   * 驳回
   */
  Return = 4,

  /**
   * 驳回
   */
  Reopen = 9,
}
