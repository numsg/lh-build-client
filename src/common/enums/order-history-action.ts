export enum OrderHistoryAction {
  /**
   * 暂存
   */
  Temporary = 0,

  /**
   * 直接处理
   */

  DirectProcessing = 1,

  /**
   * 流转
   */
  Transfer = 2,

  /**
   * 接单
   */
  Receive = 3,

  /**
   * 驳回
   */
  Return = 4,

  /**
   * 修改
   */
  modify = 5,

  /**
   * 发送邮件
   */
  SendEmail = 6,

  /**
   * 发送短信
   */
  SendMessage = 7,

  /**
   * 拨打电话
   */
  CallPhone = 8
}
