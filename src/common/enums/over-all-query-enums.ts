export enum OverAllQueryEnums {
  /**
   * 工单
   */
  Order = 'Order',
  /**
   * 邮件(收)
   */
  Email = 'Email',
  /**
   * 短信
   */
  Sms = 'Sms',
  /**
   * 邮件(发)
   */
  OutEmail = 'OutEmail'
}
