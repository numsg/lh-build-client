/**
 * 工单来源
 */

export enum OrderSource {
  /**
   * 语音留言
   */
  VoiceMessage = 0,

  /**
   * 即时通讯
   */

  InstantMessaging = 1,

  /**
   * 电子邮箱
   */
  EMail = 2,

  /**
   * 电话
   */
  Phone = 3,
  /**
   * Whatsapp
   */
  Whatsapp = 4,
  /**
   * Webchat
   */
  Webchat = 5
}
export enum OrderSourceEnum {
  /**
   * 语音留言
   */
  VoiceMessage = 'Mensagem de Voz',

  /**
   * 即时通讯
   */

  InstantMessaging = 'Facebook Messenger',

  /**
   * 电子邮箱
   */
  EMail = 'Email',

  /**
   * 电话
   */
  Phone = 'Telefone',
  /**
   * Whatsapp
   */
  Whatsapp = 'Whatsapp',
  /**
   * Webchat
   */
  Webchat = 'Webchat'
}
