export enum KnowledgeAuditStatus {
    /**
     * 待审核
     */
    WaitForAudit = 0,

    /**
     * 审核通过
     */

    AuditPassed = 1,

    /**
     * 审核未通过
     */
    AuditNotPassed = 2,

    /**
     * 已保存
     */
    IsSaved = 3

}
