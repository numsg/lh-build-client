/**
 * setting表type枚举
 */
export enum SettingType {
  /**
   * 接警参数
   */
  AppealParam = 0,

  /**
   * 处警参数
   */
  DispatchParam = 1,

  /**
   * EmergencyParam
   */
  EmergencyParam = 2,

  /**
   * GISParam
   */
  GISParam = 3,

  /**
   * PublicParam
   */
  PublicParam = 4,

  /**
   * CenterParam
   */
  CenterParam = 5,

  /**
   * PBXParam
   */
  PBXConfigParam = 6,

  /**
   * GPSWebServiceParam
   */
  GPSWebServiceParam = 7,

  /**
   * VideoWebServiceParam
   */
  VideoWebServiceParam = 8,

  /**
   * PDAWebServiceParam
   */
  PDAWebServiceParam = 9,

  /**
   * PanciButtonHarisParam
   */
  PanciButtonHarisParam = 10,

  /**
   * PanciButtonAntParam
   */
  PanciButtonAntParam = 11,

  /**
   * Mobile911ClientParam
   */
  Mobile911ClientParam = 12,

  /**
   * HWConfigParam
   */
  HWConfigParam = 13,

  /**
   * SeatStatusService
   */
  SeatStatusService = 14,

  /**
   * ApportionServiceParam
   */
  ApportionServiceParam = 15,

  /**
   * eLTEParam
   */
  eLTEParam = 16,

  /**
   * MobileAppAlarmParam
   */
  MobileAppAlarmConfigParam = 17,

  /**
   * 快速调派参数
   */
  QuickDispatchParam = 18,

  /**
   * 方案推荐参数
   */
  SchemeRecommendParam = 19,

  /**
   * 系统推荐参数
   */
  SystemRecommendParam = 20,

  /**
   * 警情处置参数
   */
  IncidentDisposalParam = 21,

  /**
   * 资源跟踪参数
   */
  ResourceTrackParam = 22,

  /**
   * 席位监控参数
   */
  SeatMonitoringParam = 23,

  /** 资源管理参数 */
  ResourceManagementParam = 24,

  /** APP参数 */
  AppParam = 25,

  /** 系统管理参数 */
  SystemParam = 26,

  /**  最大时间限制参数 */
  MaxTimeLimit = 27,

  /**
   * 电话配置
   */
  SoftPhoneParam = 28
}
