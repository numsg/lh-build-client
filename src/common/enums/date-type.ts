/**
 * 日期类型枚举
 */
export enum DateType {
    /**
     * 小时
     */
    Hour = 'hour',

    /**
     * 日
     */
    Day = 'day',

    /**
     * 月
     */
    Month = 'month',

    /**
     * 年
     */
    Year = 'year'
}
