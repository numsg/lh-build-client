export enum EmailSearchType {
  /**
   * 收件箱
   */
  InBoxSearch = 'InBoxSearch',

  /**
   * 发件箱
   */
  OutBoxSearch = 'OutBoxSearch',

  /**
   * 待处理邮件
   */
  WaitingBoxSearch = 'WaitingBoxSearch'
}
