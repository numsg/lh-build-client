/**
 * 邮件类型
 */
export enum EmailType {
  /**
   * 新增邮件
   */
  NewEmail = 'NewEmail',
  /**
   * 回复邮件
   */
  ReplyEmail = 'ReplyEmail',
  /**
   * 转发收件箱（包括收件箱，待处理列表）
   */
  TranspondInEmail = 'TranspondInEmail',
  /**
   * 转发发件箱
   */
  TranspondOutEmail = 'TranspondOutEmail'
}
