import distanceInWordsFn from 'date-fns/distance_in_words';
import distanceInWordsToNowFn from 'date-fns/distance_in_words_to_now';
import distanceInWordsStrictFn from 'date-fns/distance_in_words_strict';
import zhLocale from 'date-fns/locale/zh_cn';
import enLocale from 'date-fns/locale/en';
import ptLocal from 'date-fns/locale/pt';
import { format } from 'date-fns';
import store from '@/store';

/**
 * 记录日志时间
 */
export function logTime(): string {
  return formatDate(new Date(), 'YYYY-MM-DD HH:mm:ss.SSS');
}

/**
 * 日期到当前时间的距离
 * 返回结果为`大约3分钟前`，`大约1小时前`等
 *
 * @param date 日期
 * @param lang 语言
 */
export function distanceInWordsToNow(date: Date | string, lang: string) {
  const local = selectLocal(lang);
  return distanceInWordsToNowFn(date, { addSuffix: true, locale: local, includeSeconds: true });
}

/**
 * 两个日期间的距离
 * 例两个时间差为1小时10分钟则会返回`大约1小时前`
 *
 * @param dateToCompare 比较日期
 * @param date 日期
 * @param lang 语言
 */
export function distanceInWords(dateToCompare: Date | string, date: Date | string, lang: string) {
  const local = selectLocal(lang);
  return distanceInWordsFn(dateToCompare, date, { addSuffix: true, locale: local, includeSeconds: true });
}

/**
 * 两个日期间的距离
 * 会舍弃掉小数
 * 例两个时间差为1小时10分钟则会返回`1小时`
 *
 * @param dateToCompare 比较日期
 * @param date 日期
 * @param lang 语言
 */
export function distanceInWordsStrict(dateToCompare: Date | string, date: Date | string, lang: string) {
  const local = selectLocal(lang);
  return distanceInWordsStrictFn(dateToCompare, date, { addSuffix: false, locale: local, partialMethod: 'floor' });
}

function selectLocal(lang: string) {
  switch (lang) {
    case 'zh-CN':
      return zhLocale;
    case 'en-US':
      return enLocale;
    case 'pt-BR':
      return ptLocal;
    default:
      return zhLocale;
  }
}
// 日期转字符串
export const ToDateString = (date: any) => {
  const seperator1 = '-';
  const seperator2 = ':';
  let month = date.getMonth() + 1;
  let strDate = date.getDate();
  let h = date.getHours();
  let m = date.getMinutes();
  let s = date.getSeconds();
  if (month >= 1 && month <= 9) {
    month = '0' + month;
  }
  if (strDate >= 0 && strDate <= 9) {
    strDate = '0' + strDate;
  }
  if (h >= 0 && h <= 9) {
    h = '0' + h;
  }
  if (m >= 0 && m <= 9) {
    m = '0' + m;
  }
  if (s >= 0 && s <= 9) {
    s = '0' + s;
  }
  const currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate + ' ' + h + seperator2 + m + seperator2 + s;
  return currentdate;
};

export const convertTimeToHMS = (time: any) => {
  const h = parseInt(time / 3600 + '', 10);
  const m = parseInt((time % 3600) / 60 + '', 10);
  const s = parseInt(((time % 3600) % 60) + '', 10);
  const hh = h < 10 ? '0' + h : h;
  const mm = m < 10 ? '0' + m : m;
  const ss = s < 10 ? '0' + s : s;
  return hh + ':' + mm + ':' + ss;
};

// 中文格式
export const FMT_CN = {
  DATETIME: 'YYYY/MM/DD HH:mm:ss',
  DATE: 'YYYY/MM/DD',
  TIME: 'HH:mm:ss',
};
// 葡语格式
export const FMT_PT = {
  DATETIME: 'DD/MM/YYYY HH:mm:ss',
  DATE: 'DD/MM/YYYY',
  TIME: 'HH:mm:ss',
};
// 英语格式
export const FMT_EN = {
  DATETIME: 'DD/MM/YYYY HH:mm:ss',
  DATE: 'DD/MM/YYYY',
  TIME: 'HH:mm:ss',
};

export const getFormat = () => {
  const lang = store.getters.language;
  switch (lang) {
    case 'zh-CN':
      return FMT_CN;
    case 'en-US':
      return FMT_EN;
    case 'pt-BR':
      return FMT_PT;
    default:
      return FMT_PT;
  }
};

/***
 * 时间格式化
 */
export const formatDate = (date: any, formatStr?: string) => {
  let time = '';
  const fmtStr = formatStr || getFormat().DATETIME;
  if (date) {
    time = format(new Date(date), fmtStr);
  }
  return time;
};

/*
* 转换成odata查询时间格式
*/
export const toOdataDate = (date: any) => {
  if (date) {
    return new Date(date).toISOString();
  }
  return '';
};

/*
* element-ui 日期格式化
*/
export const getEleUIFormat = () => {
  const fmt = getFormat();
  return {
    DATETIME: fmt.DATETIME.replace(/D/g, 'd').replace(/Y/g, 'y'),
    DATE: fmt.DATE.replace(/D/g, 'd').replace(/Y/g, 'y'),
    TIME: 'HH:mm:ss',
  };
};

