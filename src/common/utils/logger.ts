import { initLogger, LogOpts, InjectClsLog, InjectLog, logFactory, Logger } from '@gsafety/cad-glog';
import { EventType } from './ipc/ipc-event-type';

export const initLog = (nodes: string[]) => {
  const logOpts: LogOpts = {
    logToElectron: true, // 是否发送日志到Electron主进程
    electronEventName: EventType.BASE.SIGT_LOG, // 发送到electron-main主进程的事件名称
    logToElastic: true,
    elasticNodes: nodes,
    logLevel: -1, // 日志等级，对应jsnlog等级
    systemName: 'sigt-log' // TODO edit 记录系统名称
  };
  initLogger(logOpts);
};

const gblog = logFactory.getLogger('sigt-log');
export { logFactory, gblog, Logger, InjectClsLog, InjectLog };
