import i18n from '@/i18n/index';
import papa, { ParseError } from 'papaparse';
import lodash from 'lodash';
import _ from 'lodash';
import { saveAs } from 'file-saver';
import format from 'date-fns/format';
import jschardet from 'jschardet';

export default {

  /**
   * 导出cvs
   */
  export(data: { value: any[], headers: string[], columns: string[], fileName: string }) {
    const { value, headers, columns, fileName } = data;
    const result = [] as any;
    value.forEach((val: any) => {
      const obj = {};
      columns.forEach((key: string, index) => {
        let colVal: any = '';
        if (key === 'gender') {
          colVal = String(val[key] === 1 ? i18n.t('SigtOrder.Men') : i18n.t('SigtOrder.Women'));
        } else if (val[key] === 0 || typeof val[key] === 'number') {
          colVal = val[key];
        } else {
          // 内容为全数字的加上制表符限制在编辑器中显示为科学计数格式数字
          // tslint:disable-next-line: quotemark

          // colVal = /^\d+$/.test(val[key]) ? "\t" + val[key] : val[key];
          colVal = !val[key] ? '' : '\t' + val[key];
        }
        const column = {
          [`col${index}`]: colVal
        };
        Object.assign(obj, column);
      });
      result.push(obj);
    });
    let dataStr = '';
    const newline = value.length > 0 ? '\r\n' : '';
    const delimiter = ',';
    if (!lodash.isEmpty(headers)) {
      dataStr += headers.join(delimiter) + newline;
    }
    dataStr += papa.unparse(result, { header: false, delimiter });
    dataStr = '\ufeff' + dataStr; // 解决中文乱码问题
    const blob = new Blob([dataStr], { type: 'text/csv,charset=utf-8' });
    const filename = fileName.endsWith('.csv') ? `${fileName.split('.csv')[0]}${format(new Date(), 'YYYYMMDDHHmmss')}.csv` : `${fileName}${format(new Date(), 'YYYYMMDDHHmmss')}.csv`;
    saveAs(blob, filename.replace(/\s/g, ''), { autoBom: true });
  },

  /**
   * 导入csv文件，完成数据转换
   * @param {File} file
   * @param {string[]} columns
   * @returns {Promise<any>}
   */
  async import(file: File, columns: string[]): Promise<any> {
    return new Promise((resolve, reject) => {
      const fileReader = new FileReader();
      fileReader.readAsDataURL(file);
      fileReader.onload = (evt) => {
        if (evt.target) {
          const fileData = evt.target.result;
          const theEncoding = this.cheeckEncoding(fileData);
          const delimiter = ',';
          papa.parse(file, {
            delimiter,
            encoding: theEncoding,
            newline: '\r\n',
            complete: async (res: any) => {
              const rows = res.data as any[];
              rows.shift();
              let dataSource = [] as any[];
              const lastRow = rows[rows.length - 1];
              // 去掉最后一行为空的数据，导出文件转换成csv导致的
              if (lastRow && lastRow.length === 1 && _.isEmpty(lastRow[0])) {
                rows.pop();
              }
              // 把导入的数据转成传入的字段对应的对应
              rows.forEach((item: any, index) => {
                const data = {} as any;
                columns.forEach((key: any, i) => {
                  data[key] = item[i] ? item[i].trim() : item[i];
                });
                dataSource.push(data);
              });
              dataSource = dataSource.filter((e: any) => !_.isEmpty(e));
              resolve(dataSource);
            },
            error: (error: ParseError) => {
              reject(error);
            }
          });
        }
      };
    });
  },
  /**
   * 检查编码格式
   * @param bese64Str 读取数据
   */
  cheeckEncoding(bese64Str: any) {
    const str = atob(bese64Str.split(';base64,')[1]);
    const encoding = jschardet.detect(str).encoding;
    if (encoding === 'windows-1252') {
      return 'ANSI';
    }
    return encoding;
  },
};
