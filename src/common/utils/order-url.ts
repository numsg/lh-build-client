// 工单API
export const OrderUrl = {
  // 工单处理
  dispose: '/order/dispose',
  // 工单类型
  orderTypes: '/order/types/lang/{lang}',
  // 根据部门ID与语言获取组织机构工单类型列表
  orderTypesByDepIdAndLang: '/api/v1/structures/getOrderTypeByIdAndLang/{id}/{lang}',
  // 工单来源
  orderResource: '/order/sources/lang/{lang}',
  // 根据父id查询工单问题类型
  getProblemTypeByParentId: '/order/problem-types/parentId/{parentId}/lang/{lang}',
  // 查询父级问题大类（业务类型）
  getAllParentProblems: '/order/problem-types/parent/lang/{lang}',
  // 查询父级问题大类（业务类型,特性）
  getAllParentProblemsByFeature : '/order/problem-types/orderTypeId/feature/{lang}',
  // 查询自己问题小类（问题类型）
  getAllChildProblems: '/order/problem-types/sub/lang/{lang}',
  // 获取所有的工单状态
  getAllOrderStates: '/order/states/lang/{lang}',
  // 通过客户id获取工单
  getByCustomerId: '/order/customerId/{0}',
  // 工单动作
  orderActions: '/order/actions/lang/{lang}',
  // 通过工单id删除
  deleteOrder: '/order/id/{id}',
  // 通过工单Id获取工单信息
  getByOrderId: '/order/id/{id}',
  // 根据工单类型查询工单问题类型
  getProblemTypeByOrderTypeId: '/order/problem-types/order-type-id/{orderTypeId}/lang/{lang}',
  // 查詢所有的工单问题类型
  // GET /order/problem-types/lang/{lang}查詢所有的工单问题类型
  getOrderAllProblemTypesByLang: '/order/problem-types/lang/{lang}',

  // 通过工单id删除
  deleteOrderTypeById: '/order/types/delete/{id}',
  // /order/types/add/lang新增工单类型
  addOrderTypeWithLang: '/order/types/add/lang',
  // /order/types/modify/lang 修改工单类型
  modifyOrderTypeWithLang: '/order/types/modify/lang',
  // 创建工单类型
  addOrderProblemWithLang: '/order/problem-types/created',
  // 创建工单问题类型
  addOrderSubProblemWithLang: '/order/problem-types/sub/created',
  // 根据语言与名称查询是否存在
  // /order/problem-types/check/name/{lang}/{name}
  checkName: '/order/problem-types/sub/check/name',
  // 修改工单问题类型
  modifyOrderProblemWithLang: '/order/problem-types/modify',
  // 修改工单业务类型
  modifyOrderSubProblemWithLang: '/order/problem-types/sub/modify',
  // deleteOrderSubProblemById
  deleteOrderSubProblemById: '/order/problem-types/delete/{id}',
  // 维护工单类型与业务类型关系
  modifyOrderTypeAndProblemRelated: '/order/problem-types/related',
};
