/**
 * 配置文件初始化
 */
import axios from 'axios';


// const isDev = process.env.NODE_ENV === 'development';
const configPath = './config.json';

const configDataPath = './data.json';

const Config = async (store: any) => {
  const configs = await axios.get(configPath);
  await store.dispatch('setConfigs', configs.data);
  const dataConfigs = await axios.get(configDataPath);
  await store.dispatch('setDataConfigs', dataConfigs.data);
};

export default Config;
