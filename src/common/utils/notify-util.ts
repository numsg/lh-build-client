import { ElNotificationOptions } from 'element-ui/types/notification';
import { Notification as notify } from 'element-ui';
import { VNode } from 'vue';
import { LocaleMessages } from 'vue-i18n';

export default {

    // tslint:disable-next-line: max-line-length
    default(message: string | LocaleMessages | VNode, title?: string, type?: 'success' | 'warning' | 'info' | 'error', closeCallback?: () => void, customClass?: string, iconClass?: string) {
        notify(this.buildNoticeMessage(message, title, type, closeCallback, customClass, iconClass));
    },

    success(message: string | LocaleMessages, title?: string, closeCallback?: () => void, customClass?: string, iconClass?: string) {
        this.default(message, title, 'success', closeCallback, customClass, iconClass);
    },

    warning(message: string | LocaleMessages, title?: string, closeCallback?: () => void, customClass?: string, iconClass?: string) {
        this.default(message, title, 'warning', closeCallback, customClass, iconClass);
    },

    info(message: string | LocaleMessages, title?: string, closeCallback?: () => void, customClass?: string, iconClass?: string) {
        this.default(message, title, 'info', closeCallback, customClass, iconClass);
    },

    error(message: string | LocaleMessages, title?: string, closeCallback?: () => void, customClass?: string, iconClass?: string) {
        this.default(message, title, 'error', closeCallback, customClass, iconClass);
    },

    /**
     * 构建界面操作的通知消息
     * @param title 消息标题
     * @param message 消息内容
     * @param type 消息类型
     * @param closeCallback 关闭的回调
     * @param customClass 自定义类名
     * @param iconClass 图标类名
     */
    // tslint:disable-next-line: max-line-length
    buildNoticeMessage(message: string | LocaleMessages | VNode, title?: string, type?: 'success' | 'warning' | 'info' | 'error' | undefined, closeCallback?: () => void, customClass?: string, iconClass?: string): ElNotificationOptions {
        return {
            title,
            message,
            type,
            onClose: closeCallback,
            customClass,
            iconClass,
            duration: 3000,
            position: 'top-right',
            offset: 120,
            showClose: false,
        } as ElNotificationOptions;

    }
};
