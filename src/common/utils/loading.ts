
import { Loading } from 'element-ui';
import i18n from '@/i18n';
/**
 * Loading帮助类
 * 使用方法：
 * 在视图.ts文件中导入showLoading、hideLoading
 * showLoading()参数:Dom节点、key
 * Dom节点：采用$.refs.* 方式获取dom；
 * 注：.vue文件 ref 属性不要放在element组件内部，element组件获取的dom节点不是正常的dom节点
 *
 * Key: 标识Loading的属性，将不同的dom下的loading互相隔离, 需要保证标识的唯一性；
 */

const loading: any = {};

const startLoading = (dom: any, key: any) => {
  if (loading[key]) {
    loading[key].close();
    loading[key] = undefined;
  }
  if (dom) {
    loading[key] = Loading.service({
      target: dom,
      lock: true,
      text: i18n.t('SigtCommon.Loading').toString(),
      spinner: 'el-icon-loading',
    });
  }
};

const endLoading = (key: any) => {
  if (loading[key]) {
    loading[key].close();
    loading[key] = undefined;
  }
};

export const showLoading = (dom: any, tips: any) => {
  startLoading(dom, tips);
};

export const hideLoading = (key: string) => {
  endLoading(key);
};


