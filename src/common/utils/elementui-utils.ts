/**
 * select 选项等宽，并且超过文字用... title提示
 */
const setElSelectWidthTitle = () => {
  // 修复span太长bug
  const list: any = document.querySelectorAll('.el-select__tags-text');
  if (list.length) {
    list.forEach((item: any) => {
      item.setAttribute('title', item.innerText);
      item.setAttribute('style', 'display: inline-block;max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;');
    });
  }
  // 修复删除按钮往下的bug
  const closeList: any = document.querySelectorAll('.el-tag__close,.el-icon-close');
  if (closeList.length) {
    closeList.forEach((item: any) => {
      item.setAttribute('style', 'top: -7px');
    });
  }
};

/**
 * 单选的select加title
 */
const setElSelectTitle = () => {
  // 修复span太长bug
  const list: any = document.querySelectorAll('.el-select-dropdown__item > span');
  if (list.length) {
    list.forEach((item: any) => {
      item.setAttribute('title', item.innerText);
    });
  }
};

/**
 * 设置el-table头部的标题title元素
 */
const setElTableHeadTitle = () => {
  const list: any = document.querySelectorAll('th,.cell');
  if (list.length) {
    list.forEach((item: any) => {
      item.setAttribute('title', item.innerText);
    });
  }
};

export { setElSelectWidthTitle, setElTableHeadTitle, setElSelectTitle };
