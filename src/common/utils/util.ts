/**
 * 延时
 * @param time
 */
export const sleep = (time: number) => new Promise((resolve: any) => setTimeout(resolve, time));

export const arrayToTree = (data: any[], parentId?: string, key = 'id', parentKey = 'parentId') => {
    const tree: any[] = [];
    let temp;
    if (data && data.length > 0) {
        for (const obj of data) {
            if (obj[parentKey] === '-1' || obj[parentKey] === parentId) {
                const children = data.filter((e: any) => e[parentKey] !== '-1' && e[parentKey] !== parentId);
                temp = arrayToTree(children, obj[key], key, parentKey);
                if (temp && temp.length > 0) {
                    obj.children = temp;
                }
                tree.push(obj);
            }
        }
    }
    return tree;
};

export const treeToArray = (nodes: any, childKey: string = 'children') => {
    if (!nodes) {
        return [];
    }
    let res: any[] = [];
    // const childKey = 'children';
    if (nodes instanceof Array) {
        for (const item of nodes) {
            const node: any = {};
            for (const key in item) {
                if (key !== childKey) {
                    node[key] = item[key];
                }
            }
            node[childKey] = [];
            res.push(node);
            if (item[childKey] && item[childKey].length > 0) {
                res = res.concat(treeToArray(item[childKey], childKey));
            }
        }
    } else {
        res.push(nodes);
        if (nodes[childKey]) {
            res = res.concat(treeToArray(nodes[childKey], childKey));
        }
    }
    return res;
};
