import BuildingMaterialInspection from '@/components/template-library/building-material-inspection/building-material-inspection';
import MaterialInspectionTemp from '@/components/template-library/force-value/force-value';
import TrialTemplateResult from '../../components/template-library/trial-template-result/trial-template-result';
import ForceValue from '@/components/template-library/force-value/force-value';
import ThermalConductivityMeter from '../../components/template-library/thermal-conductivity-meter/thermal-conductivity-meter';


export default {
  getTemplateListSource() {
    const templateList: any = [
      // {
      //   id: 'building-material-inspection',
      //   name: '压力机数据记录模板',
      //   component: BuildingMaterialInspection
      // },
      // {
      //   id: 'material-inspection-temp',
      //   name: 'XXXXXX检验记录',
      //   component: MaterialInspectionTemp
      // },
      {
        id: 'pulling-force',
        name: '拉力试验',
        title: '拉力试验',
        type: 1,
        component: TrialTemplateResult
      },
      {
        id: 'anti-folding',
        name: '抗折试验',
        title: '抗折试验（压力机）',
        type: 2,
        component: TrialTemplateResult
      },
      {
        id: 'anti-pressure',
        name: '抗压试验',
        title: '抗折试验（压力机）',
        type: 3,
        component: TrialTemplateResult
      },
      {
        id: 'thermal-conductivity-meter',
        name: '导热系数仪试验模板',
        title: '导热系数仪试验模板',
        type: 4,
        component: ThermalConductivityMeter
      },
      {
        id: 'force-value',
        name: '力值试验',
        title: '力值试验',
        type: 5,
        component: ForceValue
      },
    ];
    return templateList;
  }
};
