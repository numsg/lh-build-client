import i18n from '@/i18n/index';
/**
 * 截取字符串
 * @param str
 * @param len
 */
const subLongStr = (str: string, len: number) => {
  if (str && str.length > len) {
    str = str.slice(0, len) + '...';
  }
  return str;
};

const sortStr = (a: string | undefined, b: string | undefined): number => {
  if (a && b) {
    if (i18n.locale && i18n.locale === 'zh-CN') {
      return a.localeCompare(b, 'zh');
    } else {
      return a.localeCompare(b);
    }
  } else if (!a && b) {
    return -1;
  } else if (a && !b) {
    return 1;
  } else {
    return 0;
  }
};

// 首字母大写

const firstWordToUpperCase = (words: string) => {
  return words.slice(0, 1).toUpperCase() + words.slice(1);
};

export { subLongStr, sortStr, firstWordToUpperCase };
