// Electron壳和调度台进程之间的消息通知
export const EventType = {
  BASE: {
    SIGT_LOG: 'sigt-log', // 日志
    APP_EXIT: 'app_exit', // 应用程序退出
    EXIT_PROCESS: 'exit_process', // 进程退出
    LOGIN_SUCCESS: 'login_success', // 登录成功
    LOGOUT: 'logout', // 登出
  },
  OTHERS: {
    OPEN_MONITOR_WINDOW: 'open_monitor_window', // 打开大屏监控窗口
    CLOSE_MONITOR_WINDOW: 'close_monitor_window', // 关闭大屏监控窗口
    EXIT_MONITOR_WINDOW_PROCESS: 'exit_monitor_window_process', // 关闭大屏监控进程
    DOWNLOAD_FILE_COMPLETED: 'download_file_completed', // 文件下载完成
    OPEN_FILE_ADDRESS: 'open_file_address', // 打开文件下载位置,
    PRINTER_INFO: 'printer_info', // 打印返回信息
  },
  // 切换事件
  SWITCH: {
    CHANGE_LANGUAGE: 'change_language', // 语言切换
    CHANGE_MONITOR_SCREEN_INTERVAL: 'change_monitor_screen_interval', // 大屏刷新时间
  },
};
