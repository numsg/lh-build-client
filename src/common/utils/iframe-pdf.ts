interface Field {
  label: string;
  value: string;
}

interface Cell {
  name: string;
  prompt?: string;
  width?: number;
  align?: string;
  padding?: number;
}


export class IframePdf {

  private content = '';

  /**
   * 每页最大高度
   */
  private maxHeight: number;

  /**
   * 每页最大宽度
   */
  private maxWidth: number;

  private readonly A4_HEIGHT = 840;
  private readonly A4_WIDTH = 595;
  private readonly A4_WIDTH_PX = 740;
  private readonly pagePadding = 20;
  private readonly linePadding = 4;

  private readonly exact = `-webkit-print-color-adjust: exact;color-adjust: adjust;`;

  constructor() {
    this.maxHeight = this.A4_HEIGHT - (this.pagePadding * 2);
    this.maxWidth = this.A4_WIDTH - (this.pagePadding * 2);
  }

  /**
   * 添加标题
   *
   * @param {string} text 标题
   * @param {number} [size=30] 字体大小
   * @param {string} [color='#222222'] 颜色
   * @memberof PdfHelper
   */
  addTitle(
    text: string,
    size: number = 22,
    color: string = '#222222'
  ) {
    const title = `
      <div>
        <div style="color: ${color};
        text-align: center;
        font-size: ${size}px;
        font-weight: bold;">${text}</div>
        <div style="background: #2C62AC;
        width: 100%;
        height: 2px;
        margin-top: 10px;
        word-wrap: break-word;
        margin-bottom: 10px;${this.exact}"></div>
      </div>
    `;
    this.content += title;
    return this.content;
  }

  addSubTitle(
    text: string,
    size: number = 18,
    color: string = '#222222'
  ) {
    const subTitle = `
      <div
      style="color: ${color};
      font-size: ${size}px;
      font-weight: bold;
      margin-bottom: 24px;
      margin-top: 14px;
      word-wrap: break-word;
      position: relative;
      ${this.exact}
      ">
        <span style="width: 7px;
        height: 20px;
        left: 1px;
        display: inline-block;
        vertical-align: top;
        background: #2C62AC;${this.exact}"></span>
        ${text}
      </div>
    `;
    this.content += subTitle;
    return this.content;
  }


  addField(field: Field, size: number = 14, labelColor: string = '#222222', valueColor: string = '#666666') {
    const fieldContent =  `
      <div style="width:48%;
      margin-bottom: 10px;
      margin-left: 10px;
      word-wrap: break-word;
      display: inline-block;font-size: ${size}; vertical-align: top;${this.exact}">
        <span style="color: ${labelColor};${this.exact}">${field.label} :</span><span style="color: #555;${this.exact}"> ${field.value}</span>
      </div>
    `;
    this.content += fieldContent;
    return this.content;
  }

  addTable(data: any[][], headers: string[]) {

    const header = headers.map( (head) => {
      return `<div style="
        display: inline-block;
        width: 20%;
        color: #fff;
        padding: 10px;
        font-size: 16px;
        text-align: left;
        box-sizing: border-box;
        word-wrap: break-word;
        vertical-align: top;
        margin: 0;${this.exact}">${head}</div>`;
    }).join('');

    const columns = data.map((column, index) => {
      return `
        <div style="background: ${ index % 2 === 1 ? '#ccc' : '#fff'};${this.exact}">
          ${ column.map( (columnItem) => {
            return `<div style="
            display: inline-block;
            vertical-align: top;
            width: 20%;
            padding: 10px;
            font-size: 16px;
            text-align: left;
            word-wrap: break-word;
            box-sizing: border-box;
            ${this.exact}
             }
            ">${columnItem}</div>`;
          }).join('') }
     </div> `;
    }).join('');

    const table = `
      <div>
        <div class="th" style="background-color: #2C62AC;word-wrap: break-word;${this.exact}">
          ${header}
        </div>
        <div class="content">
          ${columns}
        </div>
      </div>
    `;
    this.content += table;
    return this.content;
  }

  addLine(
    height: number = 20
  ) {
    const line = `
      <div style="height: ${height}px"></div>
    `;
    this.content += line;
    return this.content;
  }

  addNoData(
    text: string,
    size: number = 14,
    color: string = '#222'
  ) {
   const noData = `
    <div style="color: ${color};
    font-size: ${size}px;
    text-align: center;
    margin: 38px 0;${this.exact}">${text}</div>
   `;
   this.content += noData;
   return this.content;
  }

  getContent() {
    return this.content;
  }
}
