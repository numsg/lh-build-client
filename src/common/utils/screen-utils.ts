/**
 * 大屏监控-工具类
 */

import { ToDateString } from '../utils/date-utils';
import { getMonth, subMonths } from 'date-fns';

// 大屏统计时间区间参数
export const toScreenTimeParam = (timeType: string, initialHour?: string) => {
  const endTime: string = ToDateString(new Date());
  let startTime = '';
  if (timeType === 'day') {
    // 当天小时区间(上班时间 - 当前时间)
    const initialDate = endTime.substring(0, 10);
    startTime = initialDate + ' ' + initialHour;
  } else if (timeType === 'month') {
    // 月份区间(1月-当前月)
    const currentMonth = getMonth(endTime);
    startTime = ToDateString(subMonths(endTime, currentMonth));
  }

  return {
    analysisStartTime: startTime,
    analysisEndTime: endTime
  };
};

// 大屏统计获得时间区域（开始时间根据人工服务动态获取）
export const getScreenTimeParam = (start: number) => {
  const endTime: string = ToDateString(new Date());
  const initialDate = endTime.substring(0, 10);
  let startTime = '';
  if (start >= 10) {
    startTime = initialDate + ' ' + start + ':00:00';
  } else {
    startTime = initialDate +  ' ' + '0' + start + ':00:00';
  }
  return {
    analysisStartTime: startTime,
    analysisEndTime: endTime
  };
};

// 转换时间为毫秒数
export const convertDurationMs = (time: string) => {
  const defaultMs: number = 10 * 1000;
  if (time && time !== 'string') {
    const period = Number.parseInt(time.slice(0, -1), 10);
    const unit = time.slice(-1);

    if (period) {
      switch (unit) {
        case 'h':
          return period * 60 * 60 * 1000;
        case 'm':
          return period * 60 * 1000;
        case 's':
          return period * 1000;
        default:
          return period * 1000;
      }
    }
  }

  return defaultMs;
};

// x轴小时格式化
export const formatXAxisValueToHm = (scope: number) => {
  const time = scope + '';
  if (time.length === 1) {
    return '0' + time + ':00';
  } else {
    return time + ':00';
  }
};
