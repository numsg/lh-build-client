
import { ElMessageOptions } from 'element-ui/types/message';
import { Message } from 'element-ui';
import { VNode } from 'vue';
import { LocaleMessages } from 'vue-i18n';

export default {

    // tslint:disable-next-line: max-line-length
    default(message: string | LocaleMessages | VNode, type?: 'success' | 'warning' | 'info' | 'error', duration?: number, offset?: number, onClose?: () => null) {
        Message(this.buildOptions(message, type, duration, offset, onClose));
    },

    success(message: string | LocaleMessages | VNode, duration?: number, offset?: number, onClose?: () => null) {
        this.default(message, 'success', duration, offset, onClose);
    },

    error(message: string | LocaleMessages | VNode, duration?: number, offset?: number, onClose?: () => null) {
        this.default(message, 'error', duration, offset, onClose);
    },
    warning(message: string | LocaleMessages | VNode, duration?: number, offset?: number, onClose?: () => null) {
        this.default(message, 'warning', duration, offset, onClose);
    },
    info(message: string | LocaleMessages | VNode, duration?: number, offset?: number, onClose?: () => null) {
        this.default(message, 'info', duration, offset, onClose);
    },
    // tslint:disable-next-line: max-line-length
    buildOptions(message: string | LocaleMessages | VNode, type?: 'success' | 'warning' | 'info' | 'error', duration?: number, offset?: number, onClose?: () => null) {
        const options = {
            message,
            type,
            duration: duration || 3000,
            onClose,
            offset
        } as ElMessageOptions;
        return options;
    }
};

