import jsPDF from 'jspdf';
import 'jspdf-autotable';


interface Field {
  label: string;
  value: string;
}

interface Cell {
  name: string;
  prompt?: string;
  width?: number;
  align?: string;
  padding?: number;
}

export class PdfHelper {

  doc: jsPDF;

  /**
   * 坐标
   */
  private x = 0;
  private y = 0;

  /**
   * 每页最大高度
   */
  private maxHeight: number;

  /**
   * 每页最大宽度
   */
  private maxWidth: number;

  private readonly A4_HEIGHT = 840;
  private readonly A4_WIDTH = 595;
  private readonly A4_WIDTH_PX = 740;
  private readonly pagePadding = 20;
  private readonly linePadding = 4;

  constructor() {
    this.doc = new jsPDF('p', 'pt', 'a4');
    this.maxHeight = this.A4_HEIGHT - (this.pagePadding * 2);
    this.maxWidth = this.A4_WIDTH - (this.pagePadding * 2);
    this.returnOrigin();
  }

  /**
   * 添加标题
   *
   * @param {string} text 标题
   * @param {number} [size=30] 字体大小
   * @param {string} [color='#222222'] 颜色
   * @memberof PdfHelper
   */
  addTitle(
    text: string,
    size: number = 16,
    color: string = '#222222'
  ) {
    // 设置字号和颜色
    this.doc.setFontSize(size);
    this.doc.setTextColor(color);

    // 根据字体大小获取行高，并换行
    const lineHeight = this.getLineHeight(size);
    this.newLine(lineHeight);

    // x移动到中间
    this.toHalfOfLine();
    // 居中添加标题
    this.doc.text(text, this.x, this.y, { align: 'center' });

    // 换行
    this.newLine(20);

    // 画标题下的横线
    this.doc.setDrawColor('#2C62AC');
    this.doc.setLineWidth(2);
    this.doc.line(this.x, this.y, this.x + this.maxWidth, this.y);
  }

  addSubTitle(
    text: string,
    size: number = 14,
    color: string = '#222222'
  ) {
    // 设置字号和颜色
    this.doc.setFontSize(size);
    this.doc.setTextColor(color);

    // 上间距
    this.newLine(10);

    // 根据字体大小获取行高，并换行
    const lineHeight = this.getLineHeight(size);
    if (this.isOverPage(lineHeight)) {
      this.doc.addPage();
      this.returnOrigin();
    }
    this.newLine(lineHeight);

    // 标题前的方块
    this.doc.setFillColor('#2C62AC');
    this.doc.rect(0, this.y - 14, 6, 15, 'F');

    // 子标题
    this.doc.text(text, this.x, this.y);

    // 下间距
    this.newLine(10);
  }

  /**
   * 添加文本
   *
   * @param {string} text 文本
   * @param {number} [size=16] 字体大小
   * @param {string} [color='#666666'] 颜色
   * @param {TextAlign} [align='left'] 对齐方式
   * @memberof PdfHelper
   */
  addText(
    text: string,
    size: number = 10,
    color: string = '#666666',
    align: 'left' | 'center' = 'left'
  ) {
    // 设置字体大小和文本颜色
    this.doc.setFontSize(size);
    this.doc.setTextColor(color);

    const lineHeight = this.getLineHeight(size);
    // 判断是否添加新页
    if (this.isOverPage(lineHeight)) {
      this.doc.addPage();
      this.returnOrigin();
    }
    const width = this.doc.getTextWidth(text);
    if (width > this.maxWidth) {
      // 如果文本宽度超出，则将文本根据宽度拆分成数组
      const lines: string[] = this.doc.splitTextToSize(text, this.maxWidth);
      lines.forEach((line) => {
        this.addText(line, size, color);
      });
    } else {
      // 先换行
      this.newLine(lineHeight);
      // 在添加文本，坐标为左下角的坐标
      if (align === 'center') {
        this.toHalfOfLine();
      }
      this.doc.text(text, this.x, this.y, { align });
      if (align === 'center') {
        this.toHeadOfLine();
      }
    }
  }

  addField(field: Field, size: number = 10, labelColor: string = '#222222', valueColor: string = '#666666') {
    // 设置字体大小和文本颜色
    this.doc.setFontSize(size);
    const label = field.label + ': ';
    const labelWidth = this.doc.getTextWidth(label);
    const valueWidth = this.doc.getTextWidth(field.value);
    const lineHeight = this.getLineHeight(size);
    if (labelWidth + valueWidth > this.maxWidth / 2 - this.pagePadding) {
      if (valueWidth > this.maxWidth - labelWidth) {
        this.newLine(lineHeight);
        this.doc.setTextColor(labelColor);
        this.doc.text(label, this.x, this.y);
        this.x += labelWidth;

        const lines: string[] = this.doc.splitTextToSize(field.value, this.maxWidth - labelWidth);
        const firstLine = lines[0];
        this.getNewPage(lineHeight);
        this.doc.setTextColor(valueColor);
        this.doc.text(firstLine, this.x, this.y);
        this.newLine(size);

        const OtherLines = lines.splice(1).join();
        this.doc.setTextColor(valueColor);
        const newLines: string[] = this.doc.splitTextToSize(OtherLines, this.maxWidth);
        newLines.forEach((line) => {
          this.getNewPage(lineHeight);
          this.doc.text(line, this.x, this.y);
          this.newLine(size);
        });
        this.y -= size;
      } else {
        this.getNewPage(lineHeight);
        this.newLine(lineHeight);
        this.doc.setTextColor(labelColor);
        this.doc.text(label, this.x, this.y);
        this.doc.setTextColor(valueColor);
        this.doc.text(field.value, this.x + labelWidth, this.y);
      }
    } else {
      this.getNewPage(lineHeight);
      if (this.isHeadOfLine()) {
        this.newLine(lineHeight);
        this.doc.setTextColor(labelColor);
        this.doc.text(label, this.x, this.y);
        this.doc.setTextColor(valueColor);
        this.doc.text(field.value, this.x + labelWidth, this.y);
        this.toHalfOfLine();
      } else {
        this.doc.setTextColor(labelColor);
        this.doc.text(label, this.x, this.y);
        this.doc.setTextColor(valueColor);
        this.doc.text(field.value, this.x + labelWidth, this.y);
        this.toHeadOfLine();
      }
    }

  }

  addTable(data: any[][], headers: string[]) {
    (this.doc as any).autoTable( headers, data, {
      startY: this.y,
      margin: {left: 20, right: 20  },
      styles: {
        overflow: 'linebreak',
        cellWidth: 110,
        font: 'msyh'
      }
    });
    this.y = (this.doc as any).autoTable.previous.finalY + 10;
  }

  addNoData(
    text: string,
    size: number = 10,
    color: string = '#666666'
  ) {
    // 设置字号和颜色
    this.doc.setFontSize(size);
    this.doc.setTextColor(color);

    // 根据字体大小获取行高，并换行
    const lineHeight = this.getLineHeight(size);
    this.newLine(30);

    // x移动到中间
    this.toHalfOfLine();
    // 居中添加标题
    this.doc.text(text, this.x, this.y, { align: 'center' });

    // 换行
    this.newLine(30);
  }

  getNewPage(lineHeight: number) {
    if (this.isOverPage(lineHeight)) {
      this.doc.addPage();
      this.returnOrigin();
    }
  }

  addImage(base64: string, height: number) {
    if (this.isOverPage(height)) {
      this.doc.addPage();
      this.returnOrigin();
    }
    this.newLine(20);
    this.doc.addImage(base64, 'JPEG', this.x, this.y);
    this.newLine(height);
  }

  getBase64Image(url: string): Promise<{ data: string, height: number }> {
    return new Promise((resolve, reject) => {
      const img = new Image(this.A4_WIDTH_PX);
      img.setAttribute('crossOrigin', 'Anonymous');
      img.src = url;
      img.onload = () => {
        const height = this.A4_WIDTH_PX / img.naturalWidth * img.naturalHeight;
        const pdfHeight = this.maxWidth / this.A4_WIDTH_PX * height;
        const dataURL = this.imagegToBase64(img, 740, height);
        resolve({ data: dataURL, height: pdfHeight });
      };
    });
  }

  imagegToBase64(img: HTMLImageElement, width?: number, height?: number) {
    const canvas = document.createElement('canvas');
    canvas.width = width || img.width;
    canvas.height = height || img.height;

    const ctx = canvas.getContext('2d');
    if (ctx) {
      ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
      const dataURL = canvas.toDataURL('image/jpeg', 1.0);
      return dataURL;
    } else {
      return '';
    }
  }

  save(fileName: string) {
    this.doc.save(fileName);
  }

  newLine(height: number = 0) {
    this.x = this.pagePadding;
    this.y += height;
  }

  isHeadOfLine(): boolean {
    return this.x === this.pagePadding;
  }

  toHeadOfLine() {
    this.x = this.pagePadding;
  }

  toHalfOfLine() {
    this.x = this.maxWidth / 2;
  }

  private isOverPage(height: number): boolean {
    return this.y + height > this.maxHeight;
  }

  private getLineHeight(fontSize: number): number {
    return this.linePadding * 2 + fontSize;
  }

  private returnOrigin() {
    this.x = this.pagePadding;
    this.y = this.pagePadding;
  }
}
