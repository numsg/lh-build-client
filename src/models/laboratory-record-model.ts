export default class LaboratoryRecordModel {

  // tslint:disable-next-line:no-inferrable-types
  public id: string = '';

  /**
   * 中心编号
   */
  // tslint:disable-next-line:no-inferrable-types
  public centerCode: string = '';

  /**
   * 中心名称
   */
  // tslint:disable-next-line:no-inferrable-types
  public centerName: string = '';

  /**
   * 实验室管理主键
   */
  // tslint:disable-next-line:no-inferrable-types
  public laboraryId: string = '';

  /**
   * 实验室管理名称
   */
  // tslint:disable-next-line:no-inferrable-types
  public laboraryName: string = '';

  /**
   * 项目数量
   */
  // tslint:disable-next-line:no-inferrable-types
  public projectNum: number = 0;

  /**
   * 修改中心编号时
   */
  public modifyTime: any = null;

  /**
   * 修改中心编号人
   */
  // tslint:disable-next-line:no-inferrable-types
  public modifyName: string = '';

  /**
   * 提交时间
   */
  // tslint:disable-next-line:no-inferrable-types
  public submitTime: any = null;

  recordProjectInfos: any[] = [];

}

