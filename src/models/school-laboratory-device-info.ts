export default class SchoolLaboratoryDevice {
    id!: string;
    // 监控名
    monitor_name!: string;
    // 权重
    weight!: number;
    // 监控编号
    monitor_num!: string;
    // 监控IP
    monitor_ip!: string;
    // 监控账户
    monitor_username!: string;
    // 监控密码
    monitor_password!: string;
    // 流媒体地址
    monitor_url!: string;
    // 创建时间
    createtime!: Date;
    //  设备组主键ID
    device_group_id!: string;
}
