/**
 * 实验室信息DTO
 */
export default class SchoolLaboratoryInfo {
  public id!: string;

  /**
   * 实验室名称
   */

  public laborary_name!: string;
  /**
   * 权重
   */

  public weight!: number;

  /**
   * 实验室说明
   */
  public laborary_desc!: string;

  /**
   *  创建时间
   */

  public createtime!: string;

  public schoolLaboaryComputerIps!: string;
}
