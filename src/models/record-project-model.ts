export default class RecordProjectModel {

  // tslint:disable-next-line:no-inferrable-types
  public id: string = '';

  /**
   * 实验室记录主键id
   */
  // tslint:disable-next-line:no-inferrable-types
  public recordId: string = '';

  /**
   * 项目名称
   */
  // tslint:disable-next-line:no-inferrable-types
  public projectName: string = '';

  /**
   * 修改时间
   */
  // tslint:disable-next-line:no-inferrable-types
  public modifyTime: any = null;

  /**
   * 修改人
   */
  // tslint:disable-next-line:no-inferrable-types
  public modifyName: string = '';

  /**
   * 提交时间
   */
  public submitTime: any = null;

  /**
   * 提交人
   */
  // tslint:disable-next-line:no-inferrable-types
  public submitName: string = '';

  /**
   * 审核时间
   */
  public verifyTime: any = null;

  /**
   * 审核状态（-1未完成0-未审核，1-审核中，2-已审核）
   */
  // tslint:disable-next-line:no-inferrable-types
  public verifyResult: number = 0;

  /**
   * 审核姓名
   */
  // tslint:disable-next-line:no-inferrable-types
  public verifyName: string = '';

  /**
   * 批准时间
   */
  public approvalTime: any = null;

  /**
   * 批准状态（0-未批准，1-批准中，2-已批准）
   */
  // tslint:disable-next-line:no-inferrable-types
  public approvalResult: number = 0;

  /**
   * 批准人姓名l
   */
  // tslint:disable-next-line:no-inferrable-types
  public approvalName: string = '';

  /**
   * 结果模板主键id
   */
  // tslint:disable-next-line:no-inferrable-types
  public resultId: string = '';

  /**
   * 监控分组id
   */
  // tslint:disable-next-line:no-inferrable-types
  public deviceGroupId: string = '';

  /**
   * 视频路径数组
   */
  // tslint:disable-next-line:no-inferrable-types
  public historyVideoUrls: string = '';

  /**
   * 真实实验室模板内容
   */
  // tslint:disable-next-line:no-inferrable-types
  public templateRecordContent: string = '';
  // tslint:disable-next-line:no-inferrable-types
  public templateRecordData: string = '';

 }
