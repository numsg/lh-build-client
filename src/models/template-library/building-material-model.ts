export default class BuildingMaterialModel {

  // tslint:disable-next-line:no-inferrable-types
  public centerCode: string = '';

  /**
   * 测试方法
   */
  // tslint:disable-next-line:no-inferrable-types
  public testMethod: string = '';
  /**
   * 样例名称
   */
  // tslint:disable-next-line:no-inferrable-types
  public materialName: string = '';

  /**
   * 养护日期
   */
  // tslint:disable-next-line:no-inferrable-types
  public maintenanceDate: any = null;

  /**
   * 校验时间
   */
  // tslint:disable-next-line:no-inferrable-types
  public verifyDate: any = null;

  /**
   * 拉力
   */
  // tslint:disable-next-line:no-inferrable-types
  public pullingForce: any = {
    number0: '',
    number1: '',
    number2: '',
    number3: '',
    number4: '',
    number5: '',
    number6: '',
    number7: '',
    number8: '',
    number9: '',
  };

  /**
   * 拉伸胶粘强度
   */
  public  adhesiveStrengthStretch: any = null;

  /**
   * 提交人
   */
  // tslint:disable-next-line:no-inferrable-types
  public submitName: string = '';

  /**
   * Single block 单块
   */
  public singleBlockValueNumber0 = '';
  public singleBlockValueNumber1 = '';
  public singleBlockValueNumber2 = '';
  public singleBlockValueNumber3 = '';
  public singleBlockValueNumber4 = '';
  public singleBlockValueNumber5 = '';
  public singleBlockValueNumber6 = '';
  public singleBlockValueNumber7 = '';
  public singleBlockValueNumber8 = '';
  public singleBlockValueNumber9 = '';
  public singleBlockValue: any = {
    number0: '',
    number1: '',
    number2: '',
    number3: '',
    number4: '',
    number5: '',
    number6: '',
    number7: '',
    number8: '',
    number9: '',
  };

  /**
   * 平均值
   */
  averageValue: any = 0;

  /**
   * 拉力校验日期
   */
  // tslint:disable-next-line:no-inferrable-types
  public pullingForceMaintenanceDate: number = 0;

  /**
   * 拉力检验日期
   */
  // tslint:disable-next-line:no-inferrable-types
  public pullingForceVerifyDate: string = '';

  /**
   * 实验力
   */
  experimentalForceNumber0 = '';
  experimentalForceNumber2 = '';
  experimentalForceNumber3 = '';
  experimentalForce: any = {
    number0: '',
    number1: '',
    number2: ''
  };
  /**
   * 换算系数
   */
  conversionFactorNumber0 = '';
  conversionFactorNumber1 = '';
  conversionFactorNumber2 = '';
  conversionFactor: any = {
    number0: '',
    number1: '',
    number2: ''
  };

  /**
   *
   */
  moistureContentMortar: any = '';

  quality1: any = '';
  quality2: any = '';
  quality3: any = '';
  quality4: any = '';
  quality5: any = '';
  quality6: any = '';
  quality7: any = '';
  quality8: any = '';
  quality9: any = '';
  quality10: any = '';
  quality11: any = '';
  quality12: any = '';
  quality13: any = '';
  quality14: any = '';
  /**
   * 批准时间
   */
  public approvalTime: any = null;

  /**
   * 批准状态（0-未批准，1-批准中，2-已批准）
   */
  // tslint:disable-next-line:no-inferrable-types
  public approvalResult: number = 0;

  /**
   * 批准人姓名l
   */
  // tslint:disable-next-line:no-inferrable-types
  public approvalName: string = '';

  /**
   * 结果模板主键id
   */
  // tslint:disable-next-line:no-inferrable-types
  public resultId: string = '';

  /**
   * 监控分组id
   */
  // tslint:disable-next-line:no-inferrable-types
  public deviceGroupId: string = '';

  /**
   * 视频路径数组
   */
  // tslint:disable-next-line:no-inferrable-types
  public historyVideoUrls: string = '';

  /**
   * 真实实验室模板内容
   */
  // tslint:disable-next-line:no-inferrable-types
  public templateRecordContent: string = '';

 }
