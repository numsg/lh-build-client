

/**
 * 实验室信息DTO
 */
export default class SchoolLaboraryComputerRelationInfo {
  public id!: string;
  /**
   * 实验室名称
   */
  public laboraryId!: string;

  /**
   * 权重
   */
  public computer_ip!: string;

}
