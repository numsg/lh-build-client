export default class ResultTemplateModel {
  // tslint:disable-next-line:no-inferrable-types
  public id: string = '';
  // tslint:disable-next-line:no-inferrable-types
  public resultName: string = '';
  // 权重
  // tslint:disable-next-line:no-inferrable-types
  public weight: number = 0;
  // 状态
  // tslint:disable-next-line:no-inferrable-types
  public resultStatus: string = '';
  // 描述
  // tslint:disable-next-line:no-inferrable-types
  public resultDesc: string = '';
  // 创建时间
  public createTime: any = null;
  // 模板内容
  // tslint:disable-next-line:no-inferrable-types
  public content: string = '';
  // 模板数据
  // tslint:disable-next-line:no-inferrable-types
  public data: string = '';
  // 对应项目类型
  // tslint:disable-next-line:no-inferrable-types
  public projectType: string = '';
}
