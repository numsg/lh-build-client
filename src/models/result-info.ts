import SchoolLaboratoryInfo from './school-laboratory-info';

/**
 * 实验室信息DTO
 */
export default class ResultInfo {
  /**
   *  数据集合
   */
  public dataList!: SchoolLaboratoryInfo[];

  /**
   * 结果个数
   */
  public elementCount!: number;

  /**
   * 总页数
   */
  public pageCount!: number;
}
