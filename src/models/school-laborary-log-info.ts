export default class SchoolLog {
    /**
     * 主键
     */
    id!: string;
    /**
     * 中心编号
     */
    centercode!: string;
    /**
     * 项目名称
     */
    projectname!: string;

    /**
     * 项目Id
     */
    proojectid!: string;
    /**
     * 操作页面
     */
    opeationpage!: string;
    /**
     * 操作描述
     */
    operationdesc!: string;
    /**
     * 操作IP
     */
    operationip!: string;
    /**
     * 操作时间
     */
    operationtime!: Date;
}

