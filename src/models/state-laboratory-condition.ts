
/**
 * 批准状态分页查询实验记录
 */
export default class StateLaboratoryCondition {

  /**
   * 中心编号
   */
  public centerCode = '';

  /**
   * 实验室id
   */
  public laboraryId = '';

  /**
   * 页码
   */
  public pageNumber = 1;

  /**
   *
   */
  public pageSize = 0;

  /**
   * 状态 0-未审核 1-ed
   */
  public status = 0;

}
