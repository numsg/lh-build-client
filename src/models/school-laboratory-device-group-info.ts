export default class SchoolLaboratoryDeviceGroup {
    id!: string;
    // 监控组名
    group_name!: string;
    // 实验室ID
    laborary_id!: string;
    // 实验室名称
    laborary_name!: string;
    // 监控区域
    monitor_region!: string;
    // 权重
    weight!: number;
    // 模板ID
    template_id!: number;
    // 数据同步接口
    data_interface_url!: string;
    // 同步接口类型
    interface_type!: string;
    // 创建时间
    createtime!: Date;
}
