import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from '@/store';
import ElementUI from 'element-ui';
import moment from 'moment';
import 'element-ui/lib/theme-chalk/index.css';

import { Skin } from './assets/skin/skin';
import './assets/styles/special/tooltip.scss';

import i18n from './i18n';
import Config from './common/utils/appconfig';
import './common/venders/http-client';
import './common/filters';
import './common/method/dialogDrag.js';
import { initLog } from './common/utils/logger';
import 'quill/dist/quill.core.css';
import 'quill/dist/quill.bubble.css';
import 'quill/dist/quill.snow.css';

import vTooltip from '@gsafety/v-tooltip';


Date.prototype.toISOString = function() {
  return moment(this).format('YYYY-MM-DDTHH:mm:ss.000[Z]');
};

// 加载用户主题
Skin.changeTheme(localStorage.getItem('themeValue'));


Config(store).then(() => {
  const nodes = store.state.app.configs.elasticNodes;
  initLog(nodes);
  Vue.config.productionTip = false;
  Vue.use(ElementUI);
  Vue.use(vTooltip);

  new Vue({
    router,
    store,
    i18n,
    render: (h: any) => h(App)
  }).$mount('#app');
});
