import { Vue, Component, Prop, Watch } from 'vue-property-decorator';


@Component
export default class ForceValue extends Vue {

  @Prop()
  isReadonly: any;

  @Prop()
  tableName: any;

  @Prop()
  trialTime: any;

  get dataSource() {
    return this.$store.getters.TemplateLibrary_currentTemplateData;
  }

  set dataSource(val) {
    this.$store.dispatch('setCurrentTemplateData', val);
  }

  mounted() {}

  formDataChange() {
    this.$store.dispatch('setCurrentTemplateData', this.dataSource);
  }

}
