import { Vue, Component, Prop, Watch } from 'vue-property-decorator';
import dataRecordService from '@/api/data-record-service';
@Component
export default class BuildingMaterialInspection extends Vue {
  dataList: string[] = [];
  dataType = 1;
  @Prop()
  isReadonly: any;

  get dataSource() {
    return this.$store.getters.TemplateLibrary_currentTemplateData;
  }

  set dataSource(val) {
    this.$store.dispatch('setCurrentTemplateData', val);
  }

  created() {
    dataRecordService.getLatestSchoolDataRecordInfoList(this.dataType).then((res) => {
      res.forEach((element: any) => {
        const tmepdata = JSON.parse(element.data);
        tmepdata.forEach((element2: any) => {
          element2.items.forEach((element3: any) => {
            this.dataList.push(element3);
          });
        });
      });
    });
  }
  mounted() {}

  formDataChange() {
    this.$store.dispatch('setCurrentTemplateData', this.dataSource);
  }

}
