import { Vue, Component, Prop, Watch } from 'vue-property-decorator';
import dataRecordService from '@/api/data-record-service';


@Component
export default class ThermalConductivityMeter extends Vue {
  dataList: string[] = [];
  dataType = 1;
  @Prop()
  isReadonly: any;

  @Prop()
  tableName: any;

  @Prop()
  trialTime: any;


  get dataSource() {
    return this.$store.getters.TemplateLibrary_currentTemplateData;
  }

  set dataSource(val) {
    this.$store.dispatch('setCurrentTemplateData', val);
  }

  created() {
  }
  mounted() {}

  formDataChange() {
    this.$store.dispatch('setCurrentTemplateData', this.dataSource);
  }

}
