import { Vue, Component } from 'vue-property-decorator';
import DeviceServer from '@/api/laboratory-device-service';
import { rxevent } from '@gsafety/rx-eventbus';
import SchoolLaboratoryDeviceGroup from '@/models/school-laboratory-device-group-info';
import LaboratoryManagerService from '@/api/laboratory-manager-service';

// 设备分组信息
@Component({
    components: {
    }
})
export default class DeviceGroupInfo extends Vue {

    DeviceGroup: any = [];
    operateTitle: any = '';
    devicegroupinfo: any = new SchoolLaboratoryDeviceGroup();
    DialogVisible: any = false;
    laborary_name: any = '';
    laborary_id: any = '';
    // 实验室集合
    laboratoryResultInfo: any = [];

    mounted() {
        this.init();
    }

    // 初始化
    async init() {
        this.DeviceGroup = await DeviceServer.GetAllDeviceGroupInfo();
        this.laboratoryResultInfo = await LaboratoryManagerService.GetLaboratoryInfobyPage(0, 1000, '');
    }

    // 行点击事件获取设备详细信息
    DeviceChange(val: any) {
        if (val) {
            rxevent.publish('DeviceGroupListDevice', val.id);
            this.devicegroupinfo = val;
        } else {
            this.$message.warning('没有选中数据');
        }
    }

    // 增加监控分组
    adddevicegroup() {
        this.operateTitle = '新增';
        this.DialogVisible = true;
        this.devicegroupinfo = new SchoolLaboratoryDeviceGroup();
        this.laborary_name = '';
        this.laborary_id = '';
    }

    selectedChanged(val: any) {
        this.laborary_name = val.laborary_name;
        this.laborary_id = val.id;
        // this.devicegroupinfo.laborary_name = this.laborary_name;
    }

    // 编辑监控分组
    updatedevicegroup() {
        if (this.devicegroupinfo.id === null || this.devicegroupinfo.id === undefined) {
            this.$message.warning('请选中分组信息');
            return;
        }
        this.laborary_name = this.devicegroupinfo.laborary_name;
        this.laborary_id = this.devicegroupinfo.laborary_id;
        this.operateTitle = '编辑';
        this.DialogVisible = true;
    }

    // 删除监控分组
    deleteupdategroup() {
        if (this.devicegroupinfo.id === null || this.devicegroupinfo.id === undefined) {
            this.$message.warning('请选中分组信息');
            return;
        }
        this.$confirm('是否删除当前设备分组？', '操作', {
            confirmButtonText: '确认',
            cancelButtonText: '取消',
            type: 'warning',
            closeOnClickModal: false
        }).then(async () => {
            const result = await DeviceServer.GetAllDeviceInfoByDeviceGroupId(this.devicegroupinfo.id);
            if (result && result.length > 0) {
                this.$message.warning('当前分组下有设备信息,不允许操作!');
                return;
            }
            DeviceServer.DeleteDeviceGroupInfo(this.devicegroupinfo).then((res: boolean) => {
                if (res) {
                    this.init();
                    this.$message.success('删除成功');
                } else {
                    this.$message.error('删除失败');
                }
            }).catch(() => {
                this.$message.error('删除失败');
            });
        });
    }

    // 增加和编辑监控分组操作
    dialogoperation() {
        if (this.laborary_name === '') {
            this.$message.error('实验室必填，请选择');
            return;
        }
        this.devicegroupinfo.laborary_name = this.laborary_name;
        this.devicegroupinfo.laborary_id = this.laborary_id;
        if (this.operateTitle === '编辑') {
            DeviceServer.ModifyDeviceGroupInfo(this.devicegroupinfo).then((res: boolean) => {
                if (res) {
                    this.$message.success('修改成功');
                    this.init();
                    this.DialogVisible = false;
                } else {
                    this.$message.error('修改失败');
                }
            }).catch(() => {
                this.$message.error('修改失败');
            });
        } else if (this.operateTitle === '新增') {
            // 需要在页面添加实验室的选择给实验室ID和名称赋值存储
            DeviceServer.AddDeviceGroupInfo(this.devicegroupinfo).then((res: boolean) => {
                if (res) {
                    this.$message.success('新增成功');
                    this.init();
                    this.DialogVisible = false;
                } else {
                    this.$message.error('新增失败');
                }
            }).catch(() => {
                this.$message.error('新增失败');
            });
        }

    }
}
