import { rxevent } from '@gsafety/rx-eventbus';
import { Vue, Component } from 'vue-property-decorator';
import DeviceServer from '@/api/laboratory-device-service';
import SchoolLaboratoryDevice from '@/models/school-laboratory-device-info';

// 设备信息
@Component({
    components: {
    }
})
export default class DeviceInfo extends Vue {

    DeviceData: any = [];
    loading: any = false;
    operateTitle: any = '';
    DialogVisible: any = false;
    deviceinfo: any = new SchoolLaboratoryDevice();
    devicegroupID: any = '';

    mounted() {
        rxevent.unsubscribe('DeviceGroupListDevice', 'DeviceInfo');
        rxevent.subscribe('DeviceGroupListDevice', 'DeviceInfo', (id: any) => {
            this.devicegroupID = id;
            this.initdata(id);
        });
    }

    // 初始化
    async initdata(id: any) {
        this.DeviceData = await DeviceServer.GetAllDeviceInfoByDeviceGroupId(id);
        this.$forceUpdate();
    }

    // 设备新增
    adddeviceinfo() {
        this.operateTitle = '新增';
        this.DialogVisible = true;
        this.deviceinfo = new SchoolLaboratoryDevice();
    }
    // 设备编辑
    updatedevice(val: any) {
        this.$confirm('是否编辑当前设备信息？', '操作', {
            confirmButtonText: '确认',
            cancelButtonText: '取消',
            type: 'warning',
            closeOnClickModal: false
        }).then(async () => {
            this.operateTitle = '编辑';
            this.DialogVisible = true;
            this.deviceinfo = val;
        });
    }

    // 设备新增和编辑窗口
    dialogoperation() {
        if (this.deviceinfo === null || this.deviceinfo.monitor_name === undefined) {
            this.$message.error('设备监控名必填，请输入');
            return;
        }
        if (this.operateTitle === '编辑') {
            DeviceServer.ModifyDeviceInfo(this.deviceinfo).then((res: boolean) => {
                if (res) {
                    this.initdata(this.devicegroupID);
                    this.$message.success('修改成功');
                    this.DialogVisible = false;
                } else {
                    this.$message.error('修改失败');
                }
            }).catch(() => {
                this.$message.error('修改失败');
            });
        } else if (this.operateTitle === '新增') {
            // 需要在页面添加实验室的选择给实验室ID和名称赋值存储
            this.deviceinfo.device_group_id = this.devicegroupID;
            DeviceServer.AddDeviceInfo(this.deviceinfo).then((res: boolean) => {
                if (res) {
                    this.initdata(this.devicegroupID);
                    this.$message.success('新增成功');
                    this.DialogVisible = false;
                } else {
                    this.$message.error('新增失败');
                }
            }).catch(() => {
                this.$message.error('新增失败');
            });
        }
    }
    deletedevice(val: any) {
        this.$confirm('是否删除当前设备信息？', '操作', {
            confirmButtonText: '确认',
            cancelButtonText: '取消',
            type: 'warning',
            closeOnClickModal: false
        }).then(async () => {
            DeviceServer.DeleteDeviceInfo(val).then((res: boolean) => {
                if (res) {
                    this.initdata(this.devicegroupID);
                    this.$message.success('删除成功');
                } else {
                    this.$message.error('删除失败');
                }
            }).catch(() => {
                this.$message.error('删除失败');
            });
        });
    }

    beforeDestory() {

    }

}
