import { Vue, Component } from 'vue-property-decorator';
import { Getter, Mutation } from 'vuex-class';
import { LoginTypes } from '../../store/types/login-type';
import { EditPwd } from '../../common/models/login/edit-pwd';
import userService from '../../api/system-manage/user-service';
import { ErrorCode } from '../../common/constant/error-code';
import { RegularTypes } from '../../common/constant/regular-type';
import { UserInfo } from '../../common/models/system-manage/user-info';
import { compressBase64ForSize } from '../../common/utils/base64-utils';
import { LoginInfo } from '../../common/models/loginInfo';
import { DefaultConfig } from '../../common/constant/default-config';
import SettingService from '../../api/system-manage/setting-service';
import userManagerService from '@/api/system-manage/user-manager-service';

import { InjectLog, Logger } from '@gsafety/cad-glog';

@Component({
  components: {}
})
export default class LoginInfoHeader extends Vue {
  /**
   * 日志
   */
  @InjectLog() logger!: Logger;
  @Mutation(LoginTypes.mutations.SET_LOGIN_INFO)
  setParam!: (arg: LoginInfo) => void;
  /**
   * 表单对象
   */
  theEditPwd: EditPwd = new EditPwd();
  /**
   * 修改密码
   */
  pwdDialogVisible = false;
  /**
   * 密码校验规则
   */
  pwdRules = {};
  // 头像
  userAvator = '';
  /**
   * 个人信息是否可见
   */
  pInfoDialogVisible = false;

  /**
   * 用户信息-个人
   */
  personInfo: UserInfo = new UserInfo();
  /**
   * 电话校验规则
   */
  phoneRegular = RegularTypes.Phone;
  pwdButtonVisible = false;
  /**
   * 性别
   */
  genders = [
    { value: 0, lable: 'SigtOrder.Women' },
    { value: 1, lable: 'SigtOrder.Men' }
  ];

  /**
   * 用户头像参数
   */
  userImgUrl = '';
  imageData: any;
  /**
   * 原头像
   */
  oldImage: any;
  @Getter(LoginTypes.getters.LOGIN_INFO)
  loginInfo: any;

  visible = false;

  /**
   * 加载标识
   */
  savePinfoBtnLoading = false;
  created() {
    this.getUserAvator(this.loginInfo.userImage);
  }
  /**
   * 获取头像信息
   */
  getUserAvator(userImageData: string) {
    // 获取头像信息
    if (userImageData) {
      this.userAvator = 'data:image/png;base64,' + userImageData;
    }
  }
  mounted() {
    this.pwdRules = {
      oldPassword: [
        { required: true, trigger: ['change', 'blur'], message: this.$t('SigtLogin.OldPwdEmpty').toString() },
        { pattern: RegularTypes.Password, message: '密码长度不少于6位' }
      ],
      newPassword: [
        { required: true, trigger: ['change', 'blur'], message: this.$t('SigtLogin.NewPwdEmpty').toString() },
        { pattern: RegularTypes.Password, message: '密码长度不少于6位' }
      ],
      confirmPassword: [
        { required: true, trigger: ['change', 'blur'], message: this.$t('SigtLogin.ConfirmPwdEmpty').toString() },
        { validator: this.validPwd, trigger: ['change', 'blur'] },
        { pattern: RegularTypes.Password, message: '密码长度不少于6位' }
      ]
    };
    this.$store.state.login.currentRoleMenus.forEach((e: any) => {
      if (e.title === '系统管理') {
        e.children.forEach((c: any) => {
          if (c.title === '修改密码') {
            this.pwdButtonVisible = true;
          }
        });
      }
    });
    console.log(this.loginInfo);
  }
  get personRules() {
    return {
      name: [
        { required: true, trigger: ['change', 'blur'], message: this.$t('SigtOrganization.PleaseEnterName').toString() },
        { whitespace: true, trigger: ['blur', 'change'], message: this.$t('SigtOrganization.PleaseEnterName').toString() }
      ],
      mobilePhone: [
        { pattern: this.phoneRegular, trigger: ['blur', 'change'], message: this.$t('SigtFormRules.PhoneFormatError').toString() }
      ],
      email: [{ pattern: RegularTypes.Email, trigger: ['blur', 'change'], message: this.$t('SigtFormRules.EmailNotTrue').toString() }]
    };
  }
  /**
   * 密码相同性校验
   * @param value
   * @param callback
   */
  validPwd(rule: any, value: any, callback: any) {
    if (value.trim() !== this.theEditPwd.confirmPassword || value.trim() !== this.theEditPwd.newPassword) {
      callback(new Error(this.$t('SigtLogin.PwdNotSame').toString()));
    } else {
      callback();
    }
  }
  /**
   * 编辑密码弹窗
   */
  updatePwdDialog() {
    this.pwdDialogVisible = true;
  }
  /**
   * 关闭密码弹窗
   */
  closePwdDialog() {
    this.pwdDialogVisible = false;
    this.theEditPwd = new EditPwd();
  }
  /**
   * 提交表单校验
   */
  validPwdForm() {
    const refs: any = this.$refs.pwdForm;
    refs.validate((valid: any) => {
      if (valid) {
        this.updateUserPwd();
      }
    });
  }
  /**
   * 修改密码
   */
  async updateUserPwd() {
    const loginid = sessionStorage.getItem('login_id');
    if (loginid != null) {
      await userService.getUserInfoByUserId(loginid.toString()).then((currentUser: UserInfo) => {
        console.log(currentUser);
        if (currentUser) {
          currentUser.password = this.theEditPwd.newPassword;
          userManagerService.resetUser(currentUser).then((result: any) => {
            if (result) {
              this.$message.success('修改密码户成功');
              this.closePwdDialog();
            } else {
              this.$message.error('修改密码失败');
            }
          });
        }
      });
    }
    // this.theEditPwd.username = this.loginInfo.username;
    // await userService.updatePwd(this.theEditPwd).then((result) => {
    //   if (result.success) {
    //     this.$message({
    //       type: 'success',
    //       message: this.$t('SigtLogin.UpdatePwdSuccess').toString()
    //     });
    //     this.pwdDialogVisible = false;
    //     this.$emit('pwd-update-success');
    //   } else {
    //     if (result.errorCode === ErrorCode.UsernameNotExist) {
    //       this.$message({
    //         type: 'warning',
    //         message: this.$t('SigtLogin.usernameNotExit').toString()
    //       });
    //     } else if (result.errorCode === ErrorCode.OriginalPasswordErr) {
    //       this.$message({
    //         type: 'warning',
    //         message: this.$t('SigtLogin.OriginalPasswordError').toString()
    //       });
    //     } else {
    //       this.$message({
    //         type: 'warning',
    //         message: this.$t('SigtCommon.OperateError').toString()
    //       });
    //     }
    //   }
    // });
  }
  /**
   * 修改个人信息
   */
  async updatePersonalInfo() {
    // 1.获取用户的信息
    await userService.getUserInfoByUserId(this.loginInfo.id).then((currentUser: UserInfo) => {
      if (currentUser) {
        if (currentUser.userImage) {
          this.userImgUrl = 'data:image/png;base64,' + currentUser.userImage;
        }
        this.personInfo = currentUser;
        this.pInfoDialogVisible = true;
      }
    });
  }
  /**
   * 关闭用户弹窗
   */
  closePersonInfoDialog() {
    this.pInfoDialogVisible = false;
  }
  /**
   * 个人信息表单校验
   */
  async validPersonInfo() {
    const refs: any = this.$refs.personForm;
    refs.validate((valid: any) => {
      if (valid) {
        this.savePersonInfo();
      }
    });
  }
  /**
   * 表单提交验证
   */
  async savePersonInfo() {
    this.savePinfoBtnLoading = true;
    if (this.imageData) {
      //  压缩上传图片大小到40k以内
      const img = await compressBase64ForSize(this.imageData, 0.2, 40);
      this.personInfo.userImage = img.substr(img.indexOf(',') + 1);
    }
    // 2.返回结果，提示，关闭弹窗
    const result = await userService
      .updateUser(this.personInfo)
      .catch((err: any) => {
        this.logger.error(`[OrganizationManage] -> UserService.updateUser(${this.personInfo}): ${JSON.stringify(err)}`);
      })
      .finally(() => {
        this.savePinfoBtnLoading = false;
      });
    if (result && result.success) {
      this.loginInfo.name = this.personInfo.name;
      if (this.personInfo.userImage) {
        this.getUserAvator(this.personInfo.userImage);
      }

      // 提示用户添加成功，关闭弹窗
      this.$message.success(this.$t('SigtLogin.UpdatePersonInfoSuccess').toString());
      this.pInfoDialogVisible = false;
      this.$emit('pwd-update-success');
    } else {
      this.$message.error(this.$t('SigtCommon.OptionFailed').toString());
    }
  }
  /**
   * 接收头像
   * @param data 头像文件信息
   */
  handleAvatarSuccess(data: any) {
    const _this = this;
    const rd = new FileReader();
    const file = data.file;
    rd.readAsDataURL(file);
    rd.onloadend = ((e: any) => {
      _this.imageData = rd.result;
      _this.userImgUrl = URL.createObjectURL(file);
    });
  }
  /**
   * 上传头像前的校验
   * @param file 上传文件
   */
  beforeAvatarUpload(file: any) {
    const isJPG = file.type === 'image/jpeg' || file.type === 'image/png';
    const isLt2M = file.size / 1024 / 1024 < 2;

    if (!isJPG) {
      this.$message.warning(this.$t('SigtUser.UserImageFormat').toString());
      return false;
    }
    if (!isLt2M) {
      this.$message.warning(this.$t('SigtUser.ImgSize').toString());
      return false;
    }
    return true;
  }
}
