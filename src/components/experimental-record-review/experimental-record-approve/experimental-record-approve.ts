import { Vue, Component, Prop, Watch } from 'vue-property-decorator';
import ExperimentalRecordHeader from '../experimental-record-header/experimental-record-header';
import ExperimentDetail from '../experiment-detail/experiment-detail';
import LaboratoryRecordModel from '@/models/record-project-model';
import laboratoryListService from '@/api/laboratory-list-service';
import ProjectMixin from '../project-mixin';
import StateLaboratoryCondition from '@/models/state-laboratory-condition';
import { debounce } from 'lodash';


@Component({
  components: {
    ExperimentalRecordHeader,
    ExperimentDetail
  }

})
export default class ExperimentalRecordApprove extends ProjectMixin {

  currentPage = 1;

  pageSize = 5;

  headerTitle: any[] = [];

  currentApproveState: any = 'wait';

  /**
   * 实验室列表
   */
  laboratoryList: any = [];


  /**
   * 实验记录列表
   */
  recordList: LaboratoryRecordModel[] = [];
  currentRecord: any = {};

  currentLaboratory: any = {
    value: ''
  };


  /**
   * 显示项目详情
   */
  showExperimentDetail: any = false;
  isApproval: any = true;
  /**
   * 当前项目
   */
  currentProject: any = {};
  /**
   * 记录下面的项目列表
   */
  recordProjectList: any = [];
  total: any = 0;

  queryCondition: StateLaboratoryCondition = new StateLaboratoryCondition();

  couldLoad: any = true;

  groupInfoArr: any [] = [];

  debounceTabClick = debounce(this.onStateChange, 300);
  debounceSearch = debounce(this.onSearch, 300);
  debounceReset = debounce(this.onResetSearch, 300);


  created() {
    this.headerTitle = ['实验审批管理', '实验记录批准'];
  }

  async mounted() {
    this.laboratoryList = await this.getAllLaboratoryInfo();
    this.groupInfoArr = await this.GetAllDeviceGroupInfo();
    this.onResetSearch();
  }

   onLaboratorySelect() {
    this.queryCondition.laboraryId = this.currentLaboratory;
  }

  onSearch() {
    this.recordList = [];
    this.recordProjectList = [];
    this.queryCondition.pageNumber = 1;
    this.couldLoad = true;
    this.stateQueryRecord();
  }

  onResetSearch() {
    this.queryCondition = {
      pageNumber: 1,
      pageSize: 5,
      centerCode: '',
      laboraryId: '',
      status: 0
    };
    if (this.currentApproveState === 'wait') {
      this.queryCondition.status = 0;
    } else {
      this.queryCondition.status = 1;
    }
    this.recordList = [];
    this.total = 0;
    this.currentLaboratory = '';
    this.onSearch();
  }

  async querySearch(queryString: any, cb: any) {
    const param = {
      centerCode: this.queryCondition.centerCode,
      laboraryId: '',
      pageNumber: 1,
      pageSize: 50,
      status: 0,
    };
    const recordDataSource = await laboratoryListService.GetAllChangeLaboratoryRecords(param);
    const data = this.buildSearchData(recordDataSource.records);
    cb(data);
  }


    private buildSearchData(data: any) {
    if (data && Array.isArray(data)) {
      const dataSource = data.map((i: any) => {
        const temp = {
          value: i.centerCode,
          name: i.centerCode
        };
        return temp;
      });
      console.log(dataSource);
      return dataSource;
    } else {
      return [];
    }
  }

    private async stateQueryRecord() {
    const recordDataSource = await laboratoryListService.pageConditionQueryApproval(this.queryCondition);
    if (recordDataSource && recordDataSource.records && Array.isArray(recordDataSource.records)) {
      this.recordList =  recordDataSource.records; // this.recordList.concat(recordDataSource.records);
      this.total = recordDataSource.total;
      this.currentRecord  = this.recordList[0];
      this.recordProjectList = await this.getLaboratoryRecordAndProjectById(this.currentRecord.id);
    } else {
      this.couldLoad = false;
    }
  }

  onShowDetail() {
    this.showExperimentDetail = !this.showExperimentDetail;
  }

  /**
   * tab切换， 切换数据源
   */
  onStateChange() {
    this.recordList = [];
    this.couldLoad = true;
    this.queryCondition.pageNumber = 1;
    this.recordProjectList = [];
    this.total = 0;

    this.isApproval = this.currentApproveState === 'wait';
    if (this.currentApproveState === 'wait') {
      this.queryCondition.status = 0;
    } else {
      this.queryCondition.status = 1;
    }
    this.stateQueryRecord();
  }

  async onRecordClick(record: any) {
    this.currentRecord = record;
    this.recordProjectList = await this.getLaboratoryRecordAndProjectById(this.currentRecord.id);
  }

  onShowApproval(val: any, flag: any) {
    this.currentProject = val;
    this.isApproval = flag;
    this.onShowDetail();
  }

  /**
   * 批准成功
   */
  onOperationSuccess() {
    this.showExperimentDetail = false;
    // this.recordList = [];
    this.isApproval = this.currentApproveState === 'wait';
    if (this.currentApproveState === 'wait') {
      this.queryCondition.status = 0;
    } else {
      this.queryCondition.status = 1;
    }
    this.currentProject.approvalResult = 2;
    // this.onResetSearch();
    // this.onSearch();
  }


  /**
   * 滚动加载
   * @param val
   */
  onRecordScroll(val: any) {
    // const recordListDiv = this.$refs['recordListRef'] as HTMLDivElement;
    // const isBottom = recordListDiv.scrollHeight - recordListDiv.scrollTop === recordListDiv.clientHeight;
    // if (isBottom && this.recordList.length <= this.total && this.couldLoad) {
    //   this.queryCondition.pageNumber++;
    //   this.stateQueryRecord();
    // } else if (isBottom && !this.couldLoad) {
    //   this.$message({type: 'info', message: '已加载全部数据'});
    // }
  }

  handleSizeChange(value: number) {
    this.pageSize = value;
    this.stateQueryRecord();
  }

  handleCurrentChange(vlue: number) {
    this.currentPage = vlue;
    this.queryCondition.pageNumber = vlue;
    this.stateQueryRecord();
  }

  getLaboratoryName(val: any) {
    const laboratory = this.groupInfoArr.find((i: any) => i.id === val);
    return laboratory ? laboratory.laborary_name : '';
  }
}
