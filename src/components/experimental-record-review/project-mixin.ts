import { Vue, Component, Prop, Watch } from 'vue-property-decorator';
import laboratoryListService from '@/api/laboratory-list-service';
import DeviceServer from '@/api/laboratory-device-service';
import dataDictionaryUtil from '@/common/utils/data-dictionary-util';
import laboratoryManagerService from '@/api/laboratory-manager-service';

@Component
export default class ProjectMixin extends Vue {

  projectList: any = [];
  deviceGroup: any = [];
  templateListDataSource: any = [];


  laboratoryList: any[] = [];

  mounted() {}

  async getLaboratoryRecordAndProjectById(recordId: any) {
    if (recordId) {
      const result = await laboratoryListService.GetLaboratoryRecordAndProjectById(recordId);
      const groupData = await DeviceServer.GetAllDeviceGroupInfo();
      const components = dataDictionaryUtil.getTemplateListSource();
      this.projectList = result && result.recordProjectInfos ? result.recordProjectInfos : [];
      this.deviceGroup = groupData && Array.isArray(groupData) ? groupData : [];
      this.templateListDataSource = components;
      return result.recordProjectInfos;
    }
  }



  /**
   * 获取所有实验室
   */
  public async getAllLaboratoryInfo() {
    const result = await laboratoryManagerService.GetAllLaboratoryInfo();
    this.laboratoryList = result && Array.isArray(result) ? result : [];
    return this.laboratoryList;
  }

  async GetAllDeviceGroupInfo() {
    const result = await DeviceServer.GetAllDeviceGroupInfo();
    return result && Array.isArray(result) ? result : [];
  }

}
