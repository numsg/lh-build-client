import { Vue, Component, Prop, Watch } from 'vue-property-decorator';
import laboratoryListService from '@/api/laboratory-list-service';
import VideoPlay from '../../share/video-play/video-play';
import dataDictionaryUtil from '@/common/utils/data-dictionary-util';
import LaboratoryLogService from '@/api/laborartory-log-service';
import SchoolLog from '@/models/school-laborary-log-info';

@Component({
  components: {
    VideoPlay
  }
})
export default class ExperimentDetail extends Vue {
  @Prop()
  dialogVisible: any;

  @Prop()
  project: any;

  @Prop()
  record: any;

  @Prop()
  isOperation: any;

  @Prop()
  type: any;

  templateComponent: any = {};

  videoList: any = [];

  wsUrl: any = '';

  templateListDataSource: any = [];



  mounted() {
    this.wsUrl = 'http' + this.$store.getters.configs.wsUrl;
    this.initData();
  }

  async initData() {
    this.videoList = this.project.historyVideoUrls ? JSON.parse(this.project.historyVideoUrls) : [];
    const components = dataDictionaryUtil.getTemplateListSource();
    console.log('视频列表', this.videoList);
    this.templateListDataSource = components;
    const templateDataSource = this.project.templateRecordData ?  JSON.parse(this.project.templateRecordData) : {};
    this.$store.dispatch('setCurrentTemplateData', templateDataSource);
    const temp: any = this.templateListDataSource.find((item: any) => item.id === this.project.resultId);
    this.templateComponent = temp;
    this.videoList.forEach((i: any, index: any) => {
      this.createPlayer(index, i);
    });
  }


    createPlayer(index: any, filename: string) {
    this.$nextTick(() => {
      const video: any = this.$refs[`videoRef${index}`] as HTMLVideoElement;
      if (video[0]) {
        // video[0].src = `${this.wsUrl}/dfile?filepath=1`;
        video[0].src = `${this.wsUrl}/dfile?filepath=${filename}`;
      }
    });
  }


  /**
   * 批准实验记录
   */
  async handleApprove() {
    const user: any = sessionStorage.getItem('login_name');
    const temp = {
      approvalName: user,
      approvalResult: 2,
      approvalTime: this.project.approvalTime,
      deviceGroupId: this.project.deviceGroupId,
      historyVideoUrls: this.project.historyVideoUrls,
      id: this.project.id,
      modifyName: this.project.modifyName,
      modifyTime: this.project.modifyTime,
      projectName: this.project.projectName,
      recordId: this.project.recordId,
      resultId: this.project.resultId,
      submitName: this.project.submitName,
      submitTime: this.project.submitTime,
      templateRecordContent: this.project.templateRecordContent,
      templateRecordData: this.project.templateRecordData,
      verifyName: this.project.verifyName,
      verifyResult: this.project.verifyResult,
      verifyTime: this.project.verifyTime
    };
    const result = await laboratoryListService.modifyRecordApproval(this.project.id, temp);
    console.log(result);
    if (result) {
      this.$message({type: 'success', message: '当前采集项目通过批准'});
      this.$emit('operation-success');
      this.addSystemLog('实验记录批准', '批准成功');
    }
  }

  /**
   * 实验记录通过审核
   */
  async handleAudit() {
    const user: any = sessionStorage.getItem('login_name');
    const param = {
      approvalName: this.project.approvalName,
      approvalResult: this.project.approvalResult,
      approvalTime: this.project.approvalTime,
      deviceGroupId: this.project.deviceGroupId,
      historyVideoUrls: this.project.historyVideoUrls,
      id: this.project.id,
      modifyName: this.project.modifyName,
      modifyTime: this.project.modifyTime,
      projectName: this.project.projectName,
      recordId: this.project.recordId,
      resultId: this.project.resultId,
      submitName: this.project.submitName,
      submitTime: this.project.submitTime,
      templateRecordContent: this.project.templateRecordContent,
      templateRecordData: this.project.templateRecordData,
      verifyName: user,
      verifyResult: 2,
      verifyTime: this.project.verifyTime
    };
    const result = await laboratoryListService.modifyRecordVerify(this.project.id, param);
    if (result) {
      this.$message({type: 'success', message: '当前采集项目通过审核'});
      this.$emit('operation-success');
      this.addSystemLog('实验记录审核', '审核成功');
    }
  }

  afterHandleEvents() {
    // 界面状态更新，项目列表状数据更新
  }

  /**
   * 关闭项目弹窗
   */
  handleClose() {
    this.$emit('on-detail-close');
  }

  async addSystemLog(operationPage: any, desc: string) {
    const schoolLog = new SchoolLog();
    schoolLog.centercode = this.record.centerCode;
    schoolLog.operationdesc = desc;
    schoolLog.opeationpage = operationPage;
    schoolLog.projectname = '实验审批管理';
    const result = await LaboratoryLogService.AddSystemOperationLog(schoolLog);
  }

}
