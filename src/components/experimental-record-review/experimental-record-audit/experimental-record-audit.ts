import { Vue, Component, Prop, Watch } from 'vue-property-decorator';
import ExperimentalRecordHeader from '../experimental-record-header/experimental-record-header';
import ExperimentDetail from '../experiment-detail/experiment-detail';

import laboratoryListService from '@/api/laboratory-list-service';
import ProjectMixin from '../project-mixin';
import StateLaboratoryCondition from '../../../models/state-laboratory-condition';
import { debounce } from 'lodash';

@Component({
  components: {
    ExperimentalRecordHeader,
    ExperimentDetail
  }

})
export default class ExperimentalRecordAudit extends ProjectMixin {

  currentPage = 1;

  pageSize = 5;

  headerTitle: any[] = [];
  currentAuditState: any = 'wait';

  /**
   * 实验室列表
   */
  laboratoryList: any[] = [];

  /**
   * 实验记录
   */
  recordList: any[] = [];
  currentRecord: any = {};

  currentLaboratory: any = {
    value: ''
  };

  recordProjectList: any[] = [];
  currentProject: any = {};

  /**
   * 显示项目详情的弹窗
   */
  showExperimentDetail: any = false;
  isAudit: any = true;
  /**
   * 查询条件
   */
  queryCondition: StateLaboratoryCondition = new StateLaboratoryCondition();
  total: any = 0;
  couldLoad: any = true;

  groupInfoArr: any[] = [];
  debounceTabClick = debounce(this.handleTabClick, 300);
  debounceSearch = debounce(this.onSearch, 300);
  debounceReset = debounce(this.onResetSearch, 300);

  created() {
    this.headerTitle = ['实验审批管理', '实验记录审核'];
  }

  async mounted() {
    this.laboratoryList = await this.getAllLaboratoryInfo();
    this.groupInfoArr = await this.GetAllDeviceGroupInfo();
    this.onResetSearch();
  }
  async querySearch(queryString: any, cb: any) {
    const param = {
      centerCode: this.queryCondition.centerCode,
      laboraryId: '',
      pageNumber: 1,
      pageSize: 50,
      status: 0,
    };
    const recordDataSource = await laboratoryListService.GetAllChangeLaboratoryRecords(param);
    const data = this.buildSearchData(recordDataSource.records);
    cb(data);
  }

    private buildSearchData(data: any) {
    if (data && Array.isArray(data)) {
      const dataSource = data.map((i: any) => {
        const temp = {
          value: i.centerCode,
          name: i.centerCode
        };
        return temp;
      });
      console.log(dataSource);
      return dataSource;
    } else {
      return [];
    }
  }
  /**
   * 状态查询待审核实验记录
   * 设置默认记录列表
   * 获取默认记录的项目列表
   * @returns
   */
  private async stateQueryRecord() {
    console.log(this.queryCondition);
    const recordDataSource = await laboratoryListService.pageConditionQueryVerify(this.queryCondition);
    if (recordDataSource && recordDataSource.records && Array.isArray(recordDataSource.records)) {
      this.recordList =  recordDataSource.records; // this.recordList.concat(recordDataSource.records);
      this.total = recordDataSource.total;
      this.currentRecord  = this.recordList[0];
      this.recordProjectList = await this.getLaboratoryRecordAndProjectById(this.currentRecord.id);
    } else {
      this.couldLoad = false;
    }
  }


  /**
   * 切换审核状态
   * @param val
   */
  handleTabClick() {
    this.recordList = [];
    this.couldLoad = true;
    this.queryCondition.pageNumber = 1;
    this.recordProjectList = [];
    this.total = 0;

    if (this.currentAuditState === 'wait') {
      this.queryCondition.status = 0;
    } else {
      this.queryCondition.status = 1;
    }
    this.stateQueryRecord();
  }

  /**
   * 选择实验室
   */
  onLaboratorySelect() {
    this.queryCondition.laboraryId = this.currentLaboratory;
  }

  /**
   * 点击搜索
   */
  onSearch() {
    this.couldLoad = true;
    this.recordList = [];
    this.queryCondition.pageNumber = 1;
    this.total = 0;
    this.recordProjectList = [];
    this.stateQueryRecord();
  }


  /**
   * 重置搜索条件
   */
  onResetSearch() {
    this.queryCondition = {
      pageNumber: 1,
      pageSize: 5,
      centerCode: '',
      laboraryId: '',
      status: 0
    };
    this.recordList = [];
    this.total = 0;
    if (this.currentAuditState === 'wait') {
      this.queryCondition.status = 0;
    } else {
      this.queryCondition.status = 1;
    }
    this.currentLaboratory = '';
    this.couldLoad = true;
    this.stateQueryRecord();

    // this.stateQueryRecord();
  }

  onOperationSuccess() {
    this.showExperimentDetail = false;
    // this.onResetSearch();
    this.currentProject.verifyResult = 2;
    // this.onSearch();
  }


  /**
   * 显示项目详情
   */
  onShowDetail() {
    this.showExperimentDetail = !this.showExperimentDetail;
  }

  /**
   * 项目审批 true
   * 查看审批结果 false
   */
  onShowAudit(val: any , flag: any) {
    this.currentProject = val;
    this.isAudit = flag;
    this.onShowDetail();
  }

  /**
   * 实验记录列表点击
   */
  async onRecordClick(record: any) {
    this.currentRecord = record;
    this.recordProjectList = await this.getLaboratoryRecordAndProjectById(this.currentRecord.id);
    console.log(this.recordProjectList);
  }

  /**
   * 滚动加载
   */
  onRecordScroll() {
    // const recordListDiv = this.$refs['auditRecordListRef'] as HTMLDivElement;
    // const isBottom = recordListDiv.scrollHeight - recordListDiv.scrollTop === recordListDiv.clientHeight;
    // if (isBottom && this.recordList.length <= this.total && this.couldLoad) {
    //   this.queryCondition.pageNumber++;
    //   this.stateQueryRecord();
    // } else if (isBottom && !this.couldLoad) {
    //   this.$message({type: 'info', message: '已加载全部数据'});
    // }
  }

  handleSizeChange(value: number) {
    this.pageSize = value;
    this.stateQueryRecord();
  }

  handleCurrentChange(vlue: number) {
    this.currentPage = vlue;
    this.queryCondition.pageNumber = vlue;
    this.stateQueryRecord();
  }

    getLaboratoryName(val: any) {
    const laboratory = this.groupInfoArr.find((i: any) => i.id === val);
    return laboratory ? laboratory.laborary_name : '';
  }
}
