import { Vue, Component, Prop, Watch } from 'vue-property-decorator';
import _ from 'lodash';
/**
 * 席位跟踪状态栏组件
 */
@Component
export default class StateBar extends Vue {
  @Prop({
    default: () => {
      return [];
    }
  })
  seatInfos!: any[];

  phoneStatusGroups = {};

  @Watch('seatInfos', { deep: true })
  onSeatInfosChange() {
    this.buildData();
  }

  created() {
    this.buildData();
  }

  buildData() {

  }

  //  计算通话中的数量（外呼通话 + 呼入通话）
  getTalkingNumber(a: any, b: any) {
    const outCall = a ? a.length : 0;
    const inCall = b ? b.length : 0;
    return outCall + inCall;
  }

  // 获取来电中的数量（呼入振铃 + 自动外呼接入 +  呼入彩铃)
  getInCallNumber(a: any, b: any, c: any) {
    const inCall1 = a ? a.length : 0;
    const incall2 = b ? b.length : 0;
    const incall3 = c ? c.length : 0;
    return inCall1 + incall2 + incall3;
  }

  // 获取去电中的数量（外呼振铃 + 外呼准备 + 外呼彩铃）
  getOutCallNumber(a: any, b: any, c: any) {
    const inCall1 = a ? a.length : 0;
    const incall2 = b ? b.length : 0;
    const incall3 = c ? c.length : 0;
    return inCall1 + incall2 + incall3;
  }
}
