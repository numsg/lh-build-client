import { Vue, Component, Prop } from 'vue-property-decorator';
import SeatInfo from '../../../common/models/seat/seat-info';
/**
 * 席位卡片组件
 */
@Component
export default class SeatCard extends Vue {

  @Prop({
    default: () => new SeatInfo()
  })
  seatInfo!: SeatInfo;

  @Prop({
    default: () => {
      return {};
    }
  })
  offsetWH!: { offsetWidth: number, offsetHeight: number };

  @Prop({
    default: 0
  })
  zIndex!: number;

  @Prop({
    default: false
  })
  canMove!: boolean;

  @Prop({
    default: false
  })
  isShowDetial!: boolean;

  isDown = false;
  clientX = 0;
  clientY = 0;
  left = 0;
  top = 0;
  offsetLeft = 0;
  offsetTop = 0;

  created() {
  }

  handleMouseDown(evt: MouseEvent) {
    if (!this.canMove) {
      return;
    }
    const ele: any = this.$el;
    this.isDown = true;
    this.clientX = evt.clientX;
    this.clientY = evt.clientY;
    this.offsetLeft = ele.offsetLeft;
    this.offsetTop = ele.offsetTop;
    ele.style.zIndex = this.zIndex + 1;
    this.startEventListener();
  }

  handleMouseMove(evt: MouseEvent) {
    if (!this.isDown) {
      return;
    }
    const { offsetWidth, offsetHeight } = this.offsetWH;
    const left = evt.clientX - (this.clientX - this.offsetLeft);
    const top = evt.clientY - (this.clientY - this.offsetTop);
    const el = this.$el as any;
    const width = el.offsetWidth;
    const height = el.offsetHeight;
    if (left < 0 || left > offsetWidth - width) {
      return;
    }
    if (top < 0 || top > offsetHeight - height) {
      return;
    }
    const ele = this.$el as any;
    ele.style.left = `${left}px`;
    ele.style.top = `${top}px`;
  }

  handleMouseUp(evt: MouseEvent) {
    this.isDown = false;
    const ele = this.$el as any;
    const { offsetLeft, offsetTop } = ele;
    this.seatInfo.x = offsetLeft;
    this.seatInfo.y = offsetTop;
    this.$emit('update:zIndex', parseInt(ele.style.zIndex, 10));
    this.removeEventListener();
  }


  startEventListener() {
    document.removeEventListener('mousemove', this.handleMouseMove);
    document.addEventListener('mousemove', this.handleMouseMove);
    document.removeEventListener('mouseup', this.handleMouseUp);
    document.addEventListener('mouseup', this.handleMouseUp);
    document.addEventListener('mouseout', this.handleMouseUp);
    document.removeEventListener('mouseout', this.handleMouseUp);
  }

  removeEventListener() {
    document.removeEventListener('mousemove', this.handleMouseMove);
    document.removeEventListener('mouseout', this.handleMouseUp);
    document.removeEventListener('mouseup', this.handleMouseUp);
  }

  firstUpperCase(phoneStatus: string) {
    return phoneStatus[0].toUpperCase() + phoneStatus.substring(1, phoneStatus.length).toLowerCase();
  }

  noop() {
    return false;
  }

  beforeDestroy() {
    this.removeEventListener();
  }
}
