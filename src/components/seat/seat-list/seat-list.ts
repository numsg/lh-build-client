import { FloorInfo } from '../../../common/models/seat/floor-Info';
import { Vue, Component, Prop, Watch } from 'vue-property-decorator';
import SeatCard from '@/components/seat/seat-card/seat-card';
import SeatInfo from '../../../common/models/seat/seat-info';
import _ from 'lodash';
import seatInfoService from '@/api/seat-info-service';
import SeatConfiguration from '../seat-configuration/seat-configuration';
import messageUtil from '../../../common/utils/message-util';
import { SeatPosition } from '@/common/constant/seat-position';
import AttachService from '@/api/attach/attach-service';
import { convertTimeToHMS } from '../../../common/utils/date-utils';
import { InjectLog, Logger } from '../../../common/utils/logger';

/**
 * 席位卡片组件
 */
@Component({
    components: {
        SeatCard,
        SeatConfiguration
    }
})
export default class SeatList extends Vue {

    @Prop({
        default: () => []
    })
    seatInfos!: SeatInfo[];

    @Prop({
        default: ''
    })
    floorNumber!: string;
    /**
     * 放大缩小的默认值
     */
    scaleValue = 1.5;
    /**
     * 缩放标识，超过显示详情
     */
    SCALE_FLAG = 1.3;
    /**
     * 显示详情
     */
    showDetial = true;
    /**
     * 日志组件
     */
    @InjectLog() logger!: Logger;
    /**
     * 内置默认分组，记录底图信息
     */
    defaultFloor = new FloorInfo();

    localSeatInfos: SeatInfo[] = [];

    // 容器宽高
    offsetWH: { offsetWidth: number, offsetHeight: number } = { offsetWidth: 0, offsetHeight: 0 };

    zIndex = 0;

    isEdit = false;

    offsetLeft: any;
    offsetTop: any;
    /**
     * 底图路径
     */
    bgImage = '';
    /**
     * 席位背景拖动位置
     */
    bgX = -17;
    bgY = 139;
    /**
     * 鼠标初始位置
     */
    clientX = 0;
    clientY = 0;

    /**
     * 鼠标按下
     */
    isDown = false;
    /**
     * 修改席位位置按钮
     */
    get configBtnDisabled() {
        return this.seatInfos.length ? false : true;
    }

    /**
     * 容器高度
     */
    get containerHeight() {
        const wdith = SeatPosition.WIDTH; // 席位宽度
        const hegiht = SeatPosition.HEIGHT; // 席位高度
        const paddingLeft = SeatPosition.PADDING_LEFT; // 席位之间的间距
        const paddingTop = SeatPosition.PADDING_TOP; // 席位之间的间距
        const offsetTop = SeatPosition.OFFSET_TOP; // 距离顶部的默认距离
        const offsetLeft = SeatPosition.OFFSET_LEFT; // 距离左侧的默认值
        const { offsetWidth } = this.offsetWH;
        const columns = Math.floor((offsetWidth - offsetLeft * 2) / (wdith + paddingLeft)); // 列数
        // const columns = 12; // 列数
        const inter = Math.floor((this.localSeatInfos.length) / columns);
        const height = inter * (hegiht + paddingTop) + offsetTop;
        return height + 'px';
    }
    /**
     * 定时器
     */
    timer!: NodeJS.Timeout;
    @Watch('defaultFloor', { deep: true })
    onFloorChange() {
        this.isEdit = false;
    }

    @Watch('seatInfos', { deep: true })
    onSeatInfosChange() {
        this.initData();
        this.$forceUpdate();
    }

    created() {
        this.getDefaultFloor();
    }

    mounted() {
        const ele = this.$refs['seatList'] as HTMLElement;
        const { offsetWidth, offsetHeight } = ele;
        this.offsetWH = {
            offsetWidth,
            offsetHeight
        };
        this.initData();
    }

    initData() {
        clearInterval(this.timer);
        if (_.isEmpty(this.seatInfos)) {
            this.localSeatInfos = [];
            return;
        }
        this.localSeatInfos = _.cloneDeep(this.seatInfos);
        this.phoneStatusTimer();
        this.zIndex = this.localSeatInfos.length;
    }
    /**
     * 获取默认楼层分组
     */
    async getDefaultFloor() {
        try {
            await seatInfoService.getAllFloors().then((result: FloorInfo[]) => {
                const floorData = result.filter((floorGroup: FloorInfo) => floorGroup.floorNumber === 'ALL');
                if (floorData && floorData.length > 0) {
                    this.defaultFloor = floorData[0];
                } else {
                    this.defaultFloor = new FloorInfo();
                }
                this.bgImage = this.defaultFloor.fileId ? AttachService.getPreviewUrlByFiledId(this.defaultFloor.fileId) : require('@/assets/imgs/allfloor.png');
            });
        } catch (error) {
            this.logger.info(`[seat-list.ts] -> [getDefaultFloor] errorMsg: ${error.toString()}`);
        }
    }
    /**
     * 电话状态时间定时器
     * @returns
     * @memberof SeatCard
     */
    phoneStatusTimer() {
        const self = this;
        clearInterval(this.timer);
        this.calcPhoneStatusDuration();
        this.timer = setInterval(() => {
            self.calcPhoneStatusDuration();
        }, 1000);
    }

    /**
     *
     * 计算席位电话状态时长
     * @memberof SeatList
     */
    calcPhoneStatusDuration() {
        this.localSeatInfos.forEach((seatInfo: SeatInfo) => {
            if (seatInfo.registerInfo && seatInfo.registerInfo.phoneStatusTime) {
                const time = seatInfo.registerInfo && seatInfo.registerInfo.phoneStatusTime;
                if (time) {
                    seatInfo.registerInfo.phoneStatusDuration = convertTimeToHMS((Date.now() - (new Date(time)).getTime()) / 1000);
                }
            }
        });
    }

    /**
     * 保存席位配置
     * @memberof SeatList
     */
    saveConfiguration() {
        if (_.isEmpty(this.localSeatInfos)) {
            this.$emit('start-timer');
            this.isEdit = false;
            messageUtil.success(this.$t('SigtCommon.OperateSuccess'));
            return;
        }
        seatInfoService.saveSeatConfiguration(this.localSeatInfos)
            .then((bool: boolean) => {
                if (bool) {
                    this.$emit('start-timer');
                    this.$emit('update:seatInfos', this.localSeatInfos);
                    this.isEdit = false;
                    messageUtil.success(this.$t('SigtCommon.OperateSuccess'));
                } else {
                    messageUtil.error(this.$t('SigtCommon.OperateError'));
                }
            }).catch(() => {
                messageUtil.error(this.$t('SigtCommon.OperateError'));
            });
    }
    /**
     * 取消席位配置
     * @memberof SeatList
     */
    cancelConfiguration() {
        this.isEdit = false;
        this.$emit('start-timer');
        this.localSeatInfos = _.cloneDeep(this.seatInfos);
    }

    handlePreview(imgUrl: string) {
        this.bgImage = imgUrl;
    }

    cancel() {
        this.isEdit = false;
    }

    beforeDestroy() {
        clearInterval(this.timer);
    }
    /**
     * 席位信息缩放
     * @param zoomValue 当前缩放的值
     */
    zoomSize(zoomValue: number) {
        this.scaleValue = zoomValue;
        // this.showDetial = zoomValue > this.SCALE_FLAG;
    }
    handleMouseDownForBg(evt: MouseEvent) {
        this.clientX = evt.clientX - this.bgX;
        this.clientY = evt.clientY - this.bgY;
        this.isDown = true;
        this.startEventListener();
    }

    handleMouseMove(evt: MouseEvent) {
        if (!this.isDown) {
            return;
        }
        this.bgX = evt.clientX - this.clientX;
        this.bgY = evt.clientY - this.clientY;
    }

    handleMouseUp(evt: MouseEvent) {
        this.isDown = false;
        this.removeEventListener();
    }
    startEventListener() {
        document.removeEventListener('mousemove', this.handleMouseMove);
        document.addEventListener('mousemove', this.handleMouseMove);
        document.removeEventListener('mouseup', this.handleMouseUp);
        document.addEventListener('mouseup', this.handleMouseUp);
        document.addEventListener('mouseout', this.handleMouseUp);
        document.removeEventListener('mouseout', this.handleMouseUp);
    }
    removeEventListener() {
        document.removeEventListener('mousemove', this.handleMouseMove);
        document.removeEventListener('mouseout', this.handleMouseUp);
        document.removeEventListener('mouseup', this.handleMouseUp);
    }
    noop() {
        return false;
    }
}
