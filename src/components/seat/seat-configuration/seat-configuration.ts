import { Vue, Component, Prop } from 'vue-property-decorator';
import AttachService from '@/api/attach/attach-service';
import { ElUploadInternalFileDetail } from 'element-ui/types/upload';
import _ from 'lodash';
import { FloorInfo } from '../../../common/models/seat/floor-Info';
import seatInfoService from '../../../api/seat-info-service';
import messageUtil from '../../../common/utils/message-util';

/**
 * 席位配置
 */
@Component
export default class SeatConfiguration extends Vue {
  /**
   * 缩放步长
   */
  STEP_VALUE = 0.1;
  /**
   * 放大缩小的值
   */
  zoomValue = 1.5;
  @Prop({
    default: false
  })
  isEdit!: boolean;

  // 席位配置按钮是否可用
  @Prop({
    default: false
  })
  configBtnDisabled!: boolean;

  show = false;

  fileList: ElUploadInternalFileDetail[] = [];

  file!: ElUploadInternalFileDetail;

  imgUrl = '';

  isSave = false;

  isUploaded = false;

  isPreview = false;

  @Prop({
    default: () => {
      return new FloorInfo();
    }
  })
  floor!: FloorInfo;
  /**
   * 保存席位配置
   * @memberof SeatList
   */
  save() {
    this.$emit('save-config');
  }

  /**
   * 修改坐席位置
   * @memberof SeatConfiguration
   */
  deploySeat() {
    this.$parent.$emit('deploy-seat');
    this.$emit('update:isEdit', true);
  }
  /**
   * 取消修改
   * @memberof SeatConfiguration
   */
  cancel() {
    this.$emit('cancel-config');
  }
  /**
   * 添加底图
   * @memberof SeatConfiguration
   */
  addPicture() {
    this.show = true;
  }

  handleChange(file: ElUploadInternalFileDetail, fileList: ElUploadInternalFileDetail[]) {
    this.imgUrl = URL.createObjectURL(file.raw);
    this.file = file;
    this.fileList = [];
  }

  /**
   * 预览
   * @memberof SeatConfiguration
   */
  handlePreview() {
    this.isPreview = true;
    this.$emit('preview', this.imgUrl);
  }

  /**
   * 上传
   * @memberof SeatConfiguration
   */
  async handleUpload() {
    const files = await AttachService.uploadFileBatch([this.file]);
    if (!_.isEmpty(files)) {
      const fileId = files[0].fileId;
      const floor = Object.assign({}, this.floor);
      floor.fileId = fileId;
      seatInfoService.editFloor(floor)
        .then((res: any) => {
          if (res) {
            if (this.floor.fileId) {
              AttachService.deleteFile(this.floor.fileId);
            }
            this.isUploaded = true;
            this.isPreview = false;
            this.show = false;
            this.floor.fileId = floor.fileId;
            const url = AttachService.getPreviewUrlByFiledId(fileId);
            this.$emit('preview', url);
            this.$parent.$emit('upload-success');
            messageUtil.success(this.$t('SigtCommon.OperateSuccess'));
          } else {
            messageUtil.error(this.$t('SigtCommon.OperateError'));
          }
        }).catch(() => {
          messageUtil.error(this.$t('SigtCommon.OperateError'));
        });
    }
  }

  /**
   * 取消上传
   * @memberof SeatConfiguration
   */
  cancelUpload() {
    this.show = false;
    this.fileList = [];
    if (this.isPreview) {
      const url = this.floor.fileId ? AttachService.getPreviewUrlByFiledId(this.floor.fileId) : require('@/assets/imgs/allfloor.png');
      this.$emit('preview', url);
    }
  }

  /**
   * 关闭
   * @memberof SeatConfiguration
   */
  handleClose() {
    this.isUploaded = false;
    this.fileList = [];
    if (!this.isPreview) {
      this.imgUrl = '';
    }
  }
  zoomIn() {
    this.zoomValue += this.STEP_VALUE;
    this.emitZoomValue();
  }
  zoomOut() {
    this.zoomValue -= this.STEP_VALUE;
    this.emitZoomValue();
  }
  emitZoomValue() {
    this.$emit('zoom-size', this.zoomValue);
  }
}
