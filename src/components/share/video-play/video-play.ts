import { Vue, Component, Prop, Watch } from 'vue-property-decorator';
import laboratoryListService from '@/api/laboratory-list-service';
import DeviceServer from '@/api/laboratory-device-service';
import dataDictionaryUtil from '@/common/utils/data-dictionary-util';
import { Getter } from 'vuex-class';


@Component
export default class VideoPlay extends Vue {


  videoList: any = [];

  @Prop()
  project: any;

  /**
   * 实验模板列表
   */
  templateListDataSource: any = [];

  /**
   * 当前项目的模板component
   */
  currentTemplate: any = {};

  wsUrl: any = '';


  mounted() {
    this.wsUrl = 'http' + this.$store.getters.configs.wsUrl;
    // this.$store.dispatch('setCurrentTemplateData', this.dataSource);
    this.initData();
  }

  async initData() {
    this.videoList = this.project.historyVideoUrls ? JSON.parse(this.project.historyVideoUrls) : [];
    // const components = dataDictionaryUtil.getTemplateListSource();
    console.log('视频列表', this.videoList);
    // this.templateListDataSource = components;
    // const templateDataSource = this.project.templateRecordData ?  JSON.parse(this.project.templateRecordData) : {};
    // this.$store.dispatch('setCurrentTemplateData', templateDataSource);
    this.videoList.forEach((i: any, index: any) => {
      this.createPlayer(index, i);
    });
  }

    createPlayer(index: any, filename: string) {
    this.$nextTick(() => {
      const video: any = this.$refs[`videoRef${index}`] as HTMLVideoElement;
      if (video[0]) {
        video[0].src = `${this.wsUrl}/dfile?filepath=${filename}`;
      }
    });
  }


}
