import resultTemplateService from '@/api/system-manage/result-template-service';
import ResultTemplateModel from '@/models/result-template-model';
import { Vue, Component, Prop, Watch } from 'vue-property-decorator';

@Component
export default class ResultInputTemplateComponent extends Vue {
  resultTemplates: ResultTemplateModel[] = [];
  dialogVisible = false;
  // htmlTest = ' <span>实验项目名称</span> <input></input>  <span>时间</span> <input></input>';
  htmlTest = '';
  async mounted() {
    this.resultTemplates = await resultTemplateService.GetAllResultTemplates();
    const template: ResultTemplateModel = new ResultTemplateModel();
    template.id = '21313';
    template.resultName = 'test';
    template.weight = 5;
    template.resultStatus = '';
    template.resultDesc = '负责钢材检测的实验室';
    template.createTime = '2021-6-14 21:09:10';
    this.resultTemplates.push(template);
  }

  showDetail() {
    this.dialogVisible = !this.dialogVisible;
    // this.$router.push({ path: '/laboratory-record-manager/laboratory-record' });
  }
}
