import { Vue, Component, Prop, Watch } from 'vue-property-decorator';
import dataRecordService from '@/api/data-record-service';

@Component({
  components: {}
})
export default class TemplateDataTable extends Vue {
  @Prop()
  templateType: any;

  @Prop()
  dialogVisible: any;

  dataSource: any[] = [];

  selectData: any = [];

  page: any = 0;
  allData: any = false;
  count: any = 0;

  @Watch('dialogVisible', { deep: true })
  onDialogVisibleChange(val: any) {
    if (val) {
      console.log(val);
    }
  }

  mounted() {
    this.getDataRecordInfoList();
    this.$nextTick(() => {
      this.listenerTableScroll();
    });
  }

  private listenerTableScroll() {
    const table: any = this.$refs['templateDataTable'];
    if (!table) {
      return;
    }
    const scrollContainer = table.bodyWrapper;
    scrollContainer.addEventListener('scroll', () => {
      const scrollTop = scrollContainer.scrollTop;
      const windowHeight = scrollContainer.clientHeight || scrollContainer.clientHeight;
      const scrollHeight = scrollContainer.scrollHeight || scrollContainer.scrollHeight;
      if (scrollTop + windowHeight === scrollHeight) {
        if (!this.allData) {
          this.page++;
          this.getDataRecordInfoList();
        } else {
          this.$message({ type: 'info', message: '已加载完毕所有数据' });
        }
      }
    });
  }

  findDataByIP(): any {
    const data: any = this.$store.state.app.dataConfigs;
    const ip = sessionStorage.getItem('login_ip');
    const ddata: any = data.filter((d: any) => d.ip === ip);
    return ddata[0];
  }

  getRowKeys(row: any) {
    console.log(row);
    return row.dataid;
  }

  async getDataRecordInfoList() {
    const deviceId = this.findDataByIP().deviceId;
    const result = await dataRecordService.pageQueryDataRecordInfoList(this.templateType, this.page, 10, deviceId);
    if (result && result.dataList && Array.isArray(result.dataList) && result.dataList.length > 0) {
      this.count = result.elementCount;
      this.dataSource = this.dataSource.concat(this.buildData(result.dataList));
      this.allData = this.count <= this.dataSource.length;
    } else {
      this.allData = true;
      this.$message({ type: 'info', message: '已加载全部数据' });
    }
  }

  /**
   * 确认
   */
  sureTemplateData() {
    if (this.templateType === 4 && this.selectData.length > 1) {
      this.$message({ type: 'warning', message: '当前模板做多选择一条实验结果' });
    } else if (this.templateType === 4) {
      this.$emit('on-select-data', this.selectData);
      this.$emit('close-data-popup');
    }

    if (this.templateType !== 4 && this.selectData.length > 0 && this.selectData.length <= 10) {
      this.$emit('on-select-data', this.selectData);
      this.$emit('close-data-popup');
    } else if (this.selectData.length > 10) {
      this.$message({ type: 'warning', message: '模板数据最多选择10条，请确认' });
    }
  }

  handleClose() {
    this.$emit('close-data-popup');
  }

  /**
   * 选项数据变化
   * @param val
   */
  onDataSelect(val: any) {
    this.selectData = val ? JSON.parse(JSON.stringify(val)) : [];
  }

  private buildData(data: any) {
    const tempArr: any = [];
    data.forEach((item: any) => {
      const obj = Object.assign({}, item);
      const itemData = JSON.parse(item.data);
      if (this.templateType === 4) {
        obj['value'] = itemData.items.length > 0 ? `${Number(itemData.items[0]).toFixed(3)}, ${Number(itemData.items[1]).toFixed(3)}` : '';
      } else {
        obj['value'] = itemData.items.length > 0 ? Number(itemData.items[0]).toFixed(2) : '';
      }
      tempArr.push(obj);
    });
    return tempArr;
  }

  refreshData() {
    this.page = 0;
    this.count = 0;
    this.allData = false;
    this.dataSource = [];
    this.getDataRecordInfoList();
  }
}
