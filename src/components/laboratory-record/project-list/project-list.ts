import { Vue, Component, Prop, Watch } from 'vue-property-decorator';
import laboratoryListService from '@/api/laboratory-list-service';
import DeviceServer from '@/api/laboratory-device-service';
import dataDictionaryUtil from '@/common/utils/data-dictionary-util';
import { Getter } from 'vuex-class';
import SchoolLog from '@/models/school-laborary-log-info';
import LaboratoryLogService from '@/api/laborartory-log-service';

@Component({
  components: {}
})
export default class ProjectListComponent extends Vue {
  wsUrl: any = '';

  @Prop()
  dialogVisible: any;

  @Prop()
  record: any;

  @Prop({ default: '' })
  private title!: string;

  // title: any = '项目列表';

  showList: any = true;

  projectList: any = [];

  currentRecordDetail: any = {};

  videoList: any = [];

  deviceGroup: any = [];
  deviceGroupName: any = [];

  templateListDataSource: any = [];

  DialogVisibleProjectName: any = false;

  currentProject: any = {};

  showVisible: any = false;

  currentTemplate: any = {};
  @Getter('TemplateLibrary_currentTemplateData')
  templateDataSource: any;

  @Watch('record', { deep: true })
  onRecordChange(val: any) {
    this.init();
  }

  mounted() {
    if (this.title === '实验变更记录管理') {
      this.showVisible = true;
    } else if (this.title === '实验数据采集列表') {
      this.showVisible = false;
    }
    console.log('init', this.record);
    this.wsUrl = 'http' + this.$store.getters.configs.wsUrl;
    this.init();
  }

  async init() {
    if (this.record) {
      const result = await laboratoryListService.GetLaboratoryRecordAndProjectById(this.record.id);
      const groupData = await DeviceServer.GetAllDeviceGroupInfo();
      const components = dataDictionaryUtil.getTemplateListSource();
      this.projectList = result && result.recordProjectInfos ? result.recordProjectInfos : [];
      this.deviceGroup = groupData && Array.isArray(groupData) ? groupData : [];
      this.templateListDataSource = components;
    }
  }

  /**
   * 创建播放器
   * @param url
   * @param uuid
   * @param filename
   */
  createPlayer(index: any, filename: string) {
    this.$nextTick(() => {
      const video: any = this.$refs[`videoRef${index}`] as HTMLVideoElement;
      if (video[0]) {
        video[0].src = `${this.wsUrl}/dfile?filepath=${filename}`;
      }
    });
  }

  handleClose() {
    if (this.showList) {
      this.$emit('on-close');
    } else {
      this.showList = true;
      this.title = '项目列表';
    }
  }

  handleDeleteClose() {
    if (this.showList) {
      this.$emit('on-delete-close');
    } else {
      this.showList = true;
      this.title = '项目列表';
    }
  }

  showDetail(val: any) {
    this.showList = false;
    this.currentRecordDetail = val;
    this.videoList = val.historyVideoUrls ? JSON.parse(val.historyVideoUrls) : [];
    const group = this.deviceGroup.find((i: any) => i.id === val.deviceGroupId);
    this.deviceGroupName = group ? group.group_name : '';
    this.title = '项目详情';
    const temp: any = this.templateListDataSource.find((item: any) => item.id === val.resultId);
    this.currentTemplate = temp;
    const templateDataSource = val.templateRecordData ? JSON.parse(val.templateRecordData) : {};
    this.$store.dispatch('setCurrentTemplateData', templateDataSource);
    this.videoList.forEach((i: any, index: any) => {
      this.createPlayer(index, i);
    });
  }

  /**
   * 编辑项目
   */
  editProject(project: any) {
    sessionStorage.setItem('edit_project', JSON.stringify({ projectData: project, recordData: this.record }));
    const strWindowFeatures = `
    menubar=yes,
    location=yes,
    resizable=yes,
    scrollbars=yes,
    status=yes`;
    window.open(`${this.$store.getters.configs.addWindows}/#/laboratory-record-manager/add-record`, '', strWindowFeatures);
  }

  /**
   * 编辑中心编号
   * @param record
   */
  updateProjectName(project: any) {
    this.DialogVisibleProjectName = true;
    this.currentProject = project;
  }

  deleteProject(project: any) {
    this.$confirm('是否删除？', '操作', {
      confirmButtonText: '确认',
      cancelButtonText: '取消',
      type: 'warning',
      closeOnClickModal: false
    }).then(async () => {
      if (this.projectList.length > 1) {
        const result = await laboratoryListService.deleteProject(project.id);
        if (result) {
          this.$message({ type: 'success', message: '删除成功' });
          this.init();
        } else {
          this.$message({ type: 'success', message: '删除失败' });
        }
      } else {
        const result = await laboratoryListService.deleteRecord(this.record.id);
        if (result) {
          this.$message({ type: 'success', message: '删除成功' });
          this.handleDeleteClose();
        } else {
          this.$message({ type: 'success', message: '删除失败' });
        }
      }
      this.addSystemLog(this.record.centerCode, '删除项目');
      // const isRepeatResult = await laboratoryListService.deleteProject(
      //   this.currentProject.id
      // );
    });
  }

  dialogoperationOK() {
    this.$confirm('是否编辑项目名称信息？', '操作', {
      confirmButtonText: '确认',
      cancelButtonText: '取消',
      type: 'warning',
      closeOnClickModal: false
    }).then(async () => {
      const isRepeatResult = await laboratoryListService.judgeRecordAndProjectNameIsRepeat(this.record.id, this.currentProject.projectName);
      if (isRepeatResult) {
        this.$message.warning('项目名称重复');
        this.DialogVisibleProjectName = false;
      } else {
        await laboratoryListService.modifyLaboratoryRecordProject(this.currentProject, this.currentProject.id);
        this.$message({ type: 'success', message: '修改成功' });
      }
      const result = await laboratoryListService.GetLaboratoryRecordAndProjectById(this.record.id);
      this.projectList = result && result.recordProjectInfos ? result.recordProjectInfos : [];
      this.addSystemLog(this.record.centerCode, '编辑项目名称');
      this.DialogVisibleProjectName = false;
    });
  }

  async addSystemLog(centerCode: any, desc: string) {
    const schoolLog = new SchoolLog();
    schoolLog.centercode = centerCode;
    schoolLog.operationdesc = desc;
    schoolLog.opeationpage = '实验记录变更';
    schoolLog.projectname = '实验变更记录管理';
    const result = await LaboratoryLogService.AddSystemOperationLog(schoolLog);
  }
}
