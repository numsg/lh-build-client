import { Vue, Component, Prop, Watch } from 'vue-property-decorator';
import ProjectListComponent from './project-list/project-list';
import laboratoryListService from '@/api/laboratory-list-service';
import StateLaboratoryCondition from '@/models/state-laboratory-condition';
import { debounce } from 'lodash';
import SchoolLog from '@/models/school-laborary-log-info';
import LaboratoryLogService from '@/api/laborartory-log-service';

@Component({
  components: {
    ProjectListComponent
  }
})
export default class LaboratoryRecordComponent extends Vue {

  currentPage = 1;
  pageSize = 10;

  laboratoryRecordDataSource: any = [];

  /**
   * 当前记录
   */
  currentLaboratoryRecord: any = {};

  isShowProjectList: any = false;

  DialogVisible: any = false;
  operateTitle: any = '编辑中心编号';
  showVisible: any = false;

  @Prop({ default: '' })
  private recordTitle!: string;

  popupTitle: any = '';

  @Prop()
  showAddButton: any;

  total: any = 0;

  queryCondition: StateLaboratoryCondition = new StateLaboratoryCondition();

  debounceSearch = debounce(this.onSearch, 300);

  created() {
    this.queryCondition.pageSize = this.pageSize; // 12;
    this.pageQueryRecords();
  }

  mounted() {
    if (this.recordTitle === '实验变更记录管理') {
      this.showVisible = true;
    } else if (this.recordTitle === '实验数据采集列表') {
      this.showVisible = false;
    }
  }


  async querySearch(queryString: any, cb: any) {
    const param = {
      centerCode: this.queryCondition.centerCode,
      laboraryId: '',
      pageNumber: 1,
      pageSize: 50,
      status: 0,
    };
    const recordDataSource = await laboratoryListService.GetAllLaboratoryRecords(param);
    const data = this.buildSearchData(recordDataSource.records);
    cb(data);
  }

    private buildSearchData(data: any) {
    if (data && Array.isArray(data)) {
      const dataSource = data.map((i: any) => {
        const temp = {
          value: i.centerCode,
          name: i.centerCode
        };
        return temp;
      });
      return dataSource;
    } else {
      return [];
    }
  }

  handleSelect(val: any) {
    console.log(val, this.queryCondition.centerCode);
  }


  onAdd() {
    this.$emit('add-click');
  }

  /**
   * 显示项目列表
   * @param record
   */
  showProjectList(record: any) {
    this.currentLaboratoryRecord = record ? record : this.currentLaboratoryRecord;
    this.isShowProjectList = !this.isShowProjectList;
    this.popupTitle = '项目列表';
  }

  /**
   * 显示项目列表
   * @param record
   */
  showDeleteProjectList(record: any) {
    // this.currentLaboratoryRecord = record ? record : this.currentLaboratoryRecord;
    this.laboratoryRecordDataSource = [];
    this.queryCondition.pageNumber = 1;
    this.queryCondition.centerCode = '';
    this.pageQueryRecords();
    this.isShowProjectList = !this.isShowProjectList;
    this.popupTitle = '项目列表';
  }

  /**
   * 编辑中心编号
   * @param record
   */
  editProjectInfo(record: any) {
    this.DialogVisible = true;
    this.currentLaboratoryRecord = record ? record : this.currentLaboratoryRecord;
  }

  dialogoperation() {
    this.$confirm('是否编辑中心编码信息？', '操作', {
      confirmButtonText: '确认',
      cancelButtonText: '取消',
      type: 'warning',
      closeOnClickModal: false
    }).then(async () => {
      const isRepeatResult = await laboratoryListService.judgeCenterCodeIsRepeat(this.currentLaboratoryRecord.centerCode);
      if (isRepeatResult) {
        this.$message.warning('中心编码重复');
      } else {
        await laboratoryListService.updateOneRecord(this.currentLaboratoryRecord);
        this.DialogVisible = false;
        this.laboratoryRecordDataSource = [];
        this.queryCondition.pageNumber = 1;
        this.queryCondition.centerCode = '';
        this.addSystemLog(this.currentLaboratoryRecord.centerCode, '编辑中心编码');
        this.pageQueryRecords();
      }
    });
  }

  /**
   * 删除实验变更记录
   * @param record
   */
  deleteProjectInfo(record: any) {
    this.currentLaboratoryRecord = record;
    this.$confirm('是否删除实验记录变更？', '操作', {
      confirmButtonText: '确认',
      cancelButtonText: '取消',
      type: 'warning',
      closeOnClickModal: false
    }).then(async () => {
      const result = await laboratoryListService.deleteRecord(this.currentLaboratoryRecord.id);
      if (result) {
        this.$message.warning('删除成功');
        this.laboratoryRecordDataSource = [];
        this.queryCondition.pageNumber = 1;
        this.queryCondition.centerCode = '';
        this.addSystemLog(this.currentLaboratoryRecord.centerCode, '删除实验记录');
        this.pageQueryRecords();
      } else {
        this.$message.warning('删除失败');
      }
    });
  }

  onScroll() {
    // const recordListDiv = this.$refs['recordListRef'] as HTMLDivElement;
    // const isBottom = recordListDiv.scrollHeight - recordListDiv.scrollTop === recordListDiv.clientHeight;
    // if (isBottom && this.laboratoryRecordDataSource.length < this.total) {
    //   this.queryCondition.pageNumber++;
    //   setTimeout(() => {
    //     this.pageQueryRecords();
    //   }, 300);
    // } else if (isBottom && this.laboratoryRecordDataSource.length === this.total) {
    //   this.$message({ type: 'info', message: '已加载全部数据' });
    // }
  }

  handleSizeChange(value: number) {
    this.laboratoryRecordDataSource = [];
    this.pageSize = value;
    this.pageQueryRecords();
  }
  handleCurrentChange(vlue: number) {
    this.laboratoryRecordDataSource = [];
    this.currentPage = vlue;
    this.queryCondition.pageNumber = vlue;
    this.pageQueryRecords();
  }

  resetSearch() {
    this.queryCondition.centerCode = '';
    this.queryCondition.pageNumber = 1;
    this.laboratoryRecordDataSource = [];
    // const df : any = this.$refs.elautocomplete;
    // df.clearable = true;
    this.pageQueryRecords();
  }

  onSearch() {
    this.queryCondition.pageNumber = 1;
    this.laboratoryRecordDataSource = [];
    this.pageQueryRecords();
  }

  async pageQueryRecords() {
    let recordDataSource;
    if (this.recordTitle === '实验数据采集列表') {
      recordDataSource = await laboratoryListService.GetAllLaboratoryRecords(this.queryCondition);
    } else {
      recordDataSource = await laboratoryListService.GetAllChangeLaboratoryRecords(this.queryCondition);
    }
    if (recordDataSource && recordDataSource.records && Array.isArray(recordDataSource.records)) {
      this.laboratoryRecordDataSource = recordDataSource.records; // this.laboratoryRecordDataSource.concat(recordDataSource.records);
      this.total = recordDataSource.total;
    }
  }

  async addSystemLog(centerCode: any, desc: string) {
    const schoolLog = new SchoolLog();
    schoolLog.centercode = centerCode;
    schoolLog.opeationpage = '实验记录变更';
    schoolLog.projectname = '实验记录变更管理';
    schoolLog.operationdesc = desc;
    const result = await LaboratoryLogService.AddSystemOperationLog(schoolLog);
  }

  test() {
    this.$router.push({ path: '/menuOrganization/template-manager' });
  }
}
