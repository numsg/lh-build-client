import { Vue, Component, Prop, Watch } from 'vue-property-decorator';
import dataDictionaryUtil from '@/common/utils/data-dictionary-util';

@Component({
  components: {}
})
export default class LaboratoryTemplateList extends Vue {
  templateDataSource: any[] = [];

  currentTemplate: any = {};

  templateName = '';

  mounted() {
    this.init();
  }

  async init() {
    const components = dataDictionaryUtil.getTemplateListSource();
    // this.templateDataSource = components && Array.isArray(components) ? components : [];

    const data: any = this.$store.state.app.dataConfigs;
    const ip = sessionStorage.getItem('login_ip');

    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < data.length; i++) {
      if (data[i].ip === ip) {
        components.forEach((e: any) => {
          if (JSON.stringify(data[i].templateType).indexOf(e.type) >= 0) {
            this.templateDataSource.push(e);
          }
        });
        break;
      }
    }

    this.currentTemplate = this.templateDataSource[0];
    this.setTemplate(this.currentTemplate);
  }

  onSelectChange(val: any) {
    const temp: any = this.templateDataSource.find((item: any) => item.id === val);
    this.setTemplate(temp);
  }

  private setTemplate(temp: any) {
    this.templateName = temp.name;
    this.currentTemplate = Object.assign({}, temp);
    this.$emit('template-choose', this.currentTemplate);
    this.$store.dispatch('setCurrentTemplate', this.currentTemplate);
    this.$store.dispatch('setCurrentTemplateData', {
      title: this.templateName,
      time: '',
      tableData: [],
      type: temp.type
    });
  }
}
