import { Vue, Component, Prop, Watch } from 'vue-property-decorator';
import FlvJs from 'flv.js';

@Component
export default class Player extends Vue {
  player!: FlvJs.Player;
  wsUrl: any = '';


  @Prop()
  device: any;

  @Prop()
  startPlay: any;

  @Prop()
  refStr: any;

  @Watch('startPlay')
  onStartPlayChange(val: any) {
    if (val) {
      // this.startRecording();
    }
    if (val === 0 && this.player) {
      this.player.pause();
    }
  }

  created() {
    this.wsUrl = 'ws' + this.$store.getters.configs.wsUrl + '/';
  }

  mounted() {
      this.startRecording();
  }




  /**
   * 开始录制
   */
  startRecording() {
    this.createPlayer(this.wsUrl, this.device.uuid, this.device.uuid, this.device.monitor_url);
    const video = this.$refs[this.refStr] as HTMLVideoElement;
    console.log(video, video.paused);
    if (video) {
      if (!video.paused) {
          // this.pause();
        } else {
          this.play();
        }
      }
  }

  /**
   * 播放
   * @memberof VideoPlayer
   */
  play() {
    if (this.player) {
      this.player.play();
    }
  }
  /**
   * 创建播放器
   * @param url
   * @param uuid
   * @param filename
   */
  createPlayer(url: string, uuid: string, filename: string, rstpUrl: string) {
    if (FlvJs.isSupported()) {
      if (this.player) {
        this.player.destroy();
      }
      const video = this.$refs[this.refStr] as HTMLVideoElement;
      if (video) {
        this.player = FlvJs.createPlayer({
          type: 'flv',
          isLive: true,
          url: this.wsUrl + 'rtsp?projectId=' + uuid + '&filename=' + filename + '.mp4&rurl=' + rstpUrl
        });
        this.player.attachMediaElement(video);
        try {
          this.player.load();
        } catch (error) {
          console.log(error);
        }
      }
    }
  }

  beforeDestroy() {
     if (this.player) {
        this.player.destroy();
      }
  }
}
