import { Vue, Component, Prop, Watch } from 'vue-property-decorator';
import DeviceServer from '@/api/laboratory-device-service';
import Player from './player';
import laboratoryListService from '@/api/laboratory-list-service';
import LaboratoryRecordModel from '@/models/laboratory-record-model';
import RecordProjectModel from '@/models/record-project-model';
import LaboratoryTemplateList from '../laboratory-template-list/laboratory-template-list';
import ResultTemplateModel from '@/models/result-template-model';
import SchoolLog from '@/models/school-laborary-log-info';
import LaboratoryLogService from '@/api/laborartory-log-service';

@Component({
  components: {
    Player,
    LaboratoryTemplateList
  }
})
export default class AddRecordComponent extends Vue {
  websocket: any;
  wsUrl: any = '';

  recordState: any = 0;

  /**
   * 设备列表
   */
  deviceGroup: any = [];
  /**
   * 当前设备列表GroupId
   */
  currentDeviceGroup: any = '';

  /**
   * 设备列表
   */
  deviceList: any = [];

  couldRecord: any = false;

  currentRecordProjectInfos: LaboratoryRecordModel = new LaboratoryRecordModel();

  /**
   * 中心编号
   */
  centerCode: any = '';

  /**
   * 项目名称
   */
  projectName: any = '';

  showRecord: any = true;
  uuidArr: any = [];

  showChooseTemplate: any = false;

  currentTemplate: ResultTemplateModel = new ResultTemplateModel();
  templateDataSource: any = {};

  created() {
    this.init();
  }

  mounted() {
    this.wsUrl = 'ws' + this.$store.getters.configs.wsUrl;
    this.createWebsocket();
  }

  /**
   * 验证是否可以录制
   */
  validateCouldRecord() {
    let flag = false;
    if (this.centerCode.length !== 11) {
      this.couldRecord = false;
      this.$message({ type: 'warning', message: '中心编号没有11位' });
      return;
    }
    if (!/^\d{4}\d{1,2}.[WC]\d{3}/.test(this.centerCode)) {
      this.couldRecord = false;
      this.$message({ type: 'warning', message: '中心编号不符合规则要求,正确格式如：202106AW222' });
      return;
    }
    if (
      this.projectName.replace(/\s*/g, '') !== '' &&
      this.centerCode.replace(/\s*/g, '') !== '' &&
      this.currentDeviceGroup.replace(/\s*/g, '') !== ''
    ) {
      flag = true;
    }
    this.couldRecord = flag;
  }

  async init() {
    this.deviceGroup = await DeviceServer.GetAllDeviceGroupInfo();
  }

  createWebsocket() {
    this.websocket = new WebSocket(this.wsUrl);
    // 成功连接后
    this.websocket.onopen = () => {
      console.log('websocket连接成功');
      this.websocket.send('000');
    };
    // 连接失败
    this.websocket.onerror = (evt: any) => {};
    // 收到信息后
    this.websocket.onmessage = (evt: any) => {};
    // 后端关闭ws的时候
    this.websocket.onclose = (evt: any) => {};
  }

  /**
   * 选择监控设备
   */
  async onDeviceGroupSelect(val: any) {
    this.currentDeviceGroup = val;
    this.deviceList = [];
    this.validateCouldRecord();
    const result = await DeviceServer.GetAllDeviceInfoByDeviceGroupId(val);
    if (result && Array.isArray(result)) {
      const tempData: any = [];
      result.forEach((item: any, index: any) => {
        const obj = Object.assign({}, item);
        obj['uuid'] = String(new Date().getFullYear()) + String(new Date().getMonth() + 1) + '/' + String(new Date().getTime()) + index;
        tempData.push(obj);
        this.uuidArr.push(obj.uuid);
      });
      this.deviceList = tempData;
    }
  }

  /**
   * 返回
   */
  onBack() {
    this.$alert('退出后数据将不会保留，确认退出采集？', '', {
      confirmButtonText: '确定',
      cancelButtonText: '取消'
    }).then(() => {
      this.sendStop();
      this.recordState = 0;
      this.$emit('back-add');
    });
  }

  /**
   * 开始录制
   */
  startRecording() {
    this.recordState = 1;
  }

  endRecording() {
    this.$alert('是否确认结束视频数据采集', '', {
      confirmButtonText: '确定',
      cancelButtonText: '取消'
    }).then(() => {
      this.onEndRecord();
    });
  }

  /**
   * 结束视频录制
   */
  private sendStop() {
    this.uuidArr.forEach((i: any) => {
      this.websocket.send(i);
    });
  }

  /**
   * 结束录制
   */
  onEndRecord() {
    this.sendStop();
    this.showRecord = false;
    this.showChooseTemplate = true;
    this.recordState = 0;
  }

  /**
   * 重新录制
   */
  resetRecording() {
    this.validateCouldRecord();
    this.showRecord = true;
  }

  /**
   * 提交
   */
  async onSubmit() {
    const isRepeat = await this.projectNameIsRepeat();
    if (isRepeat) {
      this.$message({ type: 'warning', message: '该中心编号下，项目名称重复，请更改项目名称' });
      return;
    }
    const recordInfoData = new LaboratoryRecordModel();
    const recordProject = new RecordProjectModel();
    recordInfoData.centerCode = this.centerCode;
    recordProject.deviceGroupId = this.currentDeviceGroup;
    recordProject.projectName = this.projectName;
    recordProject.resultId = this.currentTemplate.id;
    recordProject.templateRecordContent = this.currentTemplate.content;
    recordProject.templateRecordData = JSON.stringify(this.$store.getters.TemplateLibrary_currentTemplateData);
    const recordProjectModelList: any = [];
    this.deviceList.forEach((element: any) => {
      recordProjectModelList.push(element.uuid + '.mp4');
    });
    recordProject.historyVideoUrls = JSON.stringify(recordProjectModelList);
    const parameter = {
      recordInfo: recordInfoData,
      recordProjectInfo: recordProject
    };
    const result = await laboratoryListService.AddLaboratoryRecordProject(parameter);
    if (result && result.recordInfo) {
      this.$message({ type: 'success', message: '实验记录提交成功' });
      this.recordState = 0;
      this.addSystemLog(this.centerCode, '实验记录提交');
      this.$emit('back-add');
    }
  }

  private async projectNameIsRepeat() {
    let isRepeat = false;
    const param = {
      centerCode: this.centerCode,
      laboraryId: '',
      pageNumber: 1,
      pageSize: 1000,
      status: 0
    };
    const recordDataSource = await laboratoryListService.GetAllLaboratoryRecords(param);
    if (recordDataSource && recordDataSource.total > 0) {
      const result = await laboratoryListService.GetLaboratoryRecordAndProjectById(recordDataSource.records[0].id);
      if (result && result.recordProjectInfos && Array.isArray(result.recordProjectInfos)) {
        const repeatData = result.recordProjectInfos.find((i: any) => i.projectName === this.projectName);
        if (repeatData) {
          isRepeat = true;
        }
      }
    }
    return isRepeat;
  }

  /**
   * 确认模板
   */
  chooseTemplate() {
    if (!this.currentTemplate.id) {
      this.$alert('请选择一个模板', '', {
        confirmButtonText: '确定',
        callback: () => {}
      });
    } else {
      const obj = Object.assign({}, this.$store.getters.TemplateLibrary_currentTemplateData);
      obj.centerCode = this.centerCode;
      this.$store.dispatch('setCurrentTemplateData', obj);
      this.showChooseTemplate = false;
      console.log(this.$store.getters.TemplateLibrary_templateDataCenterCode.centerCode);
    }
  }

  /**
   * 选择模板
   * @param val
   */
  templateChoose(val: any) {
    this.currentTemplate = val;
  }

  async addSystemLog(centerCode: any, desc: string) {
    const schoolLog = new SchoolLog();
    schoolLog.centercode = centerCode;
    schoolLog.operationdesc = desc;
    schoolLog.opeationpage = '实验数据采集列表';
    schoolLog.projectname = '实验室数据采集管理';
    const result = await LaboratoryLogService.AddSystemOperationLog(schoolLog);
  }

  /**
   * 选择的中心编号
   */
  selectCenterCode() {}

  /**
   * 输入中心编号，模糊提示
   * @param queryString
   * @param cb
   */
  querySearch(queryString: any, cb: any) {
    console.log(queryString);
  }
}
