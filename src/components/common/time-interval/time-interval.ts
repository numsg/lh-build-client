/**
 * 时间区间查询
 */

import { Vue, Component, Prop } from 'vue-property-decorator';
import { Getter } from 'vuex-class';

import { formatTimeStr } from '../../../common/filters/filter';
import { AppTypes } from '../../../store/types/app-types';
import { ToDateString } from '../../../common/utils/date-utils';

@Component
export default class TimeInterval extends Vue {
  @Getter(AppTypes.getters.DATE_TYPE)
  dateType!: string;

  @Prop({ default: true })
  clearable!: boolean;


  queryModel = {
    analysisStartTime: '',
    analysisEndTime: ''
  };
  timeModel: any = [];

  created() {
    this.initDatePicker();
    this.confirmSearch();
  }

  confirmSearch() {
    if (this.timeModel) {
      this.queryModel.analysisStartTime = formatTimeStr(this.timeModel[0]);
      this.queryModel.analysisEndTime = formatTimeStr(this.timeModel[1]);
      this.$emit('confirmSearch', this.queryModel);
    }
  }
  cancelSearch() {
    this.initDatePicker();
    this.queryModel = {
      analysisStartTime: '',
      analysisEndTime: ''
    };
    this.confirmSearch();
  }

  initDatePicker() {
    const timeDay = ToDateString(new Date()).substring(0, 10);
    this.timeModel = [new Date(`${timeDay} 00:00:00`), new Date(`${timeDay} 23:59:59`)];
  }
}
