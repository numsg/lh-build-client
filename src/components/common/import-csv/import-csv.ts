import { sleep } from '../../../common/utils/util';
import { Vue, Component, Prop } from 'vue-property-decorator';
import csvUtil from '../../../common/utils/csv-util';
import _ from 'lodash';
import messageUtil from '../../../common/utils/message-util';
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';

@Component({
  components: {}
})
export default class ImportCSV extends Vue {
  @Prop({
    default: false
  })
  show!: boolean;

  @Prop({
    default: () => {
      return [];
    }
  })
  columns!: string[];

  // 表头
  @Prop({
    default: () => {
      return [];
    }
  })
  headers!: string[];

  /**
   * 模板文件名
   * @type {string}
   * @memberof ImportCSV
   */
  @Prop()
  templateFileName!: string;

  // 数据校验规则
  @Prop({
    default: () => {
      return {};
    }
  })
  rules!: any;
  // 本地的表头
  localHeaders: any[] = [];
  // 本地的列值
  localColums: any[] = [];

  fileList = [];

  // 导入的所以数据
  dataSource: any[] = [];

  // 表格数据源
  dataList: any[] = [];
  // 分页参数
  pageItem = { pageIndex: 1, pageSize: 9 };
  // 总数
  total = 0;
  // 导入按钮loading
  importLoading = false;
  exporLoading = false;
  get disabled() {
    const invalidData = this.dataSource.filter((res: any) => !!res.errormsg);
    return this.dataSource.length === 0 || invalidData.length > 0;
  }

  created() {
    this.localHeaders = this.headers.concat([this.$t('SigtCommon.State').toString()]);
    this.localColums = this.columns.concat(['errormsg']);
  }

  mounted() {
    NProgress.configure({
      parent: '#importFooter',
      showSpinner: false,
      trickleSpeed: 300
    });
  }

  /**
   * 导入csv文件
   * @param {*} file
   * @param {any[]} fileList
   * @returns
   * @memberof ImportCSV
   */
  async handleChange(file: any, fileList: any[]) {
    if (_.isEmpty(fileList)) {
      return;
    }
    const isCsv = file.raw.name.endsWith('.csv');
    if (!isCsv) {
      messageUtil.warning(this.$t('SigtCommon.PleaseImportCSV'));
      return;
    }
    if (NProgress.isStarted()) {
      NProgress.done();
    }
    NProgress.start();
    csvUtil
      .import(file.raw, this.columns)
      .then((dataSource: any[]) => {
        // excelUtil.import 导入excel文件
        this.pageItem.pageIndex = 1;
        this.dataSource = dataSource;
        this.total = dataSource.length;
        this.validate(dataSource);
        this.dataList = this.pageQuery(this.pageItem, this.dataSource);
      })
      .catch(() => {
        messageUtil.error(this.$t('SigtCommon.ImportFaild'));
        this.dataSource = [];
        this.total = 0;
      })
      .finally(async () => {
        this.fileList = [];
        await sleep(300);
        NProgress.done();
      });
  }

  /**
   * 根据规则校验数据
   * @param {any[]} dataSource
   * @returns
   * @memberof ImportCSV
   */
  validate(dataSource: any[]) {
    if (_.isEmpty(dataSource)) {
      return;
    }
    if (_.isEmpty(this.rules)) {
      return;
    }
    dataSource.forEach((data: any) => {
      data['errormsg'] = '';
      let flag = false;
      const rules = this.rules;
      // tslint:disable-next-line: forin
      for (const field in rules) {
        const ruleList = rules[field];
        if (_.isEmpty(ruleList)) {
          return;
        }
        if (!data.hasOwnProperty(field)) {
          return;
        }
        if (flag) {
          return;
        }
        for (const rule of ruleList) {
          if (rule.hasOwnProperty('required') && rule['required']) {
            if (_.isEmpty(data[field])) {
              data['errormsg'] = rule.message;
              flag = true;
              return;
            }
          }
          if (rule.hasOwnProperty('max') && data[field]) {
            if (data[field].length > rule.max) {
              data['errormsg'] = rule.message;
              flag = true;
              return;
            }
          }
          if (rule.hasOwnProperty('pattern') && data[field]) {
            if (!rule['pattern'].test(data[field])) {
              data['errormsg'] = rule.message;
              flag = true;
              return;
            }
          }
          if (rule.hasOwnProperty('unique') && rule['unique'] && data[field]) {
            let repeats = [];
            if (rule.hasOwnProperty('comkey')) {
              // 判断是否与其它字段组合成唯一
              const comkey = rule['comkey'];
              repeats = dataSource.filter((e: any) => e[field] + e[comkey] === data[field] + data[comkey]);
            } else {
              repeats = dataSource.filter((e: any) => e[field] === data[field]);
            }
            if (!_.isEmpty(repeats) && repeats.length > 1) {
              data['errormsg'] = rule.message;
              flag = true;
              return;
            }
          }
          if (rule.hasOwnProperty('validator') && rule['validator']) {
            const validator = rule['validator'];
            validator(dataSource, data[field], (err: Error) => {
              if (err) {
                data['errormsg'] = err.message;
                flag = true;
                return;
              }
            });
          }
        }
      }
    });
    const errorData = dataSource.filter((e: any) => !!e.errormsg);
    const correctData = dataSource.filter((e: any) => !e.errormsg);
    this.dataSource = [...errorData, ...correctData];
  }

  /**
   * 保存到数据库
   * @memberof ImportCSV
   */
  importData() {
    this.importLoading = true;
    if (NProgress.isStarted()) {
      NProgress.done();
    }
    NProgress.start();
    this.$emit('import-data', this.dataSource, this.handleImportResult);
  }

  /**
   * 保存成功的回调
   * @param {*} arg
   * @memberof ImportCSV
   */
  async handleImportResult(arg: any) {
    if (arg) {
      await sleep(1000);
      NProgress.done();
      messageUtil.success(this.$t('SigtCommon.OperateSuccess'));
      this.$emit('update:show', false);
    } else {
      await sleep(1000);
      NProgress.done();
      messageUtil.error(this.$t('SigtCommon.OperateError'));
    }
    this.importLoading = false;
  }

  /**
   * 下载模板
   * @memberof ImportCSV
   */
  async downTemplate() {
    this.exporLoading = true;
    const fileName = `${this.templateFileName || this.$t('SigtCommon.Template')}.csv`;
    csvUtil.export({
      value: [],
      headers: this.headers,
      columns: this.columns,
      fileName
    });
    await sleep(300);
    this.exporLoading = false;
  }

  /**
   * 本地分页
   * @param pageItem
   * @param dataSource
   */
  pageQuery(pageItem: { pageIndex: number; pageSize: number }, dataSource: any[]) {
    return dataSource.slice(pageItem.pageSize * (pageItem.pageIndex - 1), pageItem.pageSize * pageItem.pageIndex);
  }
  /**
   * 页码改变
   * @param {number} page
   * @memberof ImportCSV
   */
  handleCurrentChange(page: number) {
    this.pageItem.pageIndex = page;
    this.dataList = this.pageQuery(this.pageItem, this.dataSource);
  }

  /**
   * 表头渲染
   * @param {*} h
   * @param {*} { column }
   * @returns
   * @memberof ImportCSV
   */
  labelHead(h: any, { column }: any) {
    const l = column.label.length;
    const f = 14;
    column.minWidth = f * (l + 1);
    return h('div', { class: 'table-head', style: { width: '100%' } }, [column.label]);
  }

  /**
   * 表格宽度
   * @param {string} label
   * @returns
   * @memberof ImportCSV
   */
  tableColumnWidth(label: string) {
    return label && (label.length + 1) * 14;
  }

  handleCancel() {
    this.$emit('update:show', false);
  }

  handleClose() {
    this.dataSource = [];
    this.dataList = [];
    this.total = 0;
    NProgress.done();
    NProgress.remove();
    this.importLoading = false;
    this.pageItem = { pageIndex: 1, pageSize: 9 };
    this.$emit('update:show', false);
  }
}
