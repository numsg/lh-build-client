import { Vue, Component, Prop, Watch } from 'vue-property-decorator';
import { Getter } from 'vuex-class';

import { ElUploadInternalFileDetail } from 'element-ui/types/upload';


import { stringFormatArr } from '@gsafety/cad-gutil/dist/stringformat';
import { rxevent } from '@gsafety/rx-eventbus/dist/eventaggregator.service';

import { format } from 'date-fns';
import _lodash from 'lodash';

import AttachService from '../../../api/attach/attach-service';
import { AttachUrl } from '../../../common/url/attach/attach-url';
import { FileInfo } from '../../../common/models/attach/file-info';
import { DisplayFileInfo } from '../../../common/models/attach/display-file-info';
import { getFormat } from '../../../common/utils/date-utils';
import messageUtil from '../../../common/utils/message-util';

import { EventKeys } from '../../../common/constant/event-keys';
import { sleep } from '../../../common/utils/util';

@Component({
  components: {}
})
export default class CommonAttach extends Vue {
  /**
   * 是创建(creat),还是展示(show)
   * @private
   * @type {string}
   * @memberof CommonAttach
   */
  @Prop({ default: 'creat' })
  private type!: 'creat' | 'edit' | 'show';

  @Prop({ default: () => '' })
  private fileIds!: string;

  @Getter('configs')
  configs!: any;

  @Prop({
    default: 5
  })
  limit!: number;

  private fileList: ElUploadInternalFileDetail[] = [];

  private fileBackFiles: FileInfo[] = [];

  private dateFormat = getFormat().DATETIME;
  loading = false;
  onclick = 'return true';
  get currentDownUrl() {
    return (fileId: string) => {
      return this.configs.attachUrl + stringFormatArr(AttachUrl.downLoad, [fileId]);
    };
  }

  get displayFileInfos() {
    const fileInfos = [] as DisplayFileInfo[];
    if (this.fileBackFiles.length > 0) {
      for (const file of this.fileBackFiles) {
        const item = new DisplayFileInfo();
        item.id = file.fileId;
        item.type = this.getAttachType(file.contentType);
        item.name = file.fileName;
        item.time = format(file.uploadDate, this.dateFormat);
        fileInfos.push(item);
      }
    }
    if (this.fileList.length > 0) {
      for (const file of this.fileList) {
        const item = new DisplayFileInfo();
        item.id = file.uid.toString();
        item.type = this.getAttachType(file.raw.type);
        item.name = file.name;
        item.time = format((file.raw as any).lastModifiedDate, this.dateFormat);
        fileInfos.push(item);
      }
    }
    return _lodash.uniqBy(fileInfos, (file: DisplayFileInfo) => file.id);
  }

  /**
   * 数据回填回来的fileId, 只会显示不会修改
   * @param {string} ids
   * @memberof CommonAttach
   */
  @Watch('fileIds')
  watchFileIds(ids: string) {
    if (!!ids) {
      const queryReq = ids.split(',').map((id: any) => {
        return AttachService.queryAttachInfoByFileId(id);
      });
      Promise.all(queryReq).then((res: any) => {
        this.fileBackFiles = _lodash.flatten(res);
      });
    } else {
      this.fileBackFiles = [];
    }
  }
  async  mounted() {
    rxevent.subscribe(EventKeys.EndUploading, 'EndUploading', async (res: boolean) => {
      if (res) {
        this.loading = true;
        this.onclick = 'return false';
      } else {
        await sleep(300);
        this.loading = false;
        this.onclick = 'return true';
      }
    });
  }
  beforeDestroy() {
    rxevent.unsubscribe(EventKeys.EndUploading, 'EndUploading');
  }
  getAttachType(type: string) {
    if (type && type.includes('image')) {
      return 'image iconfont icon-tupianfujian';
    }
    if (type && type.includes('video')) {
      return 'video iconfont icon-shipinfujian';
    }
    if (type && type.includes('audio')) {
      return 'audio  iconfont icon-yinpinfujian';
    }
    if (type && type.includes('word')) {
      return 'word iconfont icon-wendangfujian';
    }
    if (type && type.includes('sheet')) {
      return 'excel iconfont icon-Excelfujian';
    }
    return 'other iconfont icon-qitafujian';
  }

  created() {
    this.watchFileIds(this.fileIds);
  }
  deleteFile(fileId: string) {
    this.fileList = this.fileList.filter((file: ElUploadInternalFileDetail) => file.uid.toString() !== fileId);
    this.fileBackFiles = this.fileBackFiles.filter((file: FileInfo) => file.fileId.toString() !== fileId);
  }

  handleChange(files: ElUploadInternalFileDetail, fileList: ElUploadInternalFileDetail[]) {
    let size = fileList.map((file: ElUploadInternalFileDetail) => file.size).reduce((pre, cur) => pre + cur);
    if (!_lodash.isEmpty(this.fileBackFiles)) {
      const fillBackSize = this.fileBackFiles.map((file: FileInfo) => file.size).reduce((pre, cur) => pre + cur);
      size += fillBackSize;
    }
    if (size <= 10 * 1024 * 1024) {
      this.fileList = _lodash.cloneDeep(fileList);
    } else {
      this.fileList = this.fileList.filter((file: ElUploadInternalFileDetail) => file.uid !== files.uid);
      messageUtil.warning(this.$i18n.t('SigtOrderAttach.OverSize'));
    }
  }
  handleExceed(files: ElUploadInternalFileDetail, fileList: ElUploadInternalFileDetail[]) {
    messageUtil.warning(this.$i18n.t('SigtOrderAttach.QuantityExceed'));
  }

  clearAll() {
    if (this.fileBackFiles.length === 0 && this.fileList.length === 0) {
      messageUtil.warning(this.$t('SigtCollapseTitles.AttachInfoEmpty'));
    } else {
      this.$confirm(this.$t('SigtOrganization.ClearWarn').toString(), this.$t('SigtCommon.Tips').toString(), {
        confirmButtonText: this.$t('SigtCommon.Confirm').toString(),
        cancelButtonText: this.$t('SigtCommon.Cancel').toString(),
        type: 'warning',
        closeOnPressEscape: false,
        closeOnClickModal: false
      }).then((res: any) => {
        this.fileBackFiles = [];
        this.fileList = [];
      });
    }
  }

  async getManualFileIds() {
    let fileIds = [];
    if (this.fileList.length > 0) {
      const fileInfoIs = await AttachService.uploadFileBatch(this.fileList);
      fileIds = fileInfoIs.map((file: any) => file.fileId);
    }
    if (!_lodash.isEmpty(this.fileIds)) {
      const ids = this.fileIds.split(',');
      const fillFileIds = this.fileBackFiles.map((e: FileInfo) => e.fileId);
      const diff = _lodash.difference(ids, fillFileIds);
      fileIds = fileIds.concat(fillFileIds);
      if (_lodash.isEmpty(diff)) {
        AttachService.batchDelete(diff);
      }
    }
    return fileIds.join(',');
  }

  getBase64Method(file: any): Promise<string> {
    return new Promise((resolve, reject) => {
      const fr = new FileReader();
      fr.readAsDataURL(file);
      fr.onload = (res: any) => {
        const base64 = fr.result ? fr.result : '';
        resolve(base64.toString());
      };
    });
  }

  async getFillBackFileIds() {
    return this.fileBackFiles.map((file: any) => file.fileId).join(',');
  }
}
