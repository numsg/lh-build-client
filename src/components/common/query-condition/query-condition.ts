import { Vue, Component, Prop} from 'vue-property-decorator';
import _lodash from 'lodash';
import { Getter } from 'vuex-class';

import { formatDate } from '../../../common/utils/date-utils';
import { QueryConditionInfo, QueryConditionType } from '../../../common/models/query-condition/query-condition-info';
import { AppTypes } from '../../../store/types/app-types';

/**
 * 参数设置
 */
@Component
export default class QueryCondition extends Vue {
  @Getter(AppTypes.getters.DATE_TYPE)
  dateType!: string;

  @Prop( { default: () => null, required: true} )
  private queryModel!: any;

  @Prop({ default: false })
  private clearButton!: boolean;

  private conditionType = QueryConditionType;

  get conditions() {
    let keys = Object.keys(this.queryModel);
    keys = keys.filter( (key) => key.substr(0, 1) === '_' );
    const conditionList: QueryConditionInfo[] = [];
    for ( const i in  this.queryModel ) {
      if ( keys.includes(i) ) {
        conditionList.push( this.queryModel[i] );
      }
    }
    return conditionList.sort((pre, next) => {
      return pre.sort - next.sort;
    });
  }

  mounted() {
    this.confirmSearch();
  }

  inputChange() {

  }


  formatDateFunc(data: any) {
    return formatDate(data, 'YYYY-MM-DD HH:mm:ss');
  }


  confirmSearch() {
    this.conditions.forEach((condition) => {
      const key = condition.key;
      if ( condition.type === QueryConditionType.DatePicker ) {
        const keys = condition.key.split('_').filter( (item) => item === '_' || item );
        if ( keys.length === 2 && Array.isArray(condition.value) && condition.value.length === 2) {
          if (this.queryModel.hasOwnProperty(keys[0])) {
            this.queryModel[keys[0]] = this.formatDateFunc(condition.value[0]);
          }
          if (this.queryModel.hasOwnProperty(keys[1])) {
            this.queryModel[keys[1]] = this.formatDateFunc(condition.value[1]);
          }
        }
      } else {
        if (this.queryModel.hasOwnProperty(key) ) {
          this.queryModel[key] = condition.value;
        }
      }
    });
    this.$emit('query', this.queryModel);
    return this.queryModel;
  }

  cancelSearch() {
    this.$emit('reset');
  }
}
