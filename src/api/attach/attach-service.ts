import store from '@/store';
import { AttachUrl } from '@/common/url/attach/attach-url';
import * as httpClient from '@gsafety/vue-httpclient/dist/httpclient';
import { FileInfo } from '../../common/models/attach/file-info';
import { stringFormatArr, stringFormat } from '@gsafety/cad-gutil/dist/stringformat';
import { ElUploadInternalFileDetail } from 'element-ui/types/upload';

export default {
  async uploadBatch(formData: FormData): Promise<FileInfo[]> {
    const url = store.getters.configs.attachUrl + AttachUrl.uploadBatch;
    const moduleName = store.getters.configs.moduleName;
    const config = { headers: { 'Content-Type': 'multipart/form-data' } };
    formData.append('metadata', `{"system":"SIGT","module":"${moduleName}"}`);
    return await httpClient.postPromise(url, formData, config);
  },
  async uploadFileBatch(fileList: ElUploadInternalFileDetail[]): Promise<any> {
      const formData = new FormData();
      fileList.forEach((o: any) => {
        formData.append('file', o.raw);
      });
      return this.uploadBatch(formData).catch( (e) => {
        return false;
      });
  },
  async queryAttachInfoByFileId(fileId: string) {
    let url = store.getters.configs.attachUrl + AttachUrl.attachesInfo;
    const moduleName = store.getters.configs.moduleName;
    url = stringFormatArr(url, [fileId, moduleName]);
    return await httpClient.getPromise(url);
  },
  async deleteFile(fileId: string) {
    let url = store.getters.configs.attachUrl + AttachUrl.deleteAttach;
    const moduleName = store.getters.configs.moduleName;
    url = stringFormatArr(url, [fileId, moduleName]);
    return await httpClient.deletePromise(url);
  },
  getPreviewUrlByFiledId(filedId: string) {
    return stringFormatArr(store.getters.configs.attachUrl + AttachUrl.attachesPreview, [filedId]);
  },

  /**
   * 根据所属模块和文件ID集合批量删除文件
   * @param {string[]} fileIds
   * @returns
   */
  async batchDelete(fileIds: string[]) {
    let url = store.getters.configs.attachUrl + AttachUrl.batchDelete;
    const moduleName = store.getters.configs.moduleName;
    url = stringFormat(url, moduleName);
    return await httpClient.postPromise(url, fileIds);
  }
};
