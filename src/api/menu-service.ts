import store from '@/store';
import { stringFormat } from '@gsafety/cad-gutil/dist/stringformat';
import * as httpclient from '@gsafety/vue-httpclient/dist/httpclient';
import { MenuUrl } from '@/common/url/menu-url';

export default {
  /**
   * 获取角色关联的功能菜单
   * @param roleId 角色ID
   */
  getMenuByRoleId(roleId: string, baseUrl: any) {
    const url = stringFormat(baseUrl + MenuUrl.getMenuByRoleId, roleId);
    return httpclient.getPromise(url);
  },
  /**
   * 获取角色关联的功能菜单【树】
   * @param roleId 角色ID
   */
  getMenuTreeByRoleId(roleId: string, baseUrl: any) {
    const url = stringFormat(baseUrl + MenuUrl.getMenuTreeByRoleId, roleId);
    return httpclient.getPromise(url);
  },
  /**
   * 获取菜单列表
   */
  getMenu() {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + MenuUrl.getMenu);
    return httpclient.getPromise(url);
  },
  /**
   * 获取菜单列表
   */
  getMenuTree() {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + MenuUrl.getMenuTree);
    return httpclient.getPromise(url);
  },
  configtRoleMenu(roleMenulist: any[]) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + MenuUrl.configMenus);
    return httpclient.postPromise(url, roleMenulist);
  },
  deleteRoleMenu(roleId: string) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + MenuUrl.deleteRoleMenus, roleId);
    return httpclient.deletePromise(url);
  }
};
