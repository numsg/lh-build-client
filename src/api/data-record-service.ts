import store from '@/store';
import * as httpclient from '@gsafety/vue-httpclient/dist/httpclient';

export default {
  /**
   *  查询所有数据记录(最新模板记录)
   * @returns
   */

  getLatestSchoolDataRecordInfoList(dataType: number) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = `${baseUrl}/api/v1/datarecord/${dataType}/latest`;
    return httpclient.getPromise(url);
  },
  /**
   * 分页查询实验数据
   * @param type
   * @param pageNum
   * @param pageSize
   * @returns
   */
  pageQueryDataRecordInfoList(type: any, pageNum: any, pageSize: any, deviceId: any) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = `${baseUrl}/api/v1/datarecord/${type}/page?pageNum=${pageNum}&pageSize=${pageSize}&deviceId=${deviceId}`;
    return httpclient.getPromise(url);
  },
  updateRecordData(data: any) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = `${baseUrl}/api/v1/datarecord/`;
    return httpclient.postPromise(url, data);
  }
};
