import store from '@/store';
import { stringFormat } from '@gsafety/cad-gutil/dist/stringformat';
import * as httpclient from '@gsafety/vue-httpclient/dist/httpclient';

export default {
    // 获取所有设备分组集合信息
    GetAllDeviceGroupInfo(): Promise<any> {
        const baseUrl = store.getters.configs.systemUrl;
        const url = stringFormat(baseUrl + '/api/v1/devicegroup/all');
        return httpclient.getPromise(url);
    },

    // 增加设备分组
    AddDeviceGroupInfo(DeviceGroup: any): Promise<any> {
        const baseUrl = store.getters.configs.systemUrl;
        const url = stringFormat(baseUrl + '/api/v1/devicegroup/add');
        return httpclient.postPromise(url, DeviceGroup);
    },

    // 编辑设备分组
    ModifyDeviceGroupInfo(DeviceGroup: any): Promise<any> {
        const baseUrl = store.getters.configs.systemUrl;
        const url = stringFormat(baseUrl + '/api/v1/devicegroup/modify');
        return httpclient.postPromise(url, DeviceGroup);
    },

    // 删除设备分组
    DeleteDeviceGroupInfo(DeviceGroup: any): Promise<any> {
        const baseUrl = store.getters.configs.systemUrl;
        const url = stringFormat(baseUrl + '/api/v1/devicegroup/delete');
        return httpclient.postPromise(url, DeviceGroup);
    },

    // 根据设备组ID获取所有设备集合信息
    GetAllDeviceInfoByDeviceGroupId(devicegroupId: any): Promise<any> {
        const baseUrl = store.getters.configs.systemUrl;
        const url = stringFormat(baseUrl + '/api/v1/device/all/' + devicegroupId);
        return httpclient.getPromise(url);
    },

    // 增加设备信息
    AddDeviceInfo(Device: any): Promise<any> {
        const baseUrl = store.getters.configs.systemUrl;
        const url = stringFormat(baseUrl + '/api/v1/device/add');
        return httpclient.postPromise(url, Device);
    },

    // 编辑设备信息
    ModifyDeviceInfo(Device: any): Promise<any> {
        const baseUrl = store.getters.configs.systemUrl;
        const url = stringFormat(baseUrl + '/api/v1/device/modify');
        return httpclient.postPromise(url, Device);
    },

    // 删除设备信息
    DeleteDeviceInfo(Device: any): Promise<any> {
        const baseUrl = store.getters.configs.systemUrl;
        const url = stringFormat(baseUrl + '/api/v1/device/delete');
        return httpclient.postPromise(url, Device);
    },
};
