import store from '@/store';
import * as httpclient from '@gsafety/vue-httpclient/dist/httpclient';
import { AreaUrl } from '@/common/url/area-url.ts';
export default {
  /**
   * 查询所有区域
   */
  async getAllAreas() {
    const url = store.getters.configs.systemUrl + AreaUrl.getAllArea;
    return httpclient.getPromise(url);
  }
};
