import SchoolLog from '@/models/school-laborary-log-info';
import store from '@/store';
import * as httpclient from '@gsafety/vue-httpclient/dist/httpclient';

export default {
    // 获取所有设备分组集合信息
    GetLogByPage(pageNum: number, pageSize: number, keyWord: string): Promise<any> {
        const baseUrl = store.getters.configs.systemUrl;
        const url = `${baseUrl}/api/v1/log/page/?pageNum=${pageNum}&pageSize=${pageSize}&keyWord=${keyWord}`;
        return httpclient.getPromise(url);
    },

    // 增加日志
    AddSystemOperationLog(schoolLog: SchoolLog) {
        const baseUrl = store.getters.configs.systemUrl;
        const url = `${baseUrl}/api/v1/log/add`;
        return httpclient.postPromise(url, schoolLog);
    }
};
