import ResultInfo from '@/models/result-info';
import SchoolLaboratoryInfo from '@/models/school-laboratory-info';
import store from '@/store';
import { stringFormat } from '@gsafety/cad-gutil/dist/stringformat';
import * as httpclient from '@gsafety/vue-httpclient/dist/httpclient';

export default {
  /**
   *  获取所有实验室信息
   * @returns
   */

  GetAllLaboratoryInfo(): Promise<SchoolLaboratoryInfo[]> {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + '/api/v1/laboratory');
    return httpclient.getPromise(url);
  },

  /**
   * 增加实验室信息
   * @param schoolLaboratoryInfo 实验室信息
   * @returns 是否添加成功
   */
  AddLaboratoryInfo(schoolLaboratoryInfo: SchoolLaboratoryInfo): Promise<boolean> {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + '/api/v1/laboratory/add');
    return httpclient.postPromise(url, schoolLaboratoryInfo);
  },

  /**
   * 修改实验室信息
   * @param schoolLaboratoryInfo 实验室信息
   * @returns 是否修改成功
   */
  UpdateLaboratoryInfo(schoolLaboratoryInfo: SchoolLaboratoryInfo): Promise<boolean> {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + '/api/v1/laboratory/modify');
    return httpclient.postPromise(url, schoolLaboratoryInfo);
  },
  /**
   * 修改实验室信息
   * @param schoolLaboratoryInfo 实验室信息
   * @returns 是否修改成功
   */
  DeletLaboratoryInfo(id: string): Promise<boolean> {
    const baseUrl = store.getters.configs.systemUrl;
    const url = `${baseUrl}/api/v1/laboratory/delete/${id}`;
    return httpclient.getPromise(url);
  },

  /**
   * 分页获取实验室数据
   * @param pageNum 页码
   * @param pageSize 每页大小
   * @param keyWord 搜索关键字
   * @returns
   */
  GetLaboratoryInfobyPage(pageNum: number, pageSize: number, keyWord: string): Promise<ResultInfo> {
    const baseUrl = store.getters.configs.systemUrl;
    const url = `${baseUrl}/api/v1/laboratory/page/?pageNum=${pageNum}&pageSize=${pageSize}&keyWord=${keyWord}`;
    return httpclient.getPromise(url);
  }
};



