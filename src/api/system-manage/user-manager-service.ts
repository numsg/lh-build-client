import { Md5 } from 'ts-md5';
import { stringFormat } from '@gsafety/cad-gutil/dist/stringformat';
import * as httpclient from '@gsafety/vue-httpclient/dist/httpclient';
import store from '@/store';

export default {
    findAll(): Promise<any> {
        const baseUrl = store.getters.configs.systemUrl;
        const url = stringFormat(baseUrl + '/api/v1/user/getall');
        return httpclient.getPromise(url);
    },
    updateUser(userInfo: any): Promise<any> {
        const baseUrl = store.getters.configs.systemUrl;
        const url = stringFormat(baseUrl + '/api/v1/user/modify');
        userInfo.password = Md5.hashStr(userInfo.password).toString();
        return httpclient.postPromise(url, userInfo);
    },
    addUser(userInfo: any): Promise<any> {
        const baseUrl = store.getters.configs.systemUrl;
        const url = stringFormat(baseUrl + '/api/v1/user/add');
        userInfo.password = Md5.hashStr(userInfo.password).toString();
        return httpclient.postPromise(url, userInfo);
    },
    resetUser(userInfo: any): Promise<any> {
        const baseUrl = store.getters.configs.systemUrl;
        const url = stringFormat(baseUrl + '/api/v1/user/reset');
        userInfo.password = Md5.hashStr(userInfo.password).toString();
        return httpclient.postPromise(url, userInfo);
    },
    deleteUser(userInfo: any): Promise<any> {
        const baseUrl = store.getters.configs.systemUrl;
        const url = stringFormat(baseUrl + '/api/v1/user/delete');
        return httpclient.postPromise(url, userInfo);
    },
};
