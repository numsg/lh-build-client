import store from '@/store';
import { stringFormat } from '@gsafety/cad-gutil/dist/stringformat';
import * as httpclient from '@gsafety/vue-httpclient/dist/httpclient';
export default {
  /**
   * 获取说有结果录入模板
   * @returns
   */
  GetAllResultTemplates(): Promise<any> {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat('http://172.22.24.179:30101' + '/api/v1/result-templates/all');
    return httpclient.getPromise(url);
  }
};
