
import store from '@/store';
import { UserUrls } from '../../common/url/system-manage/user-url';
import { UserInfo } from '../../common/models/system-manage/user-info';
import { Md5 } from 'ts-md5';
import { EditPwd } from '../../common/models/login/edit-pwd';
import { DefaultConfig } from '../../common/constant/default-config';
import lodash from 'lodash';
import { UserQueryModel } from '../../common/models/system-manage/user-query-model';

import { stringFormat } from '@gsafety/cad-gutil/dist/stringformat';
import * as httpclient from '@gsafety/vue-httpclient/dist/httpclient';
import odataClient from '@gsafety/odata-client/dist';

export default {
  /**
   * 添加用户信息
   * @param userInfo 用户信息
   */
  addUser(userInfo: any) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + UserUrls.addUser);
    userInfo.createdTime = '';
    userInfo.password = Md5.hashStr(userInfo.password).toString();
    return httpclient.postPromise(url, userInfo);
  },
  /**
   * 获取用户数据By机构Id
   */
  getUserByOrgId(currentNodeId: string): Promise<UserInfo[]> {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + UserUrls.getByNodeUrl, currentNodeId);
    return httpclient.getPromise(url);
  },
  /**
   * 递归-获取用户数据By机构Id
   */
  getUserByOrgIdResursion(currentNodeId: string): Promise<UserInfo[]> {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + UserUrls.getLowUserByStructNodeId, currentNodeId);
    return httpclient.getPromise(url);
  },
  /**
   * 更新用户
   * @param userInfo 用户
   */
  updateUser(userInfo: any) {
    const baseUrl = store.getters.configs.systemUrl;
    userInfo.createdTime = '';
    const url = stringFormat(baseUrl + UserUrls.updateUrl);
    return httpclient.putPromise(url, userInfo);
  },
  /**
   * 删除用户
   * @param userIds 用户集合
   */
  deleteUser(userIds: string[]) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + UserUrls.delUrl);
    return httpclient.postPromise(url, userIds);
  },
  /**
   * 根据用户账号查询用户信息
   * @param userName 用户名
   */
  getUserInfoByUserName(userName: string): Promise<UserInfo> {
    const url = stringFormat(store.getters.configs.systemUrl + UserUrls.getUserInfoByUserName, userName);
    return httpclient.getPromise(url);
  },
  /**
   * 根据用户Id查询用户信息
   * @param userId 用户Id
   */
  getUserInfoByUserId(userId: string): Promise<UserInfo> {
    const url = stringFormat(store.getters.configs.systemUrl + UserUrls.getUserInfoByUserId, userId);
    return httpclient.getPromise(url);
  },
  /**
   * 用户登录
   * @param userInfo 用户信息
   * @param baseUrl 服务基地址
   */
  login(userInfo: any, baseUrl: any): Promise<any> {
    const url = stringFormat(baseUrl + UserUrls.login);
    // 客户端密码加密
    // userInfo.password = Md5.hashStr(userInfo.password).toString();
    return httpclient.postPromise(url, userInfo);
  },

  loginvalidate(userInfo: any): Promise<any> {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + UserUrls.login);
    // 客户端密码加密
    // userInfo.password = Md5.hashStr(userInfo.password).toString();
    return httpclient.postPromise(url, userInfo);
  },
  /**
   * 修改密码
   */
  updatePwd(newPwd: EditPwd) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + UserUrls.updatePwd);
    const pwdInfo = lodash.cloneDeep(newPwd);
    pwdInfo.oldPassword = Md5.hashStr(pwdInfo.oldPassword).toString();
    pwdInfo.newPassword = Md5.hashStr(pwdInfo.newPassword).toString();
    pwdInfo.confirmPassword = Md5.hashStr(pwdInfo.confirmPassword).toString();
    return httpclient.postPromise(url, pwdInfo);
  },
  /**
   * 密码重置
   * @param userId 用户ID
   */
  reSetPWD(userPwds: any) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + UserUrls.reSetPwd);
    return httpclient.postPromise(url, userPwds);
  },
  /**
   * 用户机构节点更新-迁移
   * @param nodeId 机构ID
   * @param userInfos 用户信息数组
   */
  updateUserStructNode(nodeId: string, userInfos: UserInfo[]) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + UserUrls.updateUserStructNode + nodeId);
    return httpclient.postPromise(url, userInfos);
  },
  /**
   * 检查用户的电话队列绑定
   */
  checkUserPhoneQueue(currentNodeId: string, userPhoneQueue: string) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + UserUrls.checkUserPhoneQueue, currentNodeId, userPhoneQueue);
    return httpclient.getPromise(url);
  },
  /**
   * 获取所有用户
   */
  getAllUsers() {
    const q = odataClient({
      service: store.getters.configs.systemOdata,
      resources: 'UserViewEntity'
    });

    return q
      .skip(0)
      .and('username', 'ne', DefaultConfig.DEFAULT_USER)
      .orderby('username', 'asc')
      .get()
      .then((response: any) => {
        const result = JSON.parse(response.toJSON().body).value;
        return result;
      }).catch((err: any) => {
        console.log(err);
        return [];
      });
  },
  /**
   * 根据部门id和角色编码获取用户
   * @param nodeId 组织机构ID
   * @param roleCode 角色Code
   */
  queryUserByStructNodeAndRole(nodeId: string, roleCode: string): Promise<UserInfo[]> {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + UserUrls.queryUserByStructNodeAndRole, nodeId, roleCode);
    return httpclient.getPromise(url);
  },
  /**
   * 用户分页查询
   * @param queryModel 查询参数
   */
  getAllUserPaged(queryModel: UserQueryModel): Promise<{ value: UserInfo[], count: number }> {
    const q = odataClient({
      service: store.getters.configs.systemOdata,
      resources: 'UserViewEntity'
    });
    const filterStr = this.getQueryStr(queryModel);
    if (Array.isArray(queryModel.sort) && queryModel.sort.length > 0) {
      queryModel.sort.forEach((sortItem: any) => {
        if (Array.isArray(sortItem) && sortItem.length === 2) {
          q.orderby(sortItem[0], sortItem[1]);
        }
      });
    } else {
      q.orderby('createdTime', 'desc');
    }
    return q
      .skip(queryModel.pageIndex * queryModel.pageSize)
      .top(queryModel.pageSize)
      .filter(filterStr)
      .count(true)
      .get()
      .then((response: any) => {
        return {
          count: JSON.parse(response.body)['@odata.count'],
          value: JSON.parse(response.toJSON().body).value
        };
      }).catch((err: any) => {
        console.log(err);
        return {
          count: 0,
          value: []
        };
      });
  },
  /**
   * 组装查询条件
   * @param queryModel 查询条件
   */
  getQueryStr(queryModel: UserQueryModel) {
    let str = '';
    const firstStr = '(';
    const lastStr = ')';

    const defaultStr = `username ne '${DefaultConfig.DEFAULT_USER}'`;
    str = this.joint(str, defaultStr);
    if (queryModel.username.trim()) {
      const nStr = queryModel.username.trim();
      const usernameStr = `contains(username, '${nStr}')`;
      str = this.joint(str, usernameStr);
    }
    if (queryModel.usercode.trim()) {
      const codeStr = queryModel.usercode.trim();
      const usercodeStrStr = `contains(userCode, '${codeStr}')`;
      str = this.joint(str, usercodeStrStr);
    }
    if (queryModel.fixNumber.trim()) {
      const fixNumberStr = queryModel.fixNumber.trim();
      const phoneStr = `contains(phone, '${fixNumberStr}')`;
      str = this.joint(str, phoneStr);
    }
    if (queryModel.roleId.trim()) {
      const roleStr = queryModel.roleId.trim();
      const roleIdStr = `roleId eq '${roleStr}'`;
      str = this.joint(str, roleIdStr);
    }
    if (queryModel.structNodeId && queryModel.structNodeId !== '0') {
      const orgIdStr = `structNodeId eq '${queryModel.structNodeId}'`;
      str = this.joint(str, orgIdStr);
    }
    if (str) {
      str = firstStr + str + lastStr;
    }
    return str;
  },
  /**
   * 拼接参数
   * @param str 查询字符串
   * @param param 查询字段名
   */
  joint(str: string, param: string) {
    let filterStr = '';
    if (str) {
      filterStr = str + ` and ` + param;
    } else {
      filterStr = param;
    }
    return filterStr;
  }
};
