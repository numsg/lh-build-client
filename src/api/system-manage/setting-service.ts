import store from '@/store';

import { SettingUrl } from '../../common/url/system-manage/setting-url';
import { SettingInfo } from '../../common/models/system-manage/setting-info';
import { SettingTypeInfo } from '../../common/models/system-manage/setting-type-info';

import odataClient from '@gsafety/odata-client/dist';
import * as httpclient from '@gsafety/vue-httpclient/dist/httpclient';
import { stringFormatArr } from '@gsafety/cad-gutil/dist/stringformat';

export default {

  /**
   * 查询所有配置点
   * @returns {Promise<SettingInfo[]>}
   */
  findAllSettingInfos(): Promise<SettingInfo[]> {
    const url = store.getters.configs.systemUrl + SettingUrl.setting;
    return httpclient.getPromise(url);
  },

  /**
   * 查询所有设置的type名称
   */
  findAllSettingType() {
    const url = store.getters.configs.systemUrl + SettingUrl.settingTypes;
    return httpclient.getPromise(url);
  },
  /**
   * 根据参数类型查找已启用配置项
   */
  findSettingByType(typeDic: any) {
    const url = stringFormatArr(store.getters.configs.systemUrl + SettingUrl.findSettingByType, [typeDic]);
    return httpclient.getPromise(url);
  },

  /**
   * 根据参数类型查找所有配置项
   */
  findByParamType(typeDic: any) {
    const url = stringFormatArr(store.getters.configs.systemUrl + SettingUrl.findByParamType, [typeDic]);
    return httpclient.getPromise(url);
  },

  /**
   * 保存参数
   * @param settingInfo
   */
  saveSetting(settingInfo: SettingInfo) {
    if (settingInfo.settingId) {
      // 更新
      const url = stringFormatArr(store.getters.configs.systemUrl + SettingUrl.Query_Update_DeleteSetting, [settingInfo.settingId]);
      return httpclient.putPromise(url, settingInfo);
    } else {
      // 插入
      const url = `${store.getters.configs.systemUrl}` + SettingUrl.setting;
      return httpclient.postPromise(url, settingInfo);
    }
  },

  /**
   * 根据Id查询信息
   * @param {string} settingId
   *
   */
  QuerySetting(settingId: string) {
    const url = stringFormatArr(store.getters.configs.systemUrl + SettingUrl.Query_Update_DeleteSetting, [settingId]);
    return httpclient.getPromise(url);
  },

  /**
   * 根据Id删除参数
   * @param {string} settingId
   */
  deleteSetting(settingId: string) {
    const url = stringFormatArr(store.getters.configs.systemUrl + SettingUrl.Query_Update_DeleteSetting, [settingId]);
    return httpclient.deletePromise(url);
  },
  /**
   * 保存参数类型
   * @param settingTypeInfo
   */
  saveSettingType(settingTypeInfo: SettingTypeInfo) {
    const url = `${store.getters.configs.systemUrl}` + SettingUrl.settingTypes;
    return httpclient.postPromise(url, settingTypeInfo);
  },

  deleteSettingType(settingTypeId: string) {
    const url = stringFormatArr(store.getters.configs.systemUrl + SettingUrl.deleteSettingType, [settingTypeId]);
    return httpclient.deletePromise(url);
  },

  /**
   * 根据配置名称获取配置信息
   * @param {string} name
   */
  getSettingInfoByName(name: string) {
    const q = odataClient({
      service: store.getters.configs.systemOdata,
      resources: 'SchoolSettingEntity'
    });
    return q
      .skip(0)
      .filter('Name', 'eq', name)
      .get()
      .then((response: any) => {
        const res = JSON.parse(response.toJSON().body).value;
        return res[0];
      }).catch((err: any) => {
        console.log(err);
        return undefined;
      });
  }
};
