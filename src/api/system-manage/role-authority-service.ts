import * as httpclient from '@gsafety/vue-httpclient/dist/httpclient';
import store from '@/store';
import { stringFormat } from '@gsafety/cad-gutil/dist/stringformat';
import { RoleAuthorityUrl } from '@/common/url/system-manage/role-authority-url';
import { RoleInfo } from '../../common/models/role/role-info';
import odataClient from '@gsafety/odata-client/dist';

export default {
  /**
   * 获取角色信息
   */
  getAllRoles() {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + RoleAuthorityUrl.rolesManage);
    return httpclient.getPromise(url);
  },
  /**
   * 配置用户角色
   * @param userRoles 用户和角色关联关系
   * @param param
   */
  configRoles(userRoles: any[]) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + RoleAuthorityUrl.UserRolesUrl);
    return httpclient.postPromise(url, userRoles);
  },
  /**
   * 新增角色
   * @param userRoles 用户和角色关联关系
   * @param param
   */
  AddRole(roleInfo: RoleInfo) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + RoleAuthorityUrl.rolesManage);
    return httpclient.postPromise(url, roleInfo);
  },
  /**
   * 修改角色
   * @param userRoles 用户和角色关联关系
   * @param param
   */
  editRole(roleInfo: RoleInfo) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + RoleAuthorityUrl.rolesManage);
    return httpclient.putPromise(url, roleInfo);
  },
  /**
   * 删除角色
   * @param roleCode 角色编码
   * @param param
   */
  deleteRole(roleCodes: string[]) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + RoleAuthorityUrl.rolesDelete);
    return httpclient.postPromise(url, roleCodes);
  },
  /**
   * 获取用户关联的角色列表
   *
   * @param {string} userId
   */
  getRolesByUserId(userId: string, baseUrl: any): Promise<RoleInfo[]> {
    const url = stringFormat(baseUrl + RoleAuthorityUrl.getRolesByUserIdUrl, userId);
    return httpclient.getPromise(url);
  },


  // 获取所有角色类型
  getAllRoleTypes() {
    const q = odataClient({
      service: store.getters.configs.systemOdata,
      resources: 'roleEntity'
    });
    return q
      .skip(0)
      .orderby('id', 'asc')
      .get()
      .then((response: any) => {
        return JSON.parse(response.toJSON().body).value;

      }).catch((err: any) => {
        console.log(err);
        return [];
      });
  }
};
