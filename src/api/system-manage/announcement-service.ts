import store from '@/store';
import { stringFormat } from '@gsafety/cad-gutil/dist/stringformat';
import * as httpclient from '@gsafety/vue-httpclient/dist/httpclient';
import { AnnouncementUrls } from '@/common/url/system-manage/announcement-url';
import odataClient from '@gsafety/odata-client/dist';
import AnnouncementPerson from '../../common/models/system-manage/announcement-person';
import { AnnouncementQueryModel } from '../../common/models/system-manage/announcement-query-model';
export default {
  /**
   * 新增公告目录
   * @param annDirectory 公告目录
   */
  addAnnDirectory(annDirectory: any) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + AnnouncementUrls.addAnnDirectoryUrl);
    annDirectory.createTime = '';
    return httpclient.postPromise(url, annDirectory);
  },

  /**
   * 更新公告目录
   * @param annDirectory 公告目录
   */
  updateAnnDirectory(annDirectory: any) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + AnnouncementUrls.updateAnnDirectoryUrl);
    annDirectory.createTime = '';
    return httpclient.putPromise(url, annDirectory);
  },

  /**
   * 删除公告目录
   * @param id 公告Id
   */
  deleteAnnDirectory(id: string) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + AnnouncementUrls.deleteAnnDirectoryUrl, id);
    return httpclient.deletePromise(url);
  },

  /**
   * 新增公告
   * @param announcement 公告信息
   */
  addAnnouncement(announcement: any) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + AnnouncementUrls.addAnnUrl);
    announcement.createTime = '';
    return httpclient.postPromise(url, announcement);
  },

  /**
   * 更新公告
   * @param announcement 公告信息
   */
  updateAnnouncement(announcement: any) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + AnnouncementUrls.updateAnnUrl);
    announcement.createTime = '';
    return httpclient.putPromise(url, announcement);
  },

  /**
   * 删除公告
   * @param announcementIds 公告集合
   */
  deleteAnnouncement(announcementIds: string[]) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + AnnouncementUrls.deleteAnnouncementUrl);
    return httpclient.postPromise(url, announcementIds);
  },

  /**
   * 发布公告
   * @param announcement 公告信息
   */
  publishAnnouncement(announcement: any) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + AnnouncementUrls.publishAnnouncementUrl);
    return httpclient.putPromise(url, announcement);
  },

  /**
   * 撤销公告
   * @param announcementId 公告Id
   */
  revokeAnnouncement(announcementId: any) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + AnnouncementUrls.revokeAnnouncementUrl, announcementId);
    return httpclient.putPromise(url);
  },

  /**
   * 获取所有公告目录
   */
  getAllAnnDirectorys(): Promise<any> {
    const q = odataClient({
      service: store.getters.configs.systemOdata,
      resources: 'AnnouncementDirectoryEntity'
    });
    return q
      .skip(0)
      .orderby('id', 'asc')
      .get()
      .then((response: any) => {
        return JSON.parse(response.toJSON().body).value;

      }).catch((err: any) => {
        console.log(err);
        return [];
      });
  },

  /**
   * 获取所有公告
   * @param queryModel 公告查询参数
   */
  getAllAnnouncements(queryModel: AnnouncementQueryModel): Promise<any> {
    const q = odataClient({
      service: store.getters.configs.systemOdata,
      resources: 'AnnouncementEntity'
    });
    const filterStr = this.getQueryStr(queryModel);
    if (Array.isArray(queryModel.sort) && queryModel.sort.length > 0) {
      queryModel.sort.forEach((sortItem: any) => {
        if (Array.isArray(sortItem) && sortItem.length === 2) {
          q.orderby(sortItem[0], sortItem[1]);
        }
      });
    } else {
      q.orderby('createTime', 'desc');
      q.orderby('title', 'asc');
    }

    return q
      .skip(queryModel.pageIndex * queryModel.pageSize)
      .top(queryModel.pageSize)
      .filter(filterStr)
      .count(true)
      .get()
      .then((response: any) => {
        const result = {
          count: JSON.parse(response.body)['@odata.count'],
          value: JSON.parse(response.toJSON().body).value
        };
        return result;
      }).catch((err: any) => {
        console.log(err);
        return {
          count: 0,
          value: []
        };
      });
  },

  /**
   * 获取所有未过期的公告
   * @param queryModel 公告参数
   */
  getAllNotExpiredAnnouncements(queryModel: AnnouncementQueryModel) {
    const q = odataClient({
      service: store.getters.configs.systemOdata,
      resources: 'AnnouncementEntity'
    });

    const titleStr = queryModel.announcementTitle ? `contains(title, '${queryModel.announcementTitle}')` : this.getFilterStr('title');
    if (Array.isArray(queryModel.sort) && queryModel.sort.length > 0) {
      queryModel.sort.forEach((sortItem: any) => {
        if (Array.isArray(sortItem) && sortItem.length === 2) {
          q.orderby(sortItem[0], sortItem[1]);
        }
      });
    } else {
      q.orderby('createTime', 'desc');
      q.orderby('title', 'asc');
    }
    return q
      .skip(queryModel.pageIndex * queryModel.pageSize)
      .top(queryModel.pageSize)
      .and(titleStr)
      .and('expiredTime', '>=', new Date())
      .and('status', '=', 2)
      .count(true)
      .get()
      .then((response: any) => {
        const result = {
          count: JSON.parse(response.body)['@odata.count'],
          value: JSON.parse(response.toJSON().body).value
        };
        return result;
      }).catch((err: any) => {
        console.log(err);
        return [];
      });
  },

  /**
   * Odata查询条件拼接
   * @param field 查询字段
   */
  getFilterStr(field: string) {
    if (field === 'status') {
      return `(${field} ne 0 ) or (${field} eq 0)`;
    }
    return `(${field} ne \'0\') or (${field} eq \'0\')`;
  },
  /**
   * 根据标题查找公告信息
   * @param title 标题
   */
  getAnnouncementsByTitle(title: string) {
    const q = odataClient({
      service: store.getters.configs.systemOdata,
      resources: 'AnnouncementEntity'
    });
    return q
      .filter('title', '=', title)
      .get()
      .then((response: any) => {
        const result = {
          value: JSON.parse(response.toJSON().body).value
        };
        return result;
      }).catch((err: any) => {
        console.log(err);
        return [];
      });
  },
  /**
   * 获取当前用户未过期的公告-分页查询
   * @param queryModel 查询条件
   */
  getAllNotExpiredAnnouncementsByUserId(queryModel: AnnouncementQueryModel) {
    const q = odataClient({
      service: store.getters.configs.systemOdata,
      resources: 'AnnouncementViewEntity'
    });

    const titleStr = queryModel.announcementTitle ? `contains(title, '${queryModel.announcementTitle}')` : this.getFilterStr('title');
    const userIdStr = queryModel.userId ? `userId eq '${queryModel.userId}'` : this.getFilterStr('userId');
    if (Array.isArray(queryModel.sort) && queryModel.sort.length > 0) {
      queryModel.sort.forEach((sortItem: any) => {
        if (Array.isArray(sortItem) && sortItem.length === 2) {
          q.orderby(sortItem[0], sortItem[1]);
        }
      });
    } else {
      q.orderby('createTime', 'desc');
      q.orderby('title', 'asc');
    }
    return q
      .skip(queryModel.pageIndex * queryModel.pageSize)
      .top(queryModel.pageSize)
      .and(userIdStr)
      .and(titleStr)
      .count(true)
      .get()
      .then((response: any) => {
        const result = {
          count: JSON.parse(response.body)['@odata.count'],
          value: JSON.parse(response.toJSON().body).value
        };
        return result;
      }).catch((err: any) => {
        console.log(err);
        return [];
      });
  },
  /**
   * 获取当前用户未过期的公告-消息使用
   * @param queryModel 查询条件
   */
  getAllNotExpiredAnnouncementsByUserIdAndSeadState(queryModel: any) {
    const q = odataClient({
      service: store.getters.configs.systemOdata,
      resources: 'AnnouncementViewEntity'
    });
    return q
      .skip(0)
      .filter('userId', '=', queryModel.userId)
      .and('sendState', '=', 0)
      .get()
      .then((response: any) => {
        const result = JSON.parse(response.toJSON().body).value;
        return result;
      }).catch((err: any) => {
        console.log(err);
        return [];
      });
  },
  /**
   * 更新公告人员信息
   * @param announcementPersons 公告人员信息
   */
  updateAnnouncementSendState(announcementPersons: AnnouncementPerson[]) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + AnnouncementUrls.updateSendStateUrl);
    return httpclient.putPromise(url, announcementPersons);
  },
  /**
   * 组装查询条件
   * @param annQueryModel 查询条件
   */
  getQueryStr(annQueryModel: any) {
    let str = '';
    const firstStr = '(';
    const lastStr = ')';
    if (annQueryModel.announcementTitle.trim()) {
      const tStr = annQueryModel.announcementTitle.trim();
      const titleStr = `contains(title, '${tStr}')`;
      str = this.joint(str, titleStr);
    }
    if (annQueryModel.annDirectoryId.trim()) {
      const dStr = annQueryModel.annDirectoryId.trim();
      const directoryStr = `(directoryId eq '${dStr}')`;
      str = this.joint(str, directoryStr);
    }
    if (annQueryModel.announcementState !== '') {
      const sStr = annQueryModel.announcementState;
      const stateStr = `status eq ${Number(sStr)}`;
      str = this.joint(str, stateStr);
    } else {
      const defaultStr = `(status ne 0 ) or (status eq 0)`;
      str = this.joint(str, defaultStr);
    }
    if (str) {
      str = firstStr + str + lastStr;
    }
    return str;
  },
  /**
   * 拼接参数
   * @param str 查询字符串
   * @param param 查询字段名
   */
  joint(str: string, param: string) {
    let filterStr = '';
    if (str) {
      filterStr = str + ` and ` + param;
    } else {
      filterStr = param;
    }
    return filterStr;
  },
};
