import store from '@/store';
import * as httpclient from '@gsafety/vue-httpclient/dist/httpclient';
import { MessageInfo } from '@gsafety/presence-client/dist/message-info';
import { MessageType } from '@gsafety/presence-client/dist/message-type';
import { logFactory } from '@gsafety/cad-glog';
import { stringFormatArr } from '@gsafety/cad-gutil/dist/stringformat';
import { Message } from '@gsafety/cad-notice-module/dist/models/message';

import { PressenceUrl } from '../common/url/pressence-url';
import { SeatStatus } from '../common/models/seat-status';

const logger = logFactory.getLogger('ws-seat-service');

export default {
  /**
   * 发送消息通知
   * @param msgInfo
   */
  async sendMessage(msgInfo: MessageInfo) {
    const url = store.getters.configs.websocketServicePort + PressenceUrl.sendMessage;
    return await httpclient.postPromise(url, msgInfo);
  },
  /**
   *
   * 删除席位状态数据（这里是退出时候删除席位数据）目前没有给目标HUB 如果有业务可以添加对应的业务
   * @param {SeatStatus} clientInfo
   * @returns
   */
  async logOffSeatStatus(clientInfo: SeatStatus) {
    if (clientInfo != null) {
      const msgInfo = new MessageInfo('registerId', clientInfo.registerId, true, MessageType.DisConnect, {
        hub: '',
        method: '',
        args: clientInfo
      });
      return this.sendMessage(msgInfo);
    }
  },
  /**
   * 发送上线通知(这里是登录时候注册) 目前没有给目标HUB 如果有业务可以添加对应的业务
   *
   * @param {SeatStatus} clientInfo
   */
  async sendLoginNotice(clientInfo: SeatStatus) {
    if (clientInfo != null) {
      const msgInfo = new MessageInfo('registerId', clientInfo.registerId, true, MessageType.OnlineRegister, {
        hub: '',
        method: '',
        args: clientInfo
      });
      return this.sendMessage(msgInfo);
    }
  },


  /**
   * 获取所有的用户
   */
  getAllClient() {
    const url = store.getters.configs.websocketServicePort + PressenceUrl.getAllClientSeat;
    return httpclient.getPromise(url).then((res: any) => {
      return res.success ? res.data : [];
    });
  },

  /**
   * 按条件查询在线席位
   *
   * @param {string} proName 关键字名称 例如：roleId
   * @param {string} proValues  关键字内容，多个值可以用逗号分隔   例如：101,102
   * @param {string} isRegisterPro 是否存在与registryInfo中，席位信息都在里面，ip等信息在外面
   * @returns
   */
  async getClientsByProName(proName: string, proValues: string, isRegisterPro: string) {
    const url =
      store.getters.configs.websocketServicePort + stringFormatArr(PressenceUrl.getClientsByProName, [proName, proValues, isRegisterPro]);
    return httpclient.getPromise(url);
  },

  /**
   * 查询在线席位
   */
  async getAllRegisterInfos() {
    const url = store.getters.configs.websocketServicePort + PressenceUrl.getAllRegisterInfos;
    return httpclient.getPromise(url).then((res: any) => {
      return res.success ? res.data : [];
    });
  },

  /**
   * 更新席位数据中的是否协商字段属性
   * @param {SeatStatus} seatStatus  传递seatStatus对象
   * @returns
   */
  async updateSeatDataProperty(seatStatus: SeatStatus) {
    if (seatStatus != null) {
      const msgInfo = new MessageInfo('registerId', seatStatus.registerId, true, MessageType.UpdateClientInfo, {
        hub: '',
        method: '',
        args: seatStatus
      });
      return await this.sendMessage(msgInfo);
    }
  },

  /**
   * 发布通知公告消息
   * @param message
   * @param sendNumber
   */
  async announcementBoardNotice(message: Message, registerId: string) {
    const messageInfo = new MessageInfo('registerId', registerId, true, MessageType.CustomMessage, {
      hub: 'MessageHub',
      method: 'announcementBoardNotice',
      args: message
    });
    logger.info(`announcementBoardNotice: ${message.title}`);
    return await this.sendMessage(messageInfo);
  },

  /**
   * 检查是否重新登陆
   *
   */
  forcedOffline(clientInfo: any) {
    const msgInfo = new MessageInfo('registerId', clientInfo.registerInfo.registerId, true, MessageType.CustomMessage, {
      hub: 'MessageHub',
      method: 'forcedToLogoff',
      args: clientInfo
    });
    return this.sendMessage(msgInfo);
  }

};
