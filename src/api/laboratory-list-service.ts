import store from '@/store';
import { stringFormat } from '@gsafety/cad-gutil/dist/stringformat';
import * as httpclient from '@gsafety/vue-httpclient/dist/httpclient';
import StateLaboratoryCondition from '../models/state-laboratory-condition';

export default {
  /**
   * 新增实验记录极其项目详情
   * @param recordProjectInfosModel
   * @returns
   */
  AddLaboratoryRecordProject(recordProjectInfosModel: any): Promise<any> {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + '/api/v1/records');
    return httpclient.postPromise(url, recordProjectInfosModel);
  },

  /**
   * 修改项目详情
   * @param projectInfoModel
   * @param projectId
   * @returns
   */
  modifyLaboratoryRecordProject(projectInfoModel: any, projectId: any): Promise<any> {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + `/api/v1/records/project-detail/${projectId}`);
    return httpclient.putPromise(url, projectInfoModel);
  },

  /**
   * 获取所有实验记录(不包括项目详情)
   * @returns
   */
  GetAllLaboratoryRecords(condition: any) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + '/api/v1/records/page');
    return httpclient.postPromise(url, condition);
  },

  /**
   * 获取所有实验变更记录(不包括项目详情)
   * @returns
   */
  GetAllChangeLaboratoryRecords(condition: any) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + '/api/v1/records/pagechange');
    return httpclient.postPromise(url, condition);
  },

  /**
   * 根据id查询实验记录及其项目详情
   * @param id
   * @returns
   */
  GetLaboratoryRecordAndProjectById(id: string) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + `/api/v1/records/one/${id}`);
    return httpclient.getPromise(url);
  },

  /**
   * 更新中心编码
   * @param id
   * @param recordInfo
   */
  updateOneRecord(recordInfo: any): Promise<any> {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + `/api/v1/update/record`);
    return httpclient.postPromise(url, recordInfo);
  },

  /**
   * 判断中心编号是否重复
   * @param centerCode
   */
  judgeCenterCodeIsRepeat(centerCode: any): Promise<any> {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + `/api/v1/records/center-code/${centerCode}/is-repeat`);
    return httpclient.getPromise(url);
  },
  /**
   * 判断中心编号与项目名称是否重复
   * @param centerCode
   */
  judgeRecordAndProjectNameIsRepeat(recordId: any, projectName: any): Promise<any> {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + `/api/v1/records/is-repeat-record/${recordId}/${projectName}`);
    return httpclient.getPromise(url);
  },
  /**
   * 删除变更记录
   * @param id
   */
  deleteRecord(id: any): Promise<any> {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + `/api/v1/records/${id}`);
    return httpclient.deletePromise(url);
  },

  /**
   * 删除项目记录记录
   * @param projectId
   */
  deleteProject(projectId: any): Promise<any> {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + `/api/v1/records/project-detail/${projectId}`);
    return httpclient.deletePromise(url);
  },
  /**
   * 获取模板列表
   * @returns
   */
  GetTemplateList() {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + `/api/v1/result-templates/all`);
    return httpclient.getPromise(url);
  },

  /**
   * 修改审核状态
   * @param projectId
   * @param param 0- 未审核  1-批审核 2-已审核
   * @returns
   */
  modifyRecordVerify(projectId: any, param: any): Promise<any> {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + `/api/v1/records/project-detail/verify/${projectId}`);
    return httpclient.putPromise(url, param);
  },
  /**
   * 修改批准状态
   * @param projectId
   * @param param 0- 未批准  1-批准中 2-已批准
   * @returns
   */
  modifyRecordApproval(projectId: any, param: any): Promise<any> {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + `/api/v1/records/project-detail/approval/${projectId}`);
    return httpclient.putPromise(url, param);
  },
  /**
   * 批准状态分页查询实验记录
   * @param stateLaboratoryCondition
   * @returns
   */
  pageConditionQueryApproval(stateLaboratoryCondition: StateLaboratoryCondition) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + `/api/v1/records/approval-status`);
    return httpclient.postPromise(url, stateLaboratoryCondition);
  },
  /**
   * 审核状态分页查询实验记录
   * @param stateLaboratoryCondition
   * @returns
   */
  pageConditionQueryVerify(stateLaboratoryCondition: StateLaboratoryCondition) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + `/api/v1/records/verify-status`);
    return httpclient.postPromise(url, stateLaboratoryCondition);
  }
};
