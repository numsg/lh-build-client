import store from '@/store';

import { SeatUrls } from '@/common/url/seat-url';
import { FloorInfo } from '../common/models/seat/floor-Info';
import SeatInfo from '../common/models/seat/seat-info';
import SeatRecord from '../common/models/seat/seat-record';

import * as httpclient from '@gsafety/vue-httpclient/dist/httpclient';
import { stringFormat } from '@gsafety/cad-gutil/dist/stringformat';
import odataClient from '@gsafety/odata-client/dist';



export default {
  /**
   * 获取所有分组信息
   */
  getAllFloors() {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + SeatUrls.getAllFloors);
    return httpclient.getPromise(url);
  },
  /**
   * 添加分组
   */
  addFloor(floorinfo: FloorInfo) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + SeatUrls.addFloor);
    return httpclient.postPromise(url, floorinfo);
  },
  /**
   * 编辑分组
   */
  editFloor(floorinfo: FloorInfo) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + SeatUrls.updateFloor);
    return httpclient.putPromise(url, floorinfo);
  },
  /**
   * 删除分组
   */
  delFloors(ids: string[]) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + SeatUrls.delFloors);
    return httpclient.postPromise(url, ids);
  },
  /**
   * 根据分组号，获取席位信息
   */
  getSeatInfos(queryPara: any) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + SeatUrls.getSeatByParam);
    return httpclient.postPromise(url, queryPara);
  },
  /**
   * 添加席位信息
   */
  addSeatInfo(seatInfo: SeatInfo) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + SeatUrls.addSeat);
    return httpclient.postPromise(url, seatInfo);
  },
  /**
   * 编辑席位信息
   */
  updateSeatInfo(seatInfo: SeatInfo) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + SeatUrls.updateSeat);
    return httpclient.putPromise(url, seatInfo);
  },
  /**
   * 删除席位信息
   */
  delSeatInfo(ids: string[]) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + SeatUrls.delSeat);
    return httpclient.postPromise(url, ids);
  },

  /**
   * 获取所有席位信息
   */
  getAllEnableSeatInfo() {
    const q = odataClient({
      service: store.getters.configs.systemOdata,
      resources: 'SeatEntity'
    });
    return q.expand('floorEntity')
      .skip(0)
      .filter('enable', 'eq', 1)
      .get()
      .then((response: any) => {
        return JSON.parse(response.toJSON().body).value;
      }).catch(() => {
        return [];
      });
  },

  /**
   * 保存席位配置
   * @param {SeatInfo[]} seatInfos
   * @returns
   */
  saveSeatConfiguration(seatInfos: SeatInfo[]) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + SeatUrls.updateByBatch);
    return httpclient.postPromise(url, seatInfos);
  },

  /**
   * 添加席位登录信息
   */
  addSeatRecord(seatRecord: SeatRecord) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + SeatUrls.addSeatRecord);
    return httpclient.postPromise(url, seatRecord);
  },
  /**
   * 编辑席位登录信息
   */
  updateSeatRecordLoginInfo(seatRecord: SeatRecord) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + SeatUrls.updateSeatRecordLoginInfo);
    return httpclient.putPromise(url, seatRecord);
  },
  /**
   * 更新席位置忙置忙时长
   * @param seatRecord 席位信息
   */
  updateSeatRecordBusyInfo(seatRecord: SeatRecord) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + SeatUrls.updateSeatRecordBusyInfo);
    return httpclient.putPromise(url, seatRecord);
  },


  /**
   * 更改席位置忙时间
   * @param seatRecord 席位信息
   */
  updateSeatBusyTime(seatRecord: SeatRecord) {
    const baseUrl = store.getters.configs.systemUrl;
    const url = stringFormat(baseUrl + SeatUrls.updateSeatBusyTime);
    return httpclient.putPromise(url, seatRecord);
  },
};
