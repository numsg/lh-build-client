export const LoginTypes = {
  getters: {
    ROLE_MENUS: 'RoleMenus',
    LOGIN_INFO: 'loginInfo',
    ISLOGIN: 'isLogin',
    USERINFO: 'userInfo',
    /**
     * 用户id
     */
    userId: 'userId',
    /**
     *  用户名
     */
    name: 'name',
    /**
     * 账号名
     */
    userName: 'userName',
    /**
     * 登录的角色Code
     */
    roleCode: 'roleCode',
    /**
     * 部门id
     */
    deptId: 'deptId',
    /**
     * 角色名字
     */
    roleName: 'roleName',
    /**
     * 角色编号（工号）
     */
    userCode: 'userCode',
    /**
     * 部门名称
     */
    deptName: 'deptName',
    depChildrenIds: 'depChildrenIds',
    depChildren: 'depChildren',
  },
  mutations: {
    SET_ROLE_MENUS: 'setRoleMenus',
    SET_LOGIN_INFO: 'setLoginInfo',
    SET_ISLOGIN: 'setIsLogin',
    SET_USERINFO: 'setUserInfo',
    SET_CHILDREN_DEP_INFO: 'setChildrenDepInfo'
  },
  actions: {
    CHANGE_LOGIN: 'change_login',
    CHANGE_ROLEMENU: 'change_role_menu',
    CHANGE_ISLOGIN: 'change_isLogin',
    GET_CHILDREN_DEP_INFO: 'getChildrenDepInfo'
  }
};
