export const RegisterType = {
  getters: {
    registerInfo: 'registerInfo',
  },
  mutations: {
    SET_REGISTER_IFNO: 'set_register_info'
  },
  actions: {
    CHANGE_REGISTER_INFO: 'change_register_info'
  }
};
