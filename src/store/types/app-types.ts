export const AppTypes = {
  getters: {
    CONFIGS: 'configs',
    DATACONFIGS: 'dataConfigs',
    LANGUAGE: 'language',
    DATE_TIME_TYPE: 'dateTimeType',
    DATE_TYPE: 'dateType',
    TIME_TYPE: 'timeType',
    SYSTEM_INFO: 'systemInfo'
  },
  mutations: {
    SET_CONFIGS: 'setConfigs',
    SET_DATA_CONFIGS: 'setDataConfigs',
    SET_LANGUAGE: 'setLanguage',
    SET_SYSTEM_INFO: 'setsystemInfo'
  },
  actions: {
    CHANGE_CONFIGS: 'setConfigs',
    CHANGE_DATA_CONFIGS: 'setDataConfigs',
    CHANGE_LANGUAGE: 'changeLanguage',
    CHANGE_SETTING: 'changeSetting'
  }
};
