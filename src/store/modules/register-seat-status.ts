import { GetterTree, MutationTree, ActionTree, Module } from 'vuex';
import { RegisterType } from '../../store/types/register-type';
import { RegisterState } from '../../store/states/register-state';
import { RootState } from '../states/root-state';
import { SeatStatus } from '../../common/models/seat-status';


const getters: GetterTree<RegisterState, RootState> = {
  // 获取席位服务中的register对象
  [RegisterType.getters.registerInfo](state: RegisterState) {
    return state.registerInfo;
  }
};

const mutations: MutationTree<RegisterState> = {
  // 修改席位服务中的register对象
  [RegisterType.mutations.SET_REGISTER_IFNO](state: RegisterState, registerInfo: SeatStatus) {
    state.registerInfo = registerInfo;
  }
};

const actions: ActionTree<RegisterState, RootState> = {
  // 修改席位服务中的register对象
  [RegisterType.actions.CHANGE_REGISTER_INFO](context, registerInfo: SeatStatus) {
    context.commit(RegisterType.mutations.SET_REGISTER_IFNO, registerInfo);
  }
};

const Register: Module<RegisterState, RootState> = {
  state: new RegisterState(),
  getters,
  mutations,
  actions
};
export default Register;

