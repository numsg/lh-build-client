import { GetterTree, MutationTree, ActionTree, Module } from 'vuex';

import { AppTypes } from '../types/app-types';
import { RootState } from '../states/root-state';
import { AppState } from '../states/app-state';

// 中文格式
const FMT_CN = {
  DATE_TIME: 'yyyy/MM/dd HH:mm:ss',
  DATE: 'yyyy/MM/dd',
  TIME: 'HH:mm:ss',
};
// 葡语格式
const FMT_PT = {
  DATE_TIME: 'dd/MM/yyyy HH:mm:ss',
  DATE: 'dd/MM/yyyy',
  TIME: 'HH:mm:ss',
};
// 英语格式
const FMT_EN = {
  DATE_TIME: 'dd/MM/yyyy HH:mm:ss',
  DATE: 'dd/MM/yyyy',
  TIME: 'HH:mm:ss',
};

const getFormat = (lang: string) => {
  switch (lang) {
    case 'zh-CN':
      return FMT_CN;
    case 'en-US':
      return FMT_EN;
    case 'pt-BR':
      return FMT_PT;
    default:
      return FMT_PT;
  }
};




const getters: GetterTree<AppState, RootState> = {
  [AppTypes.getters.CONFIGS](state) {
    return state.configs;
  },
  [AppTypes.getters.LANGUAGE](state) {
    return state.language;
  },
  [AppTypes.getters.DATE_TIME_TYPE](state) {
    return getFormat(state.language).DATE_TIME;
  },
  [AppTypes.getters.DATE_TYPE](state) {
    return getFormat(state.language).DATE;
  },
  [AppTypes.getters.TIME_TYPE](state) {
    return getFormat(state.language).TIME;
  },
  [AppTypes.getters.SYSTEM_INFO](state) {
    return state.systemInfo;
  }
};

const mutations: MutationTree<AppState> = {
  [AppTypes.mutations.SET_CONFIGS](state, configs) {
    state.configs = configs;
  },
  [AppTypes.mutations.SET_DATA_CONFIGS](state, configs) {
    state.dataConfigs = configs;
  },
  [AppTypes.mutations.SET_LANGUAGE](state, language) {
    if (state.configs) {
      state.configs.dateType.forEach((type: any) => {
        if (type.lang === language) {
          state.dateType = type.type;
        }
      });
    }
    state.language = language;
  },
  [AppTypes.mutations.SET_SYSTEM_INFO](state, systemInfos) {
    state.systemInfo = systemInfos;
  }
};
const actions: ActionTree<AppState, RootState> = {
  [AppTypes.actions.CHANGE_CONFIGS]({ commit }: any, configs: any) {
    commit(AppTypes.mutations.SET_CONFIGS, configs);
  },
  [AppTypes.actions.CHANGE_DATA_CONFIGS]({ commit }: any, configs: any) {
    commit(AppTypes.mutations.SET_DATA_CONFIGS, configs);
  },
  [AppTypes.actions.CHANGE_LANGUAGE](context, language: string) {
    const body = document.getElementsByTagName('body')[0];
    body.style.fontFamily = 'Myriad Pro, Helvetica Neue, Helvetica, PingFang SC, Hiragino Sans GB, Microsoft YaHei, Arial, sans-serif;';
    context.commit(AppTypes.mutations.SET_LANGUAGE, language);
  },
  [AppTypes.actions.CHANGE_SETTING]({ commit }: any, systemInfo: any) {
    commit(AppTypes.mutations.SET_SYSTEM_INFO, systemInfo);
  }
};
const app: Module<AppState, RootState> = {
  state: new AppState(),
  getters,
  mutations,
  actions
};
export default app;
