import { GetterTree, MutationTree, ActionTree, Module } from 'vuex';

import { LoginTypes } from '../types/login-type';
import { RootState } from '../states/root-state';
import { LoginState } from '../states/login-state';
import { LoginInfo } from '../../common/models/loginInfo';
import { ErrorCode } from '../../common/constant/error-code';
import { RoleCode, RoleCodeEnums } from '../../common/enums/role-code';
import menuService from '../../api/menu-service';
import userService from '../../api/system-manage/user-service';
import { SeatStatus } from '../../common/models/seat-status';
import { RegisterType } from '../types/register-type';


const getters: GetterTree<LoginState, RootState> = {
  [LoginTypes.getters.ROLE_MENUS](state) {
    console.log('state.currentRoleMenus', state.currentRoleMenus);

    return [
      {
        id: '1',
        name: 'name1',
        url: '/',
        children: [
          {
            id: '2',
            name: 'name2',
            url: '/laboratory-record-manager',
            children: [

            ]
          }
        ]
      }

    ];
    // return state.currentRoleMenus;
  },
  [LoginTypes.getters.LOGIN_INFO](state) {
    return state.loginInfo;
  },
  [LoginTypes.getters.ISLOGIN](state) {
    return state.isLogin;
  },
  [LoginTypes.getters.USERINFO](state) {
    return state.userInfo;
  },
  [LoginTypes.getters.userId](state) {
    return state.loginInfo && state.loginInfo.id ? state.loginInfo.id : '';
  },
  [LoginTypes.getters.userName](state) {
    return state.loginInfo && state.loginInfo.username ? state.loginInfo.username : '';
  },
  [LoginTypes.getters.name](state) {
    return state.loginInfo && state.loginInfo.name ? state.loginInfo.name : '';
  },
  [LoginTypes.getters.deptId](state) {
    return state.loginInfo && state.loginInfo.structNodeId ? state.loginInfo.structNodeId : '';
  },
  [LoginTypes.getters.roleCode](state) {
    return state.loginInfo && state.loginInfo.currentRole && state.loginInfo.currentRole.code
      ? state.loginInfo.currentRole.code.toString()
      : '';
  },
  [LoginTypes.getters.userCode](state) {
    return state.loginInfo && state.loginInfo.userCode ? state.loginInfo.userCode : '';
  },
  [LoginTypes.getters.deptName](state) {
    return state.loginInfo && state.loginInfo.orgName ? state.loginInfo.orgName : '';
  },
  [LoginTypes.getters.roleName](state) {
    let roleName = '';
    const roleCode =
      state.loginInfo && state.loginInfo.currentRole && state.loginInfo.currentRole.code ? state.loginInfo.currentRole.code.toString() : '';
    if (roleCode === RoleCode.Supervisor.toString()) {
      roleName = RoleCodeEnums.Supervisor;
    } else if (roleCode === RoleCode.Coordenador.toString()) {
      roleName = RoleCodeEnums.Coordenador;
    } else {
      roleName = RoleCodeEnums.Operador;
    }
    return roleName;
  },
  [LoginTypes.getters.depChildrenIds](state) {
    return state.depChildrenInfos.map((dep) => dep.structNodeId);
  },
  [LoginTypes.getters.depChildren](state) {
    return state.depChildrenInfos;
  }
};

const mutations: MutationTree<LoginState> = {
  [LoginTypes.mutations.SET_ROLE_MENUS](state, menus) {
    console.log('SET_ROLE_MENUS', menus);

    state.currentRoleMenus = menus;
  },
  [LoginTypes.mutations.SET_LOGIN_INFO](state, currentUserInfo) {
    state.loginInfo = currentUserInfo;
  },
  [LoginTypes.mutations.SET_ISLOGIN](state, isLogin) {
    state.isLogin = isLogin;
  },
  [LoginTypes.mutations.SET_USERINFO](state, userInfo) {
    state.userInfo = userInfo;
  },
};
const actions: ActionTree<LoginState, RootState> = {
  async [LoginTypes.actions.CHANGE_LOGIN](context) {
    const baseUrl: any = context.rootState.app.configs.systemUrl;
    // 1.用户名和密码校验
    await userService.login(context.state.userInfo, baseUrl).then(async (result: any) => {
      if (result.success) {
        if (result.message.roles.length > 0) {
          // 存储用户信息
          const loginInfo: LoginInfo = {
            id: result.message.id,
            username: result.message.username,
            // password: result.message.password,
            name: result.message.name,
            structNodeId: result.message.structNodeId,
            orgName: result.message.orgName,
            userCode: result.message.userCode,
            phone: result.message.phone,
            currentRole: result.message.roles[0],
            userImage: result.message.userImage,
          };
          // 用户登录信息注册席位服务
          const seatStatus = new SeatStatus();
          seatStatus.registerId = result.message.userId;
          seatStatus.username = result.message.username;
          context.commit(RegisterType.mutations.SET_REGISTER_IFNO, seatStatus);

          context.dispatch(LoginTypes.actions.GET_CHILDREN_DEP_INFO, result.message.structNodeId);
          context.commit(LoginTypes.mutations.SET_ISLOGIN, true);
          context.commit(LoginTypes.mutations.SET_LOGIN_INFO, loginInfo);
          const roleId = result.message.roles[0].id;
          await menuService.getMenuTreeByRoleId(roleId, baseUrl).then((menuResult: any) => {
            if (result.success) {
              // 设置store角色数据，用于菜单加载
              context.commit(LoginTypes.mutations.SET_ROLE_MENUS, menuResult);
            }
          });
        } else {
          context.commit(LoginTypes.mutations.SET_ISLOGIN, ErrorCode.UserRolesNotExist);
        }
      } else {
        context.commit(LoginTypes.mutations.SET_ISLOGIN, false);
      }
    }).catch((err: Error) => {
      console.log(err);
      context.commit(LoginTypes.mutations.SET_ISLOGIN, ErrorCode.NetworkError);
    });
  },
  [LoginTypes.actions.GET_CHILDREN_DEP_INFO](context, depId: string) {
    if (!depId) {
      return;
    }
  },
  setIsLogin({ commit }: any, islogin: any) {
    commit(LoginTypes.mutations.SET_ISLOGIN, islogin);
  }
};
const login: Module<LoginState, RootState> = {
  state: new LoginState(),
  getters,
  mutations,
  actions
};
export default login;
