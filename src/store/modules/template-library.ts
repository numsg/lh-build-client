const TemplateLibrary = {
  state: {
    currentTemplateData: {},
    currentTemplate: {},
    templateDataCenterCode: ''
  },
  mutations: {
    SET_TEMPLATE: (state: any, payloads: any) => {
      state.currentTemplate = payloads;
    },
    SET_TEMPLATE_DATA: (state: any, payloads: any) => {
      state.currentTemplateData = payloads;
    },
    SET_TEMPLATE_DATA_CODE: (state: any, payloads: any) => {
      state.templateDataCenterCode = payloads;
      // state.currentTemplateData['centerCode'] = payloads;
    },

  },
  actions: {
    setCurrentTemplateData: ({ commit }: any, payloads: any) => {
      console.log('---------模板绑定数据------------', payloads);
      commit('SET_TEMPLATE_DATA', payloads);
    },
    setCurrentTemplate: ({ commit }: any, payloads: any) => {
      commit('SET_TEMPLATE', payloads);
    },
    setTemplateDataCenterCode: ({ commit }: any, payloads: any) => {
      commit('SET_TEMPLATE_DATA_CODE', payloads);
    },
  },
  getters: {
    TemplateLibrary_currentTemplateData: (state: any) => state.currentTemplateData,
    TemplateLibrary_currentTemplate: (state: any) => state.currentTemplate,
    TemplateLibrary_templateDataCenterCode: (state: any) => state.templateDataCenterCode,
  }
};

export default TemplateLibrary;

