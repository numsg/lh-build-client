export interface Configs {
  [key: string]: any;
}
/**
 * App 状态
 */
export class AppState {
  /**
   * 配置项
   *
   * @type {Configs}
   * @memberof AppState
   */
  configs: Configs = {};

  dataConfigs: Configs = {};

  /**
   * 语言
   *
   * @memberof AppState
   */
  language = '';

  /**
   * 日期格式
   *
   * @memberof AppState
   */
  dateType = 'yyyy/MM/dd';

  /**
   * 系统信息
   *
   * @memberof AppState
   */
  systemInfo: any = {};
}
