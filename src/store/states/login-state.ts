import { LoginInfo } from '../../common/models/loginInfo';
import { OrgStructure } from '../../common/models/system-manage/org-structure';
export interface Configs {
  [key: string]: any;
}
/**
 * App 状态
 */
export class LoginState {
  /**
   * 当前角色的菜单数据
   *
   * @type {[]}
   * @memberof AppState
   */
  currentRoleMenus: [] = [];
  /**
   * 用户登录信息
   *
   * @type {LoginInfo}
   * @memberof AppState
   */
  loginInfo: LoginInfo = new LoginInfo();

  /**
   * 是否登录
   *
   * @type {*}
   * @memberof LoginState
   */
  isLogin: any = false;

  userInfo: any = '';
  // 用户id
  userId = '';
  // 用户名
  name = '';
  // 账号名
  userName = '';
  // 登录的角色Code
  roleCode = '';
  // 部门id
  deptId = '';
  // 角色名字
  roleName = '';
  // 角色编号（工号）
  userCode = '';
  // 部门名称
  deptName = '';
  depChildrenInfos: OrgStructure[] = [];
}
