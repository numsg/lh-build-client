import { AppState } from './app-state';
import { LoginState } from './login-state';
import { RegisterState } from './register-state';

/**
 * 根状态
 */
export interface RootState {
  app: AppState;
  login: LoginState;
  registerStatus: RegisterState;
}
