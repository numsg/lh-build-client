import { SeatStatus } from '../../common/models/seat-status';

export class RegisterState {
  registerInfo: SeatStatus = new SeatStatus();
}
