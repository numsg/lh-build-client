import Vue from 'vue';
import Vuex from 'vuex';
// import createLogger from 'vuex/dist/logger';
import app from './modules/app';
import getters from '../store/getters';
import Register from './modules/register-seat-status';
import { NoticeModule } from '@gsafety/cad-notice-module';
import login from '../store/modules/login';
import { RootState } from '../store/states/root-state';
import TemplateLibrary from './modules/template-library';

Vue.use(Vuex);

const isDev = process.env.NODE_ENV === 'development';
const store = new Vuex.Store<RootState>({
  modules: {
    app,
    login,
    Register,
    TemplateLibrary,
    notice: NoticeModule
  },
  getters,
  // plugins: isDev ? [createLogger({})] : []
});

export default store;
