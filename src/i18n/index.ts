// tslint:disable
import Vue from 'vue';
import VueI18n, { LocaleMessages, DateTimeFormats } from 'vue-i18n';
import * as httpclient from '@gsafety/vue-httpclient/dist/httpclient';

import zhCN from './zh-cn/zh-CN.json';

import elementZhLocale from 'element-ui/lib/locale/lang/zh-CN';

import ElementLocale from 'element-ui/lib/locale';

const messages: LocaleMessages = {
  'zh-CN': {
    ...zhCN,
    ...elementZhLocale
  }
};

const timeFormat: VueI18n.DateTimeFormatOptions = {
  hour: '2-digit',
  minute: '2-digit',
  second: '2-digit',
  hour12: false
};

const dateFormat: VueI18n.DateTimeFormatOptions = {
  year: 'numeric',
  month: '2-digit',
  day: '2-digit',
};


const weekDateFormat: VueI18n.DateTimeFormatOptions = {
  weekday: 'long',
  year: 'numeric',
  month: 'short',
  day: 'numeric'
};

const dateFormats: DateTimeFormats = {
  'zh-CN': {
    'time': timeFormat,
    'date': dateFormat,
    'date-week': weekDateFormat
  }
};
Vue.use(VueI18n);

const i18n = new VueI18n({
  messages,
  dateTimeFormats: dateFormats
});

ElementLocale.i18n((key: string, value: any) => i18n.t(key, value));
httpclient.i18n((key: any) => {
  return i18n.t(key).toString();
});

export default i18n;

